<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cotizacionsoat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cotizacionsoats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('placa');
            $table->string('fecha');
            $table->string('total');
            $table->string('marca');
            $table->string('linea');
            $table->string('modelo');
            $table->string('cedula');
            $table->string('correo');
            $table->string('celular1');
            $table->string('descuento');
            $table->string('preciodes');
            $table->string('celular');
            $table->string('codigoreferido');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
