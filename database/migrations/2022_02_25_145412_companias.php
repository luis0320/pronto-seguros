<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Companias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('companias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('nit');
            $table->integer('digito');
            $table->string('nombre');
            $table->string('asistencia');
            $table->string('asistenciafijo');
            $table->integer('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
