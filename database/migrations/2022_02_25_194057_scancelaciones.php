<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Scancelaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('scancelaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('estadotramite');
            $table->date('fechaacancelar');
            $table->integer('celular');
            $table->string('correo');
            $table->string('placa');
            $table->string('observacion');
            $table->unsignedBigInteger('mc_idmcancelaciones');
            $table->foreign('mc_idmcancelaciones')->references('id')->on('motivcancelaciones');
            $table->unsignedBigInteger('c_idcompanias');
            $table->foreign('c_idcompanias')->references('id')->on('companias');
            $table->unsignedBigInteger('f_idfinancieras');
            $table->foreign('f_idfinancieras')->references('id')->on('financieras');
            $table->unsignedBigInteger('u_idusuarios');
            $table->foreign('u_idusuarios')->references('id')->on('users');
            $table->unsignedBigInteger('b_idbeneficiarios');
            $table->foreign('b_idbeneficiarios')->references('id')->on('beneficiarios');
            $table->string('filecarta');
            $table->string('filelevantamiento');
            $table->string('filedevolucion');
            $table->string('filecedula');
            $table->string('filetarjetadepropiedad');
            $table->string('filesarlaft');
            $table->string('filepoliza');
            $table->string('fileotros');
            $table->string('fileotros1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
