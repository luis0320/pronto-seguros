<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Observaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('observaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('observacion');
            $table->unsignedBigInteger('ob_idestado');
            $table->foreign('ob_idestado')->references('id')->on('estados');
            $table->unsignedBigInteger('ob_idcancelaciones');
            $table->foreign('ob_idcancelaciones')->references('id')->on('scancelaciones');
            $table->unsignedBigInteger('u_idusuarios');
            $table->foreign('u_idusuarios')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
