<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Mispolizas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('mispolizas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Poliza');
            $table->string('Anexo');
            $table->string('Nota');
            $table->string('FecNot');
            $table->string('Desde');
            $table->string('Hasta');
            $table->string('Ej');
            $table->string('CodCli');
            $table->string('NitCC');
            $table->string('NombreCliente');
            $table->string('Compania');
            $table->string('Rm');
            $table->string('Ramo');
            $table->string('PrimNeta');
            $table->string('GastoExp');
            $table->string('IVA');
            $table->string('PrimTot');
            $table->string('Comi');
            $table->string('Comision');
            $table->string('PrimPropi');
            $table->string('ComiPropi');
            $table->string('Ca');
            $table->string('Placas');
            $table->string('Proc01');
            $table->string('Pro1');
            $table->string('Proc1');
            $table->string('Proc02');
            $table->string('Pro2');
            $table->string('Proc2');
            $table->string('NombreProced');
            $table->string('Tv');
            $table->string('Us');
            $table->string('Tomador');
            $table->string('Beneficiario');
            $table->string('Observaciones');
            $table->string('Sub');
            $table->string('TP');
            $table->string('Ap');
            $table->string('FecNac');
            $table->string('Tel1Aseg');
            $table->string('Tel2Aseg');
            $table->string('CelAsegur');
            $table->string('DirAsegur');
            $table->string('EmailAsegur');                           	
            $table->string('Ciudad');
            $table->string('Depto'); 	
            $table->string('FecRbo');
            $table->string('Rferencia');                     	
            $table->string('Zn');
            $table->string('VrAsegPoliz');
            $table->string('VrAsegur');
            $table->string('Mode');
            $table->string('CodFase');
            $table->string('Marca');    	
            $table->string('Clase');       	
            $table->string('Tipo');       	
            $table->string('Motor');             	
            $table->string('SerieChs');
            $table->string('Servicio');        	
            $table->string('Cilindraj');
            $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
