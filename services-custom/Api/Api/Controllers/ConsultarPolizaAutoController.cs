﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [ApiController]
    public class ConsultarPolizaAutoController : ControllerBase
    {
        [HttpGet]
        [Route("services/ConsultarPolizaAuto")]
        public async Task<IActionResult> GetAsync([FromQuery] string identificacion,
                                                               string tipoIdentificacion,
                                                               string estadoCivil,
                                                               DateTime fechaNacimiento,
                                                               string genero,
                                                               string primerApellido,
                                                               string primerNombre,
                                                               string placa,
                                   string identificacionEmpresa,
                                   string codigoDistribuidor,
                                   int modelo,
                                   string fasecoldaMarcaId,
                                   string fasecoldaModeloId,
                                   decimal monto,
                                   string codigoDivipola,
                                   string detalleMontoVehiculo,
                                   bool esNuevo)
        {
            try
            {
                Services.ConsultarPolizaAuto consultarPolizaAuto = new Services.ConsultarPolizaAuto();
                object result = await consultarPolizaAuto.CallServiceAsync(identificacion,
                                       tipoIdentificacion,
                                       estadoCivil,
                                       fechaNacimiento,
                                       genero,
                                       primerApellido,
                                       primerNombre,
                                       placa,
                                       identificacionEmpresa,
                                       codigoDistribuidor,
                                       modelo,
                                       fasecoldaMarcaId,
                                       fasecoldaModeloId,
                                       monto,
                                       codigoDivipola,
                                       detalleMontoVehiculo,
                                       esNuevo);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}