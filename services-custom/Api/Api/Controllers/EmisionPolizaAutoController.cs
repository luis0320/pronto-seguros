﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [ApiController]
    public class EmisionPolizaAutoController : ControllerBase
    {
        [HttpGet]
        [Route("services/EmitirCotizacionAuto")]
        public async Task<IActionResult> GetAsync([FromQuery] string canal,
                                                   string idCorrelacionConsumidor,
                                                   int beneficiarioIndicador,
                                                    long beneficiarioNumero,
                                                    int beneficiarioTipoCorreo,
                                                    string beneficiarioEmail,
                                                    string beneficiarioConstruccionNr,
                                                    string beneficiarioDireccion,
                                                    string beneficiarioCodigoDivipola,
                                                    string beneficiarioIdentificacion,
                                                    string beneficiarioTipoIdentificacion,
                                                    DateTime beneficiarioFechaNacimiento,
                                                    string beneficiarioGenero,
                                                    string beneficiarioPrimerApellido,
                                                    string beneficiarioPrimerNombre,
                                                    int tomadorCelularPersonalIndicador,
                                                    int tomadorCelularPersonalNumero,
                                                    string tomadorEmail,
                                                    string tomadorDireccion,
                                                    string tomadorCodigoDivipola,
                                                    string tomadorIdentificacion,
                                                    string tomadorTipoIdentificacion,
                                                    DateTime tomadorFechaNacimiento,
                                                    string tomadorGenero,
                                                    string tomadorPrimerApellido,
                                                    string tomadorPrimerNombre,
                                                    int aseguradoCelularPersonalIndicador,
                                                    int aseguradoCelularPersonalNumero,
                                                    string aseguradoEmail,
                                                    string aseguradoConstruccionNr,
                                                    string aseguradoDireccion,
                                                    string aseguradoCodigoDivipola,
                                                    string aseguradoIdentificacion,
                                                    int aseguradoTipoIdentificacion,
                                                    DateTime aseguradoFechaNacimiento,
                                                    string aseguradoGenero,
                                                    string aseguradoPrimerApellido,
                                                    string aseguradoPrimerNombre,
                                                    string observaciones,
                                                    int variablesProcesoTempId,
                                                    DateTime variablesProcesoFechaActual,
                                                    bool esBeneficiarioAsegurado,
                                                    bool esTomadorAsegurado,
                                                    int planDePago,
                                                    int codigoAlizanza,
                                                    int codigoRamoAlianza,
                                                    int codigoPuntoVentaAlianza,
                                                    int numeroPropuesta,
                                                    string placa,
                                                    string numeroMotor,
                                                    string numeroChasis)
        {
            try
            {
                Services.EmisionPolizaAuto emisionPolizaAuto = new Services.EmisionPolizaAuto();
                object result = await emisionPolizaAuto.CallServiceAsync(canal,
                                                                         idCorrelacionConsumidor,
                                                                         beneficiarioIndicador,
                                                                         beneficiarioNumero,
                                                                         beneficiarioTipoCorreo,
                                                                         beneficiarioEmail,
                                                                         beneficiarioConstruccionNr,
                                                                         beneficiarioDireccion,
                                                                         beneficiarioCodigoDivipola,
                                                                         beneficiarioIdentificacion,
                                                                         beneficiarioTipoIdentificacion,
                                                                         beneficiarioFechaNacimiento,
                                                                         beneficiarioGenero,
                                                                         beneficiarioPrimerApellido,
                                                                         beneficiarioPrimerNombre,
                                                                         tomadorCelularPersonalIndicador,
                                                                         tomadorCelularPersonalNumero,
                                                                         tomadorEmail,
                                                                         tomadorDireccion,
                                                                         tomadorCodigoDivipola,
                                                                         tomadorIdentificacion,
                                                                         tomadorTipoIdentificacion,
                                                                         tomadorFechaNacimiento,
                                                                         tomadorGenero,
                                                                         tomadorPrimerApellido,
                                                                         tomadorPrimerNombre,
                                                                         aseguradoCelularPersonalIndicador,
                                                                         aseguradoCelularPersonalNumero,
                                                                         aseguradoEmail,
                                                                         aseguradoConstruccionNr,
                                                                         aseguradoDireccion,
                                                                         aseguradoCodigoDivipola,
                                                                         aseguradoIdentificacion,
                                                                         aseguradoTipoIdentificacion,
                                                                         aseguradoFechaNacimiento,
                                                                         aseguradoGenero,
                                                                         aseguradoPrimerApellido,
                                                                         aseguradoPrimerNombre,
                                                                         observaciones,
                                                                         variablesProcesoTempId,
                                                                         variablesProcesoFechaActual,
                                                                         esBeneficiarioAsegurado,
                                                                         esTomadorAsegurado,
                                                                         planDePago,
                                                                         codigoAlizanza,
                                                                         codigoRamoAlianza,
                                                                         codigoPuntoVentaAlianza,
                                                                         numeroPropuesta,
                                                                         placa,
                                                                         numeroMotor,
                                                                         numeroChasis);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
