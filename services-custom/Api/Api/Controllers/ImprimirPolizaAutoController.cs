﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [ApiController]
    public class ImprimirPolizaAutoController : ControllerBase
    {
        [HttpGet]
        [Route("services/ImprimirCotizacionAuto")]
        public async Task<IActionResult> GetAsync([FromQuery] string canal,
                                                              string idCorrelacionConsumidor,
                                                              string tempId,
                                                              string email,
                                                              string nombreCompleto,
                                                              bool printBinary,
                                                              bool JustRecordQuote)
        {
            try
            {
                Services.ImprimirPolizaAuto imprimirPolizaAuto = new Services.ImprimirPolizaAuto();
                object result = await imprimirPolizaAuto.CallServiceAsync(canal,
                                                                          idCorrelacionConsumidor,
                                                                          tempId,
                                                                          email,
                                                                          nombreCompleto,
                                                                          printBinary,
                                                                          JustRecordQuote);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
