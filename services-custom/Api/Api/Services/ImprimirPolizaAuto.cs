﻿using AxaAutoServiceReference;
using System;
using System.Threading.Tasks;

namespace Api.Services
{
    public class ImprimirPolizaAuto
    {
        public async Task<object> CallServiceAsync(string canal,
                                                   string idCorrelacionConsumidor,
                                                   string tempId,
                                                   string email,
                                                   string nombreCompleto,
                                                   bool printBinary,
                                                   bool JustRecordQuote)
        {
            AutoClient client = new AutoClient();
            ImprimirCotizacionAutoReq imprimirCotizacionAutoReq = new ImprimirCotizacionAutoReq();
            imprimirCotizacionAutoReq.HEADER = GetHeader(idCorrelacionConsumidor, canal, DateTime.Now);
            imprimirCotizacionAutoReq.BODY = GetBody(tempId, email, nombreCompleto, printBinary, JustRecordQuote);
            var result = await client.ImpresionCotizacionAutosAsync(imprimirCotizacionAutoReq);
            return result.ImprimirCotizacionAutoResp;
        }

        public ImprimirCotizacionAutoReqHEADER GetHeader(string idCorrelacionConsumidor,
                                           string canal,
                                           DateTime peticionFecha)
        {
            ImprimirCotizacionAutoReqHEADER header = new ImprimirCotizacionAutoReqHEADER()
            {
                idCorrelacionConsumidor = idCorrelacionConsumidor,
                canal = canal,
                peticionFecha = peticionFecha
            };
            return header;
        }

        public ImprimirCotizacionAutoReqBODY GetBody(string tempId,
                                                     string email,
                                                     string nombreCompleto,
                                                     bool printBinary,
                                                     bool JustRecordQuote)
        {
            ImprimirCotizacionAutoReqBODY body = new ImprimirCotizacionAutoReqBODY()
            {
                Documento = new ImprimirCotizacionAutoReqBODYDocumento()
                {
                    tempId = tempId,
                    cliente = new ImprimirCotizacionAutoReqBODYDocumentoCliente()
                    {
                        contactoPrincipal = new ImprimirCotizacionAutoReqBODYDocumentoClienteContactoPrincipal()
                        {
                            email = new ImprimirCotizacionAutoReqBODYDocumentoClienteContactoPrincipalEmail()
                            {
                                email = email
                            }
                        },
                        persona = new ImprimirCotizacionAutoReqBODYDocumentoClientePersona()
                        {
                            datosBasicos = new ImprimirCotizacionAutoReqBODYDocumentoClientePersonaDatosBasicos()
                            {
                                nombreCompleto = nombreCompleto
                            }
                        }
                    },
                    printBinary = printBinary,
                    JustRecordQuote= JustRecordQuote
                }
            };
            return body;
        }
    }
}
