﻿using AxaEmisionServiceReference;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Api.Services
{
    public class EmisionPolizaAuto
    {
        public async Task<object> CallServiceAsync(string canal,
                                                   string idCorrelacionConsumidor,
                                                   int beneficiarioIndicador,
                                                    long beneficiarioNumero,
                                                    int beneficiarioTipoCorreo,
                                                    string beneficiarioEmail,
                                                    string beneficiarioConstruccionNr,
                                                    string beneficiarioDireccion,
                                                    string beneficiarioCodigoDivipola,
                                                    string beneficiarioIdentificacion,
                                                    string beneficiarioTipoIdentificacion,
                                                    DateTime beneficiarioFechaNacimiento,
                                                    string beneficiarioGenero,
                                                    string beneficiarioPrimerApellido,
                                                    string beneficiarioPrimerNombre,
                                                    int tomadorCelularPersonalIndicador,
                                                    int tomadorCelularPersonalNumero,
                                                    string tomadorEmail,
                                                    string tomadorDireccion,
                                                    string tomadorCodigoDivipola,
                                                    string tomadorIdentificacion,
                                                    string tomadorTipoIdentificacion,
                                                    DateTime tomadorFechaNacimiento,
                                                    string tomadorGenero,
                                                    string tomadorPrimerApellido,
                                                    string tomadorPrimerNombre,
                                                    int aseguradoCelularPersonalIndicador,
                                                    int aseguradoCelularPersonalNumero,
                                                    string aseguradoEmail,
                                                    string aseguradoConstruccionNr,
                                                    string aseguradoDireccion,
                                                    string aseguradoCodigoDivipola,
                                                    string aseguradoIdentificacion,
                                                    int aseguradoTipoIdentificacion,
                                                    DateTime aseguradoFechaNacimiento,
                                                    string aseguradoGenero,
                                                    string aseguradoPrimerApellido,
                                                    string aseguradoPrimerNombre,
                                                    string observaciones,
                                                    int variablesProcesoTempId,
                                                    DateTime variablesProcesoFechaActual,
                                                    bool esBeneficiarioAsegurado,
                                                    bool esTomadorAsegurado,
                                                    int planDePago,
                                                    int codigoAlizanza,
                                                    int codigoRamoAlianza,
                                                    int codigoPuntoVentaAlianza,
                                                    int numeroPropuesta,
                                                    string placa,
                                                    string numeroMotor,
                                                    string numeroChasis)
        {
            EmisionPolizaClient client = new EmisionPolizaClient();
            EmisionPolizaReq emisionPolizaReq = new EmisionPolizaReq();
            emisionPolizaReq.HEADER = GetHeader(idCorrelacionConsumidor, canal, DateTime.Now);
            emisionPolizaReq.BODY = GetBody(beneficiarioIndicador,
                                            beneficiarioNumero,
                                            beneficiarioTipoCorreo,
                                            beneficiarioEmail,
                                            beneficiarioConstruccionNr,
                                            beneficiarioDireccion,
                                            beneficiarioCodigoDivipola,
                                            beneficiarioIdentificacion,
                                            beneficiarioTipoIdentificacion,
                                            beneficiarioFechaNacimiento,
                                            beneficiarioGenero,
                                            beneficiarioPrimerApellido,
                                            beneficiarioPrimerNombre,
                                            tomadorCelularPersonalIndicador,
                                            tomadorCelularPersonalNumero,
                                            tomadorEmail,
                                            tomadorDireccion,
                                            tomadorCodigoDivipola,
                                            tomadorIdentificacion,
                                            tomadorTipoIdentificacion,
                                            tomadorFechaNacimiento,
                                            tomadorGenero,
                                            tomadorPrimerApellido,
                                            tomadorPrimerNombre,
                                            aseguradoCelularPersonalIndicador,
                                            aseguradoCelularPersonalNumero,
                                            aseguradoEmail,
                                            aseguradoConstruccionNr,
                                            aseguradoDireccion,
                                            aseguradoCodigoDivipola,
                                            aseguradoIdentificacion,
                                            aseguradoTipoIdentificacion,
                                            aseguradoFechaNacimiento,
                                            aseguradoGenero,
                                            aseguradoPrimerApellido,
                                            aseguradoPrimerNombre,
                                            observaciones,
                                            variablesProcesoTempId,
                                            variablesProcesoFechaActual,
                                            esBeneficiarioAsegurado,
                                            esTomadorAsegurado,
                                            planDePago,
                                            codigoAlizanza,
                                            codigoRamoAlianza,
                                            codigoPuntoVentaAlianza,
                                            numeroPropuesta,
                                            placa,
                                            numeroMotor,
                                            numeroChasis);
            var result = await client.EmitirPolizaAutoAsync(emisionPolizaReq);
            return result.EmisionPolizaResp;
        }

        public EmisionPolizaReqHEADER GetHeader(string idCorrelacionConsumidor,
                                           string canal,
                                           DateTime peticionFecha)
        {
            EmisionPolizaReqHEADER header = new EmisionPolizaReqHEADER()
            {
                idCorrelacionConsumidor = idCorrelacionConsumidor,
                canal = canal,
                peticionFecha = peticionFecha
            };
            return header;
        }

        public EmisionPolizaReqBODY GetBody(int beneficiarioIndicador,
                                            long beneficiarioNumero,
                                            int beneficiarioTipoCorreo,
                                            string beneficiarioEmail,
                                            string beneficiarioConstruccionNr,
                                            string beneficiarioDireccion,
                                            string beneficiarioCodigoDivipola,
                                            string beneficiarioIdentificacion,
                                            string beneficiarioTipoIdentificacion,
                                            DateTime beneficiarioFechaNacimiento,
                                            string beneficiarioGenero,
                                            string beneficiarioPrimerApellido,
                                            string beneficiarioPrimerNombre,
                                            int tomadorCelularPersonalIndicador,
                                            int tomadorCelularPersonalNumero,
                                            string tomadorEmail,
                                            string tomadorDireccion,
                                            string tomadorCodigoDivipola,
                                            string tomadorIdentificacion,
                                            string tomadorTipoIdentificacion,
                                            DateTime tomadorFechaNacimiento,
                                            string tomadorGenero,
                                            string tomadorPrimerApellido,
                                            string tomadorPrimerNombre,
                                            int aseguradoCelularPersonalIndicador,
                                            int aseguradoCelularPersonalNumero,
                                            string aseguradoEmail,
                                            string aseguradoConstruccionNr,
                                            string aseguradoDireccion,
                                            string aseguradoCodigoDivipola,
                                            string aseguradoIdentificacion,
                                            int aseguradoTipoIdentificacion,
                                            DateTime aseguradoFechaNacimiento,
                                            string aseguradoGenero,
                                            string aseguradoPrimerApellido,
                                            string aseguradoPrimerNombre,
                                            string observaciones,
                                            int variablesProcesoTempId,
                                            DateTime variablesProcesoFechaActual,
                                            bool esBeneficiarioAsegurado,
                                            bool esTomadorAsegurado,
                                            int planDePago,
                                            int codigoAlizanza,
                                            int codigoRamoAlianza,
                                            int codigoPuntoVentaAlianza,
                                            int numeroPropuesta, 
                                            string placa,
                                            string numeroMotor,
                                            string numeroChasis)
        {
            EmisionPolizaReqBODYEmisionRiesgoBeneficiario beneficiario = new EmisionPolizaReqBODYEmisionRiesgoBeneficiario()
            {
                contactoPrincipal = new EmisionPolizaReqBODYEmisionRiesgoBeneficiarioContactoPrincipal()
                {
                    celularPersonal = new EmisionPolizaReqBODYEmisionRiesgoBeneficiarioContactoPrincipalCelularPersonal()
                    {
                        indicador = beneficiarioIndicador,
                        numero = beneficiarioNumero
                    },
                    email = new EmisionPolizaReqBODYEmisionRiesgoBeneficiarioContactoPrincipalEmail()
                    {
                        tipoCorreo = beneficiarioTipoCorreo,
                        email = beneficiarioEmail
                    }
                },
                direccionPrincipal = new EmisionPolizaReqBODYEmisionRiesgoBeneficiarioDireccionPrincipal()
                {
                    construccionNr = beneficiarioConstruccionNr,
                    lineaDireccion1 = beneficiarioDireccion,
                    CodigoDivipola = beneficiarioCodigoDivipola
                },
                persona = new EmisionPolizaReqBODYEmisionRiesgoBeneficiarioPersona()
                {
                    personaId = 1,
                    identificacion = new EmisionPolizaReqBODYEmisionRiesgoBeneficiarioPersonaIdentificacion()
                    {
                        identificacion = beneficiarioIdentificacion,
                        tipoIdentificacion = beneficiarioTipoIdentificacion
                    },
                    estadoCivil = 7,
                    fechaNacimiento = DateTime.ParseExact(beneficiarioFechaNacimiento.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture), "yyyy/MM/dd", CultureInfo.InvariantCulture),
                    genero = beneficiarioGenero,
                    nacionalidad = "1",
                    datosBasicos = new EmisionPolizaReqBODYEmisionRiesgoBeneficiarioPersonaDatosBasicos()
                    {
                        primerApellido = beneficiarioPrimerApellido,
                        primerNombre = beneficiarioPrimerNombre,
                        segundoApellido = ""
                    },
                    TipoDeNegocio = 1
                },
                PorcentajeParticipacion = 100
            };

            EmisionPolizaReqBODY body = new EmisionPolizaReqBODY()
            {
                Emision = new EmisionPolizaReqBODYEmision()
                {
                    Poliza = new EmisionPolizaReqBODYEmisionPoliza()
                    {
                        Tomador = new EmisionPolizaReqBODYEmisionPolizaTomador()
                        {
                            contactoPrincipal = new EmisionPolizaReqBODYEmisionPolizaTomadorContactoPrincipal()
                            {
                                celularPersonal = new EmisionPolizaReqBODYEmisionPolizaTomadorContactoPrincipalCelularPersonal()
                                {
                                    indicador = tomadorCelularPersonalIndicador,
                                    numero = tomadorCelularPersonalNumero
                                },
                                email = new EmisionPolizaReqBODYEmisionPolizaTomadorContactoPrincipalEmail()
                                {
                                    tipoCorreo = 13,
                                    email = tomadorEmail
                                }
                            },
                            direccionPrincipal = new EmisionPolizaReqBODYEmisionPolizaTomadorDireccionPrincipal()
                            {
                                construccionNr = 1,
                                lineaDireccion1 = tomadorDireccion,
                                CodigoDivipola = tomadorCodigoDivipola
                            },
                            persona = new EmisionPolizaReqBODYEmisionPolizaTomadorPersona()
                            {
                                identificacion = new EmisionPolizaReqBODYEmisionPolizaTomadorPersonaIdentificacion()
                                {
                                    identificacion = tomadorIdentificacion,
                                    tipoIdentificacion = tomadorTipoIdentificacion
                                },
                                fechaNacimiento = DateTime.ParseExact(tomadorFechaNacimiento.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture), "yyyy/MM/dd", CultureInfo.InvariantCulture),
                                estadoCivil = 1,
                                genero = tomadorGenero,
                                nacionalidad = "1",
                                datosBasicos = new EmisionPolizaReqBODYEmisionPolizaTomadorPersonaDatosBasicos()
                                {
                                    primerApellido = tomadorPrimerApellido,
                                    primerNombre = tomadorPrimerNombre,
                                    segundoApellido = ""
                                },
                                TipoDeNegocio = 10
                            },
                            esClienteComercial = false,
                            envioSMS = true,
                            envioCorreoElectronico = true
                        }
                    },
                    Riesgo = new EmisionPolizaReqBODYEmisionRiesgo()
                    {
                        Asegurado = new EmisionPolizaReqBODYEmisionRiesgoAsegurado()
                        {
                            contactoPrincipal = new EmisionPolizaReqBODYEmisionRiesgoAseguradoContactoPrincipal()
                            {
                                celularPersonal = new EmisionPolizaReqBODYEmisionRiesgoAseguradoContactoPrincipalCelularPersonal()
                                {
                                    indicador = aseguradoCelularPersonalIndicador,
                                    numero = aseguradoCelularPersonalNumero
                                },
                                email = new EmisionPolizaReqBODYEmisionRiesgoAseguradoContactoPrincipalEmail()
                                {
                                    tipoCorreo = 13,
                                    email = aseguradoEmail
                                }
                            },
                            direccionPrincipal = new EmisionPolizaReqBODYEmisionRiesgoAseguradoDireccionPrincipal()
                            {
                                construccionNr = aseguradoConstruccionNr,
                                lineaDireccion1 = aseguradoDireccion,
                                CodigoDivipola = aseguradoCodigoDivipola
                            },
                            persona = new EmisionPolizaReqBODYEmisionRiesgoAseguradoPersona()
                            {
                                identificacion = new EmisionPolizaReqBODYEmisionRiesgoAseguradoPersonaIdentificacion()
                                {
                                    identificacion = aseguradoIdentificacion,
                                    tipoIdentificacion = aseguradoTipoIdentificacion
                                },
                                estadoCivil = 1,
                                fechaNacimiento = DateTime.ParseExact(aseguradoFechaNacimiento.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture), "yyyy/MM/dd", CultureInfo.InvariantCulture),
                                genero = aseguradoGenero,
                                nacionalidad = "1",
                                datosBasicos = new EmisionPolizaReqBODYEmisionRiesgoAseguradoPersonaDatosBasicos()
                                {
                                    primerApellido = aseguradoPrimerApellido,
                                    primerNombre = aseguradoPrimerNombre,
                                    segundoApellido = ""
                                },
                                TipoDeNegocio = 10
                            },
                            esClienteComercial = false,
                            envioSMS = true,
                            envioCorreoElectronico = true,
                            CodigoTipoTrabajador = 1
                        },
                        Beneficiarios = new EmisionPolizaReqBODYEmisionRiesgoBeneficiario[]
                        {
                            beneficiario
                        },
                        Observaciones = observaciones
                    },
                    EmsionDetallada = new EmisionPolizaReqBODYEmisionEmsionDetallada()
                    {
                        VariablesProceso = new EmisionPolizaReqBODYEmisionEmsionDetalladaVariablesProceso()
                        {
                            TempId = variablesProcesoTempId,
                            FechaActual = DateTime.ParseExact(variablesProcesoFechaActual.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture), "yyyy/MM/dd", CultureInfo.InvariantCulture),
                            EsBeneficiarioAsegurado = esBeneficiarioAsegurado,
                            EsTomadorAsegurado = esTomadorAsegurado,
                            PlanDePago = planDePago,
                            CodigoAlizanza = codigoAlizanza,
                            CodigoRamoAlianza = codigoRamoAlianza,
                            CodigoPuntoVentaAlianza = codigoPuntoVentaAlianza,
                            NumeroPropuesta = numeroPropuesta,
                        },
                        Vehiculo = new EmisionPolizaReqBODYEmisionEmsionDetalladaVehiculo()
                        {
                            Placa = placa,
                            NumeroMotor = numeroMotor,
                            NumeroChasis = numeroChasis
                        }
                    }
                }
            };
            return body;
        }
    }
}
