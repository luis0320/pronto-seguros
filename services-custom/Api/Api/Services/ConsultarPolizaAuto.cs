﻿using AxaServiceReferenceReduced;
using System;
using System.Threading.Tasks;

namespace Api.Services
{
    public class ConsultarPolizaAuto
    {
        public async Task<object> CallServiceAsync(string identificacion,
                                   string tipoIdentificacion,
                                   string estadoCivil,
                                   DateTime fechaNacimiento,
                                   string genero,
                                   string primerApellido,
                                   string primerNombre,
                                   string placa,
                                   string identificacionEmpresa,
                                   string codigoDistribuidor,
                                   int modelo,
                                   string fasecoldaMarcaId,
                                   string fasecoldaModeloId,
                                   decimal monto,
                                   string codigoDivipola,
                                   string detalleMontoVehiculo,
                                   bool esNuevo)
        {
            GestionPolizaAutoClient client = new GestionPolizaAutoClient();
            ConsultarPolizaAutoReqType consultarPolizaAutoReq = new ConsultarPolizaAutoReqType();
            consultarPolizaAutoReq.Header = GetHeader(Guid.NewGuid().ToString(), "2", DateTime.Now);
            consultarPolizaAutoReq.Body = GetBody(identificacion,
                                   tipoIdentificacion,
                                   estadoCivil,
                                   fechaNacimiento,
                                   genero,
                                   primerApellido,
                                   primerNombre,
                                   placa,
                                   identificacionEmpresa,
                                   codigoDistribuidor,
                                   modelo,
                                   fasecoldaMarcaId,
                                   fasecoldaModeloId,
                                   monto,
                                   codigoDivipola,
                                   detalleMontoVehiculo,
                                   esNuevo);
            var result = await client.CotizarPolizaAutoAsync(consultarPolizaAutoReq);
            return result.ConsultarPolizaAutoResp;
        }

        public HeaderRequest GetHeader(string idCorrelacionConsumidor,
                                           string canal,
                                           DateTime peticionFecha)
        {
            HeaderRequest header = new HeaderRequest()
            {
                IdCorrelacionConsumidor = idCorrelacionConsumidor,
                Canal = canal,
                PeticionFecha = peticionFecha
            };
            return header;
        }

        public BodyRequest GetBody(string identificacion,
                                   string tipoIdentificacion,
                                   string estadoCivil,
                                   DateTime fechaNacimiento,
                                   string genero,
                                   string primerApellido,
                                   string primerNombre,
                                   string placa,
                                   string identificacionEmpresa,
                                   string codigoDistribuidor,
                                   int modelo,
                                   string fasecoldaMarcaId,
                                   string fasecoldaModeloId,
                                   decimal monto,
                                   string codigoDivipola,
                                   string detalleMontoVehiculo,
                                   bool esNuevo)
        {
            BodyRequest body = new BodyRequest()
            {
                Cotizacion = new Cotizacion()
                {
                    cliente = new Cliente_TYPE()
                    {
                        persona = new Persona_TYPE()
                        {
                            identificacion = new IdentificacionPersona_TYPE()
                            {
                                identificacion = identificacion,
                                tipoIdentificacion = tipoIdentificacion
                            },
                            fechaNacimiento = fechaNacimiento,
                            estadoCivil = estadoCivil,
                            genero = genero,
                            datosBasicos = new DatosBasicosPersonaNatural_TYPE()
                            {
                                primerApellido = primerApellido,
                                primerNombre = primerNombre
                            }
                        }
                    },
                    distribuidor = new Distribuidor_TYPE()
                    {
                        empresa = new Empresa_TYPE()
                        {
                            identificacion = new IdentificacionPersona_TYPE()
                            {
                                identificacion = identificacionEmpresa
                            }
                        },
                        codigoDistribuidor = codigoDistribuidor
                    },

                    contrato = new Contrato_TYPE()
                    {
                        cobertura = new Cobertura_TYPE()
                        {
                            limite = new limiteTest1()
                            {
                                limite = "0",
                                //Monto = 90890.809
                            }
                        },
                        Vehiculo = new Vehiculo_TYPE()
                        {
                            Placa = placa,
                            NumeroMotor = "",
                            NumeroChasis = "",
                            Modelo = modelo,
                            //Color = "0",
                            FasecoldaMarcaID = fasecoldaMarcaId,
                            FasecoldaModeloID = fasecoldaModeloId,
                            Monto = new Monto_TYPE()
                            {
                                monto = monto
                            }
                        }
                    },

                    subproducto = new SubProducto_TYPE()
                    {
                        producto = new Producto_TYPE()
                        {
                            identificacionProducto = new IdentificacionProducto_TYPE()
                            {
                                idProducto = ""
                            },
                        }
                    },

                    CodigoDivipola = codigoDivipola,
                    IdAcuerdoNegocio = "17",
                    ValidacionEventos = true,
                    IdZonaClasificacion = "23",
                    DetalleMontoVehiculo = detalleMontoVehiculo,
                    EsNuevo = esNuevo
                }
            };
            return body;
        }
    }
}