﻿namespace AxaServiceReferenceReduced
{
    [System.ServiceModel.ServiceContract(Namespace = "http://axacolpatria.co/Servicios/GestionPolizas/CotizacionPolizasAutos", ConfigurationName = "AxaServiceReferenceReduced.GestionPolizaAuto")]
    public interface GestionPolizaAuto
    {
        [System.ServiceModel.OperationContract(Action = "CotizarPolizaAuto", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormat(SupportFaults = true)]
        [System.ServiceModel.ServiceKnownType(typeof(CotizacionId))]
        System.Threading.Tasks.Task<CotizarPolizaAutoResponse> CotizarPolizaAutoAsync(CotizarPolizaAutoRequest request);
    }

    /// <summary>
    /// 
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough()]
    [System.Xml.Serialization.XmlType(Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoReq/1.0")]
    public partial class ConsultarPolizaAutoReqType
    {
        [System.Xml.Serialization.XmlElement("HEADER", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public HeaderRequest Header { get; set; }

        [System.Xml.Serialization.XmlElement("BODY", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public BodyRequest Body { get; set; }
    }

    /// <summary>
    /// Encabezado de la solicitud a enviar
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough()]
    [System.Xml.Serialization.XmlType(Namespace = "http://axacolpatria.co/Servicios/Base/EncabezadosSOA")]
    public partial class HeaderRequest
    {
        [System.Xml.Serialization.XmlElement("canal", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public string Canal { get; set; }

        [System.Xml.Serialization.XmlElement("idCorrelacionConsumidor", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public string IdCorrelacionConsumidor { get; set; }

        [System.Xml.Serialization.XmlElement("peticionFecha", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 3)]
        public System.DateTime PeticionFecha { get; set; }
    }

    /// <summary>
    /// Cuerpo de la solicitud a enviar
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoReq/1.0")]
    public partial class BodyRequest
    {
        [System.Xml.Serialization.XmlElement("cotizacion", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public Cotizacion Cotizacion { get; set; }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoReq/1.0")]
    public partial class Cotizacion
    {
        private Cliente_TYPE clienteField;
        private SubProducto_TYPE subproductoField;
        private Distribuidor_TYPE distribuidorField;
        private Contrato_TYPE contratoField;
        private string codigoDivipolaField;
        private string idAcuerdoNegocioField;
        private string idZonaClasificacionField;
        private string detalleMontoVehiculoField;
        private bool esNuevoField;
        private bool validacionEventosField;

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public Cliente_TYPE cliente { get { return this.clienteField; } set { this.clienteField = value; } }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public SubProducto_TYPE subproducto
        {
            get
            {
                return this.subproductoField;
            }
            set
            {
                this.subproductoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public Distribuidor_TYPE distribuidor
        {
            get
            {
                return this.distribuidorField;
            }
            set
            {
                this.distribuidorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 3)]
        public Contrato_TYPE contrato
        {
            get
            {
                return this.contratoField;
            }
            set
            {
                this.contratoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CodigoDivipola", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 4)]
        public string CodigoDivipola
        {
            get
            {
                return this.codigoDivipolaField;
            }
            set
            {
                this.codigoDivipolaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("IdAcuerdoNegocio", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 5)]
        public string IdAcuerdoNegocio
        {
            get
            {
                return this.idAcuerdoNegocioField;
            }
            set
            {
                this.idAcuerdoNegocioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("IdZonaClasificacion", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 10)]
        public string IdZonaClasificacion
        {
            get
            {
                return this.idZonaClasificacionField;
            }
            set
            {
                this.idZonaClasificacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DetalleMontoVehiculo", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 11)]
        public string DetalleMontoVehiculo
        {
            get
            {
                return this.detalleMontoVehiculoField;
            }
            set
            {
                this.detalleMontoVehiculoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EsNuevo", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 12)]
        public bool EsNuevo
        {
            get
            {
                return this.esNuevoField;
            }
            set
            {
                this.esNuevoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ValidacionEventos", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 13)]
        public bool ValidacionEventos
        {
            get
            {
                return this.validacionEventosField;
            }
            set
            {
                this.validacionEventosField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Persona/Cliente/2017/03/31")]
    public partial class Cliente_TYPE
    {
        private Persona_TYPE personaField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public Persona_TYPE persona
        {
            get
            {
                return this.personaField;
            }
            set
            {
                this.personaField = value;
            }
        }


    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Persona/2017/03/31")]
    public partial class Persona_TYPE
    {
        private IdentificacionPersona_TYPE identificacionField;
        private System.DateTime fechaNacimientoField;
        private string estadoCivilField;
        private string generoField;
        private DatosBasicosPersonaNatural_TYPE datosBasicosField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public IdentificacionPersona_TYPE identificacion
        {
            get
            {
                return this.identificacionField;
            }
            set
            {
                this.identificacionField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "date", Order = 3)]
        public System.DateTime fechaNacimiento
        {
            get
            {
                return this.fechaNacimientoField;
            }
            set
            {
                this.fechaNacimientoField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public string estadoCivil
        {
            get
            {
                return this.estadoCivilField;
            }
            set
            {
                this.estadoCivilField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 4)]
        public string genero
        {
            get
            {
                return this.generoField;
            }
            set
            {
                this.generoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 5)]
        public DatosBasicosPersonaNatural_TYPE datosBasicos
        {
            get
            {
                return this.datosBasicosField;
            }
            set
            {
                this.datosBasicosField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Persona/2017/03/31")]
    public partial class IdentificacionPersona_TYPE
    {

        private string identificacionField;

        private string tipoIdentificacionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public string identificacion
        {
            get
            {
                return this.identificacionField;
            }
            set
            {
                this.identificacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public string tipoIdentificacion
        {
            get
            {
                return this.tipoIdentificacionField;
            }
            set
            {
                this.tipoIdentificacionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Persona/2017/03/31")]
    public partial class DatosBasicosPersonaNatural_TYPE
    {


        private string primerApellidoField;

        private string primerNombreField;


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public string primerApellido
        {
            get
            {
                return this.primerApellidoField;
            }
            set
            {
                this.primerApellidoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public string primerNombre
        {
            get
            {
                return this.primerNombreField;
            }
            set
            {
                this.primerNombreField = value;
            }
        }

    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Persona/Distribuidor/2017/03/31")]
    public partial class Distribuidor_TYPE
    {
        private string codigoDistribuidorField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 6)]
        public string codigoDistribuidor
        {
            get
            {
                return this.codigoDistribuidorField;
            }
            set
            {
                this.codigoDistribuidorField = value;
            }
        }


        private Empresa_TYPE empresaField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public Empresa_TYPE empresa
        {
            get
            {
                return this.empresaField;
            }
            set
            {
                this.empresaField = value;
            }
        }


    }

    public partial class Empresa_TYPE
    {
        private IdentificacionPersona_TYPE identificacionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public IdentificacionPersona_TYPE identificacion
        {
            get
            {
                return this.identificacionField;
            }
            set
            {
                this.identificacionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Contrato/2017/03/31")]
    public partial class DetalleContrato_TYPE : Contrato_TYPE
    {
    }

    [System.Xml.Serialization.XmlIncludeAttribute(typeof(DetalleContrato_TYPE))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Contrato/2017/03/31")]
    public partial class Contrato_TYPE
    {
        private Cobertura_TYPE coberturaField;
        private Vehiculo_TYPE vehiculoField;

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public Cobertura_TYPE cobertura
        {
            get
            {
                return this.coberturaField;
            }
            set
            {
                this.coberturaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Vehiculo", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 3)]
        public Vehiculo_TYPE Vehiculo
        {
            get
            {
                return this.vehiculoField;
            }
            set
            {
                this.vehiculoField = value;
            }
        }



    }

    public partial class Cobertura_TYPE
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public limiteTest1 limite { get; set; }
    }

    public partial class limiteTest1
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public string limite { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public double Monto { get; set; }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Vehiculo/2017/03/31")]
    public partial class Vehiculo_TYPE
    {

        private string placaField;

        private string numeroMotorField;

        private string numeroChasisField;

        private int modeloField;
        private string colorField;

        private string fasecoldaMarcaIDField;

        private string fasecoldaModeloIDField;

        private Monto_TYPE montoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Placa", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public string Placa
        {
            get
            {
                return this.placaField;
            }
            set
            {
                this.placaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("NumeroMotor", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public string NumeroMotor
        {
            get
            {
                return this.numeroMotorField;
            }
            set
            {
                this.numeroMotorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("NumeroChasis", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public string NumeroChasis
        {
            get
            {
                return this.numeroChasisField;
            }
            set
            {
                this.numeroChasisField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Modelo", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 5)]
        public int Modelo
        {
            get
            {
                return this.modeloField;
            }
            set
            {
                this.modeloField = value;
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Color", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 18)]
        public string Color
        {
            get
            {
                return this.colorField;
            }
            set
            {
                this.colorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FasecoldaMarcaID", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 15)]
        public string FasecoldaMarcaID
        {
            get
            {
                return this.fasecoldaMarcaIDField;
            }
            set
            {
                this.fasecoldaMarcaIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FasecoldaModeloID", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 16)]
        public string FasecoldaModeloID
        {
            get
            {
                return this.fasecoldaModeloIDField;
            }
            set
            {
                this.fasecoldaModeloIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Monto", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 17)]
        public Monto_TYPE Monto
        {
            get
            {
                return this.montoField;
            }
            set
            {
                this.montoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/xsd/base/2017/03/31")]
    public partial class Monto_TYPE
    {
        private decimal montoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public decimal monto
        {
            get
            {
                return this.montoField;
            }
            set
            {
                this.montoField = value;
            }
        }
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Producto/2017/03/31")]
    public partial class SubProducto_TYPE
    {
        private Producto_TYPE productoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public Producto_TYPE producto
        {
            get
            {
                return this.productoField;
            }
            set
            {
                this.productoField = value;
            }
        }

    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Producto/2017/03/31")]
    public partial class Producto_TYPE
    {
        private IdentificacionProducto_TYPE identificacionProductoField;

        /// <remarks/>

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public IdentificacionProducto_TYPE identificacionProducto
        {
            get
            {
                return this.identificacionProductoField;
            }
            set
            {
                this.identificacionProductoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Producto/2017/03/31")]
    public partial class IdentificacionProducto_TYPE
    {

        private string idProductoField;


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public string idProducto
        {
            get
            {
                return this.idProductoField;
            }
            set
            {
                this.idProductoField = value;
            }
        }
    }


    //******************************************************** Aqui terminan los que se conocen


    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class CotizarPolizaAutoResponse
    {
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoResp/1.0", Order = 0)]
        public AxaServiceReferenceReduced.ConsultarPolizaAutoRespType ConsultarPolizaAutoResp;
        public CotizarPolizaAutoResponse()
        {
        }

        public CotizarPolizaAutoResponse(AxaServiceReferenceReduced.ConsultarPolizaAutoRespType ConsultarPolizaAutoResp)
        {
            this.ConsultarPolizaAutoResp = ConsultarPolizaAutoResp;
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoResp/1.0")]
    public partial class ConsultarPolizaAutoRespType
    {
        private EncabezadoSalida hEADERField;
        private BODYType1 bODYField;

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public EncabezadoSalida HEADER
        {
            get
            {
                return this.hEADERField;
            }
            set
            {
                this.hEADERField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public BODYType1 BODY
        {
            get
            {
                return this.bODYField;
            }
            set
            {
                this.bODYField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/Servicios/Base/EncabezadosSOA")]
    public partial class EncabezadoSalida
    {

        private string criticidadField;

        private string idCorrelacionConsumidorField;

        private string idTransaccionField;

        private string rtaCodCanalField;

        private string rtaCodHostField;

        private string rtaDescCanalField;

        private string rtaDescHostField;

        private string metodoField;

        private string procesoField;

        private string categoriaField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public string criticidad
        {
            get
            {
                return this.criticidadField;
            }
            set
            {
                this.criticidadField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public string idCorrelacionConsumidor
        {
            get
            {
                return this.idCorrelacionConsumidorField;
            }
            set
            {
                this.idCorrelacionConsumidorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public string idTransaccion
        {
            get
            {
                return this.idTransaccionField;
            }
            set
            {
                this.idTransaccionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 3)]
        public string rtaCodCanal
        {
            get
            {
                return this.rtaCodCanalField;
            }
            set
            {
                this.rtaCodCanalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 4)]
        public string rtaCodHost
        {
            get
            {
                return this.rtaCodHostField;
            }
            set
            {
                this.rtaCodHostField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 5)]
        public string rtaDescCanal
        {
            get
            {
                return this.rtaDescCanalField;
            }
            set
            {
                this.rtaDescCanalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 6)]
        public string rtaDescHost
        {
            get
            {
                return this.rtaDescHostField;
            }
            set
            {
                this.rtaDescHostField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 7)]
        public string metodo
        {
            get
            {
                return this.metodoField;
            }
            set
            {
                this.metodoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 8)]
        public string proceso
        {
            get
            {
                return this.procesoField;
            }
            set
            {
                this.procesoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 9)]
        public string categoria
        {
            get
            {
                return this.categoriaField;
            }
            set
            {
                this.categoriaField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(TypeName = "BODYType", Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoResp/1.0")]
    public partial class BODYType1
    {
        private CotizacionType[] cotizacionesField;

        [System.Xml.Serialization.XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        [System.Xml.Serialization.XmlArrayItemAttribute("cotizacion", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public CotizacionType[] cotizaciones
        {
            get
            {
                return this.cotizacionesField;
            }
            set
            {
                this.cotizacionesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoResp/1.0")]
    public partial class CotizacionType
    {

        private Cotizacion_TYPE[] cotizacionField;

        private string idAcuerdoNegocioField;

        private string idZonaClasificacionField;

        private string detalleMontoVehiculoField;

        private string valorPrimaField;

        private string gastosExpedicionField;

        private string impuestoField;

        private string valorTotalPrimaField;

        private string idSolicitudAgrupadoraField;

        private string rtaCotizacionField;

        private string fechaInicioVigenciaCotizacionField;

        private string fechaFinVigenciaCotizacionField;

        private string usuarioIdField;

        private string aniosSinReclamacionField;

        private string flagVigenteField;

        private string estadoEventoField;

        private ListaEventosType listaEventosField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Cotizacion", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public Cotizacion_TYPE[] Cotizacion
        {
            get
            {
                return this.cotizacionField;
            }
            set
            {
                this.cotizacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public string IdAcuerdoNegocio
        {
            get
            {
                return this.idAcuerdoNegocioField;
            }
            set
            {
                this.idAcuerdoNegocioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public string IdZonaClasificacion
        {
            get
            {
                return this.idZonaClasificacionField;
            }
            set
            {
                this.idZonaClasificacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 3)]
        public string DetalleMontoVehiculo
        {
            get
            {
                return this.detalleMontoVehiculoField;
            }
            set
            {
                this.detalleMontoVehiculoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 4)]
        public string ValorPrima
        {
            get
            {
                return this.valorPrimaField;
            }
            set
            {
                this.valorPrimaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 5)]
        public string GastosExpedicion
        {
            get
            {
                return this.gastosExpedicionField;
            }
            set
            {
                this.gastosExpedicionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 6)]
        public string Impuesto
        {
            get
            {
                return this.impuestoField;
            }
            set
            {
                this.impuestoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 7)]
        public string ValorTotalPrima
        {
            get
            {
                return this.valorTotalPrimaField;
            }
            set
            {
                this.valorTotalPrimaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 8)]
        public string IdSolicitudAgrupadora
        {
            get
            {
                return this.idSolicitudAgrupadoraField;
            }
            set
            {
                this.idSolicitudAgrupadoraField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 9)]
        public string RtaCotizacion
        {
            get
            {
                return this.rtaCotizacionField;
            }
            set
            {
                this.rtaCotizacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 10)]
        public string FechaInicioVigenciaCotizacion
        {
            get
            {
                return this.fechaInicioVigenciaCotizacionField;
            }
            set
            {
                this.fechaInicioVigenciaCotizacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 11)]
        public string FechaFinVigenciaCotizacion
        {
            get
            {
                return this.fechaFinVigenciaCotizacionField;
            }
            set
            {
                this.fechaFinVigenciaCotizacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 12)]
        public string UsuarioId
        {
            get
            {
                return this.usuarioIdField;
            }
            set
            {
                this.usuarioIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 14)]
        public string AniosSinReclamacion
        {
            get
            {
                return this.aniosSinReclamacionField;
            }
            set
            {
                this.aniosSinReclamacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 15)]
        public string FlagVigente
        {
            get
            {
                return this.flagVigenteField;
            }
            set
            {
                this.flagVigenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 16)]
        public string EstadoEvento
        {
            get
            {
                return this.estadoEventoField;
            }
            set
            {
                this.estadoEventoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 17)]
        public ListaEventosType ListaEventos
        {
            get
            {
                return this.listaEventosField;
            }
            set
            {
                this.listaEventosField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class CotizarPolizaAutoRequest
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoReq/1.0", Order = 0)]
        public AxaServiceReferenceReduced.ConsultarPolizaAutoReqType ConsultarPolizaAutoReq;

        public CotizarPolizaAutoRequest()
        {
        }

        public CotizarPolizaAutoRequest(AxaServiceReferenceReduced.ConsultarPolizaAutoReqType ConsultarPolizaAutoReq)
        {
            this.ConsultarPolizaAutoReq = ConsultarPolizaAutoReq;
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoResp/1.0")]
    public partial class ListaEventosType
    {

        private ListaEventosTypeValidacionCoEventos validacionCoEventosField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public ListaEventosTypeValidacionCoEventos ValidacionCoEventos
        {
            get
            {
                return this.validacionCoEventosField;
            }
            set
            {
                this.validacionCoEventosField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Cotizacion/2017/03/31")]
    public partial class Cotizacion_TYPE : CotizacionId
    {

        private SubProducto_TYPE subProductoField;

        private PersonaRol_TYPE[] prospectoField;

        private TipoCotizacion_TYPE tipoCotizacionField;

        private TerceroCotizacion_TYPE[] terceroField;

        private Distribuidor_TYPE distribuidorField;

        private Contrato_TYPE contratoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public SubProducto_TYPE subProducto
        {
            get
            {
                return this.subProductoField;
            }
            set
            {
                this.subProductoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("prospecto", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public PersonaRol_TYPE[] prospecto
        {
            get
            {
                return this.prospectoField;
            }
            set
            {
                this.prospectoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public TipoCotizacion_TYPE tipoCotizacion
        {
            get
            {
                return this.tipoCotizacionField;
            }
            set
            {
                this.tipoCotizacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("tercero", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 3)]
        public TerceroCotizacion_TYPE[] tercero
        {
            get
            {
                return this.terceroField;
            }
            set
            {
                this.terceroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 4)]
        public Distribuidor_TYPE distribuidor
        {
            get
            {
                return this.distribuidorField;
            }
            set
            {
                this.distribuidorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 5)]
        public Contrato_TYPE contrato
        {
            get
            {
                return this.contratoField;
            }
            set
            {
                this.contratoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Cliente_TYPE))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Distribuidor_TYPE))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Persona/2017/03/31")]
    public partial class PersonaRol_TYPE
    {
        private Persona_TYPE itemField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("persona", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
        public Persona_TYPE persona
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Cotizacion/2017/03/31")]
    public enum TipoCotizacion_TYPE
    {

        /// <remarks/>
        Oportunidad,

        /// <remarks/>
        Cotizacion,

        /// <remarks/>
        Simulacion,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Cotizacion/2017/03/31")]
    public partial class TerceroCotizacion_TYPE
    {

        private PersonaRol_TYPE terceroField;

        private RolTerceroCotizacion_TYPE rolTerceroField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public PersonaRol_TYPE tercero
        {
            get
            {
                return this.terceroField;
            }
            set
            {
                this.terceroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public RolTerceroCotizacion_TYPE rolTercero
        {
            get
            {
                return this.rolTerceroField;
            }
            set
            {
                this.rolTerceroField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://axacolpatria.co/MensajeNegocio/DominioAuto/consultarPolizaAutoResp/1.0")]
    public partial class ListaEventosTypeValidacionCoEventos
    {

        private string numeroEventoField;

        private string detalleField;

        private string detalleErrorMensajeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NumeroEvento
        {
            get
            {
                return this.numeroEventoField;
            }
            set
            {
                this.numeroEventoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Detalle
        {
            get
            {
                return this.detalleField;
            }
            set
            {
                this.detalleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DetalleErrorMensaje
        {
            get
            {
                return this.detalleErrorMensajeField;
            }
            set
            {
                this.detalleErrorMensajeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Cotizacion/2017/03/31")]
    public enum RolTerceroCotizacion_TYPE
    {

        /// <remarks/>
        Distribuidor,

        /// <remarks/>
        Prospecto,
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Cotizacion_TYPE))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://axacolpatria.co/DPW/Servicios/Negocio/Cotizacion/2017/03/31")]
    public partial class CotizacionId
    {

        private string cotizacionIdField;

        private string cotizacionIdPublicoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public string cotizacionId
        {
            get
            {
                return this.cotizacionIdField;
            }
            set
            {
                this.cotizacionIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public string cotizacionIdPublico
        {
            get
            {
                return this.cotizacionIdPublicoField;
            }
            set
            {
                this.cotizacionIdPublicoField = value;
            }
        }
    }








    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public partial class GestionPolizaAutoClient : System.ServiceModel.ClientBase<AxaServiceReferenceReduced.GestionPolizaAuto>, AxaServiceReferenceReduced.GestionPolizaAuto
    {

        /// <summary>
        /// Implemente este método parcial para configurar el punto de conexión de servicio.
        /// </summary>
        /// <param name="serviceEndpoint">El punto de conexión para configurar</param>
        /// <param name="clientCredentials">Credenciales de cliente</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);

        public GestionPolizaAutoClient() :
                base(GestionPolizaAutoClient.GetDefaultBinding(), GestionPolizaAutoClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.BasicHttpBinding_ITwoWayAsync.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public GestionPolizaAutoClient(EndpointConfiguration endpointConfiguration) :
                base(GestionPolizaAutoClient.GetBindingForEndpoint(endpointConfiguration), GestionPolizaAutoClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public GestionPolizaAutoClient(EndpointConfiguration endpointConfiguration, string remoteAddress) :
                base(GestionPolizaAutoClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public GestionPolizaAutoClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) :
                base(GestionPolizaAutoClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public GestionPolizaAutoClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<AxaServiceReferenceReduced.CotizarPolizaAutoResponse> AxaServiceReferenceReduced.GestionPolizaAuto.CotizarPolizaAutoAsync(AxaServiceReferenceReduced.CotizarPolizaAutoRequest request)
        {
            return base.Channel.CotizarPolizaAutoAsync(request);
        }

        public System.Threading.Tasks.Task<AxaServiceReferenceReduced.CotizarPolizaAutoResponse> CotizarPolizaAutoAsync(AxaServiceReferenceReduced.ConsultarPolizaAutoReqType ConsultarPolizaAutoReq)
        {
            AxaServiceReferenceReduced.CotizarPolizaAutoRequest inValue = new AxaServiceReferenceReduced.CotizarPolizaAutoRequest();
            inValue.ConsultarPolizaAutoReq = ConsultarPolizaAutoReq;
            return ((AxaServiceReferenceReduced.GestionPolizaAuto)(this)).CotizarPolizaAutoAsync(inValue);
        }

        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }

        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }

        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_ITwoWayAsync))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                result.Security.Mode = System.ServiceModel.BasicHttpSecurityMode.Transport;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }

        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_ITwoWayAsync))
            {
                return new System.ServiceModel.EndpointAddress("https://tmms.axacolpatria.co:3275/AXAColpatria/GestionPolizas/CotizacionPolizaAut" +
                        "o/GestionPolizaAuto.svc");
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }

        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return GestionPolizaAutoClient.GetBindingForEndpoint(EndpointConfiguration.BasicHttpBinding_ITwoWayAsync);
        }

        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return GestionPolizaAutoClient.GetEndpointAddress(EndpointConfiguration.BasicHttpBinding_ITwoWayAsync);
        }

        public enum EndpointConfiguration
        {

            BasicHttpBinding_ITwoWayAsync,
        }
    }
}
