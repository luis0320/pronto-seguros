@extends('layouts.app')

@section('template_title')
    {{ $financiera->name ?? 'Show Financiera' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Financiera</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('financieras.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nit:</strong>
                            {{ $financiera->nit }}
                        </div>
                        <div class="form-group">
                            <strong>Digito:</strong>
                            {{ $financiera->digito }}
                        </div>
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            {{ $financiera->nombre }}
                        </div>
                        <div class="form-group">
                            <strong>Telefono:</strong>
                            {{ $financiera->telefono }}
                        </div>
                        <div class="form-group">
                            <strong>Correo:</strong>
                            {{ $financiera->correo }}
                        </div>
                        <div class="form-group">
                            <strong>Activo:</strong>
                            {{ $financiera->activo }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
