    <!--header-area end-->
    @include('layouts.master')


    @extends('layouts.app')

@section('template_title')
    Financiera
@endsection

@if((Auth::user()->perfil) == 'admin')

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Create Financiera</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('financieras.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('financiera.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@endif

