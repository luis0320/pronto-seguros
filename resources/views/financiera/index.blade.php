    <!--header-area end-->
    @include('layouts.master')


    @extends('layouts.app')

@section('template_title')
    Financiera
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Financiera') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('financieras.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Nit</th>
										<th>Digito</th>
										<th>Nombre</th>
										<th>Telefono</th>
										<th>Correo</th>
										<th>Activo</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($financieras as $financiera)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $financiera->nit }}</td>
											<td>{{ $financiera->digito }}</td>
											<td>{{ $financiera->nombre }}</td>
											<td>{{ $financiera->telefono }}</td>
											<td>{{ $financiera->correo }}</td>
											<td>{{ $financiera->activo }}</td>

                                            <td>
                                                <form action="{{ route('financieras.destroy',$financiera->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('financieras.show',$financiera->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('financieras.edit',$financiera->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $financieras->links() !!}
            </div>
        </div>
    </div>
@endsection
