@extends('layouts.app')
@include('layouts.masteradmin')

@if((Auth::user()->perfil) == 'admin')


<div id="layoutSidenav_content">


@section('content')
    <section class=" container centre">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Compania</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('companias.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nit:</strong>
                            {{ $compania->nit }}
                        </div>
                        <div class="form-group">
                            <strong>Digito:</strong>
                            {{ $compania->digito }}
                        </div>
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            {{ $compania->nombre }}
                        </div>
                        <div class="form-group">
                            <strong>Asistencia:</strong>
                            {{ $compania->asistencia }}
                        </div>
                        <div class="form-group">
                            <strong>Asistenciafijo:</strong>
                            {{ $compania->asistenciafijo }}
                        </div>
                        <div class="form-group">
                            <strong>Activo:</strong>
                            {{ $compania->activo }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection


@endif
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
