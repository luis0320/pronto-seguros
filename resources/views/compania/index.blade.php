@include('layouts.masteradmin')
@extends('layouts.app')



@if((Auth::user()->perfil) == 'admin')


    @section('content')
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="moduloadministrador">Inicio</a></li>
                            <li class="breadcrumb-item active">Compañias</li>
                        </ol>
                        <!--div class="card mb-4">
                            <div class="card-body">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>.</div>
                        </div-->
                        <div class="card mb-4">
                            <div class="card-header" style="color:#fff"><i class="fas fa-table mr-1"></i>Compañias</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nit</th>
                                                <th>Digito</th>
                                                <th>Nombre</th>
                                                <th>Asistencia</th>
                                                <th>Fijo</th>
                                                <th>Ver</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($companias as $compania)
                                        <tr>
                                            <td>{{ ++$i }}</td>   
											<td>{{ $compania->nit }}</td>
											<td>{{ $compania->digito }}</td>
											<td>{{ $compania->nombre }}</td>
											<td>{{ $compania->asistencia }}</td>
											<td>{{ $compania->asistenciafijo }}</td>
                                            <td>
                                                <form action="{{ route('companias.destroy',$compania->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('companias.show',$compania->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('companias.edit',$compania->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                    {!! $companias->links() !!}
                </footer> 
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
        @endsection 
      
    </body>
</html>




@endif