<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('nit') }}
            {{ Form::text('nit', $compania->nit, ['class' => 'form-control' . ($errors->has('nit') ? ' is-invalid' : ''), 'placeholder' => 'Nit']) }}
            {!! $errors->first('nit', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('digito') }}
            {{ Form::text('digito', $compania->digito, ['class' => 'form-control' . ($errors->has('digito') ? ' is-invalid' : ''), 'placeholder' => 'Digito']) }}
            {!! $errors->first('digito', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('nombre') }}
            {{ Form::text('nombre', $compania->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre']) }}
            {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('asistencia') }}
            {{ Form::text('asistencia', $compania->asistencia, ['class' => 'form-control' . ($errors->has('asistencia') ? ' is-invalid' : ''), 'placeholder' => 'Asistencia']) }}
            {!! $errors->first('asistencia', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('asistenciafijo') }}
            {{ Form::text('asistenciafijo', $compania->asistenciafijo, ['class' => 'form-control' . ($errors->has('asistenciafijo') ? ' is-invalid' : ''), 'placeholder' => 'Asistenciafijo']) }}
            {!! $errors->first('asistenciafijo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('activo') }}
            {{ Form::text('activo', $compania->activo, ['class' => 'form-control' . ($errors->has('activo') ? ' is-invalid' : ''), 'placeholder' => 'Activo']) }}
            {!! $errors->first('activo', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>