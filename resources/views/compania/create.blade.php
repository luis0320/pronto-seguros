@extends('layouts.app')

@section('template_title')
    Create Compania
@endsection


@if((Auth::user()->perfil) == 'admin')

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Create Compania</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('companias.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('compania.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@endif