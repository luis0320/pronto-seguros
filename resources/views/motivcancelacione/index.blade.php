@extends('layouts.app')
@extends('layouts.master')

@section('template_title')
    Motivcancelacione
@endsection

@if((Auth::user()->perfil) == 'admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Motivcancelacione') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('motivcancelaciones.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Nombre</th>
										<th>Activo</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($motivcancelaciones as $motivcancelacione)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $motivcancelacione->motivnombre }}</td>
											<td>{{ $motivcancelacione->activo }}</td>

                                            <td>
                                                <form action="{{ route('motivcancelaciones.destroy',$motivcancelacione->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('motivcancelaciones.show',$motivcancelacione->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('motivcancelaciones.edit',$motivcancelacione->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $motivcancelaciones->links() !!}
            </div>
        </div>
    </div>
@endsection

@endif