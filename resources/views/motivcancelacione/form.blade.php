<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('motivnombre') }}
            {{ Form::text('motivnombre', $motivcancelacione->motivnombre, ['class' => 'form-control' . ($errors->has('motivnombre') ? ' is-invalid' : ''), 'placeholder' => 'Motivo de Cancelacion']) }}
            {!! $errors->first('motivnombre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('activo') }}
            {{ Form::text('activo', $motivcancelacione->activo, ['class' => 'form-control' . ($errors->has('activo') ? ' is-invalid' : ''), 'placeholder' => 'Activo']) }}
            {!! $errors->first('activo', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>