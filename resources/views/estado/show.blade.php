@extends('layouts.app')
@extends('layouts.master')

@section('template_title')
    {{ $estado->name ?? 'Show Estado' }}
@endsection


@if((Auth::user()->perfil) == 'admin')

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Estado</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('estados.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nombreestado:</strong>
                            {{ $estado->nombreestado }}
                        </div>
                        <div class="form-group">
                            <strong>Activo:</strong>
                            {{ $estado->activo }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@endif