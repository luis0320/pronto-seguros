@include('layouts.masteradmin')
@extends('layouts.app')



@if((Auth::user()->cliente_id) ==   ($cedula->id))


    @section('content')

    
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="moduloadministrador">Inicio</a></li>
                            <li class="breadcrumb-item active">Polizas</li>
                        </ol>
                        <!--div class="card mb-4">
                            <div class="card-body">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>.</div>
                        </div-->
                        <div class="card mb-4">
                            <div class="card-header" style="color:#fff"><i class="fas fa-table mr-1"></i>Mis polizas</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>                                      
                                                <th>Poliza </th>
                                                <th>Anexo</th>
                                                <th>Nota</th>
                                                <th>Desde<br>(AAmmdd)</th>
                                                <th>Hasta<br>(AAmmdd)</th>
                                                <th>Compania</th>
                                                <th>Ramo</th>
                                                <th>Placas</th>
                                                <th>Ver</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($mispolizas as $mispoliza)
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $mispoliza->Poliza }}</td>
                                                <td>{{ $mispoliza->Anexo }}</td>
                                                <td>{{ $mispoliza->Nota }}</td>
                                                <td>{{ $mispoliza->Desde }}</td>
                                                <td>{{ $mispoliza->Hasta }}</td>
                                                <td>{{ $mispoliza->Compania }}</td>
                                                <td>{{ $mispoliza->Ramo }}</td>
                                                <td>{{ $mispoliza->Placas }}</td>
                                                <td>
                                                    <a href="{{ route('mispolizas.show',$mispoliza->id) }}" class="btn btn-primary">
                                                        Ver Poliza
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>

        
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
        @endsection 
      
    </body>
</html>




@endif