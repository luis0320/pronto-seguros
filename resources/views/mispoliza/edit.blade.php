@extends('layouts.app')

@section('template_title')
    Update Mispoliza
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Update Mispoliza</span>
                    </div>
                    <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('mispolizas.index') }}"> Back</a>
                        </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('mispolizas.update', $mispoliza->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf

                            @include('mispoliza.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
