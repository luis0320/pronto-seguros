<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Poliza') }}
            {{ Form::text('Poliza', $mispoliza->Poliza, ['class' => 'form-control' . ($errors->has('Poliza') ? ' is-invalid' : ''), 'placeholder' => 'Poliza']) }}
            {!! $errors->first('Poliza', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Anexo') }}
            {{ Form::text('Anexo', $mispoliza->Anexo, ['class' => 'form-control' . ($errors->has('Anexo') ? ' is-invalid' : ''), 'placeholder' => 'Anexo']) }}
            {!! $errors->first('Anexo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Nota') }}
            {{ Form::text('Nota', $mispoliza->Nota, ['class' => 'form-control' . ($errors->has('Nota') ? ' is-invalid' : ''), 'placeholder' => 'Nota']) }}
            {!! $errors->first('Nota', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('FecNot') }}
            {{ Form::text('FecNot', $mispoliza->FecNot, ['class' => 'form-control' . ($errors->has('FecNot') ? ' is-invalid' : ''), 'placeholder' => 'Fecnot']) }}
            {!! $errors->first('FecNot', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Desde') }}
            {{ Form::text('Desde', $mispoliza->Desde, ['class' => 'form-control' . ($errors->has('Desde') ? ' is-invalid' : ''), 'placeholder' => 'Desde']) }}
            {!! $errors->first('Desde', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Hasta') }}
            {{ Form::text('Hasta', $mispoliza->Hasta, ['class' => 'form-control' . ($errors->has('Hasta') ? ' is-invalid' : ''), 'placeholder' => 'Hasta']) }}
            {!! $errors->first('Hasta', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Ej') }}
            {{ Form::text('Ej', $mispoliza->Ej, ['class' => 'form-control' . ($errors->has('Ej') ? ' is-invalid' : ''), 'placeholder' => 'Ej']) }}
            {!! $errors->first('Ej', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('CodCli') }}
            {{ Form::text('CodCli', $mispoliza->CodCli, ['class' => 'form-control' . ($errors->has('CodCli') ? ' is-invalid' : ''), 'placeholder' => 'Codcli']) }}
            {!! $errors->first('CodCli', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('NitCC') }}
            {{ Form::text('NitCC', $mispoliza->NitCC, ['class' => 'form-control' . ($errors->has('NitCC') ? ' is-invalid' : ''), 'placeholder' => 'Nitcc']) }}
            {!! $errors->first('NitCC', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('NombreCliente') }}
            {{ Form::text('NombreCliente', $mispoliza->NombreCliente, ['class' => 'form-control' . ($errors->has('NombreCliente') ? ' is-invalid' : ''), 'placeholder' => 'Nombrecliente']) }}
            {!! $errors->first('NombreCliente', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Compania') }}
            {{ Form::text('Compania', $mispoliza->Compania, ['class' => 'form-control' . ($errors->has('Compania') ? ' is-invalid' : ''), 'placeholder' => 'Compania']) }}
            {!! $errors->first('Compania', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Rm') }}
            {{ Form::text('Rm', $mispoliza->Rm, ['class' => 'form-control' . ($errors->has('Rm') ? ' is-invalid' : ''), 'placeholder' => 'Rm']) }}
            {!! $errors->first('Rm', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Ramo') }}
            {{ Form::text('Ramo', $mispoliza->Ramo, ['class' => 'form-control' . ($errors->has('Ramo') ? ' is-invalid' : ''), 'placeholder' => 'Ramo']) }}
            {!! $errors->first('Ramo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('PrimNeta') }}
            {{ Form::text('PrimNeta', $mispoliza->PrimNeta, ['class' => 'form-control' . ($errors->has('PrimNeta') ? ' is-invalid' : ''), 'placeholder' => 'Primneta']) }}
            {!! $errors->first('PrimNeta', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('GastoExp') }}
            {{ Form::text('GastoExp', $mispoliza->GastoExp, ['class' => 'form-control' . ($errors->has('GastoExp') ? ' is-invalid' : ''), 'placeholder' => 'Gastoexp']) }}
            {!! $errors->first('GastoExp', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('IVA') }}
            {{ Form::text('IVA', $mispoliza->IVA, ['class' => 'form-control' . ($errors->has('IVA') ? ' is-invalid' : ''), 'placeholder' => 'Iva']) }}
            {!! $errors->first('IVA', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('PrimTot') }}
            {{ Form::text('PrimTot', $mispoliza->PrimTot, ['class' => 'form-control' . ($errors->has('PrimTot') ? ' is-invalid' : ''), 'placeholder' => 'Primtot']) }}
            {!! $errors->first('PrimTot', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Comi') }}
            {{ Form::text('Comi', $mispoliza->Comi, ['class' => 'form-control' . ($errors->has('Comi') ? ' is-invalid' : ''), 'placeholder' => 'Comi']) }}
            {!! $errors->first('Comi', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Comision') }}
            {{ Form::text('Comision', $mispoliza->Comision, ['class' => 'form-control' . ($errors->has('Comision') ? ' is-invalid' : ''), 'placeholder' => 'Comision']) }}
            {!! $errors->first('Comision', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('PrimPropi') }}
            {{ Form::text('PrimPropi', $mispoliza->PrimPropi, ['class' => 'form-control' . ($errors->has('PrimPropi') ? ' is-invalid' : ''), 'placeholder' => 'Primpropi']) }}
            {!! $errors->first('PrimPropi', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('ComiPropi') }}
            {{ Form::text('ComiPropi', $mispoliza->ComiPropi, ['class' => 'form-control' . ($errors->has('ComiPropi') ? ' is-invalid' : ''), 'placeholder' => 'Comipropi']) }}
            {!! $errors->first('ComiPropi', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Ca') }}
            {{ Form::text('Ca', $mispoliza->Ca, ['class' => 'form-control' . ($errors->has('Ca') ? ' is-invalid' : ''), 'placeholder' => 'Ca']) }}
            {!! $errors->first('Ca', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Placas') }}
            {{ Form::text('Placas', $mispoliza->Placas, ['class' => 'form-control' . ($errors->has('Placas') ? ' is-invalid' : ''), 'placeholder' => 'Placas']) }}
            {!! $errors->first('Placas', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Proc01') }}
            {{ Form::text('Proc01', $mispoliza->Proc01, ['class' => 'form-control' . ($errors->has('Proc01') ? ' is-invalid' : ''), 'placeholder' => 'Proc01']) }}
            {!! $errors->first('Proc01', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Pro1') }}
            {{ Form::text('Pro1', $mispoliza->Pro1, ['class' => 'form-control' . ($errors->has('Pro1') ? ' is-invalid' : ''), 'placeholder' => 'Pro1']) }}
            {!! $errors->first('Pro1', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Proc1') }}
            {{ Form::text('Proc1', $mispoliza->Proc1, ['class' => 'form-control' . ($errors->has('Proc1') ? ' is-invalid' : ''), 'placeholder' => 'Proc1']) }}
            {!! $errors->first('Proc1', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Proc02') }}
            {{ Form::text('Proc02', $mispoliza->Proc02, ['class' => 'form-control' . ($errors->has('Proc02') ? ' is-invalid' : ''), 'placeholder' => 'Proc02']) }}
            {!! $errors->first('Proc02', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Pro2') }}
            {{ Form::text('Pro2', $mispoliza->Pro2, ['class' => 'form-control' . ($errors->has('Pro2') ? ' is-invalid' : ''), 'placeholder' => 'Pro2']) }}
            {!! $errors->first('Pro2', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Proc2') }}
            {{ Form::text('Proc2', $mispoliza->Proc2, ['class' => 'form-control' . ($errors->has('Proc2') ? ' is-invalid' : ''), 'placeholder' => 'Proc2']) }}
            {!! $errors->first('Proc2', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('NombreProced') }}
            {{ Form::text('NombreProced', $mispoliza->NombreProced, ['class' => 'form-control' . ($errors->has('NombreProced') ? ' is-invalid' : ''), 'placeholder' => 'Nombreproced']) }}
            {!! $errors->first('NombreProced', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Tv') }}
            {{ Form::text('Tv', $mispoliza->Tv, ['class' => 'form-control' . ($errors->has('Tv') ? ' is-invalid' : ''), 'placeholder' => 'Tv']) }}
            {!! $errors->first('Tv', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Us') }}
            {{ Form::text('Us', $mispoliza->Us, ['class' => 'form-control' . ($errors->has('Us') ? ' is-invalid' : ''), 'placeholder' => 'Us']) }}
            {!! $errors->first('Us', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Tomador') }}
            {{ Form::text('Tomador', $mispoliza->Tomador, ['class' => 'form-control' . ($errors->has('Tomador') ? ' is-invalid' : ''), 'placeholder' => 'Tomador']) }}
            {!! $errors->first('Tomador', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Beneficiario') }}
            {{ Form::text('Beneficiario', $mispoliza->Beneficiario, ['class' => 'form-control' . ($errors->has('Beneficiario') ? ' is-invalid' : ''), 'placeholder' => 'Beneficiario']) }}
            {!! $errors->first('Beneficiario', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Observaciones') }}
            {{ Form::text('Observaciones', $mispoliza->Observaciones, ['class' => 'form-control' . ($errors->has('Observaciones') ? ' is-invalid' : ''), 'placeholder' => 'Observaciones']) }}
            {!! $errors->first('Observaciones', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Sub') }}
            {{ Form::text('Sub', $mispoliza->Sub, ['class' => 'form-control' . ($errors->has('Sub') ? ' is-invalid' : ''), 'placeholder' => 'Sub']) }}
            {!! $errors->first('Sub', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('TP') }}
            {{ Form::text('TP', $mispoliza->TP, ['class' => 'form-control' . ($errors->has('TP') ? ' is-invalid' : ''), 'placeholder' => 'Tp']) }}
            {!! $errors->first('TP', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Ap') }}
            {{ Form::text('Ap', $mispoliza->Ap, ['class' => 'form-control' . ($errors->has('Ap') ? ' is-invalid' : ''), 'placeholder' => 'Ap']) }}
            {!! $errors->first('Ap', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('FecNac') }}
            {{ Form::text('FecNac', $mispoliza->FecNac, ['class' => 'form-control' . ($errors->has('FecNac') ? ' is-invalid' : ''), 'placeholder' => 'Fecnac']) }}
            {!! $errors->first('FecNac', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Tel1Aseg') }}
            {{ Form::text('Tel1Aseg', $mispoliza->Tel1Aseg, ['class' => 'form-control' . ($errors->has('Tel1Aseg') ? ' is-invalid' : ''), 'placeholder' => 'Tel1Aseg']) }}
            {!! $errors->first('Tel1Aseg', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Tel2Aseg') }}
            {{ Form::text('Tel2Aseg', $mispoliza->Tel2Aseg, ['class' => 'form-control' . ($errors->has('Tel2Aseg') ? ' is-invalid' : ''), 'placeholder' => 'Tel2Aseg']) }}
            {!! $errors->first('Tel2Aseg', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('CelAsegur') }}
            {{ Form::text('CelAsegur', $mispoliza->CelAsegur, ['class' => 'form-control' . ($errors->has('CelAsegur') ? ' is-invalid' : ''), 'placeholder' => 'Celasegur']) }}
            {!! $errors->first('CelAsegur', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('DirAsegur') }}
            {{ Form::text('DirAsegur', $mispoliza->DirAsegur, ['class' => 'form-control' . ($errors->has('DirAsegur') ? ' is-invalid' : ''), 'placeholder' => 'Dirasegur']) }}
            {!! $errors->first('DirAsegur', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('EmailAsegur') }}
            {{ Form::text('EmailAsegur', $mispoliza->EmailAsegur, ['class' => 'form-control' . ($errors->has('EmailAsegur') ? ' is-invalid' : ''), 'placeholder' => 'Emailasegur']) }}
            {!! $errors->first('EmailAsegur', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Ciudad') }}
            {{ Form::text('Ciudad', $mispoliza->Ciudad, ['class' => 'form-control' . ($errors->has('Ciudad') ? ' is-invalid' : ''), 'placeholder' => 'Ciudad']) }}
            {!! $errors->first('Ciudad', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Depto') }}
            {{ Form::text('Depto', $mispoliza->Depto, ['class' => 'form-control' . ($errors->has('Depto') ? ' is-invalid' : ''), 'placeholder' => 'Depto']) }}
            {!! $errors->first('Depto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('FecRbo') }}
            {{ Form::text('FecRbo', $mispoliza->FecRbo, ['class' => 'form-control' . ($errors->has('FecRbo') ? ' is-invalid' : ''), 'placeholder' => 'Fecrbo']) }}
            {!! $errors->first('FecRbo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Rferencia') }}
            {{ Form::text('Rferencia', $mispoliza->Rferencia, ['class' => 'form-control' . ($errors->has('Rferencia') ? ' is-invalid' : ''), 'placeholder' => 'Rferencia']) }}
            {!! $errors->first('Rferencia', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Zn') }}
            {{ Form::text('Zn', $mispoliza->Zn, ['class' => 'form-control' . ($errors->has('Zn') ? ' is-invalid' : ''), 'placeholder' => 'Zn']) }}
            {!! $errors->first('Zn', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('VrAsegPoliz') }}
            {{ Form::text('VrAsegPoliz', $mispoliza->VrAsegPoliz, ['class' => 'form-control' . ($errors->has('VrAsegPoliz') ? ' is-invalid' : ''), 'placeholder' => 'Vrasegpoliz']) }}
            {!! $errors->first('VrAsegPoliz', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('VrAsegur') }}
            {{ Form::text('VrAsegur', $mispoliza->VrAsegur, ['class' => 'form-control' . ($errors->has('VrAsegur') ? ' is-invalid' : ''), 'placeholder' => 'Vrasegur']) }}
            {!! $errors->first('VrAsegur', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Mode') }}
            {{ Form::text('Mode', $mispoliza->Mode, ['class' => 'form-control' . ($errors->has('Mode') ? ' is-invalid' : ''), 'placeholder' => 'Mode']) }}
            {!! $errors->first('Mode', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('CodFase') }}
            {{ Form::text('CodFase', $mispoliza->CodFase, ['class' => 'form-control' . ($errors->has('CodFase') ? ' is-invalid' : ''), 'placeholder' => 'Codfase']) }}
            {!! $errors->first('CodFase', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Marca') }}
            {{ Form::text('Marca', $mispoliza->Marca, ['class' => 'form-control' . ($errors->has('Marca') ? ' is-invalid' : ''), 'placeholder' => 'Marca']) }}
            {!! $errors->first('Marca', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Clase') }}
            {{ Form::text('Clase', $mispoliza->Clase, ['class' => 'form-control' . ($errors->has('Clase') ? ' is-invalid' : ''), 'placeholder' => 'Clase']) }}
            {!! $errors->first('Clase', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Tipo') }}
            {{ Form::text('Tipo', $mispoliza->Tipo, ['class' => 'form-control' . ($errors->has('Tipo') ? ' is-invalid' : ''), 'placeholder' => 'Tipo']) }}
            {!! $errors->first('Tipo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Motor') }}
            {{ Form::text('Motor', $mispoliza->Motor, ['class' => 'form-control' . ($errors->has('Motor') ? ' is-invalid' : ''), 'placeholder' => 'Motor']) }}
            {!! $errors->first('Motor', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('SerieChs') }}
            {{ Form::text('SerieChs', $mispoliza->SerieChs, ['class' => 'form-control' . ($errors->has('SerieChs') ? ' is-invalid' : ''), 'placeholder' => 'Seriechs']) }}
            {!! $errors->first('SerieChs', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Servicio') }}
            {{ Form::text('Servicio', $mispoliza->Servicio, ['class' => 'form-control' . ($errors->has('Servicio') ? ' is-invalid' : ''), 'placeholder' => 'Servicio']) }}
            {!! $errors->first('Servicio', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Cilindraj') }}
            {{ Form::text('Cilindraj', $mispoliza->Cilindraj, ['class' => 'form-control' . ($errors->has('Cilindraj') ? ' is-invalid' : ''), 'placeholder' => 'Cilindraj']) }}
            {!! $errors->first('Cilindraj', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>