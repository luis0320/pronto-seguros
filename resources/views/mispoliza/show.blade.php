@extends('layouts.app')
@include('layouts.masteradmin')


    @section('content')
   
      
 
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="../moduloadministrador">Inicio</a></li>
                            <li class="breadcrumb-item active">Datos de su poliza</li>
                        </ol>
                        <!--div class="card mb-4">
                            <div class="card-body">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>.</div>
                        </div-->
                        <div class="card mb-4">
                            <div class="card-header" style="color:#fff"></i>Ver Poliza</div>
                                <div class="card-body">
                                    <div id="layoutAuthentication">
                                        <div id="layoutAuthentication_content">
                                            <main>
                                                <div class="container">
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-7">
                                                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                                                <div class="card-body">
                                                                    @if(($mispoliza->NitCC) ==  ($cedula->identificacion) )
                                                                    <form action="{{ route('mailrenovaciones') }}" method="post" id="form-contact" accept-charset="utf-8">
                                                                        {!! csrf_field() !!}
                                                                        
                                                                            <div class="form-row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="name">Nombres</label>
                                                                                        <input class="form-control py-4" name="nombres" type="text" placeholder=" {{ $mispoliza->NombreCliente }}"  value="{{ $mispoliza->NombreCliente }}" required="required" disabled/>
                                                                                    </div>
                                                                                </div>   
                                                                            </div>
                                                                            
                                                                                @if((Auth::user()->id)>0)
                                                                                    {{ Form::hidden('email', Auth::user()->email) }}
                                                                                @endif

                                                                                <input type='hidden' name='nombres' value='{{ $mispoliza->NombreCliente }}' />
                                                                                <input type='hidden' name='placas' value='{{ $mispoliza->Placas }}' />
                                                                                <input type='hidden' name='cedulas' value='{{ $mispoliza->NitCC }}' />
                                                                            
                                                                            <div class="form-row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <div class="form-group">
                                                                                            <label class="small mb-1" for="compania">Compañia</label>
                                                                                            <input class="form-control py-4" id="compania" type="text"  value="{{ $mispoliza->Compania }}" placeholder="{{ $mispoliza->Compania }}" name="compania" required="required" disabled/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @if($mispoliza->Compania == 'SOLIDARIA')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                        <input class="form-control py-4" id="Asistencia" type="text"  value="#789 - 018000512021" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                    <!--div style="text-align:center">
                                                                                        <a href="https://www.segurossura.com.co/paginas/pago-express.aspx#/Pagos" target="_blank" class="btn-common" role="button">
                                                                                            <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                                                        </a>
                                                                                    </div-->
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'ZURICH')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#723 - 018000112723" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'SURAMERI')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#888 - 018000518888" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'BOLIVAR')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#322 - 018000123322" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'ESTADO')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#388 - 018000123010" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'H D I')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#204 - 018000121206" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'LIBERTY')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#224 - 018000117224" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'MAPFRE')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#624 - 018000977025" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'MUNDIAL')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#935 - 018000111935" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'ALLIANZ')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#265" placeholder="{{ $mispoliza->Hasta }}" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'PREVISORA')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#345 - 018000910554" placeholder="" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'SBS SEGUROS')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#360 - 018000522244" placeholder="" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            @if($mispoliza->Compania == 'AXA COLPATRIA')
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Asistencia">Asistencia</label>
                                                                                    <input class="form-control py-4" id="Asistencia" type="text"  value="#247 - 018000512620" placeholder="" name="Asistencia" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Desde">Desde (aammdd)</label>
                                                                                        <input class="form-control py-4" id="Desde" type="text"  value="{{ $mispoliza->Desde }}" placeholder="{{ $mispoliza->Hasta }}" name="Desde" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Hasta">Hasta (aammdd)</label>
                                                                                    <input class="form-control py-4" id="Hasta" type="text"  value="{{ $mispoliza->Hasta }}" placeholder="{{ $mispoliza->Hasta }}" name="Hasta" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="placas">Placa</label>
                                                                                    <input class="form-control py-4" name="placas" type="text"  value="{{ $mispoliza->Placas }}" placeholder="{{ $mispoliza->Placas }}"  required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Ramo">Ramo</label>
                                                                                    <input class="form-control py-4" id="Ramo" type="text"  value="{{ $mispoliza->Ramo }}" placeholder="{{ $mispoliza->Ramo }}" name="Ramo" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="PrimTot">Valor total</label>
                                                                                    <input class="form-control py-4" id="PrimTot" type="text"  value="$ {{ $mispoliza->PrimTot }}" placeholder="{{ $mispoliza->PrimTot }}" name="PrimTot" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="form-group">
                                                                                        <label class="small mb-1" for="Observaciones">Observacion</label>
                                                                                    <input class="form-control py-4" id="Observaciones" type="text"  value="{{ $mispoliza->Observaciones }}" placeholder="{{ $mispoliza->Observaciones }}" name="Observaciones" required="required" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                         </div>
                                                                        <div class="form-group mt-4 mb-0">
                                                                            <!--a class="btn btn-primary btn-block" href="{{ url('administrador') }}">Crear la cuenta</a-->
                                                                            
                                                                            <button type="submit" class="btn btn-primary">Solicitar Renovacion</button>
                                                                        </div>
                                                                    </form>
                                                                    @else
                                                                    <div class="container-fluid">
                                                                        <div class="d-flex align-items-center justify-content-between small">       
                                                                            <div class="text-muted">No hay datos para mostrar.</div>
                                                                        </div>      
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                    @if ($message = Session::get('success'))
                                                        <div class="alert alert-success">
                                                            <p>{{ $message }}</p>
                                                        </div>
                                                    @endif
                                                </div>                     
                                            </main>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
      
    </body>
</html>






@endsection










