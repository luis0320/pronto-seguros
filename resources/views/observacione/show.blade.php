@extends('layouts.app')

@section('template_title')
    {{ $observacione->name ?? 'Show Observacione' }}
@endsection

@if((Auth::user()->perfil) == 'admin')

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Observacione</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('observaciones.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Observacion:</strong>
                            {{ $observacione->observacion }}
                        </div>
                        <div class="form-group">
                            <strong>Ob Idestado:</strong>
                            {{ $observacione->ob_idestado }}
                        </div>
                        <div class="form-group">
                            <strong>Ob Idcancelaciones:</strong>
                            {{ $observacione->ob_idcancelaciones }}
                        </div>
                        <div class="form-group">
                            <strong>U Idusuarios:</strong>
                            {{ $observacione->u_idusuarios }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@endif