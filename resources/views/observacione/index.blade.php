@extends('layouts.app')

@section('template_title')
    Observacione
@endsection

@if((Auth::user()->perfil) == 'admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Observacione') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('observaciones.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Observacion</th>
										<th>Ob Idestado</th>
										<th>Ob Idcancelaciones</th>
										<th>U Idusuarios</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($observaciones as $observacione)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $observacione->observacion }}</td>
											<td>{{ $observacione->estado->nombreestado }}</td>
											<td>{{ $observacione->ob_idcancelaciones }}</td>
											<td>{{ $observacione->u_idusuarios }}</td>

                                            <td>
                                                <form action="{{ route('observaciones.destroy',$observacione->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('observaciones.show',$observacione->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('observaciones.edit',$observacione->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $observaciones->links() !!}
            </div>
        </div>
    </div>
@endsection

@endif