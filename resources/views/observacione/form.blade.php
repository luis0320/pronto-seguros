<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('observacion') }}
            {{ Form::text('observacion', $observacione->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observacion']) }}
            {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('ob_idestado') }}
            {{ Form::text('ob_idestado', $observacione->ob_idestado, ['class' => 'form-control' . ($errors->has('ob_idestado') ? ' is-invalid' : ''), 'placeholder' => 'Ob Idestado']) }}
            {!! $errors->first('ob_idestado', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('ob_idcancelaciones') }}
            {{ Form::text('ob_idcancelaciones', $observacione->ob_idcancelaciones, ['class' => 'form-control' . ($errors->has('ob_idcancelaciones') ? ' is-invalid' : ''), 'placeholder' => 'Ob Idcancelaciones']) }}
            {!! $errors->first('ob_idcancelaciones', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('u_idusuarios') }}
            {{ Form::text('u_idusuarios', $observacione->u_idusuarios, ['class' => 'form-control' . ($errors->has('u_idusuarios') ? ' is-invalid' : ''), 'placeholder' => 'U Idusuarios']) }}
            {!! $errors->first('u_idusuarios', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>