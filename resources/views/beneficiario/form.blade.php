<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('nit') }}
            {{ Form::text('nit', $beneficiario->nit, ['class' => 'form-control' . ($errors->has('nit') ? ' is-invalid' : ''), 'placeholder' => 'Nit']) }}
            {!! $errors->first('nit', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('digito') }}
            {{ Form::text('digito', $beneficiario->digito, ['class' => 'form-control' . ($errors->has('digito') ? ' is-invalid' : ''), 'placeholder' => 'Digito']) }}
            {!! $errors->first('digito', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('benefnombre') }}
            {{ Form::text('benefnombre', $beneficiario->benefnombre, ['class' => 'form-control' . ($errors->has('benefnombre') ? ' is-invalid' : ''), 'placeholder' => 'Benefnombre']) }}
            {!! $errors->first('benefnombre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('activo') }}
            {{ Form::text('activo', $beneficiario->activo, ['class' => 'form-control' . ($errors->has('activo') ? ' is-invalid' : ''), 'placeholder' => 'Activo']) }}
            {!! $errors->first('activo', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>