@extends('layouts.app')
@extends('layouts.master')


@section('template_title')
    Create Beneficiario
@endsection


@if((Auth::user()->perfil) == 'admin')

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Create Beneficiario</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('beneficiarios.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('beneficiario.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@endif