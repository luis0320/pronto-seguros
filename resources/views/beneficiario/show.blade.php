
<!DOCTYPE html>

@extends('layouts.app')
@include('layouts.masteradmin')

@section('template_title')
    Scancelacione
@endsection

@if((Auth::user()->perfil) == 'admin')


    <div class="container">


<br><br>

@section('content')
<section class=" container centre">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title"> <strong>SOLICITUD DE CANCELACION</strong></span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('scancelaciones.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                            
                        <div class="form-group">
                            <strong>Nit:</strong>
                            {{ $beneficiario->nit }}
                        </div>
                        <div class="form-group">
                            <strong>Digito:</strong>
                            {{ $beneficiario->digito }}
                        </div>
                        <div class="form-group">
                            <strong>Benefnombre:</strong>
                            {{ $beneficiario->benefnombre }}
                        </div>
                        <div class="form-group">
                            <strong>Activo:</strong>
                            {{ $beneficiario->activo }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection


@endif