@extends('layouts.app')

@section('template_title')
    {{ $cotizacionSoat->name ?? 'Show Cotizacion Soat' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Cotizacion Soat</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('cotizacion-soats.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Placa:</strong>
                            {{ $cotizacionSoat->placa }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha:</strong>
                            {{ $cotizacionSoat->fecha }}
                        </div>
                        <div class="form-group">
                            <strong>Total:</strong>
                            {{ $cotizacionSoat->total }}
                        </div>
                        <div class="form-group">
                            <strong>Marca:</strong>
                            {{ $cotizacionSoat->marca }}
                        </div>
                        <div class="form-group">
                            <strong>Linea:</strong>
                            {{ $cotizacionSoat->linea }}
                        </div>
                        <div class="form-group">
                            <strong>Modelo:</strong>
                            {{ $cotizacionSoat->modelo }}
                        </div>
                        <div class="form-group">
                            <strong>Cedula:</strong>
                            {{ $cotizacionSoat->cedula }}
                        </div>
                        <div class="form-group">
                            <strong>Correo:</strong>
                            {{ $cotizacionSoat->correo }}
                        </div>
                        <div class="form-group">
                            <strong>Celular1:</strong>
                            {{ $cotizacionSoat->celular1 }}
                        </div>
                        <div class="form-group">
                            <strong>Descuento:</strong>
                            {{ $cotizacionSoat->descuento }}
                        </div>
                        <div class="form-group">
                            <strong>Preciodes:</strong>
                            {{ $cotizacionSoat->preciodes }}
                        </div>
                        <div class="form-group">
                            <strong>Celular:</strong>
                            {{ $cotizacionSoat->celular }}
                        </div>
                        <div class="form-group">
                            <strong>Codigoreferido:</strong>
                            {{ $cotizacionSoat->codigoreferido }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha Creacion:</strong>
                            {{ $cotizacionSoat->fecha_creacion }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
