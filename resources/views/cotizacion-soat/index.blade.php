@extends('layouts.app')

@section('template_title')
    Cotizacion Soat
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Cotizacion Soat') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('cotizacion-soats.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Placa</th>
										<th>Fecha</th>
										<th>Total</th>
										<th>Marca</th>
										<th>Linea</th>
										<th>Modelo</th>
										<th>Cedula</th>
										<th>Correo</th>
										<th>Celular1</th>
										<th>Descuento</th>
										<th>Preciodes</th>
										<th>Celular</th>
										<th>Codigoreferido</th>
										<th>Fecha Creacion</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cotizacionSoats as $cotizacionSoat)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $cotizacionSoat->placa }}</td>
											<td>{{ $cotizacionSoat->fecha }}</td>
											<td>{{ $cotizacionSoat->total }}</td>
											<td>{{ $cotizacionSoat->marca }}</td>
											<td>{{ $cotizacionSoat->linea }}</td>
											<td>{{ $cotizacionSoat->modelo }}</td>
											<td>{{ $cotizacionSoat->cedula }}</td>
											<td>{{ $cotizacionSoat->correo }}</td>
											<td>{{ $cotizacionSoat->celular1 }}</td>
											<td>{{ $cotizacionSoat->descuento }}</td>
											<td>{{ $cotizacionSoat->preciodes }}</td>
											<td>{{ $cotizacionSoat->celular }}</td>
											<td>{{ $cotizacionSoat->codigoreferido }}</td>
											<td>{{ $cotizacionSoat->fecha_creacion }}</td>

                                            <td>
                                                <form action="{{ route('cotizacion-soats.destroy',$cotizacionSoat->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('cotizacion-soats.show',$cotizacionSoat->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('cotizacion-soats.edit',$cotizacionSoat->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $cotizacionSoats->links() !!}
            </div>
        </div>
    </div>
@endsection
