<!DOCTYPE html>

@extends('layouts.app')
@include('layouts.masteradmin')

@section('template_title')
    Scancelacione
@endsection



<div id="layoutSidenav_content">


@section('content')
<section class=" container centre">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title" id="card_title" style="color:#fff"> <strong>SOLICITUD DE CANCELACION</strong></span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('scancelaciones.index') }}"> Atras</a>
                        </div>
                    </div>

                    <div class="card-body">
                    <div class="form-group">
                        <strong>No. solicitud:</strong>
                            {{ $scancelacione->id }}
                        </div>
                        <div class="form-group">
                            <strong>Estado:</strong>
                            {{ $scancelacione->estado->nombreestado }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha a cancelar:</strong>
                            {{ $scancelacione->fechaacancelar }}
                        </div>
                        <div class="form-group">
                            <strong>Celular:</strong>
                            {{ $scancelacione->celular }}
                        </div>
                        <div class="form-group">
                            <strong>Placa:</strong>
                            {{ $scancelacione->placa }}
                        </div>
                        <div class="form-group">
                            <strong>Email:</strong>
                            {{ $scancelacione->email }}
                        </div>
                        <div class="form-group">
                            <strong>Motivo de cancelacion:</strong>
                            {{ $scancelacione->motivcancelacione->motivnombre }}
                        </div>
                        <div class="form-group">
                            <strong>Aseguradora:</strong>
                            {{ $scancelacione->compania->nombre }}
                        </div>
                        <div class="form-group">
                            <strong>Forma de pago:</strong>
                            {{ $scancelacione->financiera->nombre }}
                        </div>
                        <div class="form-group">
                            <strong>Beneficiario:</strong>
                            {{ $scancelacione->beneficiario->benefnombre }}
                        </div>

                        <div class="form-group">
                            <strong>Observacion:</strong>
                            {{ $scancelacione->observacion }}
                        </div>

                        @if(Empty($scancelacione->filecarta)) 
                            @else      
                            <div class="form-group">
                                <strong>Carta de cancelacion:</strong>
                                <a class="btn btn-sm btn-info" href="{{ asset('storage/'.$scancelacione->filecarta) }}" target='_blank'><i class="fa fa-fw fa-eye"></i> Ver &nbsp;</a>
                            </div>
                        @endif

                        @if(Empty($scancelacione->filelevantamiento)) 
                            @else      
                        <div class="form-group">                            
                            <strong>filelevantamiento:</strong>
                            <a class="btn btn-sm btn-info" href="{{ asset('storage/'.$scancelacione->filelevantamiento) }}" target='_blank'><i class="fa fa-fw fa-eye"></i> Ver &nbsp;</a>
                        </div>
                        @endif

                        @if(Empty($scancelacione->filedevolucion)) 
                            @else    
                        <div class="form-group">
                            <strong>Filedevolucion:</strong>                            
                            <a class="btn btn-sm btn-info" href="{{ asset('storage/'.$scancelacione->filedevolucion) }}" target='_blank'><i class="fa fa-fw fa-eye"></i> Ver &nbsp;</a>
                        </div>
                        @endif
                        @if(Empty($scancelacione->filecedula)) 
                            @else 
                        <div class="form-group">
                            <strong>Filecedula:</strong>                            
                            <a class="btn btn-sm btn-info" href="{{ asset('storage/'.$scancelacione->filecedula) }}" target='_blank'><i class="fa fa-fw fa-eye"></i> Ver</a>
                        </div>
                        @endif
                        @if(Empty($scancelacione->filetarjetadepropiedad)) 
                            @else 
                        <div class="form-group">
                            <strong>Filetarjetadepropiedad:</strong>
                            <a class="btn btn-sm btn-info" href="{{ asset('storage/'.$scancelacione->filetarjetadepropiedad) }}" target='_blank'><i class="fa fa-fw fa-eye"></i> Ver</a>
                        </div>
                        @endif
                        @if(Empty($scancelacione->filesarlaft)) 
                            @else 
                        <div class="form-group">
                            <strong>Filesarlaft:</strong>
                            <a class="btn btn-sm btn-info" href="{{ asset('storage/'.$scancelacione->filesarlaft) }}" target='_blank'><i class="fa fa-fw fa-eye"></i> Ver</a>
                        </div>
                        @endif
                        @if(Empty($scancelacione->filepoliza)) 
                            @else 
                        <div class="form-group">
                            <strong>Filepoliza:</strong>                            
                            <a class="btn btn-sm btn-info" href="{{ asset('storage/'.$scancelacione->filepoliza) }}" target='_blank'><i class="fa fa-fw fa-eye"></i> Ver</a>
                        </div>
                        @endif
                        @if(Empty($scancelacione->fileotros)) 
                            @else 
                        <div class="form-group">
                            <strong>Fileotros:</strong>
                            <a class="btn btn-sm btn-info" href="{{ asset('storage/'.$scancelacione->fileotros) }}" target='_blank'><i class="fa fa-fw fa-eye"></i> Ver</a>
                        </div>
                        @endif
                        @if(Empty($scancelacione->fileotros1)) 
                            @else 
                        <div class="form-group">
                            <strong>Fileotros1:</strong>
                            <a class="btn btn-sm btn-info" href="{{ asset('storage/'.$scancelacione->fileotros1) }}" target='_blank'><i class="fa fa-fw fa-eye"></i> Ver</a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


@if($observaciones->isEmpty()) 
    

    @else   

    <section class=" container centre">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title" id="card_title" style="color:#fff"><strong>Observaciones</strong></span>
                    </div>
                    <div class="card-body">
                       
                        <div class="box box-info padding-1">
                            <div class="box-body">
                              
                            <table class="table table-striped table-hover">
                            <thead class="thead">
                                <tr>
                                    
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                    <th>Usuario</th>
                                    <th>Observacion</th>

                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($observaciones as $observacione)
                                    <tr>
                                        
                                        
                                        <td>{{ $observacione->created_at }}</td>
                                        <td>{{ $observacione->estado->nombreestado }}</td>
                                        <td>{{ $observacione->name }}</td>
                                        <td>{{ $observacione->observacion }}</td> 
                                        <td></td> 
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    @endif


    <section class=" container centre">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title" id="card_title" style="color:#fff"><strong>Registrar Observacion</strong></span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('observaciones.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                        <div class="box box-info padding-1">
                            <div class="box-body">
                              
                            @if(Auth::check() )
                                <div class="form-group" >
                                    @if(( Auth::user()->perfil) == 'admin')
                                        {{ Form::label('estadotramite') }}
                                        {{ Form::select('ob_idestado', $estados, $scancelacione->estadotramite, ['class' => 'form-control' . ($errors->has('estadotramite') ? ' is-invalid' : ''), 'placeholder' => 'Estadotramite']) }}
                                    
                                        @else
                                            {{ Form::hidden('estadotramite', '1')}}
                                    @endif
                                </div>
                            @endif

                                <div class="form-group">
                                    {{ Form::label('observacion') }}
                                    {{ Form::text('observacion', '',['class' => 'form-control' . ($errors->has('') ? ' is-invalid' : ''), 'placeholder' => 'observacion']) }}
                                  
                                </div>
                                <div class="form-group">
                                    {{ Form::hidden('ob_idcancelaciones',$scancelacione->id) }}                                
                                </div>
                              
                                <div class="form-group">
                                        @if((Auth::user()->id)>0)
                                            {{ Form::hidden('u_idusuarios', Auth::user()->id) }}
                                        @endif
                                </div>

                            </div>
                            <div class="box-footer mt20">
                                <button type="submit" class="btn btn-primary">Grabar</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <br><br>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
@endsection

