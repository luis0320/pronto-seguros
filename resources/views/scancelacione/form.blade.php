<div class="box box-info padding-1">
    <div class="box-body">
       <div class="form-group">
            {{ Form::label('Motivo de su cancelacion') }}
            {{ Form::select('mc_idmcancelaciones',$motivcancelaciones, $scancelacione->mc_idmcancelaciones, ['class' => 'form-control' . ($errors->has('mc_idmcancelaciones') ? ' is-invalid' : ''), 'placeholder' => '']) }}
            {!! $errors->first('mc_idmcancelaciones', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('fechaacancelar') }}
            {{ Form::date('fechaacancelar', $scancelacione->fechaacancelar, ['class' => 'form-control' . ($errors->has('fechaacancelar') ? ' is-invalid' : ''), 'placeholder' => 'Fechaacancelar']) }}
            {!! $errors->first('fechaacancelar', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('celular') }}
            {{ Form::text('celular', $scancelacione->celular, ['class' => 'form-control' . ($errors->has('celular') ? ' is-invalid' : ''), 'placeholder' => 'Celular']) }}
            {!! $errors->first('celular', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('email') }}
            {{ Form::text('email', $scancelacione->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'email']) }}
            {!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('placa') }}
            {{ Form::text('placa', $scancelacione->placa, ['class' => 'form-control' . ($errors->has('placa') ? ' is-invalid' : ''), 'placeholder' => 'Opcional']) }}
            {!! $errors->first('placa', '<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            {{ Form::label('Aseguradora') }}
            {{ Form::select('c_idcompanias', $companias, $scancelacione->c_idcompanias, ['class' => 'form-control' . ($errors->has('c_idcompanias') ? ' is-invalid' : ''), 'placeholder' => 'Elegir']) }}
            {!! $errors->first('c_idcompanias', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Forma de pago') }}
            {{ Form::select('f_idfinancieras', $financieras, $scancelacione->f_idfinancieras, ['class' => 'form-control' . ($errors->has('f_idfinancieras') ? ' is-invalid' : ''), 'placeholder' => 'Elegir como realizo el pago']) }}
            {!! $errors->first('f_idfinancieras', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        @if(Auth::check())
        <div class="form-group">
            @if((   Auth::user()->id)>0)
                {{ Form::hidden('u_idusuarios', Auth::user()->id) }}
            @endif
        </div>
        @endif
        <div class="form-group">
            {{ Form::label('u_idusuarios') }}
            {{ Form::select('u_idusuarios', $beneficiarios, $scancelacione->b_idbeneficiarios, ['class' => 'form-control' . ($errors->has('b_idbeneficiarios') ? ' is-invalid' : ''), 'placeholder' => 'B Idbeneficiarios']) }}
            {!! $errors->first('u_idusuarios', '<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            {{ Form::label('Su poliza tiene Beneficiario?') }}
            {{ Form::select('b_idbeneficiarios', $beneficiarios, $scancelacione->b_idbeneficiarios, ['class' => 'form-control' . ($errors->has('b_idbeneficiarios') ? ' is-invalid' : ''), 'placeholder' => 'B Idbeneficiarios']) }}
            {!! $errors->first('b_idbeneficiarios', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('observacion') }}
            {{ Form::text('observacion', $scancelacione->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observacion']) }}
            {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('filecarta') }}
            {{ Form::text('filecarta', $scancelacione->filecarta, ['class' => 'form-control' . ($errors->has('filecarta') ? ' is-invalid' : ''), 'placeholder' => 'Filecarta']) }}
            {!! $errors->first('filecarta', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('filelevantamiento') }}
            {{ Form::text('filelevantamiento', $scancelacione->filelevantamiento, ['class' => 'form-control' . ($errors->has('filelevantamiento') ? ' is-invalid' : ''), 'placeholder' => 'Filelevantamiento']) }}
            {!! $errors->first('filelevantamiento', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('filedevolucion') }}
            {{ Form::text('filedevolucion', $scancelacione->filedevolucion, ['class' => 'form-control' . ($errors->has('filedevolucion') ? ' is-invalid' : ''), 'placeholder' => 'Filedevolucion']) }}
            {!! $errors->first('filedevolucion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('filecedula') }}
            {{ Form::text('filecedula', $scancelacione->filecedula, ['class' => 'form-control' . ($errors->has('filecedula') ? ' is-invalid' : ''), 'placeholder' => 'Filecedula']) }}
            {!! $errors->first('filecedula', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('filetarjetadepropiedad') }}
            {{ Form::text('filetarjetadepropiedad', $scancelacione->filetarjetadepropiedad, ['class' => 'form-control' . ($errors->has('filetarjetadepropiedad') ? ' is-invalid' : ''), 'placeholder' => 'Filetarjetadepropiedad']) }}
            {!! $errors->first('filetarjetadepropiedad', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('filesarlaft') }}
            {{ Form::text('filesarlaft', $scancelacione->filesarlaft, ['class' => 'form-control' . ($errors->has('filesarlaft') ? ' is-invalid' : ''), 'placeholder' => 'Filesarlaft']) }}
            {!! $errors->first('filesarlaft', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('filepoliza') }}
            {{ Form::text('filepoliza', $scancelacione->filepoliza, ['class' => 'form-control' . ($errors->has('filepoliza') ? ' is-invalid' : ''), 'placeholder' => 'Filepoliza']) }}
            {!! $errors->first('filepoliza', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('fileotros') }}
            {{ Form::text('fileotros', $scancelacione->fileotros, ['class' => 'form-control' . ($errors->has('fileotros') ? ' is-invalid' : ''), 'placeholder' => 'Fileotros']) }}
            {!! $errors->first('fileotros', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('fileotros1') }}
            {{ Form::text('fileotros1', $scancelacione->fileotros1, ['class' => 'form-control' . ($errors->has('fileotros1') ? ' is-invalid' : ''), 'placeholder' => 'Fileotros1']) }}
            {!! $errors->first('fileotros1', '<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group" >
            {{ Form::label('estadotramite') }}
            {{ Form::select('estadotramite', $estados, $scancelacione->estadotramite, ['class' => 'form-control' . ($errors->has('estadotramite') ? ' is-invalid' : ''), 'placeholder' => 'Estadotramite']) }}
            {!! $errors->first('estadotramite', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>