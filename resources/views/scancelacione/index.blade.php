@extends('layouts.app')
@extends('layouts.masteradmin')

@section('template_title')
    Scancelacione
@endsection



@section('content')



<div id="layoutSidenav_content">
    <div class="container-fluid">

    @if(  (Auth::user()->perfil) == 'admin')
        <form method="POST" action="{{ route('consulta') }}"  role="form" enctype="multipart/form-data">  
            @csrf  
    
            <div class="form-row">
                <div class="col-md-6">
                    <div class="form-group">
                        
                    <input class="form-control" id="placa" type="text"  value="" placeholder="placa" name="placa"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        
                        <button type="submit" class="btn btn-primary">Buscar</button> 
                    </div>
                </div>
            </div>

        </form>
        @endif

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
                
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title" style="color:#fff">
                                <i class="fas fa-table mr-1"></i><strong>   {{ __('CANCELACIONES') }} </strong>
                            </span>

                            <div class="float-right">
                                <a href="{{ route('scancelaciones.create') }}" class="btn btn-info btn-sm float-right"  data-placement="left">
                                  {{ __('Nueva solicitud') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th> 
                                        <th>Fecha solicitud</th>
										<th>Celular</th>
										<th>Placa</th>
										<th>Motivo cancelacion</th>
										<th>Compañia</th>
										<th>Estado tramite</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($scancelaciones as $scancelacione)
                                        @if(  (Auth::user()->perfil) == 'admin')
                                            <tr> 
                                                <td>{{ $scancelacione->id }}</td>
                                                <td>{{ $scancelacione->created_at }}</td>
                                                <td>{{ $scancelacione->celular }}</td>
                                                <td>{{ $scancelacione->placa }}</td>
                                                <td>{{ $scancelacione->motivcancelacione->motivnombre }}</td>
                                                <td>{{ $scancelacione->compania->nombre }}</td>
                                                <td>{{ $scancelacione->estado->nombreestado }}</td>
                                                <td>
                                                    <a class="btn btn-sm btn-primary " href="{{ route('scancelaciones.show',$scancelacione->id) }}"><i class="fa fa-fw fa-eye"></i> Ver</a>
                                                </td>
                                            </tr>                                                                                   
                                        @endif

                                        @if(((Auth::user()->perfil) == 'cliente') &&  ((Auth::user()->id) == $scancelacione->u_idusuarios))
                                        
                                            <tr>
                                                <td>{{ $scancelacione->id }}</td>
                                                <td>{{ $scancelacione->created_at }}</td>
                                                <td>{{ $scancelacione->celular }}</td>
                                                <td>{{ $scancelacione->placa }}</td>
                                                <td>{{ $scancelacione->motivcancelacione->motivnombre }}</td>
                                                <td>{{ $scancelacione->compania->nombre }}</td>
                                                <td>{{ $scancelacione->estado->nombreestado }}</td>
                                                <td>
                                                    <a class="btn btn-sm btn-primary " href="{{ route('scancelaciones.show',$scancelacione->id) }}"><i class="fa fa-fw fa-eye"></i> Ver</a>
                                                </td>
                                                
                                            </tr>   
                                                                                                                        
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
                {!! $scancelaciones->links() !!}
            </div>
        </div>
    </div>
</div>   

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
@endsection
