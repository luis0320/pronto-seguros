@include('layouts.masteradmin')
@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<head>

    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <body>
        
    <div class="container">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link type="text/css" href="css/styles.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">

	function docbeneficiario(id) {
		if (id != "1") {
			$("#enviobene").show();
		}
		if (id == "1") {
			$("#enviobene").hide();
		}
        if (id != "1") {
			$("#enviobene1").show();
		}
		if (id == "1") {
			$("#enviobene1").hide();
		}
	}
	</script>
</head>

    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">

            <!--div class="card mb-4">
            <div class="card-body">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>.</div>
            </div-->
                        
                <div class="card-body">
                    <div id="layoutAuthentication">
                        <div id="layoutAuthentication_content">
                            <main>
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-12">
                                            <div class="card shadow-lg border-0 rounded-lg mt-6">
                                                <div class="card-body">
                                                    <div class="card-body">
                                                    
                                                    <ol class="breadcrumb"><label style="color: #1c75bb; "><strong>CREAR SOLICITUD DE CANCELACION    </strong></label></ol>
                                                        <form method="POST" action="{{ route('scancelaciones.store') }}"  role="form" enctype="multipart/form-data">
                                                            @csrf
                                                        <div class="form-row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="small mb-1" for="type">Motivo de cancelacion</label>
                                                                    {{ Form::select('mc_idmcancelaciones',$motivcancelaciones, $scancelacione->mc_idmcancelaciones, ['class' => 'form-control', 'placeholder' => 'Motivo de su cancelacion']) }}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="small mb-1" for="type">Fecha de cancelacion</label>
                                                                    {{ Form::date('fechaacancelar', $scancelacione->fechaacancelar, [ 'class' => 'form-control' . ($errors->has('fechaacancelar') ? ' is-invalid' : '')]) }}
                                                                    {!! $errors->first('fechaacancelar', '<div class="invalid-feedback">:message</div>') !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>

                                                        <ol class="breadcrumb"><label style="color: #1c75bb; "><strong>DATOS DEL ASEGURADO</strong></label></ol>

                                                        <div class="form-row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="small mb-1" for="type">Cedula</label>
                                                                    {{ Form::text('fileotros1', $scancelacione->fileotros1, ['class' => 'form-control' . ($errors->has('fileotros1') ? ' is-invalid' : ''), 'placeholder' => 'Numero de cedula']) }}
                                                                    {!! $errors->first('placa', '<div class="invalid-feedback">:message</div>') !!}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="small mb-1" for="type">Placa</label>
                                                                    {{ Form::text('placa', $scancelacione->placa, ['class' => 'form-control' . ($errors->has('placa') ? ' is-invalid' : ''), 'placeholder' => 'Placa opcional']) }}
                                                                    {!! $errors->first('placa', '<div class="invalid-feedback">:message</div>') !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                <label class="small mb-1" for="type">Celular</label>
                                                                {{ Form::text('celular', $scancelacione->celular, ['class' => 'form-control' . ($errors->has('celular') ? ' is-invalid' : ''), 'placeholder' => 'Celular']) }}
                                                                {!! $errors->first('celular', '<div class="invalid-feedback">:message</div>') !!}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="small mb-1" for="type">Fecha de cancelacion</label>
                                                                    {{ Form::text('email', $scancelacione->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'email']) }}
                                                                    {!! $errors->first('celular', '<div class="invalid-feedback">:message</div>') !!}       
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <ol class="breadcrumb"><label style="color: #1c75bb; "><strong>DATOS DE LA POLIZA</strong></label></ol>
                                                        
                                                        <div class="form-group">
                                                            {{ Form::select('c_idcompanias', $companias, $scancelacione->c_idcompanias, ['class' => 'form-control' . ($errors->has('c_idcompanias') ? ' is-invalid' : ''), 'placeholder' => 'Aseguradora']) }}
                                                            {!! $errors->first('c_idcompanias', '<div class="invalid-feedback">:message</div>') !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::select('f_idfinancieras', $financieras, $scancelacione->f_idfinancieras, ['class' => 'form-control' . ($errors->has('f_idfinancieras') ? ' is-invalid' : ''), 'placeholder' => 'Elegir como realizo el pago de su poliza']) }}
                                                            {!! $errors->first('f_idfinancieras', '<div class="invalid-feedback">:message</div>') !!}
                                                        </div>
                                                        @if(Auth::check())
                                                        <div class="form-group">
                                                            @if((   Auth::user()->id)>0)
                                                                {{ Form::hidden('u_idusuarios', Auth::user()->id) }}
                                                            @endif
                                                        </div>
                                                        @endif
                                                        <div class="form-group">
                                                            {{ Form::select('b_idbeneficiarios', $beneficiarios, $scancelacione->b_idbeneficiarios, ['onchange'=>'docbeneficiario(this.value)','class' => 'form-control', 'placeholder' => 'Su poliza tiene Beneficiario?']) }}
                                                            {!! $errors->first('b_idbeneficiarios', '<div class="invalid-feedback">:message</div>') !!}
                                                        </div>
                                                        <ol class="breadcrumb"><label style="color: #1c75bb; "><strong>ARCHIVOS ADJUNTOS</strong></label></ol>
                                                        <div class="form-group">
                                                            <span id="card_title" style="color:#fff">
                                                                <i class="fas fa-file"></i> <label>Carta de cancelacion</label><br>
                                                            </span> 
                                                                {{ Form::file('filecarta') }}            
                                                        </div>
                                                        <div  id="enviobene" class="form-group" style="display: none;">
                                                            <span id="card_title" style="color:#fff">
                                                                <i class="fas fa-file"></i> <label >Nueva tarjeta de propiedad o paz y salvo con placa del vehiculo</label><br/>
                                                            </span> 
                                                                {{ Form::file('filelevantamiento') }}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::hidden('filedevolucion') }}
                                                            {{ Form::hidden('filedevolucion', $scancelacione->filedevolucion, ['class' => 'form-control' . ($errors->has('filedevolucion') ? ' is-invalid' : ''), 'placeholder' => 'Filedevolucion']) }}
                                                            {!! $errors->first('filedevolucion', '<div class="invalid-feedback">:message</div>') !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::hidden('filecedula') }}
                                                            {{ Form::hidden('filecedula', $scancelacione->filecedula, ['class' => 'form-control' . ($errors->has('filecedula') ? ' is-invalid' : ''), 'placeholder' => 'Filecedula']) }}
                                                            {!! $errors->first('filecedula', '<div class="invalid-feedback">:message</div>') !!}
                                                        </div>
                                                        <div class="form-group" > 
                                                            {{ Form::hidden('filetarjetadepropiedad') }}
                                                            {{ Form::hidden('filetarjetadepropiedad', $scancelacione->filetarjetadepropiedad, ['class' => 'form-control' . ($errors->has('filetarjetadepropiedad') ? ' is-invalid' : ''), 'placeholder' => 'Filetarjetadepropiedad']) }}
                                                            {!! $errors->first('filetarjetadepropiedad', '<div class="invalid-feedback">:message</div>') !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::hidden('filesarlaft') }}
                                                            {{ Form::hidden('filesarlaft', $scancelacione->filesarlaft, ['class' => 'form-control' . ($errors->has('filesarlaft') ? ' is-invalid' : ''), 'placeholder' => 'Filesarlaft']) }}
                                                            {!! $errors->first('filesarlaft', '<div class="invalid-feedback">:message</div>') !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::hidden('filepoliza') }}
                                                            {{ Form::hidden('filepoliza', $scancelacione->filepoliza, ['class' => 'form-control' . ($errors->has('filepoliza') ? ' is-invalid' : ''), 'placeholder' => 'Filepoliza']) }}
                                                            {!! $errors->first('filepoliza', '<div class="invalid-feedback">:message</div>') !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::hidden('fileotros') }}
                                                            {{ Form::hidden('fileotros', $scancelacione->fileotros, ['class' => 'form-control']) }}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::hidden('fileotros1') }}
                                                            {{ Form::hidden('fileotros1', $scancelacione->fileotros1, ['class' => 'form-control']) }}
                                                        </div>
                                                        <br>
                                                        @if(Auth::check() )
                                                        <ol class="breadcrumb"><label style="color: #1c75bb; "><strong>ESTADO DEL TRAMITE</strong></label></ol>
                                                        <div class="form-group" >
                                                            @if(( Auth::user()->perfil) == 'admin')
                                                                {{ Form::select('estadotramite', $estados, $scancelacione->estadotramite, ['class' => 'form-control' . ($errors->has('estadotramite') ? ' is-invalid' : ''), 'placeholder' => 'Estadotramite']) }}
                                                                @else
                                                                {{ Form::hidden('estadotramite', '1')}}
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            @if((Auth::user()->id)>0)
                                                                {{ Form::hidden('u_idusuarios', Auth::user()->id) }}
                                                            @endif
                                                        </div>
                                                        @endif
                                                        <div class="form-group">
                                                            {{ Form::label('observacion') }}
                                                            {{ Form::textarea('observacion', $scancelacione->observacion, ['class' => 'form-control' , 'placeholder' => 'Observacion']) }}
                                                        </div>
                                                        <div class="box-footer mt20">
                                                            <button type="submit" class="btn btn-primary">Solicitar</button>
                                                        </div>    
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                     
                                    </div>
                                </div>
                            </main>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <footer class="py-7 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>  
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/scripts.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
      
</body>
</html>
    




