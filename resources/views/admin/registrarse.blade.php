<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Pronto y Seguros</title>
        <link href="{{ asset('admin/css/styles.css') }}" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication_footer">
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                         <div class="logo" >
                            <a href="{{ url('inicio') }}"><img src="assets/images/logo.png" alt="logo" style="width:30%"></a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        @if($mensaje != '')
            <div class="row">
                <span>{{ $mensaje }}</span>
            </div>
        @endif
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header" style="color:#fff"><h3 class="text-center font-weight-light my-4">Crear una cuenta</h3></div>
                                    <div class="card-body">
                                        <form action="#" method="post" id="formregister" accept-charset="utf-8">
                                            {!! csrf_field() !!}
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="firtsname">Nombres</label>
                                                        <input class="form-control py-4" id="firtsname" type="text" placeholder="Ingrese el nombre" name="firtsname" required="required"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="lastname">Apellidos</label>
                                                        <input class="form-control py-4" id="lastname" type="text" placeholder="Ingrese el apellido" name="lastname" required="required"/>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="type">Tipo de identificación</label>
                                                        <select style="border: 2px solid #e6e6e6; height: 40px;" id="typeIdent" name="typeIdent">
                                                            <option value="cc">Cédula de Ciudadanía</option>
                                                            <option value="nit">NIT</option>
                                                            <option value="ce">Cédula de Extranjería</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="lastname">Identificación </label>
                                                        <input class="form-control py-4" id="identification" type="text" placeholder="Ingrese el numero de identificación" name="identification" required="required"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="email">Email</label>
                                                <input class="form-control py-4" id="email" type="email" aria-describedby="emailHelp" placeholder="Ingrese el Email" name="email" required="required"/>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="password">Contraseña</label>
                                                        <input class="form-control py-4" id="password" type="password" placeholder="Ingrese la contraseña" name="password" required="required"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="password-register">Confirmar Contraseña</label>
                                                        <input class="form-control py-4" id="password-register" type="password" placeholder="Confirmar la contraseña" name="password-register" required="required"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-4 mb-0">
                                                <!--a class="btn btn-primary btn-block" href="{{ url('administrador') }}">Crear la cuenta</a-->
                                                <button type="submit" class="btn btn-primary">Registrarse</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="{{ url('login') }}" style="color:#fff">Tiene una cuenta? Acceda al sistema aquí.</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; 2019 Pronto Seguros. Todos los derechos reservados.</div>
                            <!--div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div-->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="{{ asset('assets/js/javascript/jquery.blockUI.js') }}"></script>
        <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("form#formregister").on("submit", function (e) {
                e.preventDefault();
                $("button.registrarse").prop("disabled", true);
                jQuery.ajax({
                    url: "{{ route('registro-cliente') }}",
                    type: "post",
                    dataType: "json",
                    data: jQuery(this).serialize(),
                    beforeSend: function () {
                        jQuery.blockUI({ 
                            message: '', 
                            css: { 
                                border: "0",
                                background: "transparent"
                            },
                            overlayCSS:  { 
                                backgroundColor: "#fff",
                                opacity:         0.6, 
                                cursor:          "wait" 
                            },
                            baseZ: 10000
                        });
                    },
                    success: function (data) {
                        jQuery.unblockUI();
                       if(data.ok) {
                           window.location.href = "{{ url('inicio') }}";
                       } else {
                           alert(data.mensaje);
                           $("button.registrarse").prop("disabled", false);
                       }
                    }
                });
            });
        });
    </script>
    </body>
</html>
