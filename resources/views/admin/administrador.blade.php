@include('layouts.masteradmin')

<style>
.square-button {
  display: inline-block;
  width: 250px; /* Anchura fija del botón */
  height: 120px; /* Anchura fija del botón */
  padding: 40px 80px;
  border: none;
  background-color: #0078d7;
  color: #fff;
  text-align: center;
  text-decoration: none;
  font-size: 14px;
  font-weight: bold;
  transition: background-color 0.1s ease;
}

.square-button:hover {
  background-color: #005eaf;
  color: #fff;
}
.square-button:focus {
  outline: none;
  box-shadow: 0 0 0 4px #0078d7;
}
.square-button.orange {
  background-color: #ff8800;
}
.square-button.orange:focus {
  outline: none;
  box-shadow: 0 0 0 4px #ff8800;
}
.square-button.amarillo {
  background-color: #ffff33;
}
.square-button.amarillo:focus {
  outline: none;
  box-shadow: 0 0 0 4px #ffff33;
}
</style>


    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Inicio</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Bienvenid@ {{ (auth()->check()) ? auth()->user()->name : 'Login' }} </li>
                </ol>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
        </main>

        <div class="container">
            <div class="row">
                <div class="col-md-4" style="padding: 10px;">
                    <a href="scancelaciones/create" class="square-button">Solicitar Cancelacion</a>         
                </div>
                <div class="col-md-4" style="padding: 10px;"> 
                    <a href="#" class="square-button amarillo">Solicitar Renovacion</a>      
                </div>
                <div class="col-md-4" style="padding: 10px;">
                    <a href="#" class="square-button orange">Solicitar Cotizacion</a>      
                </div>
            </div>
        </div>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                        <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/scripts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/assets/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('admin/assets/demo/chart-bar-demo.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
    </body>
</html>
