 @include('layouts.masteradmin')
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Seguimiento Email</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
                            <li class="breadcrumb-item active">Seguimiento Email</li>
                        </ol>
                        <!--div class="card mb-4">
                            <div class="card-body">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>.</div>
                        </div-->
                        <div class="card mb-4">
                            <div class="card-header" style="color:#fff"></i>Seguimiento Email</div>
                            <div class="card-body">
                                <div id="layoutAuthentication">
                                    <div id="layoutAuthentication_content">
                                        <main>
                                            <div class="container">
                                                <div class="row justify-content-center">
                                                    <div class="col-lg-7">
                                                        <div class="card shadow-lg border-0 rounded-lg mt-5">
                                                            <div class="card-body">
                                                                <form action="#" method="post" id="formupdateemail" accept-charset="utf-8">
                                                                    {!! csrf_field() !!}
                                                                    <div class="form-row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="seguro">Seguro</label>
                                                                                <input class="form-control py-4" id="seguro" type="text" placeholder="{{ $envios_email->seguro }}" name="seguro" value="{{ $envios_email->seguro }}" disabled/>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="name">Nombre</label>
                                                                                <input class="form-control py-4" id="name" type="text" placeholder="{{ $envios_email->nombre }}" name="name" value="{{ $envios_email->nombre }}" disabled/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="phone">Teléfono</label>
                                                                                <input class="form-control py-4" id="phone" type="text" placeholder="{{ $envios_email->telefono }}" name="phone" value="{{ $envios_email->telefono }}" disabled/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="email">Email</label>
                                                                                <input class="form-control py-4" id="email" type="email" aria-describedby="emailHelp" value="{{ $envios_email->email }}" placeholder="{{ $envios_email->email }}" name="email" disabled/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="email">Mensaje</label>
                                                                                <input class="form-control py-4" id="mensaje" type="text" aria-describedby="emailHelp" value="{{ $envios_email->mensaje }}" placeholder="{{ $envios_email->mensaje }}" name="mensaje" required="required" disabled/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="email">Observaciones</label>
                                                                                <textarea name="observaciones" placeholder="" id="observaciones" value="{{ $envios_email->observaciones }}" required style="width: 100%;"></textarea>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    
                                                                    
                                                                    
                                                                    <div class="form-group mt-4 mb-0">
                                                                        <!--a class="btn btn-primary btn-block" href="{{ url('administrador') }}">Crear la cuenta</a-->
                                                                        <input type="hidden" name="id" value="{{ $envios_email->id }}">
                                                                        <button type="submit" class="btn btn-primary">Actualizar</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </main>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>


        
        
        <script src="{{ asset('assets/js/javascript/jquery.blockUI.js') }}"></script>
        <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("form#formupdateemail").on("submit", function (e) {
                e.preventDefault();
                $("button.registrarse").prop("disabled", true);
                jQuery.ajax({
                    url: "{{ route('actualizar-mensaje-email') }}",
                    type: "post",
                    dataType: "json",
                    data: jQuery(this).serialize(),
                    beforeSend: function () {
                        jQuery.blockUI({ 
                            message: '', 
                            css: { 
                                border: "0",
                                background: "transparent"
                            },
                            overlayCSS:  { 
                                backgroundColor: "#fff",
                                opacity:         0.6, 
                                cursor:          "wait" 
                            },
                            baseZ: 10000
                        });
                    },
                    success: function (data) {
                        jQuery.unblockUI();
                       if(data.ok) {
                           alert(data.mensaje);
                       } else {
                           alert(data.mensaje);
                           $("button.registrarse").prop("disabled", false);
                       }
                    }
                });
            });
        });
    </script>
      
    </body>
</html>
