 @include('layouts.masteradmin')
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Actualización datos</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
                            <li class="breadcrumb-item active">Actualización datos</li>
                        </ol>
                        <!--div class="card mb-4">
                            <div class="card-body">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>.</div>
                        </div-->
                        <div class="card mb-4">
                            <div class="card-header" style="color:#fff"></i>Actualización datos</div>
                            <div class="card-body">
                                <div id="layoutAuthentication">
                                    <div id="layoutAuthentication_content">
                                        <main>
                                            <div class="container">
                                                <div class="row justify-content-center">
                                                    <div class="col-lg-7">
                                                        <div class="card shadow-lg border-0 rounded-lg mt-5">
                                                            <div class="card-body">
                                                                <form action="#" method="post" id="formupdatedata" accept-charset="utf-8">
                                                                    {!! csrf_field() !!}
                                                                    <div class="form-row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <h4><label class="small mb-1" for="firtsname"><strong>Tu codigo para Referidos es:</strong></label>
                                                                                <span id="card_title" style="color:#373737"> {{ $usuario->codigo }} </span></h4>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="firtsname">Nombres</label>
                                                                                <input class="form-control py-4" id="firtsname" type="text" placeholder="{{ $usuario->razon_social }}" name="firtsname" value="{{ $usuario->razon_social }}" required="required"/>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    @php
                                                                        $tipoident = '';
                                                                        if ($usuario->tipo_ident == 'cc'){
                                                                            $tipoident='Cédula de Ciudadanía';
                                                                        }
                                                                        else if ($usuario->tipo_ident == 'nit'){
                                                                             $tipoident='Nit';
                                                                        }
                                                                        else if ($usuario->tipo_ident == 'ce'){
                                                                             $tipoident='Cédula de Extranjería';
                                                                        }
                                                                            
                                                                        
                                                                    @endphp
                                                                    <div class="form-row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="type">Tipo de identificación</label>
                                                                                <select style="border: 2px solid #e6e6e6; height: 40px;" id="typeIdent" name="typeIdent">
                                                                                    <option value="{{ $usuario->tipo_ident }}" selected>{{ $tipoident }}</option>
                                                                                    @if($usuario->tipo_ident == 'cc')
                                                                                        <option value="nit">NIT</option>
                                                                                        <option value="ce">Cédula de Extranjería</option>
                                                                                    @elseif($usuario->tipo_ident == 'nit')
                                                                                        <option value="cc">Cédula de Ciudadanía</option>
                                                                                        <option value="ce">Cédula de Extranjería</option>
                                                                                    @else
                                                                                        <option value="cc">Cédula de Ciudadanía</option>
                                                                                        <option value="nit">NIT</option>
                                                                                    @endif
                                                                                    
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="lastname">Identificación </label>
                                                                                <input class="form-control py-4" id="identification" type="text" placeholder="{{ $usuario->identificacion }}" name="identification" value="{{ $usuario->identificacion }}" required="required">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="form-row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="lastname">Teléfono </label>
                                                                                <input class="form-control py-4" id="phone" type="text" placeholder="Ingrese su teléfono" name="phone" value="{{ $usuario->telefono1 }}" required="required">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="lastname">Dirección </label>
                                                                                <input class="form-control py-4" id="adress" type="text" placeholder="Ingrese su dirección" name="adress" required="required" value="{{ $usuario->direccion }}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="small mb-1" for="email">Email</label>
                                                                        <input class="form-control py-4" id="email" type="email" aria-describedby="emailHelp" value="{{ $usuario->email }}" placeholder="{{ $usuario->email }}" name="email" required="required" disabled/>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="password">Contraseña</label>
                                                                                <input class="form-control py-4" id="password" type="password" placeholder="Ingrese la contraseña" name="password" required="required"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="small mb-1" for="password-register">Confirmar Contraseña</label>
                                                                                <input class="form-control py-4" id="password-register" type="password" placeholder="Confirmar la contraseña" name="password-register" required="required"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group mt-4 mb-0">
                                                                        <!--a class="btn btn-primary btn-block" href="{{ url('administrador') }}">Crear la cuenta</a-->
                                                                        <input type="hidden" name="email_reg" value="{{ $usuario->email }}">
                                                                        <button type="submit" class="btn btn-primary">Actualizar</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </main>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>


        
        
        <script src="{{ asset('assets/js/javascript/jquery.blockUI.js') }}"></script>
        <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("form#formupdatedata").on("submit", function (e) {
                e.preventDefault();
                $("button.registrarse").prop("disabled", true);
                jQuery.ajax({
                    url: "{{ route('actualizar-usuario') }}",
                    type: "post",
                    dataType: "json",
                    data: jQuery(this).serialize(),
                    beforeSend: function () {
                        jQuery.blockUI({ 
                            message: '', 
                            css: { 
                                border: "0",
                                background: "transparent"
                            },
                            overlayCSS:  { 
                                backgroundColor: "#fff",
                                opacity:         0.6, 
                                cursor:          "wait" 
                            },
                            baseZ: 10000
                        });
                    },
                    success: function (data) {
                        jQuery.unblockUI();
                       if(data.ok) {
                           alert(data.mensaje);
                       } else {
                           alert(data.mensaje);
                           $("button.registrarse").prop("disabled", false);
                       }
                    }
                });
            });
        });
    </script>
      
    </body>
</html>
