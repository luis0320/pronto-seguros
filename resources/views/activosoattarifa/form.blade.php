

@include('layouts.masteradmin')
@extends('layouts.app')


    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="../moduloadministrador">Inicio</a></li>
                    <li class="breadcrumb-item active">Activar o bloquear tarifa SOAT</li>
                </ol>    
                <div class="card-body">
                    <div id="layoutAuthentication">
                        <div id="layoutAuthentication_content">
                            <main>
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-20">
                                            <div class="card shadow-lg border-0 rounded-lg mt-1">
                                                <div class="card-body">
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <label class="small mb-1" for="Desde">Tarifa codigo</label>
                                                                    <input type="text" class="form-control py-4" id="tarifa" name="tarifa"   value="{{ $activosoattarifa->tarifa }}" placeholder="Tarifa" required="required" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <select  id="activ" name="activ" style="padding: 9px; width: 100%;" placeholder="Regimen" required>
                                                                    <option value="0">Activo</option>
                                                                    <option value="1">Bloqueo</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group mt-4 mb-0">
                                                                <button type="submit" class="btn btn-primary">Grabar</button>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>   
                                    </div>
                                    @if ($message = Session::get('success'))
                                        <div class="alert alert-success">
                                            <p>{{ $message }}</p>
                                        </div>
                                    @endif
                                </div>                     
                            </main>
                        </div>
                    </div>
                </div>     
            </div>   
        </main>
    </div>


