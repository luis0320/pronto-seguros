@extends('layouts.app')

@section('template_title')
    {{ $activosoattarifa->name ?? 'Show Activosoattarifa' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Activosoattarifa</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('activosoattarifas.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Tarifa:</strong>
                            {{ $activosoattarifa->tarifa }}
                        </div>
                        <div class="form-group">
                            <strong>Activ:</strong>
                            {{ $activosoattarifa->activ }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
