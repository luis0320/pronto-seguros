

@include('layouts.masteradmin')
@extends('layouts.app')

@if((Auth::user()->perfil) ==   'admin')

@section('content')
    
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="moduloadministrador">Inicio</a></li>
                            <li class="breadcrumb-item active">Polizas</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header" style="color:#fff"><i class="fas fa-table mr-1"></i>Tarifas SOAT
                            <a href="{{ route('activosoattarifas.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nueva tarifa') }}
                                </a>
                        </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead class="thead">
                                            <tr>
                                                <th>No</th>
                                                <th>Tarifa</th>
                                                <th>Activo</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($activosoattarifas as $activosoattarifa)
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                
                                                <td>{{ $activosoattarifa->tarifa }}</td>
                                                <td>{{ $activosoattarifa->activ == 0 ? 'activo' : 'bloqueo'}}</td>

                                                <td>
                                                    <form action="{{ route('activosoattarifas.destroy',$activosoattarifa->id) }}" method="POST">
                                                        <a class="btn btn-sm btn-primary " href="{{ route('activosoattarifas.show',$activosoattarifa->id) }}"><i class="fa fa-fw fa-eye"></i> Ver</a>
                                                        <a class="btn btn-sm btn-success" href="{{ route('activosoattarifas.edit',$activosoattarifa->id) }}"><i class="fa fa-fw fa-edit"></i> Editar</a>
                                                        @csrf
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>

        
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
        @endsection 
      
    </body>
</html>




@endif