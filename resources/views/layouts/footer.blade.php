
<!--footer-area start-->
    <footer class="footer-area">
        <!--footer-top-->
        <div class="footer-widgets" style="border-top: 2px solid #e00310">
            <div class="row" style="text-align: center;">
                
                    <div class="container-fluid">
                         <div class="row">
                            <div class="col-lg-4 col-sm-12 text-center" style="margin: auto;">
                            </div>
                            <div class="col-lg-4 col-sm-12 text-center" style="margin: auto;">
                                <div class="footer-widget">
                                    <h3 style="margin-bottom:0px; text-align: center;">Trabajamos con las mejores aseguradoras</h3>
                                    <!--style="font-family: 'Delius Swash Caps', cursive;"-->
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12 text-center" style="margin: auto;">
                            </div>
                          
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-lg-12 col-sm-12">
                                <div class="footer-widget" style="box-shadow:1px 1px 10px #7b7a7a">
                                    <div class="instagram-imgages" style="text-align: center;">
                                        <MARQUEE BEHAVIOR=ALTERNATE>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/sura.png" alt="sura" style="" /></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/solidaria.png" alt="estado" /></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/sbs.png" alt="sbs"/></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/previsora.png" alt="previsora"/></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/mapfre.png" alt="mapfre"/></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/liberty.png" alt="liberty"/></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/hdi.png" alt="hdi" style="width:70px" /></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/bolivar.png" alt="bolivar"/></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/axa_colpatria.png" alt="axa"/></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/Allianz.png" alt="allianz" style=""/></a>
                                            <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/colmena.png" alt="colmena" style=""/></a>
                                            <a href="#"><img src="assets/images/blog/seguros_del_estado.png" alt="seguros del estado" style=""/></a>
                                        </MARQUEE>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
            <br/>
            <div class="row" style="text-align: center;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 text-center">
                            <div class="footer-widget" id="footer">
                                <h4 style="margin-bottom:0px; text-align: center;">Medios de pago: &nbsp;&nbsp;
                                    <img src="assets/images/mastercard.png" alt="mastercard" style="width:70px" />&nbsp;&nbsp;
                                    <img src="assets/images/visa.png" alt="visa" style="width:85px" />&nbsp;&nbsp;
                                    <img src="assets/images/pse.png" alt="pse" style="width:60px"/>
                                </h4>
                                <!--style="font-family: 'Delius Swash Caps', cursive;"-->
                            </div>
                            <div class="footer-widget" id="footer_mobile">
                                <h4 style="margin-bottom:0px; text-align: center;">Medios de pago: &nbsp;&nbsp;
                                    <img src="assets/images/mastercard.png" alt="mastercard" style="width:30px" />&nbsp;&nbsp;
                                    <img src="assets/images/visa.png" alt="visa" style="width:30px" />&nbsp;&nbsp;
                                    <img src="assets/images/pse.png" alt="pse" style="width:25px"/>
                                </h4>
                                <!--style="font-family: 'Delius Swash Caps', cursive;"-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="container">
                <div class="row ">
                    <div class="col-lg-3 col-sm-4">
                        <div class="footer-widget">
                            <h4>Nuestra Oficina</h4>
                            <div class="work-hours">
                                <ul class="list-none">
                                    <li>Cra. 66a No 10-15</li>
                                    <li>Barrio Limonar</li>
                                    <li>Cali, Colombia.</li>
                                    
                                    <li></li> 
                                    <!--li  style="font-weight:800">Redes Sociales: <span></span></li>
                                    <li>  &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/prontoysegurosdelvalle" target="_blank" style="font-family:Arial Display"><img src="assets/images/facebook.png" alt="" style="width:21px; margin: -1px -22px; position: absolute" /><i class="fa fa-whatsapp"></i> &nbsp;Facebook </a></li>
                                    <li>  &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/prontoyseguros/" target="_blank" style="font-family:Arial Display"><img src="assets/images/instagram2.png" alt="" style="width:21px; margin: -1px -22px; position: absolute" /><!--i class="fa fa-whatsapp"></i>  &nbsp;Instagram </a></li-->                                    
                                </ul>
                            </div>
                        </div>
                    </div>                     
                    <div class="col-lg-6 col-sm-4">
                        <div class="footer-widget" style="text-align: center;" id="footer">
                            <h4 style="text-align: center;">Contáctenos</h4>
                            <div class="work-hours">
                                <ul class="list-none">           
                                    <li>Teléfono: (602) 485 11 05</li>
                                    <li><a href="https://api.whatsapp.com/send?phone=573128932013" target="_blank" style="font-family:Arial Display"><img src="assets/images/W5.png" alt="" style="width:21px; margin: -1px -22px; position: absolute" /><!--i class="fa fa-whatsapp"></i--> Whatsapp: +57 312 893 2013</a></li>         
                                    <li>Email: ayuda@prontoyseguros.com</li>
                                    <li><a href="{{ url('nosotros') }}"  style="font-weight:800;color:#e00310"> ¡Te acompañamos en cada momento!</a></li>
                                    <li></li>
                                </ul>
                            </div>
                            <!--div class="instagram-imgages">

                                <a href="#"><img src="assets/images/blog/sura 480x420.jpg" alt="sura" /></a>
                                <a href="#"><img src="assets/images/blog/aig 480x420.jpg" alt="aig" /></a>
                                <a href="#"><img src="assets/images/blog/allianz 480x420.jpg" alt="allianz" /></a>
                                <a href="#"><img src="assets/images/blog/axa colpatria 480x420.jpg" alt="axa" /></a>
                                <a href="#"><img src="assets/images/blog/bolivar 480x420.jpg" alt="bolivar" /></a>
                                <a href="#"><img src="assets/images/blog/estado 480x420.jpg" alt="estado" /></a>
                                <a href="#"><img src="assets/images/blog/hdi 480x420.jpg" alt="hdi" /></a>
                                <a href="#"><img src="assets/images/blog/liberty 480x420.jpg" alt="liberty" /></a>
                                <a href="#"><img src="assets/images/blog/mapfre 480x420.jpg" alt="mapfre" /></a>
                                <a href="#"><img src="assets/images/blog/mundial 480x420.jpg" alt="mapfre" /></a>
                                <a href="#"><img src="assets/images/blog/previsora 480x420.jpg" alt="previsora" /></a>
                                <a href="#"><img src="assets/images/blog/sbs 480x420.jpg" alt="sbs" /></a>
                                
                                
                            </div-->
                        </div>
                        <div class="footer-widget" style="text-align: left;" id="footer_mobile">
                            <h4 style="text-align: left;">Contáctenos</h4>
                            <div class="work-hours">
                                <ul class="list-none">
                                    
                                    <li>Teléfono: +57 (2) 485 11 05</li>
                                    <li>Celular: +57 313 68 049 88</li>
                                    <li>WhatsApp: +57 313 680 49 88</li>
                                    <li>Email: servicioalcliente@prontoyseguros.com</li>
                                    <li><a href="{{ url('nosotros') }}"  style="font-weight:800; color:#e00310">Tienes Dudas? Contáctanos!</a></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4">
                        <div class="footer-widget">
                            <h4>Horario de Atención</h4>
                            <div class="work-hours">
                                <ul class="list-none">
                                    <li>Lunes a Viernes <span>8 am - &nbsp;5 pm</span></li>
                                    <!--li>Martes <span>8 am - &nbsp;5 pm</span></li>
                                    <li>Miercoles <span>8 am - &nbsp;5 pm</span></li>
                                    <li>Jueves <span>8 am - &nbsp;5 pm</span></li>
                                    <li>Viernes <span>8 am - &nbsp;5 pm</span></li-->
                                    <li>Sábado <span>9 am - 1 pm</span></li>    
                                    <li  style="font-weight:800">Domingo <span>CERRADO</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <br/>
            </div>
        </div>
        <!--footer copyright-->
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Copyright &copy; 2020 Pronto y Seguros. Todos los derechos reservados.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--footer-area end-->
  
    <!-- Compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

    
    <!-- Minified JS library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Compiled and minified Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
    <!-- modernizr js -->
    <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jquery-1.12.0 version -->
    <script src="assets/js/vendor/jquery-1.12.0.min.js"></script>
    <!-- bootstra.min js -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- meanmenu js -->
    <script src="assets/js/jquery.meanmenu.min.js"></script>
    <!-- easing js -->
    <script src="assets/js/jquery.easing.min.js"></script>
    <!---venobox-js-->
    <script src="assets/js/venobox.min.js"></script>
    <!---slick-js-->
    <script src="assets/js/slick.min.js"></script>
    <!---waypoints-js-->
    <script src="assets/js/waypoints.js"></script>
    <!---counterup-js-->
    <script src="assets/js/jquery.counterup.min.js"></script>
    <!---isotop-js-->
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <!-- jquery-ui js -->
    <script src="assets/js/jquery-ui.min.js"></script>
    <!-- jquery.countdown js -->
    <script src="assets/js/jquery.countdown.min.js"></script>
    <!-- plugins js -->
    <script src="assets/js/plugins.js"></script>
    <!-- main js -->
    <script src="assets/js/main.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("form#formregister").on("submit", function (e) {
                e.preventDefault();
                $("button.registrarse").prop("disabled", true);
                jQuery.ajax({
                    url: "{{ route('registro-cliente') }}",
                    type: "post",
                    dataType: "json",
                    data: jQuery(this).serialize(),
                    beforeSend: function () {
                        jQuery.blockUI({ 
                            message: '', 
                            css: { 
                                border: "0",
                                background: "transparent"
                            },
                            overlayCSS:  { 
                                backgroundColor: "#fff",
                                opacity:         0.6, 
                                cursor:          "wait" 
                            },
                            baseZ: 10000
                        });
                    },
                    success: function (data) {
                        jQuery.unblockUI();
                       if(data.ok) {
                           window.location.href = '/';
                       } else {
                           alert(data.mensaje);
                           $("button.registrarse").prop("disabled", false);
                       }
                    }
                });
            });
        });
    </script>
</body>
</html>