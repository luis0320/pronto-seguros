<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pronto y seguros</title>
    <meta name="description" content="Brindamos servicios profesionales con gran experiencia y conocimiento en soluciones integrales para el manejo eﬁciente de los riesgos." />
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:title" content="Pronto y Seguros" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content=" http://www.prontoyseguros.com.co" />
    <meta property="og:image" content="https://prontoyseguros.com.co/assets/images/logo_circulo.png" />
    <meta property="og:description" content="Brindamos servicios profesionales con gran experiencia y conocimiento en soluciones integrales para el manejo eﬁciente de los riesgos" />
    <meta property="og:site_name" content="San Roque 2014 Pollos">
    <meta property="og:type" content="website" />
    <meta property="og:updated_time" content="1440432930" />
    <!--link rel="manifest" href="site.webmanifest"-->
    <link rel="apple-touch-icon" href="icon.png">

    <link  rel="icon"   href="assets/images/prontoyseguros.ico" type="prontoyseguros.ico" />
    <!-- Place favicon.ico in the root directory -->

    <!-- bootstrap v4.0.0 -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <!-- fontawesome-icons css -->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <!-- themify-icons css -->
    <link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css') }}">
    <!-- elegant css -->
    <link rel="stylesheet" href="{{ asset('assets/css/elegant.css') }}">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{ asset('assets/css/meanmenu.min.css') }}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <!-- venobox css -->
    <link rel="stylesheet" href="{{ asset('assets/css/venobox.css') }}">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css') }}">
    <!-- slick css -->
    <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">
    <!-- slick-theme css -->
    <link rel="stylesheet" href="{{ asset('assets/css/slick-theme.css') }}">
    <!-- helper css -->
    <link rel="stylesheet" href="{{ asset('assets/css/helper.css') }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('assets/style.css') }}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
    

<!-- Global site tag (gtag.js) - Google Analytics>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-224723297-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-224723297-1');
        </script>
<FIN  Global site tag (gtag.js) - Google Analytics -->

<!-- Google tag (gtag.js) -->
 <script async src="https://www.googletagmanager.com/gtag/js?id=G-MM4Q2LW25H"></script>
 <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-MM4Q2LW25H'); 
</script>


</head>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ebda17c8ee2956d73a134f6/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

<!--End of Tawk.to Script-->
 <style>
    .row:before, .row:after {display: none !important;}

    #linea::after { 
        content: "";
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        border-bottom: .125rem solid #646567;
        width: 5rem;
        margin: 0 auto;
        padding-top: 1rem;
    }

    #linea_tittle::after { 
        content: "";
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        border-bottom: .2rem solid #646567;
        width: 8rem;
        margin: 0 auto;
        padding-top: 1.5rem;
    }
    #listado{
        padding-bottom: 10px;
        list-style-image: url('assets/images/icon/check.png')
    }
        @media (max-width: 2800px) {
            #estilo_row {
                display: none;
            }
            #footer_mobile {
                display: none;
            }
            #footer {
                display: initial;
            }
            #menu_mobile {
                display: none;
            }
            #menu {
                display: initial;
            }
            #soat_venta_mobile {
                display: none;
            }
            #soat_venta {
                display: initial;
            }
            #soat_items_mobile {
                display: none;
            }
            #soat_items {
                display: initial;
            }
            #soat_mobile {
                display: none;
            }
            #soat {
                display: initial;
            }
            #banner_soat
            {
                display: initial;
            }
            #banner_trafico
            {
                display: initial;
            }
            #banner_prueba
            {
                display: initial;
            }
            #banner_portadasoat
            {
                display: initial;
            }
            #banner_portadasoat_mobile
            {
                display: initial;
            }
            #banner_soat_mobile
            {
                display: none;
            }
            #banner_riesgo
            {
                display: initial;
            }
            #banner_riesgo_mobile
            {
                display: none;
            }
            #banner_vida
            {
                display: initial;
            }
            #banner_vida_mobile
            {
                display: none;
            }
            #banner_hogar
            {
                display: initial;
            }
            #banner_hogar_mobile
            {
                display: none;
            }
            
            #empresas
            {
                display: initial;
            }
            #empresas_mobile
            {
                display: none;
            }
            #seguros
            {
                display: initial;
            }
            #seguros_cuadros
            {
                display: inherit;
                height:105px;
            }
            #seguros_cuadros_home
            {
                
                height: 242px;
            }
            #seguros_mobile
            {
                display: none;
            }
            #estilo_borde
            {
                border-right: 2px solid #7b7a7a;
            }
        }
        @media (max-width: 600px) {
            #estilo_row_mobil {
                display: initial;
            }
            #footer_mobile {
                display: initial;
            }
            #footer {
                display: none;
            }
            #menu_mobile {
                display: initial;
            }
            #menu {
                display: none;
            }
            #soat_venta_mobile {
                display: initial;
            }
            #soat_venta {
                display: none;
            }
            #soat_items_mobile {
                display: initial;
            }
            #soat_items {
                display: none;
            }
            #soat_mobile {
                display: initial;
            }
            #soat {
                display: none;
            }
            #banner_soat
            {
                display: none;
            }
            #banner_soat_mobile
            {
                display: initial;
            }
            #banner_riesgo
            {
                display: none;
            }
            #banner_riesgo_mobile
            {
                display: initial;
            }
            #banner_vida
            {
                display: none;
            }
            #banner_vida_mobile
            {
                display: initial;
            }
            #banner_hogar
            {
                display: none;
            }
            #banner_hogar_mobile
            {
                display: initial;
            }
            #empresas
            {
                display: none;
            }
            #empresas_mobile
            {
                display: initial;
            }
            #seguros
            {
                display: none;
            }
            #seguros_cuadros
            {
                display: none;
            }
            #seguros_cuadros_home
            {
                display: inherit;
                height:105px;
            }
            #seguros_mobile
            {
                display: initial;
            }
             #estilo_borde
            {
                border-right: none;
            }
        }
    </style>

<body>
  <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

    <!--header-area start-->
    <header class="header-area">
        <!--header-top-->
        <div class="header-top d-none d-sm-block">
            
            <div class="row align-items-center">
                <div class="col-xs-6 col-sm-12">
                    <div class="contact-info">
                    </div>
                </div>
                <div class="col-xs-5 col-sm-12">
                    <div class="social-icons pull-right"  style="margin-right: 30px;">
                        
                        <a href="https://www.facebook.com/prontoysegurosdelvalle" target="_blank" style="font-family:Arial"><!-- img src="assets/images/w3.png" alt="" style="width:21px; margin: -1px -22px; position: absolute" /--><i class="fa fa-facebook-square"></i> Facebook  </a>
                        <a href="https://www.instagram.com/prontoyseguros/" target="_blank" style="font-family:Arial"><!-- img src="assets/images/w3.png" alt="" style="width:21px; margin: -1px -22px; position: absolute" /--><i class="fa fa-instagram"></i> Instagram  </a>
                        <a href="https://api.whatsapp.com/send?phone=573128932013" target="_blank" style="font-family:Arial"><!-- img src="assets/images/w3.png" alt="" style="width:21px; margin: -1px -22px; position: absolute" /--><i class="fa fa-whatsapp"></i> Whatsapp</a>
                        <a href="{{ url('contactenos') }}" style="font-family:Arial"><!--img src="assets/images/facebook.png" alt="blog-image" style="width: 25px;"--><i class="fa fa-map-marker"></i> Dónde estamos</a>
                        <!--a href="https://www.facebook.com/prontoysegurosCol/" target="_blank"><img src="assets/images/facebook.png" alt="blog-image" style="width: 25px;"><!--i class="fa fa-facebook"></i-></a>
                        <a href="https://www.instagram.com/prontoyseguros/" target="_blank"><img src="assets/images/instagram.png" alt="blog-image" style="width: 50px;"><!--i class="fa fa-instagram"></i-></a-->
                    </div>
                    <!--marquee BEHAVIOR=slide width="50%" scrolldelay="50" scrollamount="5" direction="center" loop="infinite">
                        <FONT COLOR="white"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MEJOR ES ESTAR TRANQUILO.</strong> </FONT>
                    </marquee-->
                </div>
                
            </div>
            
        </div>
        
        <!--header-bottom-->
        
            <div id="sticker" class="header-bottom">
                <div class="row align-items-center flex-initial estilo_row">

                    <div class="col-lg-2 col-md-5 col-xs-12"  id="menu">
                        <div class="logo text-left">
                            <a href="{{ url('inicio') }}"><img src="assets/images/logo.png" alt="logo"></a>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-5" style="padding: 6px 9px;"  id="menu_mobile">
                        <div class="logo text-left">
                            <a href="{{ url('inicio') }}"><img src="assets/images/logo.png" alt="logo" style="width:100%; padding:0 10px;"></a>
                        </div>
                    </div>
                    
                    <div class="col-lg-7 col-md-7 col-xs-12" >
                        <div class="mainmenu text-center">
                            <nav >
                                <ul style="margin: 0 10px;">
                                    <li id="menu_mobile"><a href="{{ url('inicio') }}">Inicio</a></li>
                                     <!--li><a href="{{ url('soat') }}">Soat</a></li-->
                                    <li><a href="{{ url('todo-riesgo') }}">Todo Riesgo</a></li>
                                    <li><a href="{{ url('vida') }}">Seguro de Vida</a>
                                    <li><a href="#">Seguros</a>
                                        <ul class="mega-menu" style="font-size:14px">
                                            <li class="megamenu-single">
                                                <span class="mega-menu-title">Seguros vehículos</span>
                                                <ul>
                                                    <!--li><a href="{{ url('soat') }}" style="font-size:15px">SOAT</a></li-->
                                                    <li><a href="{{ url('todo-riesgo') }}"  style="font-size:15px">Seguro todo riesgo</a></li>
                                                    <!--li><a href="shop-filter.html">Mapfre Seguros</a></li-->
                                                </ul>
                                            </li>
                                            <li class="megamenu-single">
                                                <span class="mega-menu-title">Seguros personas</span>
                                                <ul>
                                                    <li><a href="{{ url('vida') }}" style="font-size:15px">Seguro de vida</a></li>
                                                    <li><a href="{{ url('salud') }}" style="font-size:15px">Seguro de salud</a></li>
                                                    <li><a href="{{ url('exequial') }}" style="font-size:15px">Seguro exequial</a></li>
                                                    <li><a href="{{ url('educativo') }}" style="font-size:15px">Seguro educativo</a></li>
                                                    <li><a href="{{ url('personal') }}" style="font-size:15px">Seguro accidentes personales</a></li>
                                                    <li><a href="{{ url('hogar') }}" style="font-size:15px">Seguro de hogar</a></li>
                                                    <li><a href="{{ url('mascotas') }}" style="font-size:15px">Seguro mascotas</a></li>
                                                    <!--li><a href="#" style="font-size:15px">Otros seguros</a></li-->
                                                </ul>
                                            </li>
                                            <li class="megamenu-single">
                                                <span class="mega-menu-title">Seguros empresas</span>
                                                <ul>
                                                    <li><a href="{{ url('pyme') }}" style="font-size:15px">Seguro multiriesgo-pyme</a></li>
                                                    <li><a href="{{ url('cumplimiento') }}" style="font-size:15px">Seguro de cumplimiento</a></li>
                                                    <li><a href="{{ url('maquinaria') }}" style="font-size:15px">Todo riesgo equipo y maquinaria</a></li>
                                                    <li><a href="{{ url('construccion') }}" style="font-size:15px">Todo riesgo construccion</a></li>
                                                    <li><a href="{{ url('civil') }}" style="font-size:15px">Seguro de responsabilidad civil</a></li>
                                                    <li><a href="{{ url('carga') }}" style="font-size:15px">Seguro transporte mercancías/carga</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ url('inicio') }}#pago_linea">Pagos</a></li>
                                    <li><a href="{{ url('cancelaciones') }}">Asistencias</a>
                                        <ul class="submenu">
                                            <li><a href="{{ url('cancelaciones') }}">Cancelaciones</a></li>
                                            <li><a href="{{ url('contactenos') }}">Contáctenos</a></li>
                                            <li><a href="{{ url('aseguradoras') }}">Aseguradoras</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ url('nosotros') }}">Nosotros</a>
                                        <ul class="submenu">
                                            <li><a href="{{ url('nosotros') }}">Empresa</a></li>
                                            <li><a href="{{ url('contactenos') }}">Contactenos</a></li>
                                            <!--li><a href="{{ url('nosotros') }}">Blog</a></li-->
                                        </ul>
                                    </li>
                                     @php 
                                    if(auth()->check()){
                                    @endphp
                                        <li id="menu_mobile"><a href="#" >{{ (auth()->check()) ? auth()->user()->name : 'Login' }}</a></li>
                                        <li id="menu_mobile"><a href="{{ url('logout') }}">Mi cuenta</a></li>
                                        <li id="menu_mobile"><a href="{{ url('logout') }}">Cerrar Sesión</a></li>
                                    @php 
                                        } else {
                                    @endphp
                                        <!--li id="menu_mobile"><a href="{{ (!auth()->check()) ? route('login') : '#' }}" target='_blank'>Login</a></li>
                                        <li id="menu_mobile"><a href="{{ (!auth()->check()) ? route('registrarse'): '#'  }}" target='_blank'>Registrarse</a></li-->
                                    @php 
                                        }
                                    @endphp
                                    <!--li id="menu_mobile"><a href="{{ (!auth()->check()) ? route('login') : '#' }}">Login</a></li>
                                    <li id="menu_mobile"><a href="{{ (!auth()->check()) ? route('registrarse'): '#'  }}">Registrarse</a></li-->
                                </ul>
                            </nav>
                            
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-xs-12 text-center"  id="menu">
                        @php 
                            if(auth()->check()){
                        @endphp
                            <div class="col-lg-5 col-xs-12 text-center" style="border-right: 1px solid gray; padding: 10px;height: 62px;">
                                <a href="{{ (!auth()->check()) ? route('login') : '#' }}" title="" style="color:#646567; font-family:Playfair Display;  font-size:15px;    font-weight: 800; height:62px"> {{ (auth()->check()) ? auth()->user()->name : 'Login' }}</a>
                            </div>
                            <div class="col-lg-3 col-xs-12 text-center" style="border-right: 1px solid gray; padding: 10px; font-family:Playfair Display; height:62px">
                                <a style="color:#646567; font-size:15px; font-weight: 800;" href="{{ url('moduloadministrador') }}"> Mi cuenta</a>
                            </div>
                            <div class="col-lg-4 col-xs-12 text-center" style="padding: 10px; font-family:Playfair Display">
                                <a style="color:#646567; font-size:15px; font-weight: 800;" href="{{ url('logout') }}"> Cerrar Sesión</a>
                            </div>
                        @php 
                            } else {
                        @endphp
                            <!--div class="col-lg-6 col-xs-6 text-right" style="border-right: 1px solid gray; padding: 10px;">
                                <a href="{{ (!auth()->check()) ? route('login') : '#' }}" title="" style="color:#646567; font-family:Playfair Display;  font-size:15px;    font-weight: 800;"><i class="fa fa-unlock-alt"></i> {{ (auth()->check()) ? auth()->user()->name : 'Login' }}</a>
                            </div>
                            <div class="col-lg-6 col-xs-6 text-left" style="border-left: 1px solid gray; padding: 10px; font-family:Playfair Display">
                                <a style="color:#646567;  font-size:15px;    font-weight: 800;" href="{{ url('registrarse') }}"><i class="fa fa-key"></i> Registrarse</a>
                            </div-->
                        @php 
                            }
                        @endphp
                    </div>

                </div>
            
            </div>
        
        <!--div id="sticker" class="header-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-sm-12">

                    </div>
                    <div class="col-lg-10 col-sm-12">
                        <div class="mainmenu text-right">
                            
                            <p style="font-size: 12px"> <span style="font-size: 14px; font-weight:800">Contactanos</span>&nbsp;&nbsp;&nbsp;
                                <img src="assets/images/icons/telefono.png" alt="coberturas" style="width:2%"/> 4851105 Extención 737-785 
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:4%"/>3206964550 
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:4%"/>3046126643 
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:4%"/>312 893 2013 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div-->
        <!--header-bottom-->
        
    </header>