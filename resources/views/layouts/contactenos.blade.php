  <!--contact-area start-->
    <div class="contact-area" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <br/><br/>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="contact-info">
                        <h3 style="font-size: 24px; margin-top: 0">Contáctanos</h3></br>
                        <!--h4 style="margin-top: 0">Resuelve tus dudas o cuéntanos sobre tu experiencia</h4-->
                        <div class="single-contact-info">
                            <h4><i class="fa fa-map-marker"></i>Dirección</h4>
                            <p>Cra. 66a No 10-15 Br .Limonar<br/>
                                Cali, Colombia</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-phone"></i>Teléfono</h4>
                            <p>Fijo: +602 4851105 Ext 771 - 705 - 706</p>
                            <p>Celular: 3216425482</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-envelope"></i>Email</h4>
                            <p>generales@prontoyseguros.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="contact-form style-3">
                            
                            <form action="{{ route('enviar-email') }}" method="post" id="form-contact" accept-charset="utf-8">
                            {!! csrf_field() !!}
                            <div class="row">
                                
                                <div class="col-lg-6">
                                    <input type="text" name="nombres" placeholder="Nombre" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="apellidos" placeholder="Apellido" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="telefono" placeholder="Teléfono" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="email" placeholder="Email" required/>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="asunto" placeholder="Motivo Consulta" required/>
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="mensaje" placeholder="Mensaje" required></textarea>
                                </div>
                                <div class="col-lg-12" style="text-align: right;">
                                    <button class="btn-common" id="form-submit">Enviar Mensaje</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--div class="col-lg-12 col-md-12">
                    <p>Resuelve tus dudas o cuéntanos sobre tu experiencia</p>
                </div-->
            </div>
        </div>
    </div>
    <div class="benefit-area mt-80 ">
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Promesa de Servicio</h3>
                    <p>Resuelve tus dudas o cuéntanos sobre tu experiencia. En las próximas 24 horas recibirás nuestra llamada o si quieres información inmediata </br>nos puedes contactar a nuestro e-mail, WhatsApp o asesor virtual.</P>
                </div>
            </div>
        </div>
    </div>