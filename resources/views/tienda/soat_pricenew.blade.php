 <div class="row">
    <div class="col-lg-12 col-sm-12 text-center">
        <h3 class="font-weight-bold" id="linea_tittle">Precio</h3>
    </div>
@php 
    if($descuento == 0){
@endphp
    <div class="col-lg-3 col-xs-12 text-left">
    </div>
    <div class="col-lg-6 col-xs-12 text-left"  style="background-color: #eaeaea; border-radius: 14px; padding: 25px 0px; border: 1px solid #e2032b">
        <div class="col-lg-4 col-xs-12 text-center">
            <h3 style="margin-top:10px"><span style="font-weight:bold">Antes</span></br> <span style="text-decoration: line-through;">@php print  $total; @endphp</span></h3>
        </div>
        <div class="col-lg-4 col-xs-12 text-center">
            <h3 style="margin-top:10px"><span style="font-weight:bold">Ahora</span></br> @php print  $totaldescuento; @endphp</h3>
        </div>
        <div class="col-lg-4 col-xs-12 text-center">
            <h3 style="margin-top:10px"><span style="font-weight:bold">Tu descuento</span></br> @php print  $descuento; @endphp</h3>
        </div>
    </div>
    <div class="col-lg-3 col-xs-12 text-left">
    </div>

@php 
    } else {
@endphp
    <div class="col-lg-3 col-xs-12 text-left">
    </div>
    <div class="col-lg-6 col-xs-12 text-left"  style="background-color: #eaeaea; border-radius: 14px; padding: 25px 0px; border: 1px solid #e2032b">
        <div class="col-lg-4 col-xs-12 text-center">
        </div>
        <div class="col-lg-4 col-xs-12 text-center">
            <h3 style="margin-top:10px"><span style="font-weight:bold">Ahora</span></br> @php print  $totaldescuento; @endphp</h3>
        </div>
        <div class="col-lg-4 col-xs-12 text-center">
        </div>
    </div>
    <div class="col-lg-3 col-xs-12 text-left">
    </div>
@php 
    }    
@endphp
</div>