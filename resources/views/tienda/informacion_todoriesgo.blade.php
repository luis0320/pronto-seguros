
    @include('layouts.master')

    
    <div class="benefit-area sm-mt-65">
       
        <div class="container"   style="padding: 0 20px;">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                        <div class="card-header" style="color:#fff">
                            <h3 class="text-center font-weight-light my-4" id="linea_tittle">Cotiza tu Seguro Todo Riesgo</h3>
                        </div>
                        <div class="card-body">
                            <form action="#" method="post" id="todoriesgo" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                <div class="col-md-12">
                                    <h4 class="text-center" id="linea_tittle">Datos personales</h4>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="primerNombre">Nombres</label>
                                            <input class="form-control py-4" id="primerNombre" type="text" placeholder="Ingrese el nombre" name="primerNombre" required="required"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="primerApellido">Apellidos</label>
                                            <input class="form-control py-4" id="primerApellido" type="text" placeholder="Ingrese el apellido" name="primerApellido" required="required"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="email">Email</label>
                                            <input class="form-control py-4" id="email" type="text" placeholder="Ingrese el email" name="email" required="required"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="mobile">Celular</label>
                                            <input class="form-control py-4" id="mobile" type="text" placeholder="Ingrese el número de celular" name="mobile" required="required"/>
                                        </div>
                                    </div>
                                    
                                </div>
                               
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class=" mb-1" for="tipoIdentificacion">Tipo de identificación</label><br/>
                                            <select name="tipoIdentificacion" id="tipoIdentificacion" style="border: 2px solid #e6e6e6; height: 35px; width: 100%" placeholder="Tipo de identificación"  >
                                                <option value="0">Seleccione tipo identificación</option>
                                                @foreach($identificacion as $ident)
                                                    <option value="{{$ident->codigo}}">{{ $ident->descripcion }}</option> 
                                                    
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class=" mb-1" for="identificacion">Número de Identificación</label>
                                            <input class="form-control py-4" id="identificacion" type="text" placeholder="Ingrese el número de identificación" name="identificacion" required="required"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class=" mb-1" for="genero">Género</label><br/>
                                            <select style="border: 2px solid #e6e6e6; height: 35px; width: 100%" id="genero" name="genero">
                                                <option value="0">Seleccione el género</option>
                                                <option value="1">Femenino</option>
                                                <option value="2">Masculino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class=" mb-1" for="estadoCivil">Estado Civil</label><br/>
                                            <select style="border: 2px solid #e6e6e6; height: 35px; width: 100%" id="estadoCivil" name="estadoCivil">
                                                <option value="0">Seleccione el estado civil</option>
                                            @foreach($estado_civil as $civil)
                                                <option value="{{ $civil->codigo }}">{{ $civil->descripcion }}</option> 
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class=" mb-1" for="fechaNacimiento">Fecha Nacimiento </label><br/>
                                            <input type="text" id="fechaNacimiento" name="fechaNacimiento" value="" style="border: 2px solid #e6e6e6; height: 35px; width: 100%"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="text-center" id="linea_tittle">Datos del Vehículo</h4>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="placa">Placa</label>
                                            <input style="padding: 9px; text-transform: uppercase;" class="form-control py-4" id='placa' name="placa" minlength="6" maxlength="6" placeholder="Placa del vehículo" required="required"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="esNuevo">Estado</label>
                                            <select style="border: 2px solid #e6e6e6; height: 35px; width: 100%" id="esNuevo" name="esNuevo">
                                                <option value="0">Seleccione estado de su vehículo</option>
                                                <option value="true">Nuevo</option>
                                                <option value="false">Usado</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="lastname">Marca</label>
                                            <select style="border: 2px solid #e6e6e6; height: 35px; width: 100%" id="estado" name="estado">
                                                <option value="2020">Ford</option>
                                                <option value="2019">Audi</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="modelo">Modelo</label>
                                            <select style="border: 2px solid #e6e6e6; height: 35px; width: 100%" id="modelo" name="modelo">
                                                <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option>
                                                <option value="2017">2017</option>
                                                <option value="2016">2016</option>
                                                <option value="2015">2015</option>
                                                <option value="2014">2014</option>
                                                <option value="2013">2013</option>
                                                <option value="2012">2012</option>
                                                <option value="2011">2011</option>
                                                <option value="2010">2010</option>
                                                <option value="2009">2009</option>
                                                <option value="2008">2008</option>
                                                <option value="2007">2007</option>
                                                <option value="2006">2006</option>
                                                <option value="2005">2005</option>
                                                <option value="2004">2004</option>
                                                <option value="2003">2003</option>
                                                <option value="2002">2002</option>
                                                <option value="2001">2001</option>
                                                <option value="2000">2000</option>
                                                <option value="1999">1999</option>
                                                <option value="1998">1998</option>
                                                <option value="1997">1997</option>
                                                <option value="1996">1996</option>
                                                <option value="1995">1995</option>
                                                <option value="1994">1994</option>
                                                <option value="1993">1993</option>
                                                <option value="1992">1992</option>
                                                <option value="1991">1991</option>
                                                <option value="1990">1990</option>
                                            </select>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="mb-1" for="lastname">Elija Referencia</label>
                                            <select style="border: 2px solid #e6e6e6; height: 35px; width: 100%" id="estado" name="estado">
                                                <option value="2020">xxxxx</option>
                                                <option value="2019">xxxxx</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="text-center" id="linea_tittle">Información de circulación</h4>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="firtsname">Zona</label>
                                            <select name="zona" id="zona" style="border: 2px solid #e6e6e6; height: 35px; width: 100%" placeholder="Zona"  >
                                                <option value="0">Seleccione la zona</option>
                                                @foreach($zonas as $zona)
                                                    <option value="{{$zona->codigo}}">{{ $zona->zona }}</option> 
                                                    
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-1" for="CodigoDivipola">Ciudad</label>
                                            <select name="CodigoDivipola" id="CodigoDivipola" style="border: 2px solid #e6e6e6; height: 35px; width: 100%" placeholder="Ciudad"  >
                                                <option value="0">Seleccione la ciudad</option>
                                                @foreach($ciudades as $ciudad)
                                                    <option value="{{$ciudad->id}}">{{ $ciudad->municipio }}</option> 
                                                    
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn-common" type="submit">Cotiza Ya</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">¡Compártenos tus datos y te ayudamos a adquerir tu Seguro Todo Riesgo! </h3>
                </div>
            </div>
        </div>
    </div>
      <!--contact-area start-->
    <div class="contact-area" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <br/><br/>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="contact-info">
                        <h3 style="font-size: 24px; margin-top: 0">Contáctanos</h3></br>
                        <!--h4 style="margin-top: 0">Resuelve tus dudas o cuéntanos sobre tu experiencia</h4-->
                        <div class="single-contact-info">
                            <h4><i class="fa fa-map-marker"></i>Dirección</h4>
                            <p>Cra. 66a No 10-15 Br .Limonar<br/>
                                Cali, Colombia</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-phone"></i>Teléfono</h4>
                            <p>Fijo: +602 4851105 Ext 706</p>
                            <p>Celular: 312 893 2013</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-envelope"></i>Email</h4>
                            <p>servicioalcliente@prontoyseguros.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="contact-form style-3">
                            
                            <form action="{{ route('enviar-email') }}" method="post" id="form-contact" accept-charset="utf-8">
                            {!! csrf_field() !!}
                            <div class="row">
                                
                                <div class="col-lg-6">
                                    <input type="text" name="nombres" placeholder="Nombre" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="apellidos" placeholder="Apellido" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="telefono" placeholder="Teléfono" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="email" placeholder="Email" required/>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="asunto" placeholder="Motivo Consulta" required/>
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="mensaje" placeholder="Mensaje" required></textarea>
                                </div>
                                <div class="col-lg-12" style="text-align: right;">
                                    <button class="btn-common" id="form-submit">Enviar Mensaje</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--div class="col-lg-12 col-md-12">
                    <p>Resuelve tus dudas o cuéntanos sobre tu experiencia</p>
                </div-->
            </div>
        </div>
    </div>
    <div class="benefit-area mt-80 ">
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Promesa de Servicio</h3>
                    <p>Resuelve tus dudas o cuéntanos sobre tu experiencia. En las próximas 24 horas recibirás nuestra llamada o si quieres información inmediata </br>nos puedes contactar a nuestro e-mail, WhatsApp o asesor virtual.</P>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div>
    @include('layouts.footer')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/javascript/jquery.blockUI.js') }}"></script>
    <!--script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script-->
    <script type="text/javascript">
        $(document).ready(function () {

            

            jQuery("form#todoriesgo").on("submit", function (e) {
                e.preventDefault();
                jQuery.ajax({
                    url: "{{ route('consultar_poliza') }}",
                    type: "post",
                    dataType: "json",
                    data: jQuery(this).serialize(),
                    beforeSend: function () {
                        jQuery.blockUI({ 
                            message: '', 
                            css: { 
                                border: "0",
                                background: "transparent"
                            },
                            overlayCSS:  { 
                                backgroundColor: "#fff",
                                opacity:         0.6, 
                                cursor:          "wait" 
                            },
                            baseZ: 10000
                        });
                    },
                    success: function (data) {
                        jQuery.unblockUI();
                       if(data.ok) {
                           window.location.href = "{{ url('inicio') }}";
                       } else {
                           alert(data.mensaje);
                           //$("button.registrarse").prop("disabled", false);
                       }
                    }
                });
            });

            
        });
    </script>
