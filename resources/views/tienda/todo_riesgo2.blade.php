
    <!--header-area end-->
    @include('layouts.master')
    
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-bannertodoriesgo">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-lg-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/carro.png" style="width:16%">
                                <h3> — Tu seguro todo riesgo — </h3>
                                <div style="padding: 5px;">
                                    <input class="" style="padding: 9px;width: 225px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Placa del Vehículo" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                                </div>
                                <div style="padding: 5px;">
                                    <input class="" style="padding: 9px;width: 50px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Tipo de documento" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0">
                                    <input class="" style="padding: 9px;width: 172px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Número de documento" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                                </div>
                                <div style="padding: 5px;">
                                    <input class="" style="padding: 9px;width: 225px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Número de Celular" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                                </div>
                                <div>
                                    <input type="checkbox" value="">
                                    <a>Acepta terminos y condiciones</a>
                                </div>
                                <div style="padding: 5px;">
                                    <a href="{{ url('soat') }}"  class="btn-common">Compra aquí</a>   
                                </div> 
                                </br>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
                <div class="col-xs-8 text-center" id="banner_soat_mobile"  style="background:#fff;margin: -100px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/carro.png" style="width:16%">
                        <h4> — Tu seguro todo riesgo — </h4>
                        <div style="padding: 5px;">
                            <input class="" style="padding: 9px;width: 183px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Placa del Vehículo" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                        </div>
                        <div style="padding: 5px;">
                            <input class="" style="padding: 9px;width: 50px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Tipo de documento" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0">
                            <input class="" style="padding: 9px;width: 130px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Num. Doc" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                        </div>
                        <div style="padding: 5px;">
                            <input class="" style="padding: 9px;width: 183px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Número de Celular" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                        </div>
                        <div>
                            <input type="checkbox" value="">
                            <a>Acepta terminos y condiciones</a>
                        </div>
                        <div style="padding: 5px;">
                            <a href="{{ url('soat') }}"  class="btn-common">Compra aquí</a>   
                        </div> 
                        </br>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    
    <!--div class="blog-area mt-62 sm-mt-58">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-8 offset-sm-2">
                    <div class="section-title style-2">
                        <h2>Tu seguro a un clic</h2>
                    </div>
                </div>
            </div>
            <div class="row mt-42">
                <div class="col-lg-3 col-sm-12 single-blog style-3 style-4 sm-mb-0 p-0">
                </div>
                <div class="col-lg-6 col-sm-12 single-blog style-3 style-4 sm-mb-0 p-0">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="blog-thumb">
                                <a href="#"><img src="assets/images/camioneta.png" alt="blog-image"></a>
                               
                            </div>
                        </div>
                        <div class="col-sm-6 p-0">
                            <div class="blog-desc text-center">
                                <h3 style="font-size: 30px;">Poliza todo riesgo</h3>
                                <p>Cotiza, compara y compra al mejor precio y sin salir de tu hogar. Nosotros te cuidamos </p>
                                <input class="" style="padding: 9px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Placa del Vehículo" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0">
                                <input class="" style="padding: 9px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Tipo de documento" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0">
                                <input class="" style="padding: 9px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Número de documento" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0">
                                <a href="#" class="btn-common">Cotiza</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 single-blog style-3 style-4 sm-mb-0 p-0">
                </div>
                
            </div>
        </div>
    </div-->
    <!--about-area start-->
    <!--div class="about-area mt-85 sm-mt-35" style="margin-top:5px; ">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="section-title text-center">
                        <h2 style="">Cotiza tu poliza de vehículo</h2>
                        <p>Podras generar la cotización para tu vehículo, en donde uno de nuestros asesores se comunicara <br/>
                            con usted para brindarle y darle las mejores opciones</p>
                        <a href="{{ url('cotizar') }}" class="readmore" style="font-size: 27px; color: #e2032c;">Cotizar</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div-->
    <!--about-area end-->
    <div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-12 p-0">
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingTOne">
                                        <h5 class="mb-0">
                                            <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Pague su poliza aquí
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://www.segurossura.com.co/paginas/pago-express.aspx#/Pagos" target="_blank" >
                                                        <img src="assets/images/blog/sura 480x420.jpg" alt="blog-image" style="width:100%">
                                                    </a>
                                                    <a href="https://www.segurossura.com.co/paginas/pago-express.aspx#/Pagos" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://www.allianz.co/clientes/todos-los-clientes/pagos.html" target="_blank"><img src="assets/images/blog/allianz 480x420.jpg" alt="blog-image"></a>
                                                    <a href="https://www.allianz.co/clientes/todos-los-clientes/pagos.html" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://www.arl-colpatria.co/pagosenlinea/opcionespago" target="_blank"><img src="assets/images/blog/axa colpatria 480x420.jpg" alt="blog-image"></a>
                                                    <a href="https://www.arl-colpatria.co/pagosenlinea/opcionespago" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://www.segurosbolivar.com/RecaudosElectronicos/faces/muestrapagos.jspx/pages/layout/consultUser.action" target="_blank"><img src="assets/images/blog/bolivar 480x420.jpg" alt="blog-image"></a>
                                                    <a href="https://www.segurosbolivar.com/RecaudosElectronicos/faces/muestrapagos.jspx/pages/layout/consultUser.action" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://www.segurosdelestado.com/pages/Tips" target="_blank" ><img src="assets/images/blog/estado 480x420.jpg" alt="blog-image"></a>
                                                    <a href="https://www.segurosdelestado.com/pages/Tips" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://www.hdi.com.co/pagos.aspx" target="_blank" ><img src="assets/images/blog/hdi 480x420.jpg" alt="blog-image"></a>
                                                    <a href="https://www.hdi.com.co/pagos.aspx" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                            </div>
                                            </br>  
                                            <div class="row">
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://aplicaciones.libertyseguros.co/caja_external/faces/start?_adf.ctrl-state=weoa5vevo_3" target="_blank" ><img src="assets/images/blog/liberty 480x420.jpg" alt="blog-image"></a>
                                                    <a href="https://aplicaciones.libertyseguros.co/caja_external/faces/start?_adf.ctrl-state=weoa5vevo_3" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://cotiza.mapfre.com.co/pagosWeb/vista/paginas/noFilterIniPagosPublico.jsf" target="_blank" ><img src="assets/images/blog/mapfre 480x420.jpg" alt="blog-image"></a>
                                                    <a href="https://cotiza.mapfre.com.co/pagosWeb/vista/paginas/noFilterIniPagosPublico.jsf" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://recaudos.mundialseguros.com.co/" target="_blank" ><img src="assets/images/blog/mundial 480x420.jpg" alt="blog-image"></a>
                                                    <a href="https://recaudos.mundialseguros.com.co/" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="http://200.24.63.5/portal2/previsora3/previpagos/" target="_blank" ><img src="assets/images/blog/previsora 480x420.jpg" alt="blog-image"></a>
                                                    <a href="http://200.24.63.5/portal2/previsora3/previpagos/" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    <a href="https://checkout.payulatam.com/invoice-collector/#/login/661764" target="_blank" ><img src="assets/images/blog/sbs 480x420.jpg" alt="blog-image"></a>
                                                    <a href="https://checkout.payulatam.com/invoice-collector/#/login/661764" target="_blank" class="btn-common" role="button" >
                                                        <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2 col-md-4 col-xs-6">
                                                    
                                                </div>

                                            </div>
                                            </br>  </br>  
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                                                    <p>Quieres obtener mas información de las aseguradoras da clic 
                                                    <a href="{{ url('aseguradoras') }}" role="button" >
                                                        <span class="fl-button-text"><strong> AQUI</strong></span>
                                                    </a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>  



    
    <!--blog-area end-->
    <div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-left pt-10">
                        <h2>Seguro todo riesgo: La cobertura ideal para tu carro o moto</h2>
                        <div class="col-lg-12 col-xs-12">
                            <div class="sin-service style-2 text-justify">
                                <p >Con el Seguro Todo Riesgo puede viajar con tranquilidad, proteger a su familia y a su patrimonio, ya que este seguro está pensado en acompañarle antes, durante y después de cualquier eventualidad que ocurra con su carro o moto y que pueda interrumpir su vida cotidiana o la de sus familiares. Sentirte protegido en todo momento debe ser su principal prioridad y la cobertura todo riesgo ha sido diseñada para ello.</p>
                                <!--p>En Pronto y Seguros su mejor opción en seguros, le realiza   un comparativo el cual usted puede revisar antes de contratar un seguro.</p>
                                <p>Hacer comparaciones entre las pólizas ofrecidas por distintas aseguradoras para determinar cuál ofrece las mejores opciones en coberturas y precios. </p>
                                <p>Es cierto que lo seguros son  similares  por eso  vale la  pena comparar las  pequeñas  diferencias  que valen la pena considerar</p-->
                            </div>
                        </div>
                        <!--div class="col-sm-6">
                            <div class="sin-service style-2 text-left">
                                <p>El Seguro Todo Riesgo busca proteger tu patrimonio de daños propios o daños causados a terceros, te cubre en caso de un desastre natural, hurto o daño. En Agencia y pequeños accesorios tales como: llanta estalladas, vidrios laterales estallados, remplazo de llaves, grúa por avería, entre otras.</p>
                                <p>Adquiere un Seguro Todo Riesgo, y vive tranquilo, de esta manera disminuyes el impacto económico y material que te pueda producir, una perdida o un daño.</p>
                                
                            </div>
                        </div>
                         <div class="col-sm-6">
                            <div class="sin-service style-2 text-left">
                                <h3>Perdidas parciales</h3>
                                <p>Son aquellos daños que pueda sufrir tu Auto o tu Moto, por responsabilidad del propio asegurado o de un tercero.</p>
                                <h3>Pérdida Total</h3>
                                <p>Se considerada   una   pérdida total, cuando la reparación de los daños ocasionados al Auto o a la Moto, superan el 75% de su valor asegurado  por  daños  o hurto del vehículo.</p>
                                <h3>Responsabilidad Civil</h3>
                                <p>Se encarga de cubrir los gastos por daños a terceros, ya sean materiales o a personas, provocados por el auto o la moto asegurada.</p>
                            </div>
                        </div>
                        

                    </div>
                    <div class="row mt-30">
                        <div class="section-title style-4 text-left pt-10">
                            <div class="sin-service style-2 text-left">
                                
                                <h3>QUE ES UN SEGURO TODO RIESGO VEHICULOS:  </h3>
                                <p>El seguro todo riesgo, permite la protección integral de tu vehículo ante cualquier eventualidad, este seguro ampara todos los eventos que se produzcan a razón de un accidente de cualquier tipo. La cobertura incluye todos riesgo al casco del automóvil, por eventos parciales o totales,  de daños ocasionados por accidente, adicionalmente protege los pequeños accesorios del vehículo y muchos beneficios más que posee una póliza de AUTOS TALES COMO:</p>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="sin-service style-2 text-left">
                                <h4>Coberturas:</h4>
                                <p>El primer amparo de una póliza de AUTOMOVILES ES:</p>
                                <p>Hoy en día, contar con una póliza de vehículo es definitivamente una prioridad, no solo porque en caso de un accidente la ley  hace   responder civil y penalmente por cualquier evento al   propietario  del  vehículo, por  eso  es de  gran responsabilidad  manejar un vehículo.</p>
                                <p>Por eso es mejor estar bien asegurado </p>
                                <p>El primer amparo de una póliza todo riesgo autos es el amparo de:</p>

                                <h4>Responsabilidad Civil:</h4>
                                <p>El seguro de responsabilidad civil es la cobertura   de seguro de auto, que ampara el asegurado por todos los daños o lesiones que   causas en un accidente de tránsito.<p/>
                                <p>La cobertura de responsabilidad civil, es el primer amparo de   una póliza de seguros y esta   a su vez   es la garante en el pago de los daños, lesiones o muerte de terceros afectados.<p/>
                                <p>Hay varios tipos de cobertura de responsabilidad civil de Auto:<p/>
                                <ul>
                                    <li>Cobertura de responsabilidad civil por lesiones corporales a terceros afectados, causados en accidentes de tránsito.</li>
                                    <li>Responsabilidad civil por daños a propiedad o bienes de terceros</li>
                                    <li>Responsabilidad civil por daños a propiedad o bienes de terceros</li>
                                    <li>Cobertura de responsabilidad civil por lesiones corporales a terceros afectados, causados en accidentes de tránsito.</li>
                                    <li>Lesión, Muerte y Daños a terceros: son daños materiales a cosas o daños físicos ocasionados a personas.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="sin-service style-2 text-left">
                                <h4>Coberturas al Vehículo</h4>
                                <p>Existe gran variedad de seguros   en todas las compañías que promoción cada una distintos productos</p>
                                <p>Los amparos más   relevantes son <p/>
                                <p>Pérdidas totales por daños o hurto.<p/>
                                <p>Pérdida total por daños: Esta cobertura protege al asegurado contra la pérdida total del vehículo por daños ocasionados por un accidente o actos malintencionados de terceros.<p/>
                                <p>Y la pérdida parcial protege al asegurado contra la desaparición por hurto de los accesorios o partes del vehículo de manera parcial<p/>
                                <p>Y La   pérdida total por hurto es la desaparición del bien.<p/>
                                <p>Y La total por Daños es cuando la cotización por la reparación del vehículo supera el 75% del valor asegurado, si esto ocurre la compañía declara el vehículo pérdida total por  <p/>
                                <ul>
                                    <li><span style="font-weight: 800;">Terremoto:</span> Este amparo brinda protección al asegurado contra las pérdidas o daños materiales que sufran el vehículo, durante un evento de la naturaleza y sus aliados.</li>
                                    <li><span style="font-weight: 800;">Amparo Patrimonial:</span>cubre la Responsabilidad Civil del asegurado en caso de culpa grave, por ejemplo, si comete una infracción al momento del accidente.</li>
                                    <li><span style="font-weight: 800;">Asistencia:</span>servicio proporcionado a los conductores cuyos vehículos han sufrido de una falla o problema mecánico, Vehículo de Reemplazo, Carro taller, varada, despichada, conductor elegido, cerrajería, Asistente Jurídica en sitio.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12" >
                        <div class="sin-service style-2 text-left">
                            <h4>Requisito para cotizar</h4>
                            <ul>
                                <li>Nombre propietario</li>
                                <li>Numero de Cedula</li>
                                <li>Fecha de Naciemiento</li>
                                <li>Zona de circulación</li>
                                <li>Datos del vehículo</li>
                                <li>Marca</li>
                                <li>Modelo</li>
                                <li>Línea</li>
                                <li>Placa</li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="row">                   
                        <div class="section-title style-4 text-left pt-10">
                            <h2>Contáctanos</h2>
                            <p>  
                                <img src="assets/images/icons/telefono.png" alt="coberturas" style="width:4%"/> 4851105 Extención 737-785 &nbsp;&nbsp;&nbsp;
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/>3206964550 &nbsp;&nbsp;&nbsp;
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/>3046126643 &nbsp;&nbsp;&nbsp;
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/>312 893 2013 &nbsp;&nbsp;&nbsp;
                            </p>
                        </div>
                    </div>
                </div-->
            </div>

            <div class="row">
                    <div class="col-lg-6 col-sm-12" style="padding: 0px 20px;">
                        <div class="sin-service" style="height: 270px; padding: 0 27px; background-color: #eaeaea;">
                            <img src="assets/images/salud.png" style="width:15%; margin: 12px 0 0 0;">
                            <h3 style="margin-top: 5px;">Servicios de salud</h3>
                            <p style="font-size:16px;text-align: justify;">Si en un accidente tú, las personas que van contigo o un peatón que atropellaste necesitan atención médica, el SOAT les cubre gastos como cirugías, exámenes, terapias, medicamentos y todo lo necesario para su recuperación.</p> 
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12" style="padding: 0px 20px;">
                        <div class="sin-service" style="height: 270px; padding: 0 27px; background-color: #eaeaea;">
                            <img src="assets/images/incapacidad.png" style="width:15%; margin: 12px 0 0 0; ">
                            <h3 style="margin-top: 5px;">Incapacidad </h3>
                            <p style="font-size:16px;text-align: justify;">Si luego de tener un accidente de tránsito, el médico determina alguna pérdida de capacidad laboral, tienes una indemnización de máximo 180 salarios mínimos diarios legales vigentes. Ten en cuenta que si es una incapacidad temporal, el SOAT no te cubre, sino que debes acudir a tu EPS.</p> 
                        </div>
                    </div>
                </div>
        </div>
    </div>
      <div class="row">
        <br/><br/>
    </div>
    <!--subscribe-area start-->
    <!--div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="subscribe-form">
                        <h3>Escribenos</h3>
                        <p>Le enviaremos noticias mensuales </p>
                        <input type="email" placeholder="Su Email" />
                        <button class="btn-common">Suscribase</button>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--subscribe-area end-->
    
    
    <!--google-map-->
    <script src="https://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.22&amp;key=AIzaSyChs2QWiAhnzz0a4OEhzqCXwx_qA9ST_lE"></script>
    <script>
        google.maps.event.addDomListener(window, 'load', init);
        function init() {
            var mapOptions = {
                zoom: 11,
                scrollwheel: true,
                center: new google.maps.LatLng(40.6700, -73.9400), // New York

                styles: 
                    [
                        {
                            "featureType": "all",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "saturation": 36
                                },
                                {
                                    "color": "#333333"
                                },
                                {
                                    "lightness": 40
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "visibility": "on"
                                },
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 17
                                },
                                {
                                    "weight": 1.2
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#dedede"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 29
                                },
                                {
                                    "weight": 0.2
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 18
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                },
                                {
                                    "lightness": 19
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#e9e9e9"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        }
                    ]
            };
            var mapElement = document.getElementById('googleMap');

            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6700, -73.9400),
                map: map
            });
        }
    </script>

   @include('layouts.footer')