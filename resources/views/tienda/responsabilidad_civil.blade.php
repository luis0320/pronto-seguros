
    <!--header-area end-->
    @include('layouts.master')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-civil">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/responsabilidad_civil.png" style="width:16%; margin-top: 15px;">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle" > Seguro Responsabilidad Civil </h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                    </div>
                                    <div>
                                        <input type='hidden' name='seguro' value='Responsabilidad Civil' />
                                        <input type='hidden' name='contacto' value='directorcomercial1@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;"> 
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/responsabilidad_civil.png" style="width:16%; margin-top: 15px;">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle" > Seguro Responsabilidad Civil </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                            </div>
                            <div>
                                <input type='hidden' name='seguro' value='Responsabilidad Civil' />
                                <input type='hidden' name='contacto' value='directorcomercial1@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>

    <!--Estandar-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold">Seguro responsabilidad civil</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">El seguro de responsabilidad civil es un seguro en virtud del cual una persona (tomador) traslada a otra (aseguradora) el riesgo de ser responsable civilmente por haberle causado daños a un tercero. De esta forma, este seguro entraría a operar en el evento en que el tomador causara un daño a otra persona y fuera demandado por la víctima para obtener la respectiva indemnización.</p>
                        <p style="font-size:16px;line-height: 25px;">A diario estamos expuestos a causar daños a otros en uno u otro momento. Por esto, es fundamental mitigar ese riesgo por medio de la adquisición de una póliza de responsabilidad civil, protegiéndose patrimonialmente.</p>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-left "  style="background-color: #eaeaea;">
                        <img src="assets/images/civil_dos.jpg" alt="promo"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <br/>
                    <ul class="list-basic" style="margin-top:40px" >
                        <li id="listado">Garantiza la protección del patrimonio personal ante las diferentes situaciones de riesgo por responsabilidad civil profesional.</li>
                        <li id="listado">Permite a los profesionales de la salud protegerse frente a los perjuicios que deban asumir por lesiones personales y/o muerte, ocasionadas en el ejercicio de su profesión.</li>
                        <li id="listado">Cubre además los gastos de defensa, ante una reclamación de responsabilidad civil profesional.</li>
                        
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-uno-tab" data-toggle="pill" href="#pills-uno" role="tab" aria-controls="pills-uno" aria-selected="true" style="color: #000;">Beneficios  Adicionales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Cobertura Básica</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas Adicionales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-uno" role="tabpanel" aria-labelledby="pills-uno-tab">
                    <h3 style="margin-top: 0;">Beneficios  Adicionales </h3>
                    
                    <ul class="list-basic">
                        <li>El seguro puede ser contratado de manera independiente, sin la necesidad  de la contratación obligatoria de otros seguros.</li>
                        <li>Costos y condiciones competitivas.</li>
                        <li>Elige la forma de pago: de contado o fraccionado..</li>
                        <li>Cubre la Asesoría Legal en caso de requerirse, hasta un 50% de la suma asegurada contratada..</li>
                        <li>Responsabilidad Civil por daños a terceros en sus bienes y en sus personas.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Cobertura Básica</h3>
                    
                    <ul class="list-basic">
                        <li>Cubre el pago de las indemnizaciones por daños corporales, materiales o patrimoniales causados a terceros que pudieran ser culpa del asegurado o de las personas de quien deba responder, por hechos derivados de su vida privada o profesional.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Responsabilidad civil extra contractual patronal  </li>
                        <li>Responsabilidad civil extracontractual  contratistas y subcontratistas </li>
                        <li>Responsabilidad civil extracontractual por contaminación. </li>
                        <li>Responsabilidad civil extracontractual de los parqueaderos.  </li>
                        <li>Responsabilidad civil extracontractual  vehículos propios y no propios. </li>
                        <li>Responsabilidad civil extracontractual bienes bajo cuidado tenencia y control.     </li>
                        <li>Gastos medicos </li>
                        <li>Gastos  de defensa   </li>
                        <li>Costas  del proceso.  </li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos" role="tabpanel" aria-labelledby="pills-documentos-tab">
                    <h3 style="margin-top: 0;">Documentos</h3>
                    <p dir="ltr">Conoce en las condiciones generales del Seguro Exequial, las exclusiones y más detalles de este producto.</p>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_DE _CANCELACION.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i> Condiciones generales</a>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_DE _CANCELACION.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i> Cancelación del Seguro Exequial</a>
                        </div>  
                    </div>  
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-uno-mobile-tab" data-toggle="pill" href="#pills-uno-mobile" role="tab" aria-controls="pills-uno-mobile" aria-selected="true" style="color: #000;">Beneficios  Adicionales </a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Cobertura Básica</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas Adicionales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-uno-mobile" role="tabpanel" aria-labelledby="pills-uno-mobile-tab">
                    <h3 style="margin-top: 0;">Beneficios  Adicionales </h3>
                    
                    <ul class="list-basic">
                        <li>El seguro puede ser contratado de manera independiente, sin la necesidad  de la contratación obligatoria de otros seguros.</li>
                        <li>Costos y condiciones competitivas.</li>
                        <li>Elige la forma de pago: de contado o fraccionado..</li>
                        <li>Cubre la Asesoría Legal en caso de requerirse, hasta un 50% de la suma asegurada contratada..</li>
                        <li>Responsabilidad Civil por daños a terceros en sus bienes y en sus personas.</li>
                    </ul>
                </div>
                <div class="tab-pane fade show active" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Cobertura Básica</h3>
                    
                    <ul class="list-basic">
                        <li>Cubre el pago de las indemnizaciones por daños corporales, materiales o patrimoniales causados a terceros que pudieran ser culpa del asegurado o de las personas de quien deba responder, por hechos derivados de su vida privada o profesional.</li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Responsabilidad civil extra contractual patronal  </li>
                        <li>Responsabilidad civil extracontractual  contratistas y subcontratistas </li>
                        <li>Responsabilidad civil extracontractual por contaminación. </li>
                        <li>Responsabilidad civil extracontractual de los parqueaderos.  </li>
                        <li>Responsabilidad civil extracontractual  vehículos propios y no propios. </li>
                        <li>Responsabilidad civil extracontractual bienes bajo cuidado tenencia y control.     </li>
                        <li>Gastos medicos </li>
                        <li>Gastos  de defensa   </li>
                        <li>Costas  del proceso.  </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                
            </div>
            

        </div>

    </div>
    <div class="row">
        <br/>
    </div>
    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Qué  es  la  responsabilidad civil  para  empresas  y  que cubre?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Es importante tener en cuenta que una empresa incurre en varios riesgos de diferentes tipos.</p>
                                    <p>Un cliente  que entre en una oficina o  establecimiento, puede sufrir lesiones  porque se caiga dentro  del  establecimiento u  oficina  o se  le caiga  enzima un producto.</p>
                                    <p>Existen  diferentes  riesgo  que   pueden causar  daño a los demás dentro  de una empresa  y es esta  la que  debe  responder, por esto la empresa  debe  estar cubierta con una poliza  de Responsabilidad civil.</p>
                                    <ul class="list-basic">
                                        <li>Responsabilidad  civil vehicular.</li>
                                        <li>Responsabilidad Civil Extracontractual que cubre la obligación del asegurado propietario de un vehículo , moto, vehículo  pesado, dé  cualquier  tipo que pueda causar daño  material,  lesiones o muerte que haya causado a otro(s), originados en un accidente de tránsito donde el asegurado resulte responsable.  </li>
                                        <li>Responsabilidad Civil Profesional, responde  por los  daños personales, materiales y consecuenciales que, involuntariamente, por errores u omisiones, que el  profesional haya podido causar a su clientes, o paciente  en el ejercicio de su profesión, así como los perjuicios que de ellos se pudiera causar. “El que por acción u omisión causa daño a otro, interviniendo culpa o negligencia, está obligado a reparar el daño causado”.   </li>
                                        <li>Responsabilidad civil  general, es la obligación de resarcir que surge como consecuencia del daño provocado por un incumplimiento contractual (responsabilidad contractual) o de reparar el daño que ha causado a otro con el que no existía un vínculo previo</li>
                                        <li>Responsabilidad Civil Eextracontractual (posibilidad, el asegurado  transfiere a cargo de la compañía de seguros la obligación de indemnizar los perjuicios patrimoniales que cause con motivo de determinada responsabilidad en que incurra de acuerdo con la ley y tiene como propósito el resarcimiento de la víctima.  </li>
                                        <li>Responsabilidad contractual  es  el conjunto de consecuencias jurídicas que la ley asigna a las obligaciones derivadas de un contrato, cuando se establece que por acción u omisión causa daño a otro, interviniendo culpa o negligencia, está obligado a reparar el daño causado.</p></li>
                                    </ul>       
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Qué  significa  contractual  y  extra contractual?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>RESPONSABILIDAD CIVIL  CONTRACTUAL tiene su origen de ser en el incumplimiento de una obligación derivada de un contrato entre dos o más partes.</p>
                                    <p>RESPONSABILIDAD EXTRACONTRACTUAL No parte de la existencia de un acuerdo de voluntades, sino del perjuicio que un sujeto pueda cometer a un tercero afectado.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Qué  diferencia  hay entre  Responsabilidad Civil Contractual y Extra Contractual?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>RESPONSABILIDAD CONTRACTUAL tiene su origen de ser en el incumplimiento de una obligación derivada de un contrato entre dos o más partes, mientras que la RESPONSABILIDAD EXTRACONTRACTUAL no parte de la existencia de un acuerdo de voluntades, sino del perjuicio que un sujeto ha podido cometer a otro y del que es responsable. </p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿La  Responsabilidad  civil prescribe?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Plazo de prescripción. En la responsabilidad contractual, tras la última reforma del Código Civil que entró en vigor el 7 de octubre de 2015, se produce a los 5 años. Además, la Ley 42/2015, de 5 de octubre, de reforma de la Ley 1/2000, de 7 de enero, de Enjuiciamiento Civil, modificó el plazo de prescripción, reduciéndolo de 15 a 5 años, y estableció un régimen transitorio, por el cual a las relaciones nacidas entre las fechas 7 de octubre de 2000 y el 7 de octubre de 2005 se les aplica el régimen de  prescripción de quince años. </p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Que son  daños  resarcibles?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <p>La responsabilidad contractual se encuentran limitados por aquellos que se hubieran previsto o hubieran podido preverse al tiempo de constituirse la obligación y los cuales sean consecuencia necesaria de la falta de cumplimiento. A salvo de esta limitación quedan los casos de dolo o mala fe. Por su parte, la responsabilidad extracontractual carece de límite.</p>
                                    <p>Ejemplo</p>
                                    <ul class="list-basic">
                                        <li>Responsabilidad Contractual:  Luis  y Monica realizan un contrato  de obra, por el cual Luis se obliga a reparar la fachada de la casa de. MONICA, este   hace mal su trabajo y provoca que la pared de la casa de Luis se caiga, provocándole a este unos daños materiales. Esta responsabilidad será contractual, puesto que su fundamento se encuentra en el  incumplimiento - o defectuoso cumplimiento - del contrato y tendrá 5 años para reclamar los daños.</li>
                                        <li>Responsabilidad Extracontractual: Martha  Mónica, dueña de un perro de la raza brava. salió al parque, y el perro, que estaba sin collar, muerde a   Juan.  En este caso, entre Martha  Mónica  y Juan no existe ningún contrato, sino que lo que se ha producido es una responsabilidad civil extracontractual,  genérico de no causar daño a otro.  Ante este  evento Juan, tendrá un año para reclamarle a Martha Mónica por los gastos causados ,Y Martha Mónica  deberá de indemnizar todos los gastos  en que incurra  Juan por  la  mordedura  de su mascota.</li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        Tipos  de responsabilidad  Civil
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-basic">
                                        <li>Responsabilidad  civil para  empresas</li>
                                        <li>Responsabilidad civil vehicular</li>
                                        <li>Responsabilidad civil  profesional</li>
                                        <li>Responsabilidad civil general</li>
                                        <li>Responsabilidad civil extracontractual</li>
                                        <li>Responsabilidad civil  contractual</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Cubrimos toda clase de responsabilidad para darte tranquilidad. </h3> 
                    
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    <div class="row">
        <br/><br/>
    </div>
    <!--Fin Estandar-->
    
    <!--blog-area start-->
    <!--div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <div class="blog-thumb mb-30">
                            <h2>Seguro responsabilidad civil</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-left pt-10">
                        <div class="blog-thumb mb-30">
                            <p>El seguro de responsabilidad civil es un seguro en virtud del cual una persona (tomador) traslada a otra (aseguradora) el riesgo de ser responsable civilmente por haberle causado daños a un tercero. De esta forma, este seguro entraría a operar en el evento en que el tomador causara un daño a otra persona y fuera demandado por la víctima para obtener la respectiva indemnización.</p>
                            <p>A diario estamos expuestos a causar daños a otros en uno u otro momento. Por esto, es fundamental mitigar ese riesgo por medio de la adquisición de una póliza de responsabilidad civil, protegiéndose patrimonialmente.</p></br>
                            <h3>Responsabilidad Civil Médica</h3>
                            <p>Este amparo tiene como propósito indemnizar los perjuicios ocasionados por errores u omisiones con ocasión de la prestación de un servicio médico por los cuales el asegurado sea civilmente responsable.</p>
                        </div>
                        <div class="section-title style-4 text-right pt-10">
                            <a href="{{ url('contactenos') }}"><h2>Contáctanos</h2></a>
                            <p> <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/> Viviana Melo +57 316 6825935</p>
                        </div>
                        <br/>
                    </div>
                    
                </div>
                
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>Razones para tener este seguro</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <div class="blog-thumb mb-30">
                            <img src="assets/images/civil_uno.jpg" alt="" style="border-radius:180px;width:70%"/>
                        </div>
                    </div>            
                </div> 
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="section-title style-4 text-left pt-10">
                        <div class="blog-details-text">
                            <p>Garantiza la protección del patrimonio personal ante las diferentes situaciones de riesgo por responsabilidad civil profesional.</p>
                            <p>Permite a los profesionales de la salud protegerse frente a los perjuicios que deban asumir por lesiones personales y/o muerte, ocasionadas en el ejercicio de su profesión.</p>
                            <p>Cubre además los gastos de defensa, ante una reclamación de responsabilidad civil profesional.</p>
                            
                            <h4>Coberturas - Amparos</h4>
                            <ul>
                                <li>Responsabilidad civil profesional médica</li>
                                <li>Predios, labores y operaciones del consultorio</li>
                                <li>Gastos de defensa</li>
                            </ul>
                        </div>  
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-left pt-10">
                        <h4>Responsabilidad Civil Para Directores y Administradores</h4>
                        <p>Los directivos de una organización tienen la gran responsabilidad de definir estrategias y orientar su gestión hacia el logro de los objetivos propuestos. Sin embargo, esta posición puede representar un riesgo de responsabilidad civil al tomar decisiones que puedan generar perjuicios a otros.</p>
                        <p>Esta póliza cubre los perjuicios causados a terceros, a consecuencia de acciones u omisiones, imputables a uno o varios funcionarios que desempeñen cargos como directores, gerentes, miembros de junta directiva y demás administradores en el ejercicio de sus funciones administrativas.</p>
                    </div>
                </div>
                
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>Razones para tener este seguro</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="section-title style-4 text-left pt-10">
                        <div class="blog-details-text">
                            <p>El desarrollo empresarial y administrativo trae consigo responsabilidades asociadas a la toma de decisiones. Empoderar al equipo directivo y permitirle tomar decisiones que estén dentro de su marco de accióntrae consigo responsabilidades corporativas que podrían afectar a las audiencias de interés y que no se pueden eludir.</p>
                            <p>No todas las variables se tienen en cuenta al momento de tomar una decisión de alto nivel, y esto puede acarrear consecuencias económicas que no se tenían previstas.</p>
                            <p>Tener el control de las conductas inexpertas, negligentes, inoportunas o imprudentes por parte de los directivos no siempre es posible, aun cuando se actúe de buena fe.</p>
                        </div>  
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <div class="blog-thumb mb-30">
                            <img src="assets/images/civil_dos.jpg" alt="" style="border-radius:180px;width:70%"/>
                        </div>
                    </div>            
                </div> 
            </div>
        </div>
    </div>

    <br/-->
    <!--blog-area end-->
    
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>