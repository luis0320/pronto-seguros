
    <!--header-area end-->
    @include('layouts.master')
    

    <div class="contact-area" >
        <div class="container">
            <div class="row">                        
                <div class="col-lg-12 col-md-12">
                    <div class="contact-form style-3">
                        <div class="section-title style-4 text-center pt-10">
                            <h2 id="linea_tittle">Cancelaciones</h2>
                        </div>
                        <form action="{{ route('enviar-cancelacion') }}" method="post" id="form-contact" accept-charset="utf-8">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-lg-4">
                                    <input type="text" name="motivo" placeholder="Motivo de la cancelación" required />
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="aseguradora" placeholder="Aseguradora" required />
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="fechacancelacion" placeholder="Fecha de cancelacion" required />
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="cedula"placeholder="Cedula" required />
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="asunto" placeholder="Motivo Consulta" required/>
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="nombre" placeholder="Nombre" required />
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="beneficiario" placeholder="Beneficiario" required />
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="metododepago" placeholder="Metodo de pago utilizado" required />
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="email" placeholder="Email" required />
                                </div>
                                <!--div class="col-lg-4">
                                    <input type="text" name="estado" placeholder="Estado" required />
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="fecharegistro" placeholder="Fecha registro" required />
                                </div-->
                                <div class="col-lg-12" style="text-align: right;">
                                    <button class="btn-common" id="form-submit">Enviar Cancelación</button>
                                </div>
                                
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
            
        </div>
               
               
                
    </div>
   
    <div class="row">
        <br/><br/>
    </div>
    <!--blog-area end-->
    
   @include('layouts.footer')