<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
       
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
              
  
  <style>
 @import url('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
.seminor-login-modal-body .close{
  position: relative;
  top: -45px;
  left: 10px;
 color: #1cd8ad;
 }
 .seminor-login-modal-body .close{
     opacity:0.75;
 }
 
 .seminor-login-modal-body .close:focus, .seminor-login-modal-body .close:hover {
     color: #39e8b0;
  opacity: 1;
  text-decoration: none;
  outline:0;
 }
 
 .seminor-login-modal .modal-dialog .modal-content{
     border-radius:0px;
 }
 
 /* form animation */
 .seminor-login-form .form-group {
   position: relative;
   margin-bottom: 1.5em !important;
 }
 .seminor-login-form .form-control{
  border: 0px solid #ced4da !important;
  border-bottom:1px solid #adadad !important;
  border-radius:0 !important;
 }
 .seminor-login-form .form-control:focus, .seminor-login-form .form-control:active{
  outline:none !important;
  outline-width: 0;
  border-color: #adadad !important;
  box-shadow: 0 0 0 0.2rem transparent;
 }
 *:focus {
  outline: none;
 }
 .seminor-login-form{
  padding: 2em 0 0;
 }
 
 .form-control-placeholder {
 position: absolute;
 top: 0;
 padding: 7px 0 0 13px;
 transition: all 200ms;
 opacity: 0.5;
 border-top: 0px;
 border-left: 0;
 border-right: 0;
 }
 
 .form-control:focus + .form-control-placeholder,
 .form-control:valid + .form-control-placeholder {
 font-size: 75%;
 -webkit-transform: translate3d(0, -100%, 0);
        transform: translate3d(0, -100%, 0);
 opacity: 1;
 }
 
 .container-checkbox input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
 }
 .checkmark-box {
  position: absolute;
  top: -5px;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #adadad;
 }
 .container-checkbox {
  display: block;
  position: relative;
  padding-left: 40px;
  margin-bottom: 20px;
  cursor: pointer;
  font-size: 14px;
  font-weight: bold;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  line-height: 1.1;
 }
 .container-checkbox input:checked ~ .checkmark-box:after {
  color: #fff;
 }
 .container-checkbox input:checked ~ .checkmark-box:after {
  display: block;
 }
 .container-checkbox .checkmark-box:after {
  left: 10px;
  top: 4px;
  width: 7px;
  height: 15px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
 }
 .checkmark:after, .checkmark-box:after {
  content: "";
  position: absolute;
  display: none;
 }
 .container-checkbox input:checked ~ .checkmark-box {
  background-color: #f58220;
  border: 0px solid transparent;
 }
 .btn-check-log .btn-check-login {
  font-size: 16px;
  padding: 10px 0;
 }
 button.btn-check-login:hover {
     color: #fff;
     background-color: #f58220;
     border: 2px solid #f58220;
 }
 .btn-check-login {
  color: #f58220;
  background-color: transparent;
  border: 2px solid #f58220;
  transition: all ease-in-out .3s;
 }
 .btn-check-login {
  display: inline-block;
  padding: 12px 0;
  margin-bottom: 0;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
  touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  background-image: none;
  border-radius: 0;
  width: 100%;
 }
 .forgot-pass-fau a {
     text-decoration: none !important;
     font-size: 14px;
 }
 .text-primary-fau {
     color: #1959a2;
 }
 
 .select-form-control-placeholder{
   font-size: 100%;
     padding: 7px 0 0 13px;
     margin: 0;
 }
 
.modal
{
  overflow-y: scroll;
}
  </style>
</head>

@include('layouts.master')

<!--div class="benefit-area mt-80 sm-mt-65" >
    <div class="container" style="background-color: #eaeaea;">
        <div class="row">
            <div class="col-lg-12 col-sm-12 text-center">
                <div>
                    <p>Placa:
                        @php
    print $data['car']->NumberPlate;
@endphp
    </p>
</div>
</div>
</div>
</div>
</div--> 
@php
if(($data['car']->VehicleClassMinistryName)  == 'MOTOCICLETA'){ @endphp
    <div class="text-center">
        <img class="card-img-top" src="assets/images/pruebasoat.gif" alt="Card image cap">
    </div>
    @php
}else{
    @endphp
    <div class="text-center">
        <img class="card-img-top" src="assets/images/pruebasoat1.gif" alt="Card image cap">
    </div>
    
    @php
}
@endphp


<div class="benefit-area sm-mt-65">
    <div class="container"  style="padding: 0 20px;">
        <div class="row">
            <div class="col-lg-12 col-sm-12 text-center">
                <h3>Hemos encontrado tu vehículo con placa </h3>
                <h2 class="font-weight-bold" id="linea_tittle" style="font-family: serif;">@php print $data['car']->NumberPlate; @endphp</h2>
            </div>
        </div>
        <div class="row" style="background-color: #eaeaea; border-radius: 14px; padding: 25px 0px; border: 1px solid #e2032b"">
            <div class="col-lg-12 col-xs-12 text-left">
                <p style="font-size:16px;line-height: 25px;">Por favor verifica que los datos se encuentren de manera correcta.</p>
            </div>
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Marca:</span> @php print  $data['car']->BrandName; @endphp</p>
            </div>
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Línea:</span> @php print  $data['car']->VehicleLineDescription; @endphp</p>
            </div>
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Clase:</span> @php print  $data['car']->VehicleClassMinistryName; @endphp</p>
            </div>
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Modelo:</span> @php print  $data['car']->VehicleYear; @endphp</p>
            </div>
            @php
                $cilindraje = $data['car']->CylinderCapacity;
            @endphp
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Cilindraje:</span> @php print  $cilindraje; @endphp</p>
            </div>

            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Cant Pasajeros:</span> @php print  $data['car']->PassengerCapacity; @endphp</p>
            </div>
            <div class="col-lg-4 col-xs-6 text-left ">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Servicio:</span> @php print  $data['car']->ServiceTypeName; @endphp</p>
            </div>
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Línea Vehículo:</span> @php print  $data['car']->VehicleBodyTypeName; @endphp</p>
            </div>
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Tipo Vehículo:</span> @php print  $data['car']->VehicleClassName; @endphp</p>
            </div>
        <!--div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">VIN:</span> @php print  $data['car']->Vin; @endphp </p>
            </div-->
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Número Motor:</span> @php print  $data['car']->MotorNumber; @endphp</p>
            </div>
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Número Chasis:</span> @php print  $data['car']->ChasisNumber; @endphp</p>
            </div>
            <div class="col-lg-4 col-xs-6 text-left">
                <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Tipo de combustible:</span> {{ $tipo_combustible }}</p>
            </div>
                       
            <!--div class="col-lg-12 col-sm-12 text-left" style="margin-top:20px">
                <p style="font-size:16px;line-height: 25px;">! RECUERDA ! Debes seleccionar correctamente el tipo de vehículo para que no tengas problemas con el transito, Pronto y Seguros no es responsable si eliges un tipo de vehículo diferente al tuyo.</p>
            </div-->

        </div>
        <div class="row">
            </br>
        </div>
        @php

        if($errorrunt == 1){
            if($data['car']->ServiceTypeId != 2){
        @endphp
        <div class="row">
            <div class="col-lg-12 col-sm-12 text-center">
                <h3 class="font-weight-bold" id="linea_tittle">Precio</h3>
            </div>
        </div>
        @php

    if(($data['car']->NewTariff->DiscountAmountFormatted == 0)){

        if($data['car']->FuelTypeId == 5){
            @endphp
            <div class="row">
                <div class="col-lg-3 col-xs-12 text-left">
                </div>
                <div class="col-lg-6 col-xs-12 text-left"  style="background-color: #eaeaea; border-radius: 14px; padding: 25px 0px; border: 1px solid #e2032b">
                    <div class="col-lg-4 col-xs-12 text-center">
                        <h3 style="margin-top:10px"><span style="font-weight:bold">Antes</span><br/> <span style="text-decoration: line-through;">@php print  $data['car']->NewTariff->TotalFormatted; @endphp</span></h3>
                    </div>
                    <div class="col-lg-4 col-xs-12 text-center">
                        <h3 style="margin-top:10px"><span style="font-weight:bold">Ahora</span> @php print  $resTatiffDecode->TotalWithDiscountAmountFormatted; @endphp</h3>
                    </div>
                    <div class="col-lg-4 col-xs-12 text-center">
                        <h3 style="margin-top:10px"><span style="font-weight:bold">Tu descuento</span> @php print  $resTatiffDecode->ElectricDiscountFormatted;  @endphp</h3>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12 text-left">
                </div>
            </div>
            @php
        } else {
        @endphp
        <div class="row">
            <div class="col-lg-3 col-xs-12 text-left">
            </div>
            <div class="col-lg-6 col-xs-12 text-left"  style="background-color: #eaeaea; border-radius: 14px; padding: 25px 0px; border: 1px solid #e2032b">
                <div class="col-lg-4 col-xs-12 text-center">
                    <h3 style="margin-top:10px"><span style="font-weight:bold">Antes</span><br/> <span style="text-decoration: line-through;">@php print  $data['car']->NewTariff->TotalFormatted; @endphp</span></h3>
                </div>
                <div class="col-lg-4 col-xs-12 text-center">
                    <h3 style="margin-top:10px"><span style="font-weight:bold">Ahora</span></br>{{ $resTatiffDecode->TotalWithDiscountAmountFormatted }}</h3>
                </div>
                <div class="col-lg-4 col-xs-12 text-center">
                    <h3 style="margin-top:10px"><span style="font-weight:bold">Tu descuento</span></br>{{ $resTatiffDecode->ElectricDiscountFormatted }}</h3>
                </div>
            </div>
            <div class="col-lg-3 col-xs-12 text-left">
            </div>
        </div>
        @php
                }
        @endphp

        @php
                } else {
        @endphp
        <div class="row">
            <div class="col-lg-3 col-xs-12 text-left">
            </div>
            <div class="col-lg-6 col-xs-12 text-left"  style="background-color: #eaeaea; border-radius: 14px; padding: 25px 0px; border: 1px solid #e2032b">
                <div class="col-lg-4 col-xs-12 text-center">
                </div>
                <div class="col-lg-4 col-xs-12 text-center">
                    <h3 style="margin-top:10px"><span style="font-weight:bold">Ahora</span> @php print  $data['car']->NewTariff->TotalWithDiscountAmountFormatted; @endphp</h3>
                </div>
                <div class="col-lg-4 col-xs-12 text-center">
                </div>
            </div>
            <div class="col-lg-3 col-xs-12 text-left">
            </div>
        </div>
        @php
                }
        @endphp


        <div class="row" style="text-align: center;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 text-center">
                            <div class="col-lg-12 col-sm-12 text-center">
        
                                <a href="" data-toggle="modal" data-target="#sem-login"> Requisitos para descuento.</a>
          
                             </div>
                             <div class="row">
                                  </br>   
                             </div>
                            <div class="footer-widget" id="footer">
                                <h4 style="margin-bottom:0px; text-align: center;">Medios de pago: &nbsp;&nbsp;
                                    <img src="assets/images/mastercard.png" alt="mastercard" style="width:70px" />&nbsp;&nbsp;
                                    <img src="assets/images/visa.png" alt="visa" style="width:85px" />&nbsp;&nbsp;
                                    <img src="assets/images/pse.png" alt="pse" style="width:60px"/>
                                </h4>
                                <!--style="font-family: 'Delius Swash Caps', cursive;"-->
                            </div>
                            <div class="footer-widget" id="footer_mobile">
                                <h4 style="margin-bottom:0px; text-align: center;">Medios de pago: &nbsp;&nbsp;
                                    <img src="assets/images/mastercard.png" alt="mastercard" style="width:30px" />&nbsp;&nbsp;
                                    <img src="assets/images/visa.png" alt="visa" style="width:30px" />&nbsp;&nbsp;
                                    <img src="assets/images/pse.png" alt="pse" style="width:25px"/>
                                </h4>
                                <!--style="font-family: 'Delius Swash Caps', cursive;"-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row" id="datos_personales_urbano" >
            <div class="col-lg-12 col-sm-12 text-center">
                <h3 class="font-weight-bold" id="linea_tittle">Datos personales</h3>
                <p style="font-size:16px;line-height: 25px;"></p>
            </div>
            <div class="col-lg-2 col-xs-12 text-left">
            </div>
            <div class="col-lg-8 col-xs-12 text-left">
                <!--form action="#" method="post" id="userdata" accept-charset="utf-8"-->
                <form action="{{ route('newInsurancePolicyBudget') }}" method="post" id="form-contact" accept-charset="utf-8">
                    @method('POST')
                    {!! csrf_field() !!}

                    
                    <input type="hidden" id="dueValidateDate" name="dueValidateDate" value="{{ $dueValidateDate }}">
                    <input type="hidden" id="policy" name="policy" value="{{ $data['policy']['data']->Message }}">
                    <input type="hidden" id="brandid" name="brandid" value="{{ $data['car']->BrandId }}">
                    @php
                        if($data['car']->ChasisNumber == null){
                    @endphp   
                        <input type="hidden" id="chasisnumber" name="chasisnumber" value="null">
                    @php
                        }else{
                    @endphp 
                        <input type="hidden" id="chasisnumber" name="chasisnumber" value="{{ $data['car']->ChasisNumber }}">
                    @php
                        }
                    @endphp 

                    <input type="hidden" id="cylindercapacity" name="cylindercapacity" value="{{ $cilindraje }}">
                    <input type="hidden" id="loadcapacity" name="loadcapacity" value="{{ $data['car']->LoadCapacity }}">
                    <input type="hidden" id="motornumber" name="motornumber" value="{{ $data['car']->MotorNumber }}">
                    <input type="hidden" id="numberplate" name="numberplate" value="{{ $data['car']->NumberPlate }}">
                    <input type="hidden" id="passengercapacity" name="passengercapacity" value="{{ $data['car']->PassengerCapacity }}">
                    <input type="hidden" id="servicetypeid" name="servicetypeid" value="{{ $data['car']->ServiceTypeId }}">
                    <input type="hidden" id="vin" name="vin" value="{{ $data['car']->Vin }}">
                    <input type="hidden" id="vehicleclassid" name="vehicleclassid" value="{{ $data['car']->VehicleClassId }}">
                    <input type="hidden" id="vehicleclassministryid" name="vehicleclassministryid" value="{{ $data['car']->VehicleClassMinistryId }}">
                    <input type="hidden" id="vehiclelinedescription" name="vehiclelinedescription" value="{{ $data['car']->VehicleLineDescription }}">
                    <input type="hidden" id="vehiclelineid" name="vehiclelineid" value="{{ $data['car']->VehicleLineId }}">
                    <input type="hidden" id="vehicleyear" name="vehicleyear" value="{{ $data['car']->VehicleYear }}">
                    <input type="hidden" id="vehiclebodytypeid" name="vehiclebodytypeid" value="{{ $data['car']->VehicleBodyTypeId }}">
                    <input type="hidden" id="fueltypeid" name="fueltypeid" value="{{ $data['car']->FuelTypeId }}">
                    <input type="hidden" id="codigorefe" name="codigorefe" value="{{ $last_cost->id }}">
                    <input type="hidden" id="VehicleBodyTypeName" name="VehicleBodyTypeName" value="{{ $data['car']->VehicleBodyTypeName }}">

                    <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                        <select name="documenttypeid" id="documenttypeid" style="padding: 9px; width: 100%;" placeholder="Tipo Doc" required>
                            <option value="">Tipo de documento</option>
                            @foreach($tipoDoc as $tipo)
                                <option value="{{$tipo->id}}">{{ $tipo->desc_corta }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-12 col-xs-12 form-group" id="type_document" display="none">
                    </div>
                    <!--div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="documentnumber" name="documentnumber" placeholder="Número Documento" required  maxlength="12"/>
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">

                        <input type="text" style="padding: 9px; width: 100%;" id="firstname" name="firstname" placeholder="Primer Nombre" required />
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="firstname1" name="firstname1" placeholder="Segundo Nombre"  />
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="lastname" name="lastname" placeholder="Primer Apellido"  />
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="lastname1" name="lastname1" placeholder="Segundo Apellido"   />
                    </div-->
                    @php
                        if($data['car']->FuelTypeId == 5 && $cilindraje == null){
                    @endphp 
                    <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 3px; " id="cilindraje" name="cilindraje" maxlength="4" placeholder="Cilindraje"/>
                    </div>
                    @php
                        }
                    @endphp

                    <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                        <select name="cityid" id="cityid" style="padding: 9px; width: 100%;" placeholder="Ciudad" required >
                            <option value="">Seleccione la ciudad</option>
                            @foreach($ciudades as $ciudad)
                                <option value="{{$ciudad->id}}">{{ $ciudad->municipio }}</option>

                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-12 col-xs-12 text-left" style="background-color: #eaeaea; border-radius: 14px; padding: 5px">
                        <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                            <label>Diligencia la dirección de residencia ingresando los siguientes datos:</label>
                        </div>
                        <div class="col-lg-3 col-xs-12 text-left" style="padding: 5px;">
                            <select name="dir1" id="dir1" style="padding: 9px; width: 100%;" placeholder="Regimen" required>
                                <option value="">Selecciona</option>
                                @foreach($direccion as $dir)
                                    <option value="{{$dir->abreviacion}}">{{ $dir->descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3 col-xs-12 text-left" style="padding: 5px;">
                            <input type="text" style="padding: 9px; width: 90%;" id="dir2" name="dir2" maxlength="10" placeholder="EJ: 135 NORTE"  required /> <span>#</span>
                        </div>
                        <div class="col-lg-3 col-xs-12 text-left" style="padding: 5px;">
                            <input type="text" style="padding: 9px; width: 90%;" id="dir3" name="dir3" maxlength="6" placeholder="EJ: 35B" required /><span> - </span>
                        </div>
                        <div class="col-lg-3 col-xs-12 text-left" style="padding: 5px;">
                            <input type="text" style="padding: 9px; width: 100%;" id="dir4" name="dir4" maxlength="4" placeholder="EJ: 24(Número)"/>
                        </div>
                        <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                            <input type="text" style="padding: 9px; width: 100%;" id="dir5" aria-describedby="indicaciones" maxlength="40" name="indicaciones" placeholder="Indicaciones (interior, torre, apto/casa)"  />
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="email" style="padding: 9px; width: 100%;" id="email" aria-describedby="emailHelp" name="email" placeholder="Email" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" required />
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="phone" name="phone" placeholder="Celular" maxlength="11" minlength="7" required />
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <select name="regimentypeid" id="regimentypeid" style="padding: 9px; width: 100%;" placeholder="Regimen" required>
                            <option value="">Seleccione el régimen </option>
                            <option value="1">Responsable de IVA</option>
                            <option value="2" selected>No responsable de IVA</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <select name="rutid" id="rutid" style="padding: 9px; width: 100%;" placeholder="Rut" required>
                            <option value="">Seleccione la responsabilidad RUT</option>
                            @foreach($rut as $rut)
                                @if($rut->id == 5)
                                    <option value="{{$rut->id}}" selected>{{ $rut->descripcion }}</option>
                                @else
                                    <option value="{{$rut->id}}">{{ $rut->descripcion }}</option>
                                @endif

                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                        <select name="ciiuid" id="ciiuid" style="padding: 9px; width: 100%;" placeholder="Actividad económica" required>
                            <option value="">Seleccione la actividad económica</option>
                            @foreach($ciiu as $ciiu)
                                @if($ciiu->id == 10)
                                    <option value="{{$ciiu->id}}" selected>{{ $ciiu->descripcion }}</option>
                                @else
                                    <option value="{{$ciiu->id}}">{{ $ciiu->descripcion }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="last_cost"  maxlength="12" name="last_cost" placeholder="Codigo Referido - Opcional"  />
                    </div>

                    @php
                    if($activotarifa->activ  == 1){ @endphp
                       
                        @php
                    }else{
                        @endphp
                        <div class="col-lg-12 col-xs-12 text-center" style="padding: 5px;">
                            <button class="btn-common" type="submit">Compra tú SOAT</button>
                        </div>
                        
                        @php
                    }
                    @endphp
                    
                </form>
            </div>
            <div class="col-lg-2 col-xs-12 text-left">
            </div>
        </div>
        @php

            }else{
        @endphp
        <div class="row" id="operacion">
            <div class="col-lg-3 col-xs-12 text-left">
            </div>
            <div class="col-lg-6 col-xs-12 text-left" style="background-color: #eaeaea; border-radius: 14px; padding: 25px">
                <p>Su vehículo es publico, Indícanos si tienes tarjeta de Operación Nacional</p>
                <select name="nationalOperation" style="padding: 9px; width: 100%;"  placeholder="Ciudad" id="nationalOperation" data-code="{{ $data['car']->VehicleClassMinistryId }}" data-plate="{{ $data['car']->NumberPlate }}"  data-cylinder="{{ $cilindraje }}" data-year="{{ $data['car']->VehicleYear }}" data-passenger="{{ $data['car']->PassengerCapacity }}" data-load="{{ $data['car']->LoadCapacity }}" data-total="{{$data['car']->NewTariff->TotalFormatted}}" data-totaldescuento="{{ $resTatiffDecode->TotalWithDiscountAmountFormatted }}" data-descuento="{{ $resTatiffDecode->ElectricDiscountFormatted }}">
                    <option value="0">Seleccione la clasificación</option>
                    <option value="1">Urbano</option>
                    <option value="2">Intermunicipal</option>
                </select>
            </div>
            <div class="col-lg-3 col-xs-12 text-left">
            </div>
        </div>
        @php
            }

        }else{

        @endphp
        <div class="row">
            <div class="col-lg-12 col-sm-12 text-center">
                <h3 class="font-weight-bold" id="linea_tittle">Lo sentimos no pudimos validar el Precio de su SOAT</h3>
                Contáctenos debemos cotizar directamente con la compañia. </br>
                    Teléfono: +57 (2) 485 11 05 </br>
                    Celular: +57 300 452 88 44 </br> 
                    WhatsApp: +57 313 678 59 99 </br>
                    <a href="{{ url('soat') }}">volver</a>
            </div>
        </div>
        @php
            }     
        @endphp
        
        <ddiv id="div_newprice">
        </div>
        <div class="row" id="datos_personales" style="display:none">
            <div class="col-lg-12 col-sm-12 text-center">
                <h3 class="font-weight-bold" id="linea_tittle">Datos personales</h3>
                <p style="font-size:16px;line-height: 25px;"></p>
            </div>
            <div class="col-lg-2 col-xs-12 text-left">
            </div>
            <div class="col-lg-8 col-xs-12 text-left">
                <form action="#" method="post" id="userdata" accept-charset="utf-8">
                <form action="{{ route('newInsurancePolicyBudget') }}" method="post" id="form-contact" accept-charset="utf-8">
                    @method('POST')
                    {!! csrf_field() !!}

                    <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                        <select name="documenttypeid" id="documenttypeid" style="padding: 9px; width: 100%;" placeholder="Tipo Doc" required>
                            <option value="">Tipo de documento</option>
                            @foreach($tipoDoc as $tipo)
                                <option value="{{$tipo->id}}">{{ $tipo->desc_corta }}</option>
                            @endforeach
                        </select>
                    </div>                  

                    <input type="hidden" id="dueValidateDate" name="dueValidateDate" value="{{ $dueValidateDate }}">
                    <input type="hidden" id="policy" name="policy" value="{{ $data['policy']['data']->Message }}">
                    <input type="hidden" id="brandid" name="brandid" value="{{ $data['car']->BrandId }}">
                    @php
                        if($data['car']->ChasisNumber == null){
                    @endphp   
                        <input type="hidden" id="chasisnumber" name="chasisnumber" value="null">
                    @php
                        }else{
                    @endphp 
                        <input type="hidden" id="chasisnumber" name="chasisnumber" value="{{ $data['car']->ChasisNumber }}">
                    @php
                        }
                    @endphp 
                    <input type="hidden" id="cylindercapacity" name="cylindercapacity" value="{{ $cilindraje }}">
                    <input type="hidden" id="loadcapacity" name="loadcapacity" value="{{ $data['car']->LoadCapacity }}">
                    <input type="hidden" id="motornumber" name="motornumber" value="{{ $data['car']->MotorNumber }}">
                    <input type="hidden" id="numberplate" name="numberplate" value="{{ $data['car']->NumberPlate }}">
                    <input type="hidden" id="passengercapacity" name="passengercapacity" value="{{ $data['car']->PassengerCapacity }}">
                    <input type="hidden" id="servicetypeid" name="servicetypeid" value="{{ $data['car']->ServiceTypeId }}">
                    <input type="hidden" id="vin" name="vin" value="{{ $data['car']->Vin }}">
                    <input type="hidden" id="vehicleclassid" name="vehicleclassid" value="{{ $data['car']->VehicleClassId }}">
                    <input type="hidden" id="vehicleclassministryid" name="vehicleclassministryid" value="{{ $data['car']->VehicleClassMinistryId }}">
                    <input type="hidden" id="vehiclelinedescription" name="vehiclelinedescription" value="{{ $data['car']->VehicleLineDescription }}">
                    <input type="hidden" id="vehiclelineid" name="vehiclelineid" value="{{ $data['car']->VehicleLineId }}">
                    <input type="hidden" id="vehicleyear" name="vehicleyear" value="{{ $data['car']->VehicleYear }}">
                    <input type="hidden" id="vehiclebodytypeid" name="vehiclebodytypeid" value="{{ $data['car']->VehicleBodyTypeId }}">
                    <input type="hidden" id="fueltypeid" name="fueltypeid" value="{{ $data['car']->FuelTypeId }}">
                    <input type="hidden" id="codigorefe" name="codigorefe" value="{{ $last_cost->id }}">
                    <input type="hidden" id="VehicleBodyTypeName" name="VehicleBodyTypeName" value="{{ $data['car']->VehicleBodyTypeName }}">

                    
                    <div iv class="col-lg-12 col-xs-12 form-group" id="type_document" display="none">
                    </div>
                    <!-- div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="documentnumber" name="documentnumber" placeholder="Número Documento" required/>
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">

                        <input type="text" style="padding: 9px; width: 100%;" id="firstname" name="firstname" placeholder="Primer Nombre" required />
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="firstname1" name="firstname1" placeholder="Segundo Nombre"  />
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="lastname" name="lastname" placeholder="Primer Apellido"   />
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="lastname1" name="lastname1" placeholder="Segundo Apellido"  />
                    </div-->
                    <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                        <select name="cityid" id="cityid" style="padding: 9px; width: 100%;" placeholder="Ciudad" required >
                            <option value="">Seleccione la ciudad</option>
                            @foreach($ciudades as $ciudad)
                                <option value="{{$ciudad->id}}">{{ $ciudad->municipio }}</option>

                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-12 col-xs-12 text-left" style="background-color: #eaeaea; border-radius: 14px; padding: 5px">
                        <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                            <label>Diligencia la dirección de residencia ingresando los siguientes datos:</label>
                        </div>
                        <div class="col-lg-3 col-xs-12 text-left" style="padding: 5px;">
                            <select name="dir1" id="dir1" style="padding: 9px; width: 100%;" placeholder="Regimen" required>
                                <option value="">Selecciona</option>
                                @foreach($direccion as $dir)
                                    <option value="{{$dir->abreviacion}}">{{ $dir->descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3 col-xs-12 text-left" style="padding: 5px;">
                            <input type="text" style="padding: 9px; width: 90%;" id="dir2" name="dir2" maxlength="10" placeholder="EJ: 135 NORTE" required /> <span>#</span>
                        </div>
                        <div class="col-lg-3 col-xs-12 text-left" style="padding: 5px;">
                            <input type="text" style="padding: 9px; width: 90%;" id="dir3" name="dir3" maxlength="6" placeholder="EJ: 35B" required/><span> - </span>
                        </div>
                        <div class="col-lg-3 col-xs-12 text-left" style="padding: 5px;">
                            <input type="text" style="padding: 9px; width: 100%;" id="dir4" name="dir4" maxlength="4" placeholder="EJ: 24(Número)"/>
                        </div>
                        <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                            <input type="text" style="padding: 9px; width: 100%;" id="dir5" aria-describedby="emailHelp" maxlength="40" name="dir5" placeholder="Indicaciones (interior, torre, apto/casa)"  />
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="email" style="padding: 9px; width: 100%;" id="email" aria-describedby="emailHelp" name="email" placeholder="Email" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" required/>
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="phone" name="phone" placeholder="Celular" maxlength="10" minlength="7" required/>
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <select name="regimentypeid" id="regimentypeid" style="padding: 9px; width: 100%;" placeholder="Regimen" required>
                            <option value="">Seleccione el régimen </option>
                            <option value="1">Responsable de IVA</option>
                            <option value="2" selected>No responsable de IVA</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-xs-12 text-left" style="padding: 5px;">
                        <select name="rutid" id="rutid" style="padding: 9px; width: 100%;" placeholder="Rut" required>
                            <option value="">Seleccione la responsabilidad RUT</option>
                            @foreach($rut_nacional as $rut_nacional)
                                @if($rut_nacional->id == 5)
                                    <option value="{{$rut_nacional->id}}" selected>{{ $rut_nacional->descripcion }}</option>
                                @else
                                    <option value="{{$rut_nacional->id}}">{{ $rut_nacional->descripcion }}</option>
                                @endif

                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                        <select name="ciiuid" id="ciiuid" style="padding: 9px; width: 100%;" placeholder="Actividad económica" required>
                            <option value="">Seleccione la actividad económica</option>
                            @foreach($ciiu_nacional as $ciiu_nacional)
                                @if($ciiu_nacional->id == 10)
                                    <option value="{{$ciiu_nacional->id}}" selected>{{ $ciiu_nacional->descripcion }}</option>
                                @else
                                    <option value="{{$ciiu_nacional->id}}">{{ $ciiu_nacional->descripcion }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-12 col-xs-12 text-left" style="padding: 5px;">
                        <input type="text" style="padding: 9px; width: 100%;" id="last_cost" maxlength="12" name="last_cost" placeholder="Codigo Referido - Opcional"  />
                    </div>

                    @php
                    if($activotarifa->activ == 1){ @endphp
                       
                        @php
                    }else{
                        @endphp
                        <div class="col-lg-12 col-xs-12 text-center" style="padding: 5px;">
                            <button class="btn-common" type="submit">Compra tú SOAT</button>
                        </div>
                        
                        @php
                    }
                    @endphp
                </form>
            </div>
            <div class="col-lg-2 col-xs-12 text-left">
            </div>
        </div>

        <div id="div_datauser">
        </div>
    </div>
</div>
<div class="row">
    <br/><br/>
</div>
<div class="benefit-area mt-80 sm-mt-65" >
    <div class="container" style="background-color: #eaeaea;">
        <div class="row">
            <div class="col-lg-12 col-sm-12 text-center">
                <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">¡Compártenos tus datos y te ayudamos a adquerir tu SOAT! </h3>
            </div>
        </div>
    </div>
</div>
<!--contact-area start-->
<div class="contact-area" >
    <div class="container" style="background-color: #eaeaea;">
        <div class="row">
            <br/><br/>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-4">
                <div class="contact-info">
                    <h3 style="font-size: 24px; margin-top: 0">Contáctanos</h3></br>
                    <!--h4 style="margin-top: 0">Resuelve tus dudas o cuéntanos sobre tu experiencia</h4-->
                    <div class="single-contact-info">
                        <h4><i class="fa fa-map-marker"></i>Dirección</h4>
                        <p>Cra. 66a No 10-15 Br .Limonar<br/>
                            Cali, Colombia</p>
                    </div>
                    <div class="single-contact-info">
                        <h4><i class="fa fa-phone"></i>Teléfono</h4>
                        <p>Fijo: +57 4851105 Ext 785 - 737</p>
                        <p>Celular: 3147708336 - 3046126643 - 3004783630</p>
                    </div>
                    <div class="single-contact-info">
                        <h4><i class="fa fa-envelope"></i>Email</h4>
                        <p>jefesoat@prontoyseguros.com</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-8">
                <div class="contact-form style-3">

                    <form action="{{ route('enviar-email') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                        <div class="row">

                            <div class="col-lg-6">
                                <input type="text" name="nombres" placeholder="Nombre" required/>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" name="apellidos" placeholder="Apellido" required/>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" name="telefono" placeholder="Teléfono" required/>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" name="email" placeholder="Email" required/>
                            </div>
                            <div class="col-lg-12">
                                <input type="text" name="asunto" placeholder="Motivo Consulta" required/>
                            </div>
                            <div class="col-lg-12">
                                <textarea name="mensaje" placeholder="Mensaje" required></textarea>
                            </div>
                            <div class="col-lg-12" style="text-align: right;">
                                <button class="btn-common" id="form-submit">Enviar Mensaje</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--div class="col-lg-12 col-md-12">
                <p>Resuelve tus dudas o cuéntanos sobre tu experiencia</p>
            </div-->
        </div>
    </div>
</div>
<div class="benefit-area mt-80 ">
    <div class="container" style="background-color: #eaeaea;">
        <div class="row">
            <div class="col-lg-12 col-sm-12 text-center">
                <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Promesa de Servicio</h3>
                <p>Resuelve tus dudas o cuéntanos sobre tu experiencia. En las próximas 24 horas recibirás nuestra llamada o si quieres información inmediata </br>nos puedes contactar a nuestro e-mail, WhatsApp o asesor virtual.</P>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <br/><br/>
</div>
</div>




@include('layouts.footer')
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="{{ asset('admin/js/scripts.js') }}"></script>
<script src="{{ asset('assets/js/javascript/jquery.blockUI.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("form#userdata").on("submit", function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{ route('userdata') }}",
                type: "post",
                dataType: "json",
                data: jQuery(this).serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '',
                        css: {
                            border: "0",
                            background: "transparent"
                        },
                        overlayCSS:  {
                            backgroundColor: "#fff",
                            opacity: 0.6,
                            cursor:          "wait"
                        },
                        baseZ: 10000
                    });
                },
                success: function (data) {
                    $('#div_datauser').html(data.html);
                    $.unblockUI();
                }
            });
        });
        $("#documenttypeid").on("change", function(e) {
            
            var documentType = $("#documenttypeid").val();

            $.ajax({
                url: "{{ route('typeDocument') }}",
                type: "get",
                dataType: "html",
                data:{
                    documentType: documentType,
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '',
                        css: {
                            border: "0",
                            background: "transparent"
                        },
                        overlayCSS:  {
                            backgroundColor: "#fff",
                            opacity: 0.6,
                            cursor:          "wait"
                        },
                        baseZ: 10000
                    });
                },
                success: function (data) {

                    //$('#datos_personales').removeAttr("style")
                    $('#operacion').attr("style", "display: none");
                    info = JSON.parse(data);
                    $('#div_variable').html(info.variables);
                    $('#type_document').html(info.html);
                    $.unblockUI();
                }
            });
        });

        $("#nationalOperation").on("change", function(e) {
            e.preventDefault();
            var operation = $("#nationalOperation").val();
            var code = $(this).data('code');
            var plate = $(this).data('plate');
            var cylinder = $(this).data('cylinder');
            var year = $(this).data('year');
            var passenger = $(this).data('passenger');
            //var passenger = 5;
            var load = $(this).data('load');
            var total = $(this).data('total');
            var totaldescuento = $(this).data('totaldescuento');
            var descuento = $(this).data('descuento');
            $.ajax({
                url: "{{ route('operationcar') }}",
                type: "get",
                dataType: "html",
                data:{
                    operation: operation,
                    code: code,
                    plate: plate,
                    cylinder: cylinder,
                    year: year,
                    passenger: passenger,
                    load: load,
                    total: total,
                    totaldescuento: totaldescuento,
                    descuento: descuento
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '',
                        css: {
                            border: "0",
                            background: "transparent"
                        },
                        overlayCSS:  {
                            backgroundColor: "#fff",
                            opacity: 0.6,
                            cursor:          "wait"
                        },
                        baseZ: 10000
                    });
                },
                success: function (data) {
                    $('#datos_personales').removeAttr("style")
                    $('#operacion').attr("style", "display: none");
                    info = JSON.parse(data);
                    $('#div_variable').html(info.variables);
                    $('#div_newprice').html(info.html);
                    $.unblockUI();
                },
                statusCode: {
                    404: function(xhr) {
                        $('#datos_personales').removeAttr("style")
                        $('#operacion').attr("style", "display: none");
                        info = JSON.parse(xhr.responseText);
                        $('#div_newprice').html(info.html);
                        $.unblockUI();
                    }
                }
            });
        });

        $("#documentType").on("change", function(e) {
            e.preventDefault();
            $(".con-json select").append('<option value="'+id+'">'+value+'</option>');
            $.ajax({
                url: "{{ route('nationalOperation') }}",
                type: "get",
                dataType: "json",
                data: jQuery(this).serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '',
                        css: {
                            border: "0",
                            background: "transparent"
                        },
                        overlayCSS:  {
                            backgroundColor: "#fff",
                            opacity: 0.6,
                            cursor:          "wait"
                        },
                        baseZ: 10000
                    });
                },
                success: function (data) {
                    $('#div_datauser').html('ok');
                    $.unblockUI();
                }
            });
        });
    });
</script>



 <!-- The Modal Login -->
 <div class="modal fade seminor-login-modal" data-backdrop="static" id="sem-login">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
  
			<!-- Modal body -->
				<div class="modal-body seminor-login-modal-body">
			
					<h4 class="modal-title text-center"><strong>Requisitos para el 10% descuento de ley:</strong></h4>
                    </br>
	  
					<button type="button" class="close" data-dismiss="modal">
						<span><i class="fa fa-times-circle" aria-hidden="true"></i></span>
					</button>
					
					<div class="form-group">          
					   <label for="Name"><strong>Continuidad:</strong></label></br>
						 * Ser<strong> PROPIETARIO </strong>del vehículo.
					</div>
					<div class="form-group">          
					   <label for="Name"><strong>Buen Conductor:</strong></label></br>
						 * <strong> NO </strong> haber tenido <strong>SINIESTRO</strong>, los dos ultimos años.
					</div>
					<div class="form-group">          
						<label for="Name"><strong>Buen Comportamiento:</strong></label></br>
						* Haber <strong>RENOVADO</strong> oportunamente su <strong>SOAT</strong>.
					</div>
				</div>
			</div>
		</div>
    </div>
   <!-- FIN The Modal Login -->