
    <!--header-area end-->
    @include('layouts.master')
    
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-8">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/salud.png" style="width:16%; margin-top: 10px;">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle" > — Seguro de Salud — </h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                    </div>
                                    <div>
                                        <input type='hidden' name='seguro' value='vida' />
                                        <input type='hidden' name='contacto' value='directorcomercial1@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;">  
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/salud.png" style="width:16%">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle" > — Seguro de Salud — </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" required oninvalid="setCustomValidity('Por favor agregar tu número de Celular')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                            </div>
                            <div>
                                <input type='hidden' name='seguro' value='vida' />
                                <input type='hidden' name='contacto' value='directorcomercial1@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>

    <!--banner-area end-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Seguro de Salud: Protección y bienestar a tu alcance</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">Este seguro busca brindarte respaldo y tranquilidad a ti y a tú familia en caso de emergencia y/o enfermedad con nuestras amplias asistencias en salud; además de proteger tu futuro y el de tus seres queridos en el caso de muerte accidental. También te permitirá acceder de manera preferencial a servicios de alta calidad ante la eventualidad de una enfermedad o accidente, tanto en atención ambulatoria como hospitalaria en instituciones de primer nivel de atención y con orientación prioritaria.</p>                        
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center"  style="background-color: #eaeaea;">
                        <img src="assets/images/banners/salud_banner.jpg" alt="promo"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <br/>
                    <ul class="list-basic" style="margin-top: 35px" >
                        <li id="listado">Brinda protección y bienestar a tu familia.</li>
                        <li id="listado">Puedes elegir el medico y la institución de tu preferencia.</li>
                        <li id="listado">Cobertura y calidad a nivel nacional e internacional.</li>
                        <li id="listado">Accedes a mayores y mejores beneficios.</li>
                        <li id="listado">Cubre gastos en caso de enfermedad o accidente .</li>
                        <li id="listado">Cobertura hospitalaria y ambulatoria en cualquier eventualidad.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <!--div class="faq-area ">
        <div class="container" style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <h3 style="font-weight: 800; margin-top: 5px; text-align:center">Elige el seguro de salud que se adapta a tu necesidad</h3>
                </div>
                <div class="col-lg-12 col-xs-12" id="seguros" >
                    </br></br></br>
                </div>
                <div class="col-lg-4 col-xs-12"  style="background-color: #eaeaea;">
                    <div class="section-title style-4 text-center ">
                        <div style="padding-top: 10px;">
                            <h4 style="font-weight: 800; margin-top: 5px; text-align:center">MediacAll Plus</h4>
                        </div>
                        <p>Acceso a coberturas hospitalarias y ambulatorias a nivel nacional e internacional.</p></br>
                        <div style="padding: 5px;">
                            <a href="#"  class="btn-common">Conoce más</a>
                        </div>
                    </div>
                </div>
                <div id="seguros"  class="col-lg-4 col-xs-12"  style="background-color: #eaeaea; height:280px; margin: -40px 0;-webkit-box-shadow: 0px 1px 6px 8px #999; z-index: 1; ">
                    <div style="background: #e2032b;color: white; margin: 0 -14px;">
                        <p class="text-center mt-0" style="padding: 12px;">Nuestro recomendado</p>
                    </div>
                    <div class="section-title style-4 text-center " >
                        
                        <div style="padding-top: 10px;">
                            <h4 style="font-weight: 800; margin-top: 5px; text-align:center">MedicAll Gold</h4>
                        </div>
                        <p>Coberturas hospitalarias y ambulatorias en nuestra red médica, coberturas plus y beneficios adicionales.</p></br>
                        <div style="padding: 5px;">
                            <a href="#"  class="btn-common">Conoce más</a>
                        </div>
                    </div>
                </div>
                <div id="seguros_mobile" class="col-lg-4 col-xs-12"  style="background-color: #eaeaea; height:280px;-webkit-box-shadow: 0px 1px 6px 8px #999; z-index: 1; ">
                    <div style="background: #e2032b;color: white; margin: 0 -14px;">
                        <p class="text-center mt-0" style="padding: 12px;">Nuestro recomendado</p>
                    </div>
                    <div class="section-title style-4 text-center " >
                        
                        <div style="padding-top: 10px;">
                            <h4 style="font-weight: 800; margin-top: 5px; text-align:center">MedicAll Gold</h4>
                        </div>
                        <p>Coberturas hospitalarias y ambulatorias en nuestra red médica, coberturas plus y beneficios adicionales.</p></br>
                        <div style="padding: 5px;">
                            <a href="#"  class="btn-common">Conoce más</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12"  style="background-color: #eaeaea;">
                    <div class="section-title style-4 text-center ">
                        <div style="padding-top: 10px;">
                            <h4 style="font-weight: 800; margin-top: 5px; text-align:center">MedicAll Care</h4>
                        </div>
                        <p>Coberturas hospitalarias y ambulatorias en una red de instituciones determinada</p></br>
                        <div style="padding: 5px;">
                            <a href="#"  class="btn-common">Conoce más</a>
                        </div> 
                    </div>
                </div>
                
            </div>


        </div>
    </div-->

    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Qué es el deducible de un Seguro de Salud?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>El deducible de un seguro es un monto que debe pagar el asegurado por los servicios utilizados y que el seguro no cubre. Una vez que se supera este monto, la cobertura del seguro comenzará a regir y cubrirá los gastos hasta los topes de cobertura establecido (si es que tiene tope).</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Qué es la prima en un Seguro de Salud?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>La prima de un Seguro de Salud es el precio que paga el cliente para estar cubierto. Generalmente la prima se paga mensualmente y mientras el pago este al día, la cobertura estará vigente.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        Cómo funciona el seguro de salud complementario?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>El seguro entrega cobertura a la diferencia en el gasto de salud que se produce entre lo que aporta la institución previsional a la que la persona está afiliada (Isapre o Fonasa) y lo que debe financiar el asegurado, lo que se denomina copago.</p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿Cómo obtener un Seguro de Salud?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>
                                        Para contratar un Seguro de Salud lo puedes hacer a través de nuestra agencia de seguros PRONTO Y SEGUROS   proporcionar la información requerida para cotizar el seguro  que se ajuste a tus necesidades.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Qué cubre un Seguro de Salud?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <p>
                                        Es un instrumento mediante el cual una compañía de seguros se compromete a cubrir gastos en salud del asegurado o de sus beneficiarios a cambio del pago de una prima. Estos gastos pueden ser médicos, clínicos, farmacéuticos o de hospitalización y tienen que estar indicados en la póliza. 
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        ¿Qué es el Seguro Catastrofico de Salud?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>El seguro catastrófico de salud cubre los gastos por enfermedades o accidentes de alto costo. Entre estas enfermedades se encuentran el cáncer, infarto al miocardio, accidente vascular cerebral, revascularización coronaria, entre otros. La cobertura dependera del seguro. Para mayor información conóce aqui el Seguro de Salud Catastrófico.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Queremos ayudarte a encontrar el seguro de salud que buscas! Nuestro equipo esta dispuesto a buscar las mejores opciones para ti. </h3> 
                    
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    
    <div class="row">
        <br/><br/>
    </div>
    <!--blog-area start-->
    <!--div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="blog-details">
                        <div class="section-title style-4 text-center pt-10">
                            <h2>Póliza de Salud</h2>
                        </div>
                        <div class="blog-details-text">
                            <p>Protegemos tu salud con un plan para cubrir tus gastos de hospitalización y cirugía, consulta ambulatoria y domiciliaria, exámenes de laboratorio, entre otros, de acuerdo a las coberturas que contrates.</p>
                            
                            <h4>Ventajas de adquirir este seguro</h4>
                            <ul class="list-none">
                                <li>Acceso a una amplia red de especialistas médicos con tarifas preferenciales.</li>
                                <li>Directorio médico en nuestra página web y en la app AXA COLPATRIA.</li>
                                <li>Autorizaciones médicas en nuestra página web.</li>
                                <li>Línea médica orientada por médicos las 24 horas del día.</li>
                                <li>Asistencia en viaje en caso de accidente o enfermedad no preexistente al viaje.</li>
                                <li>Clínica VIP en Bogotá (bono de atención urgencias sin costo).</li>
                                
                            </ul>
                        </div>
                        
                        <div class="section-title style-4 text-left pt-10">
                            <h3>Póliza Salud alterno</h3>
                        </div>
                        <div class="blog-details-text">
                            
                            <h4>Los mejores Beneficios</h4>
                            <ul>
                                <li>Cobertura del 100% sin deducibles ni topes en servicios médicos.</li>
                                <li>Acceso ilimitado en todas las especialidades.</li>
                                <li>La mejor red hospitalaria y IPS a nivel nacional.</li>
                                <li>Asistencia a nivel nacional.</li>
                            </ul>
                            <h4>Atención ambulatoria</h4>
                            <ul>
                                <li>Acceso directo a un médico de atención  integral y todos los especialistas.</li>
                                <li>Pago de bonos directamente en el consultorio.</li>
                                <li>Exámenes de laboratorio clínico sin autorización previa.</li>
                                <li>Exámenes de diagnóstico simple y especializado ilimitados.</li>
                                <li>Terapias sin límite de sesiones.</li>
                                <li>Urgencias a nivel nacional.</li>
                                <li>Consulta médica domiciliaria.</li>
                                <li>Servicio de ambulancia.</li>                                
                            </ul>
                            <h4>Atención hospitalaria integral</h4>
                            <ul>
                                <li>Tratamiento médico y quirúrgico ilimitado.</li>
                                <li>Hospitalización ilimitada en Unidad de Cuidado Intensivo.</li>
                                <li>Habitación individual.</li>
                                <li>Cama de acompañante y Enfermera para usuarios menores de 15 años y mayores de 65 años sin límite de días.</li>
                                <li>Cobertura intrahospitalaria para todas las especialidades, ilimitada y sin cobro de bonos.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>Cotiza gratis tu póliza de salud.</h2>
                    </div>
                    
                        
                    <div class="blog-details-text">
                        <p>Te asesoramos en  tu compra</p>
                        <p>Convenio con todas las compañías para que sea  tu el que elija  la mejor opción en tu póliza de Salud.</p>
                        <p>Podrás programar tu cita con  atención prioritaria, acceso a los mejores médicos especialistas</p>
                        <p>Cobertura en las mejores clínicas  en  todo  el país  con convenio  en el extranjero durante  su estadía</p>
                        <p>Prepagada Adquiérela  ahora.</p>
                        
                        <h4>Para ti y tu núcleo familiar</h4>
                        <ul>
                            <li>Nombre</li>
                            <li>C.C.</li>
                            <li>Fecha de nacimiento.</li>
                            <li>Historia de las prepagadas  o  pólizas  de Salud</li>
                            
                        </ul>
                        <p>La ley 100 de 1993, hizo entre otras varias modificaciones al sistema de salud colombiano, esta cambio radicalmente la estructura de la  salud  en nuestro país  separando cada uno de los oles que intervenían en el esquema anterior.</p>
                        <p>Una  póliza  de Salud  mejora las  condiciones de servicios hospitalarios  del plan obligatorio de  salud, ajustándose  a las  necesidad especificas de nuestros clientes, garantizando tranquilidad, comodidad, y calidad  de vida mediante  el otorgamiento de habitaciones  hospitalarias  individuales  y todas  las  coberturas a su medida. </p>
                        
                        <h4>Ventajas de adquirir un seguro  de salud</h4>
                        <ul>
                            <li>Consulta de inmediato con especialistas </li>
                            <li>Acceso a una amplia red de  médicos  especialista con tarifas  preferenciales</li>
                            <li>Directorio medio en nuestra página web</li>
                            <li>Autorizaciones para exámenes  </li>
                            <li>Línea medica de orientación   24  horas</li>
                            
                        </ul>


                    </div>
                    <div><br/></div>
                         
                    <div class="section-title style-4 text-left pt-10">
                        <a href="{{ url('contactenos') }}"><h2>Contáctanos</h2></a>
                        <p> 
                            <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/> Viviana Melo +57 316 6825935
                        </p>
                    </div>
                </div>   
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" style="border: 4px; border-style: ridge;">
                    <div class="blog-details">
                        <div class="section-title style-4 text-center pt-10">
                            <h2>Coberturas</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12" style="border: 4px; border-style: ridge;">
                    <div class="blog-details">
                        <div class="section-title style-4 text-left pt-10">
                            <h3>Cobertura Básica </h3>
                        </div>
                        <div class="blog-details-text">
                            <h4>Gastos Hospitalarios</h4>
                            <p>Dentro de los rubros que se cubren en gastos por hospitalización se incluyen por lo general los siguientes:</p>
                            <ul>
                                <li>Habitación diaria</li>
                                <li>Unidad de cuidados intensivos</li>
                                <li>Atención del parto</li>
                                <li>Atención del recién nacido</li>
                                <li>Visitas médicas por tratamiento médico hospitalario</li>
                            </ul>
                            <p>Otros servicios hospitalarios: Alimentación, servicios de enfermería, derechos de sala de cirugía, elementos de anestesia, exámenes de laboratorio y radiológicos y otros</p>
                            <p>Se reconocen también las consultas pre y post hospitalarias relacionadas con la hospitalización</p>
                            <ul>
                                <li>Honorarios médicos</li>
                            </ul>
                            <p>Cuando una hospitalización requiere procedimientos quirúrgicos, en esta cobertura se reconocen los honorarios de los profesionales de la salud que deben intervenir. Usualmente, estos honorarios se remuneran de acuerdo con el nivel de complejidad del procedimiento quirúrgico, y en línea con precios razonables en el mercado. Por lo general se incluyen los siguientes:</p>
                            <ul>
                                <li>Honorarios quirúrgicos</li>
                                <li>Honorarios de anestesiólogo</li>
                                <li>Honorarios de ayudantía quirúrgica</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12" style="border: 4px; border-style: ridge;">
                    <div class="blog-details">
                        <div class="section-title style-4 text-left pt-10">
                            <h3>Cobertura Adicionales </h3>
                        </div>
                        <div class="blog-details-text">
                            <h4>Amparo adicional de coberturas ambulatorias</h4>
                            <p>Este beneficio cubre procedimientos que no requieren hospitalización dentro de los que se pueden encontrar los siguientes:</p>
                            <ul>
                                <li>Consulta con médico general y especialistas</li>
                                <li>Exámenes de laboratorio, rayos X y ecografías</li>
                                <li>Exámenes especializados de diagnóstico</li>
                                <li>Terapias ambulatorias de todo tipo</li>
                                <li>Tratamiento ambulatorio del cáncer y/o sida</li>
                                <li>Atención para la mujer embarazada después de 10 meses</li>
                            </ul>
                        </div>
                    </div>
                </div> 
            </div>



        </div>
    </div-->
    <!--blog-area end-->
    
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>