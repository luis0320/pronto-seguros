
    <!--header-area end-->
    @include('layouts.master')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-mascotas">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/mascotas.png" style="width:16%;margin-top:20px">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle"> — Seguro de Mascotas — </h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">Tu mascota es única e importante ¡Protegela!</p>   
                                    </div>
                                    <div>
                                        <input type='hidden' name='seguro' value='Mascotas' />
                                        <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;">  
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/mascotas.png" style="width:16%;margin-top:20px">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle"> — Seguro de Mascotas — </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">Tu mascota es única e importante ¡Protegela!</p>   
                            </div>
                            <div>
                                <input type='hidden' name='seguro' value='Mascotas' />
                                <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <!--Estandar-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold">Seguro para mascotas</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">Porque tu mascota necesita que la consientan y la cuiden como se merecen, por eso en Pronto y Seguros estamos siempre contigo para ayudarte en los cuidados que necesita tu mascota: Con nuestros seguros de mascotas contaras con las asistencias que desees para tu gran amigo.</p>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/vida/mascotas.jpg" alt="promo"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <ul class="list-basic"  style="margin-top: 40px" >
                        <li id="listado">Evitar sobre costos asociados a su salud</li>
                        <li id="listado">Es uno de los seguros más económicos del mercado</li>
                        <li id="listado">Se pueden asegurar perros y gatos</li>
                        <li id="listado">Fácil de adquirir</li>
                        <li id="listado">Ofrece la cobertura de responsabilidad civil en salarios mínimos según la necesidad del cliente</li>
                        <li id="listado">La cobertura de muerte accidental va hasta 1.000.000</li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                   
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Coberturas</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Requisitos y restricciones</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-documentos-tab" data-toggle="pill" href="#pills-documentos" role="tab" aria-controls="pills-documentos" aria-selected="true" style="color: #000;">Formato Cotización</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Coberturas</h3>
                    
                    <ul class="list-basic">
                        <li>Responsabilidad ante Perjuicios a terceros (Codigo de policia )</li>
                        <li>Consulta Médica Veterinaria y orientación médica telefónica</li>
                        <li>Vacunación en red</li>
                        <li>Traslado médico por emergencia</li>
                        <li>Medicamentos a domicilio</li>
                        <li>Servicio de guardería para mascotas</li>
                        <li>Servicio paseo de caninos</li>
                        <li>Asistencia estética </li>
                        <li>Asistencia exequial mascotas</li>
                        <li>Ayuda en localización por pérdida de la mascota</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Requisitos y mascotas</h3>
                    <ul class="list-basic">
                        <li>Diligenciar formulario.</li>
                        <li>Sarlaft.</li>
                        <li>Fotocopia de la cédula del dueño.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos" role="tabpanel" aria-labelledby="pills-documentos-tab" style="height: 230px;">
                    <h3 style="margin-top: 0;">Formato Cotización</h3>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_MASCOTAS.xlsx" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Formato cotización seguro MASCOTAS</a>
                        </div>  
                    </div>  
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Coberturas</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Requisitos y restricciones</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-documentos-tab" data-toggle="pill" href="#pills-documentos-mobile" role="tab" aria-controls="pills-documentos" aria-selected="true" style="color: #000;">Formato Cotización</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Coberturas</h3>
                    
                    <ul class="list-basic">
                        <li>Responsabilidad ante Perjuicios a terceros (Codigo de policia )</li>
                        <li>Consulta Médica Veterinaria y orientación médica telefónica</li>
                        <li>Vacunación en red</li>
                        <li>Traslado médico por emergencia</li>
                        <li>Medicamentos a domicilio</li>
                        <li>Servicio de guardería para mascotas</li>
                        <li>Servicio paseo de caninos</li>
                        <li>Asistencia estética </li>
                        <li>Asistencia exequial mascotas</li>
                        <li>Ayuda en localización por pérdida de la mascota</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Requisitos y mascotas</h3>
                    <ul class="list-basic">
                        <li>Diligenciar formulario.</li>
                        <li>Sarlaft.</li>
                        <li>Fotocopia de la cédula del dueño.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos-mobile" role="tabpanel" aria-labelledby="pills-documentos-tab">
                    <h3 style="margin-top: 0;">Formato Cotización</h3>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_MASCOTAS.xlsx" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Formato cotización seguro MASCOTAS</a>
                        </div>  
                    </div>    
                </div>
                
            </div>
            

        </div>

    </div>
    <div class="row">
        <br/>
    </div>
    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Qué es un seguro de mascotas?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Es una póliza que respalda los posibles gastos derivados de los daños que pueda ocasionar y/o sufrir tu mascota (en caso de accidente o enfermedad), generalmente perro o gato.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Qué cubre el seguro para mascotas?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>El seguro para mascotas cubre los daños causados a terceros y los gastos veterinarios en caso de accidente o enfermedad, así como la hospitalización en residencia canina (con limitaciones). También tu mascota estará cubierta en caso de muerte accidental o sacrificio.</p>
                                </div>
                            </div>
                        </div>
                        <!--div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Los peludos son parte fundamental de la familia. </h3>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    <div class="row">
        <br/><br/>
    </div>
    <!--Fin Estandar-->

    <!--div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>Seguro para mascotas</h2>
                    </div>
                    <div class="row mt-30">
                        <div class="col-sm-6">
                            <div class="sin-service style-2 text-left">
                                <p>Nuestro seguro para mascotas no tiene limitación en tipo de raza y se aseguran a partir de los tres (3) meses y máximo doce (12) años de edad, con una tarifa preferencial.</p>
                                <h4>Amparo Básico</h4>
                                <p>Los perjuicios materiales correspondientes al daño emergente derivado de las lesiones corporales, la enfermedad derivada de ésta, la muerte de personas o animales.</p>
                                <p>Los perjuicios materiales correspondientes a daño emergente derivado de la destrucción, la avería, el deterioro de bienes muebles e inmuebles de propiedad de terceros.</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="sin-service style-2 text-left">
                                <h4>Coberturas Principales</h4>
                                <ul>
                                    <li>Responsabilidad ante perjuicios a terceros (Código de policia)</li>
                                    <li>Consulta medica veterinaria</li>
                                    <li>Orientacion medica veterinaria telefonica</li>
                                    <li>Vacunación en red veterinaria</li>
                                    <li>Traslado médico veterinario</li>
                                    <li>Medicamentos a domicilio</li>
                                    <li>Servicio de guarderia para mascotas</li>
                                    <li>Servicio paseo caninos</li>
                                    <li>Asistencia estética y complementaria</li>
                                    <li>Asistencia exequial para mascotas</li>
                                    <li>Ayuda en localización por perdida de la mascota</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="sin-service style-2 text-center">
                                <img src="assets/images/mascotas-uno.png" alt="coberturas" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="sin-service style-2 text-center">
                                <img src="assets/images/mascotas-dos.png" alt="coberturas" style="width: 80%;" />
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="row">                   
                        <div class="section-title style-4 text-right pt-10">
                            <a href="{{ url('contactenos') }}"><h2>Contáctanos</h2></a>
                            <p> <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/> Carlos Augusto Mona +573 12 787 6231</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="blog-details">
                        <div class="section-title style-4 text-center pt-10">
                            <h2>MAPFRE - Seguros de Mascotas</h2>
                            
                        </div>
                        <div class="blog-details-text">
                            <p>Nuestra amplia experiencia nos ha enseñado que las mascotas son sinónimos de compañía y 
                                amistad. Por eso hemos diseñado un seguro para mascotas, ofreciéndote a ti y a ellos 
                                tranquilidad, bienestar y seguridad.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="blog-details">
                       
                        <div class="blog-details-text">
                            
                            
                            <h3>Seguro para perros y gatos</h3>
                            
                            <p>En MAPFRE puedes cubrir a tus mascotas, sean una o varias, con un seguro para perros y 
                                gatos que ha sido diseñado pensando en tu tranquilidad y en la protección de tu mascota. 
                                Gracias a las coberturas de un seguro para perros o gatos como éste, tus mascotas estarán 
                                protegidas de cualquier riesgo que puedan correr.</p>

                            <h3>Beneficios y Ventajas</h3>
                            
                            <p>En MAPFRE sabemos que tu mascota hace parte de tu familia, y por eso te ofrecemos el 
                                seguro de MASCOTAS MAPFRE, que ampara tu peludito frente a sucesos que puedan ocurrir 
                                de forma inesperada.</p>
                            <div class="work-hours">
                                <ul>
                                    <li>Es uno de los seguros más económicos del mercado</li>
                                    <li>Se pueden asegurar perros y gatos</li>
                                    <li>Fácil de adquirir</li>
                                    <li>Ofrece la cobertura de responsabilidad civil en salarios mínimos según la necesidad del cliente</li>
                                    <li>La cobertura de muerte accidental va hasta 1.000.000</li>
                                    <li></li>
                                </ul>
                            </div>
                            

                            <h3>Requisitos y restricciones</h3>
                            <p>El producto de Mascotas establece una serie de requisitos y restricciones que los usuarios 
                                deben conocer para saber cuáles son sus derechos y obligaciones después de contratar el seguro.
                            </p>
                            <ul>
                                    <li>Se excluyen como terceros afectados a: El Cónyuge, compañero permanente o quien de hecho 
                                        ostente esta condición siempre que conviva con el Asegurado. A los hijos u otros menores que 
                                        dependen económicamente del Asegurado, o incapacitados que estén bajo su autoridad en 
                                        tanto convivan con el Asegurado. A familiares hasta cuarto grado de consanguinidad o afinidad 
                                        que convivan y dependan económicamente del Asegurado. Los socios, directivos, asalariados 
                                        (Incluso de Contratistas y Subcontratistas) y personas que de hecho o de derecho dependan 
                                        del Tomador y/o Asegurado, mientras actúen en el ámbito de dependencia.</li>
                                    <li>Se excluyen: El dolo o culpa grave del Tomador de seguro o Asegurado, sus familiares hasta el 
                                        cuarto grado de consanguinidad, segundo de afinidad o único civil, sus socios, representantes 
                                        legales o personal directivo del mismo, a quienes este haya confiado la dirección y control de la 
                                        empresa para el desarrollo de su objeto social. </li>
                                    <li>Caída de cuerpos siderales y aerolitos.</li>
                                    <li>La participación de los animales asegurados en apuestas, desafíos o deportes</li>
                                    <li>Destinarse los animales asegurados a funciones o servicios distintos a los consignados en las 
                                        Condiciones Particulares.</li>
                                    <li>Malos tratos, exceso de trabajo, falta, insuficiencia o mala calidad higiénica de alimentos o 
                                        cuidados a los animales asegurados, cuando estas circunstancias sean imputables al asegurado</li>
                                    <li>Siniestros que por su extensión e importancia sean calificados por el Gobierno de “catástrofe o 
                                        calamidad nacional”. Lesiones ya existentes anteriormente a la vigencia del seguro, así como 
                                        los vicios ocultos, preexistencias de las enfermedades o malformaciones congénitas.</li>
                                </ul>
                        </div>
                        
                    </div>
                   
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="blog-details">
                        <div class="blog-details-text">
                            <h3>Coberturas</h3>
                            
                            <p><span style="font-weight: 800;">Responsabilidad civil extracontractual: </span>Se amparan los daños o perjuicios corporales omateriales que ocasione la mascota</p>
                            <p><span style="font-weight: 800;">Asistencia jurídica Telefónica: </span>Asistencia legal telefónica en cuestiones derivadas de la vida personal y familiar por la propiedad y/o tenencia de animales domésticos.</p>
                            <p><span style="font-weight: 800;">Muerte accidental</span></p>
                            <p><span style="font-weight: 800;">Cobertura de sacrificio </span></p>
                            <p><span style="font-weight: 800;">Estadía en residencia canina por hospitalización del asegurado </span></p>
                            <p><span style="font-weight: 800;">Exequias Mascotas</span></p>
                            
                           
                            <h3>Preguntas sobre Seguros de Mascotas</h3>
                            
                            <p><span style="font-weight: 800;">¿Qué es un seguro de mascotas?</span> Es una póliza que respalda los posibles gastos derivados de los daños que pueda 
                                ocasionar y/o sufrir tu mascota (en caso de accidente o enfermedad), generalmente perro o gato.
                            </p>
                            <p><span style="font-weight: 800;">¿Qué cubre un seguro para mascotas?</span> El seguro para mascotas de MAPFRE cubre los daños causados a terceros y los gastos 
                                veterinarios en caso de accidente o enfermedad, así como la hospitalización en 
                                residencia canina (con limitaciones). También tu mascota estará cubierta en caso de muerte accidental o sacrificio.
                            </p>
                            <h4>ASISTENCIA VETERINARIA</h4>
                            <p>ALCANCE: Servicio deasistencia veterinaria para mascota, la cual para perros y gatos</p>
                            
                            <div class="shopping-cart-area mt-100 sm-mt-80">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="table-responsive-md">
                                                <table class="table table-bordered">
                                                    <thead style="text-align: center;">
                                                        <tr>
                                                            <th>SERVICIOS</th>
                                                            <th>COBERTURA</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0; ">Orientacion veterinaria telefonica</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">Ilimitado</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0;">Emergencia medica hospitalaria a consecuencia de un accidente:</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">10 SMDLV</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0;">Consultas medicas</p>
                                                                    <p style="margin-bottom: 0;">Examenes Medicos Complementarios</p>
                                                                    <p style="margin-bottom: 0;">Hospitalizaciones</p>
                                                                    <p style="margin-bottom: 0;">Intervenciones (quirurgicas un evento durante la vigencia)</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">2 eventos por vigencia anual</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0;">Informacion sobre vacunas</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">Ilimitado</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0;">Referencia y coordinacion de guarderias y casas de adiestramiento</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">Ilimitado</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0;">Informacion servicios funebre</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">Ilimitado</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0;">Referencia y coordinacion de transporte</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">Ilimitado</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0;">Referencia de clinicas veterinarias</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">Ilimitado</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0;">Paseo canino por enfermedad del asegurado</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">2 eventos por vigencia anual</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-thumb">
                                                                    <p style="margin-bottom: 0;">Bano y peluqueria canina luego del accidente</p>
                                                                </div>
                                                            </td>
                                                            <td style="border: 1px solid #ebebeb">
                                                                <div class="cart-product-name">
                                                                    <p style="margin-bottom: 0;">5 SMDLV</p>
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>

                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <p>Las coberturas de asistencia se prestarán en las ciudades de Bogotá, Chía, Medellín, Envigado, Itaguí y Cali</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="blog-details">
                        <div class="section-title style-4 text-center pt-10">
                            <h2>HDI - Seguros de Mascotas</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="blog-details">
                        <div class="blog-details-text">
                            <h3>Seguro de Mascotas: Perros y Gatos</h3>
                            <p>En HDI Seguros S.A. sabemos que ellos son muy importantes y que hacen parte de nuestras familias. Por esto, no tenemos limitación en tipo de raza y los aseguramos a partir de los tres (3) meses y máximo, doce (12) años de edad con una tarifa preferencial.</p>

                            <h3>Amparo básico</h3>
                            <p>Responsabilidad Civil Extracontractual: Los perjuicios materiales y correspondientes al daño emergente derivado de las lesiones corporales, la enfermedad derivada de éstas, la muerte de personas o animales.</p>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="blog-details">
                        <div class="blog-details-text">
                            <h3>Asistencia 24 horas</h3>
                            <p>Orientación médica veterinaria telefónica, orientación legal telefónica ilimitada, asistencia en caso de accidente o enfermedad, traslado para la mascota a hotel o guardería en caso de incapacidad del tomador por un periodo superior a 5 días que le imposibilite cuidar a su mascota y Ayuda en localización de mascotas extraviadas. Para mayor información sobre el seguro de mascotas, por favor consultar el condicionado Seguro de Responsabilidad Civil Protección Mascotas</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--blog-area end-->
    
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>