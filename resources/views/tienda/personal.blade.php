
    <!--header-area end-->
    @include('layouts.master')
    
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-accidentes">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/personales.png" style="width:16%;margin-top:15px">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle">Seguro Accidentes Personales</h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protegete ante lo inesperado! </p>   
                                    </div>
                                    <div>
                                        <input type='hidden' name='seguro' value='Accidentes Personales' />
                                        <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;">  
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -20px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/personales.png" style="width:16%;margin-top:15px">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4id="linea_tittle"> —Seguro Accidentes Personales— </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protegete ante lo inesperado!</p>   
                            </div>
                            <div>
                                <input type='hidden' name='seguro' value='Accidentes Personales' />
                                <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                                <!--input type="checkbox" required title="El número de referencia consta de 3 letras mayúsculas y 4 dígitos."/>
                                <a>Acepta terminos y condiciones</a-->
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
   <!--banner-area end-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Accidentes Personales: Protégete ante lo inesperado</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">Los accidentes, aunque no nos guste pensar en ellos, pueden estar aguardándonos a la vuelta de la esquina. Necesitamos protección ante esas pequeñas grandes sorpresas que tiene la vida y la póliza de Accidentes Personales es un producto diseñado para hacer frente a las consecuencias económicas que de los accidentes se derivan.</p>
                    </div>
                </div>
            </div>
            <div class="row">
        <br/>
    </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/banners/accidentes_uno.jpg" alt="promo" style="height: 400px;" >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <ul class="list-basic"  style="margin-top: 30px" >
                        <li id="listado">Es la póliza que protege al asegurado frente a las consecuencias económicas de un accidente.</li>
                        <li id="listado">Puede contratarse de forma individual o colectiva.</li>
                        <li id="listado">Es una póliza flexible que permite añadir coberturas adicionales.</li>
                    </ul>
                    <h4 style="font-weight: bold;">Los asegurados pueden acceder a los siguientes amparos adicionales:</h4>
                    <ul class="list-basic" >
                        <li id="listado">Inhabilitación Total y Permanente.</li>
                        <li id="listado">Indemnización adicional por fallecimiento o inhabilitación derivada en un accidente de tránsito.</li>
                        <li id="listado">Gastos de Exequias.</li>
                        <li id="listado">Renta Mensual.</li>
                        <li id="listado">Honorarios Médicos y Gastos por Tratamiento.</li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
     <div class="row">
        <br/>
    </div>
    <!--div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Razones para preferirnos</h3>
                </div>
            </div>
            <br/>
            <div class="row">
                
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;">
                    <div class="sin-service" style="padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/companias.png" style="width:35%; margin-top: 33px;height: 84px;">
                        <p style="font-size:14px;text-align: justify; height: 57px; margin-top: 20px;"><br/>Accidentes personales estandar</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12"style="padding: 0px 10px;">
                    <div class="sin-service" style=" padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/asesor2.png" style="width:35%; margin-top: 33px;height: 84px;">
                        <p style="font-size:14px;text-align: justify;  height: 57px; margin-top: 20px;"><br/>Accidentes personales viajero.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;">
                    <div class="sin-service" style="padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/planes.png" style="width:35%; margin-top: 33px;">
                        <p style="font-size:14px;text-align: justify; margin-top: 20px;"><br/>Accidentes personales corporativo.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;">
                    <div class="sin-service" style="padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/mapa.png" style="width:35%; margin-top: 33px;">
                        <p style="font-size:14px;text-align: justify; margin-top: 20px;"><br/>AP en tus manos.</p> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div-->
    <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="color: #000;">Por qué tener este seguro</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Cobertura Básica</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas Adicionales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Por qué tener este seguro</h3>
                    <p>Cuando en un evento accidental la persona que genera ingresos en una familia muere o pierde la capacidad de desarrollar un trabajo remunerado, hay un deterioro significativo en la calidad de vida de todos los miembros de este grupo familiar, ya que los ingresos se ven mermados y los gastos generalmente se incrementan, especialmente, cuando quien sufre el accidente queda en estado de incapacidad o invalidez.</p>
                    <p>El seguro de Accidentes Personales es una herramienta que permite evitar este tipo de situaciones, pues en caso de no tener esta protección, la familia tendría que recurrir al gasto de los ahorros, pedir dinero a familiares o amigos, endeudarse con el sistema financiero formal o informal, reducir los gastos de la familia para ajustar el presupuesto a la nueva realidad o buscar otras fuentes de ingresos, entre otros.</p>
                </div>
                <div class="tab-pane fade" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Cobertura Básica</h3>
                    </br>
                    <p>Muerte Accidental</p>
                </div>
                <div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Homicidio</li>
                        <li>Incapacidad total y permanente por accidente</li>
                        <li>Incapacidad permanente parcial por accidente</li>
                        <li>Desmembración accidental</li>
                        <li>Gastos médicos por accidente</li>
                        <li>Renta diaria por hospitalización por accidente</li>
                        <li>Renta por incapacidad total temporal</li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos" role="tabpanel" aria-labelledby="pills-documentos-tab">
                    <h3 style="margin-top: 0;">Documentos</h3>
                    <p dir="ltr">Conoce en las condiciones generales del Seguro Exequial, las exclusiones y más detalles de este producto.</p>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_DE _CANCELACION.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i> Condiciones generales</a>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_DE _CANCELACION.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i> Cancelación del Seguro Exequial</a>
                        </div>  
                    </div>  
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link active" id="pills-profile-mobile-tab" data-toggle="pill" href="#pills-profile-mobile" role="tab" aria-controls="pills-profile-mobile" aria-selected="false" style="color: #000;">Por qué tener este seguro</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Cobertura Básica</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas Adicionales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
               
                <div class="tab-pane fade show active" id="pills-profile-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Por qué tener este seguro</h3>
                    <p>Cuando en un evento accidental la persona que genera ingresos en una familia muere o pierde la capacidad de desarrollar un trabajo remunerado, hay un deterioro significativo en la calidad de vida de todos los miembros de este grupo familiar, ya que los ingresos se ven mermados y los gastos generalmente se incrementan, especialmente, cuando quien sufre el accidente queda en estado de incapacidad o invalidez.</p>
                    <p>El seguro de Accidentes Personales es una herramienta que permite evitar este tipo de situaciones, pues en caso de no tener esta protección, la familia tendría que recurrir al gasto de los ahorros, pedir dinero a familiares o amigos, endeudarse con el sistema financiero formal o informal, reducir los gastos de la familia para ajustar el presupuesto a la nueva realidad o buscar otras fuentes de ingresos, entre otros.</p>
                </div>
                <div class="tab-pane fade" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Cobertura Básica</h3>
                    </br>
                    <p>Muerte Accidental</p>
                    <h3 style="margin-top: 0;">Condiciones Generales</h3>
                    <p>Las condiciones generales de los Seguros Su Futuro Seguro MAPFRE definen los requisitos que deben cumplir los asegurados para beneficiarse de todas las garantías establecidas en el acuerdo.</p>
                    
                </div>
                <div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Homicidio</li>
                        <li>Incapacidad total y permanente por accidente</li>
                        <li>Incapacidad permanente parcial por accidente</li>
                        <li>Desmembración accidental</li>
                        <li>Gastos médicos por accidente</li>
                        <li>Renta diaria por hospitalización por accidente</li>
                        <li>Renta por incapacidad total temporal</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                
            </div>
            

        </div>

    </div>
    <!--page-banner-area start-->
    <!--div class="page-banner-area bg-personales">
        <div class="container">
            <div class="row align-items-center height-400">
                <div class="col-lg-12">
                    <div class="page-banner-text text-center text-white">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12" >
                    <div class="section-title style-4 text-center pt-10" style="border: 1px solid; border-radius: 19px; color: #e2022c;">
                        <h2>Accidentes Personales</h2>
                    </div>
                    <div class="row mt-30">
                        <div class="col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <p>Protege contra los accidentes que pueda sufrir una persona causándole la muerte, invalidez permanente (total o parcial), incapacidad temporal para el trabajo, gastos de sepelio, y adicionalmente, gastos de curación.</p>
                                <h4>¿Qué es una póliza de Accidentes Personales?</h4>
                                <p>Es un contrato que se hace entre un cliente, llamado tomador del seguro y una compañía de seguros, en donde la compañía se compromete a pagar un monto dinero convenido de antemano, en la póliza de seguros. Su pago se hace a los beneficiarios del asegurado cuando éste muera, o al asegurado cuando éste sufra lesiones corporales o pérdida funcional de una o varias partes del cuerpo. Estos valores también pueden pagarse como un reembolso al cliente, con base en las facturas de los gastos médicos y hasta un límite máximo establecido de antemano.</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-12" style="border: 1px solid; border-radius: 19px; color: #e2022c;">
                            <div class="sin-service style-2 text-left">
                                <h3>RAZONES PARA TENER ESTE SEGURO</h3>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <p>Cuando en un evento accidental la persona que genera ingresos en una familia muere o pierde la capacidad de desarrollar un trabajo remunerado, hay un deterioro significativo en la calidad de vida de todos los miembros de este grupo familiar, ya que los ingresos se ven mermados y los gastos generalmente se incrementan, especialmente, cuando quien sufre el accidente queda en estado de incapacidad o invalidez.</p>
                                <p>El seguro de Accidentes Personales es una herramienta que permite evitar este tipo de situaciones, pues en caso de no tener esta protección, la familia tendría que recurrir al gasto de los ahorros, pedir dinero a familiares o amigos, endeudarse con el sistema financiero formal o informal, reducir los gastos de la familia para ajustar el presupuesto a la nueva realidad o buscar otras fuentes de ingresos, entre otros.</p>
                            </div>
                        </div>
                        <div cclass="col-lg-6 col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <h4>Cobertura Básica:</h4>
                                <ul style="list-style-type: circle;">
                                    <li>Muerte Accidental</li>
                                </ul>
                                <h4>Coberturas Adicionales:</h4>
                                <ul style="list-style-type: circle;">
                                    <li>Homicidio</li>
                                    <li>Incapacidad total y permanente por accidente</li>
                                    <li>Incapacidad permanente parcial por accidente</li>
                                    <li>Desmembración accidental</li>
                                    <li>Gastos médicos por accidente</li>
                                    <li>Renta diaria por hospitalización por accidente</li>
                                    <li>Renta por incapacidad total temporal</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">                   
                        <div class="section-title style-4 text-left pt-10">
                            <h2>Contáctanos</h2>
                            <p>  
                                <img src="assets/images/icons/telefono.png" alt="coberturas" style="width:4%"/> 4851105 Extención 737-785 &nbsp;&nbsp;&nbsp;
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/>3206964550 &nbsp;&nbsp;&nbsp;
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/>3046126643 &nbsp;&nbsp;&nbsp;
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/>312 893 2013 &nbsp;&nbsp;&nbsp;
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--blog-area start-->
    <!--div class="row">
        <br/>
    </div>
    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Gastos exequiales a quien demuestre haber pagado los gastos, o los servicios funerales de cualquiera de los asegurados.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">¡ Diseñemos juntos un seguro para accidentes personales ! </h3> 
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')

    <div class="row">
        <br/><br/>
    </div>

   @include('layouts.footer')
    <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>
