
    <!--header-area end-->
    @include('layouts.master')
    
    <!--div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-camiones">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-lg-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/carro.png" style="width:16%">
                                <h3> — Tu Seguro Todo Riesgo — </h3>
                                <div style="padding: 5px;">
                                    <input class="" style="padding: 9px;width: 225px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Placa del Vehículo" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                                </div>
                                <div style="padding: 5px;">
                                    <input class="" style="padding: 9px;width: 50px;" matinput="" maxlength="20" id="mat-input-0" placeholder="Tipo de documento" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0">
                                    <input class="" style="padding: 9px;width: 172px;" matinput="" maxlength="10" id="mat-input-0" placeholder="Número de documento" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                                </div>
                                <div style="padding: 5px;">
                                    <input class="" style="padding: 9px;width: 225px;" matinput="" maxlength="10" id="mat-input-0" placeholder="Número de Celular" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                                </div>
                                <div>
                                    <input type="checkbox" value="">
                                    <a>Acepta terminos y condiciones</a>
                                </div>
                                <div style="padding: 5px;">
                                    <a href="{{ url('soat') }}"  class="btn-common">Compra aquí</a>   
                                </div> 
                                </br>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-camiones">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-lg-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/carro.png" style="width:16%">
                                <h3> — Tu Seguro Todo Riesgo — </h3>
                                <div style="padding: 5px;">
                                    <a href="todo-riesgo3" class="btn-common">Cotizar</a>   
                                </div> 
                                </br>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
                <div class="col-xs-8 text-center" id="banner_soat_mobile"  style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/carro.png" style="width:16%">
                        <h4> — Tu Seguro Todo Riesgo — </h4>
                        <div style="padding: 5px;">
                            <a href="todo-riesgo3" class="btn-common">Cotizar</a>     
                            <!--input class="" style="padding: 9px;width: 183px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Placa del Vehículo" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br-->
                        </div>
                        <!--div style="padding: 5px;">
                            <input class="" style="padding: 9px;width: 50px;" matinput="" maxlength="20" id="mat-input-0" placeholder="Tipo de documento" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0">
                            <input class="" style="padding: 9px;width: 130px;" matinput="" maxlength="10" id="mat-input-0" placeholder="Num. Doc" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                        </div>
                        <div style="padding: 5px;">
                            <input class="" style="padding: 9px;width: 183px;" matinput="" maxlength="10" id="mat-input-0" placeholder="Número de Celular" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"></br>
                        </div>
                        <div>
                            <input type="checkbox" value="">
                            <a>Acepta terminos y condiciones</a>
                        </div>
                        <div style="padding: 5px;">
                            <a href="{{ url('soat') }}"  class="btn-common">Compra aquí</a>   
                        </div> 
                        </br-->
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    
    <!--blog-area end-->
    <div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Seguro todo riesgo: La cobertura ideal para tu carro , moto y otros vehiculos.</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">Con el Seguro Todo Riesgo puede viajar con tranquilidad, proteger a su familia y a su patrimonio, ya que este seguro está pensado en acompañarle antes, durante y después de cualquier eventualidad que ocurra con su carro o moto y que pueda interrumpir su vida cotidiana o la de sus familiares. Sentirte protegido en todo momento debe ser su principal prioridad y la cobertura todo riesgo ha sido diseñada para ello.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;" id="seguros">
                    <div class="sin-service" style="height: 380px; padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/icon/proteccion.png" style="width:35%; margin-top: 33px;">
                        <h3 style="margin-top: 5px;">Protección</h3>
                        <p style="font-size:14px;text-align: justify; margin-top: 50px;">Protegemos su patrimonio familiar brindándole nuestra asesoría ante un choque que lo afecte a usted o otra persona.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;" id="seguros">
                    <div class="sin-service" style="height: 380px; padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/icon/tranquilidad.png" style="width:35%; margin-top: 33px;">
                        <h3 style="margin-top: 5px;">Tranquilidad</h3>
                        <p style="font-size:14px;text-align: justify; margin-top: 50px;">Le brindamos tranquilidad después de momentos difíciles como choques, varadas, robos, entre otros.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;" id="seguros">
                    <div class="sin-service" style="height: 380px; padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/icon/asesoria.png" style="width:35%; margin-top: 33px;">
                        <h3 style="margin-top: 5px;">Asesoría</h3>
                        <p style="font-size:14px;text-align: justify; margin-top: 50px;">Le damos acompañamiento con nuestra red de talleres a nivel nacional que le garantizarán la reparación necesaria a su carro.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;" id="seguros">
                    <div class="sin-service" style="height: 380px; padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/icon/acompanamiento.png" style="width:35%; margin-top: 33px;">
                        <h3 style="margin-top: 5px;">Acompañamiento</h3>
                        <p style="font-size:14px;text-align: justify; margin-top: 50px;">Resolvemos sus requerimientos en tiempo real a través del boton verde de nuestra aplicación móvil.</p> 
                    </div>
                </div>


                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;" id="seguros_mobile">
                    <div class="sin-service" style="padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/icon/proteccion.png" style="width:35%; margin-top: 10px;">
                        <h3 style="margin-top: 5px;">Protección</h3>
                        <p style="font-size:14px;text-align: justify; margin-top: 15px;">Protegemos su patrimonio familiar brindándole nuestra asesoría ante un choque que lo afecte a usted o otra persona.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;" id="seguros_mobile">
                    <div class="sin-service" style="padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/icon/tranquilidad.png" style="width:35%; margin-top: 10px;">
                        <h3 style="margin-top: 5px;">Tranquilidad</h3>
                        <p style="font-size:14px;text-align: justify; margin-top: 15px;">Le brindamos tranquilidad después de momentos difíciles como choques, varadas, robos, entre otros.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;" id="seguros_mobile">
                    <div class="sin-service" style="padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/icon/asesoria.png" style="width:35%; margin-top: 10px;">
                        <h3 style="margin-top: 5px;">Asesoría</h3>
                        <p style="font-size:14px;text-align: justify; margin-top: 15px;">Le damos acompañamiento con nuestra red de talleres a nivel nacional que le garantizarán la reparación necesaria a su carro.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;" id="seguros_mobile">
                    <div class="sin-service" style="padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/icon/acompanamiento.png" style="width:35%; margin-top: 10px;">
                        <h3 style="margin-top: 5px;">Acompañamiento</h3>
                        <p style="font-size:14px;text-align: justify; margin-top: 15px;">Resolvemos sus requerimientos en tiempo real a través del boton verde de nuestra aplicación móvil.</p> 
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Qué cubre</h3>
                </div>
            </div>
            <br/>
            <div class="row">
                
                <div class="col-lg-4 col-xs-12" style="padding: padding: 0px 10px;"   id="soat_items_mobile">
                    <div class="sin-service" style="padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/riesgo/parciales.png" style="margin: 12px 0 0 0;">
                        <h3 style="margin-top: 5px;">Perdidas parciales</h3>
                        <p style="font-size:14px;text-align: justify;">Son aquellos daños que puede sufrir tu Auto o tu Moto, por responsabilidad del propio asegurado o por el tercero.</p> 
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12"style="padding: padding: 0px 10px;"   id="soat_items_mobile">
                    <div class="sin-service" style=" padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/riesgo/total.png" style="margin: 12px 0 0 0; ">
                        <h3 style="margin-top: 5px;">Pérdida total</h3>
                        <p style="font-size:14px;text-align: justify;">Se considera perdida total, cuando la separación de los daños ocacionados al Auto o a la Moto, superan el 75% de su valor comercial o hurto de vehículo.</p> 
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12" style="padding: padding: 0px 10px;"   id="soat_items_mobile">
                    <div class="sin-service" style="padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/riesgo/civil.png" style="margin: 12px 0 0 0;">
                        <h3 style="margin-top: 5px;">Responsabilidad civil</h3>
                        <p style="font-size:14px;text-align: justify;">Se encargara de cubrir los gastos por daños a terceros, ya sean materiales o a personas, provocadas por el Auto o Moto asegurados.</p> 
                    </div>
                </div>

                <div class="col-lg-4 col-xs-12" style="padding: 0 0 0 10px;" id="soat_items">
                    <div class="sin-service" style="height: 380px; padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/riesgo/parciales.png" style="margin-top: 33px;">
                        <h3 style="margin-top: 50px;">Perdidas parciales</h3>
                        <p style="font-size:14px;text-align: justify;">Son aquellos daños que puede sufrir tu Auto o tu Moto, por responsabilidad del propio asegurado o por el tercero.</p> 
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12" style="padding: 0 0 0 0;" id="soat_items">

                    <div class="sin-service" style="height: 380px; padding: 0 27px; background-color: #eaeaea;">

                        <img src="assets/images/riesgo/total.png" style="width:297px;margin-top: 33px; ">
                        <h3 style="margin-top: 50px;">Pérdida total</h3>
                        <p style="font-size:14px;text-align: justify;">Se considera perdida total, cuando la separación de los daños ocacionados al Auto o a la Moto, superan el 75% de su valor comercial o hurto de vehículo.</p> 
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12" style="padding: 0 10px 0 0;" id="soat_items">
                    <div class="sin-service" style="height: 380px; padding: 0 27px; background-color: #eaeaea;">
                        <img src="assets/images/riesgo/civil.png" style="margin-top: 33px;">
                        <h3 style="margin-top: 50px;">Responsabilidad civil</h3>
                        <p style="font-size:14px;text-align: justify;">Se encargara de cubrir los gastos por daños a terceros, ya sean materiales o a personas, provocadas por el Auto o Moto asegurados.</p> 
                    </div>
                </div>
            </div>

            <br/>
            <div class="row">
                <div class="col-lg-2 col-sm-12 text-center">
                </div>
                <div class="col-lg-8 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Algunos de los amparos que se pueden contratar con los seguros de vehículos son:</h3>
                </div>
                <div class="col-lg-2 col-sm-12 text-center">
                </div>
            </div>

            <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
                <!--Slides-->
                <div class="carousel-inner" role="listbox">

                    <!--First slide-->
                    <div class="carousel-item active">

                        <div class="col-md-4" style="padding: 0px;">
                            <div class="card mb-2 text-center" style="border: none;">
                                <div class="text-center">
                                    <img class="card-img-top" src="assets/images/riesgo/hurto.png" alt="Card image cap">
                                </div>
                                <div id="seguros"  class="card-body" style="height:80px; margin-top: 30px;">
                                    <h4 class="card-title font-weight-bold text-center">Pérdida total por daños y/o Hurto</h4>
                                </div>
                                <div id="seguros_mobile"  class="card-body" style="height:50px;">
                                    <h4 class="card-title font-weight-bold text-center">Pérdida total por daños y/o Hurto</h4>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4" style="padding: 0px;">
                            <div class="card mb-2" style="border: none;">
                                <div class="text-center">
                                    <img class="card-img-top" src="assets/images/riesgo/civil.png" alt="Card image cap">
                                </div>
                                <div id="seguros" class="card-body" style="height:80px; margin-top: 30px;">
                                    <h4 class="card-title font-weight-bold text-center">Responsabilidad civil extracontractual</h4>
                                </div>
                                <div id="seguros_mobile" class="card-body" style="height:50px;">
                                    <h4 class="card-title font-weight-bold text-center">Responsabilidad civil extracontractual</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4" style="padding: 0px;">
                            <div class="card mb-2" style="border: none;">
                                <div class="text-center">
                                    <img class="card-img-top" src="assets/images/riesgo/asistencia.png" alt="Card image cap" >
                                </div>
                                <div id="seguros" class="card-body" style="height:80px; margin-top: 30px;">
                                    <h4 class="card-title font-weight-bold text-center">Asistencia al vehículo</h4>
                                </div>
                                <div id="seguros_mobile"  class="card-body" style="height:50px;">
                                    <h4 class="card-title font-weight-bold text-center">Asistencia al vehículo</h4>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--/.First slide-->

                    <!--Second slide-->
                    <div class="carousel-item">

                        <div class="col-md-4" style="padding: 0px;">
                            <div class="card mb-2" style="border: none;">
                                <div class="text-center">
                                    <img class="card-img-top" src="assets/images/riesgo/conductor.png" alt="Card image cap" style="width:42%;">
                                </div>
                                <div id="seguros" class="card-body" style="height:80px; margin-top: 30px;">
                                    <h4 class="card-title font-weight-bold text-center">Conductor elegido</h4>
                                </div>
                                <div id="seguros_mobile" class="card-body" style="height:50px;">
                                    <h4 class="card-title font-weight-bold text-center">Conductor elegido</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4" style="padding: 0px;">
                            <div class="card mb-2" style="border: none;">
                                <div class="text-center">
                                    <img class="card-img-top" src="assets/images/riesgo/reemplazo.png" alt="Card image cap">
                                </div>
                                <div id="seguros" class="card-body" style="height:80px;margin-top: 30px;">
                                    <h4 class="card-title font-weight-bold text-center">Vehículo de reemplazo</h4>
                                </div>
                                <div id="seguros_mobile" class="card-body" style="height:50px;">
                                    <h4 class="card-title font-weight-bold text-center">Vehículo de reemplazo</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4" style="padding: 0px;">
                            <div class="card mb-2" style="border: none;">
                                <div class="text-center">
                                    <img class="card-img-top" src="assets/images/riesgo/grua.png" alt="Card image cap" style="width:90%;">
                                </div>
                                <div id="seguros" class="card-body" style="height:80px; margin-top: 30px;">
                                    <h4 class="card-title font-weight-bold text-center">Servicio de grúa</h4>
                                </div>
                                <div id="seguros_mobile" class="card-body" style="height:50px;">
                                    <h4 class="card-title font-weight-bold text-center">Servicio de grúa</h4>
                                </div>
                            </div>
                        </div>
                <!--/.Second slide-->
                </div>
                <!--/.Slides-->

                 <!--Controls-->
                
                <div class="controls-top text-center" style="font-size:24px" id="banner_soat">

                    <a class="carousel-control-prev" href="#multi-item-example" role="button" data-slide="prev" style="width: 3%; margin: 0 -9px;">
                        <span class="carousel-control-prev-icon" aria-hidden="true" style="background-image:url(assets/images/riesgo/menor.png)"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#multi-item-example" role="button" data-slide="next" style="width: 3%; margin: 0 -9px;">
                        <span class="carousel-control-next-icon" aria-hidden="true" style="background-image:url(assets/images/riesgo/mayor.png)"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!--a class="btn-floating" href="#multi-item-example" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span></a>
                    <a class="btn-floating" href="#multi-item-example" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a-->
                </div>

                <div class="controls-top text-center" style="font-size:24px" id="banner_soat_mobile">
                    <a class="carousel-control-prev" href="#multi-item-example" role="button" data-slide="prev" style="width: 3%;">
                        <span class="carousel-control-prev-icon" aria-hidden="true" style="background-image:url(assets/images/riesgo/menor.png)"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#multi-item-example" role="button" data-slide="next" style="width: 3%;">
                        <span class="carousel-control-next-icon" aria-hidden="true" style="background-image:url(assets/images/riesgo/mayor.png)"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <!--/.Controls-->
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-justify">
                </br>
                    <p style="font-size:16px; line-height: 25px;">En Pronto y Seguros, su mejor opción en seguros, puede realizar un comparativo entre las pólizas ofrecidas entre varias aseguradoras reconocidas del país que pueden ofrecerle un excelente servicio al mejor precio, así como también la más calificada atención las 24 horas del día.</p>
                </div>
            </div>

        </div>
    </div>


    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Diseñemos juntos tu seguro todo riesgo, te ayudamos a que quede a tu medida.</h3>
                </div>
            </div>
        </div>
    </div>
      <!--contact-area start-->
    <div class="contact-area" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <br/><br/>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="contact-info">
                        <h3 style="font-size: 24px; margin-top: 0">Contáctanos</h3></br>
                        <!--h4 style="margin-top: 0">Resuelve tus dudas o cuéntanos sobre tu experiencia</h4-->
                        <div class="single-contact-info">
                            <h4><i class="fa fa-map-marker"></i>Dirección</h4>
                            <p>Cra. 66a No 10-15 Br .Limonar<br/>
                                Cali, Colombia</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-phone"></i>Teléfono</h4>
                            <p>Fijo: +602 4851105 Ext 703 - 701</p>
                            <p>Celular: 3218312185 - 3147916424</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-envelope"></i>Email</h4>
                            <p> jeferenovaciones@prontoyseguros.com</p>
                            <p>jefeindependientes@prontoyseguros.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="contact-form style-3">
                            
                            <form action="{{ route('enviar-email') }}" method="post" id="form-contact" accept-charset="utf-8">
                            {!! csrf_field() !!}
                            <div class="row">
                                
                                <div class="col-lg-6">
                                    <input type="text" name="nombres" placeholder="Nombre" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="apellidos" placeholder="Apellido" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="telefono" placeholder="Teléfono" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="email" placeholder="Email" required/>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="asunto" placeholder="Motivo Consulta" required/>
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="mensaje" placeholder="Mensaje" required></textarea>
                                </div>
                                <div class="col-lg-12" style="text-align: right;">
                                    <button class="btn-common" id="form-submit">Enviar Mensaje</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--div class="col-lg-12 col-md-12">
                    <p>Resuelve tus dudas o cuéntanos sobre tu experiencia</p>
                </div-->
            </div>
        </div>
    </div>
    <div class="benefit-area mt-80 ">
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Promesa de Servicio</h3>
                    <p>Resuelve tus dudas o cuéntanos sobre tu experiencia. En las próximas 24 horas recibirás nuestra llamada o si quieres información inmediata </br>nos puedes contactar a nuestro e-mail, WhatsApp o asesor virtual.</P>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div>
    <!--subscribe-area start-->
    <!--div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="subscribe-form">
                        <h3>Escribenos</h3>
                        <p>Le enviaremos noticias mensuales </p>
                        <input type="email" placeholder="Su Email" />
                        <button class="btn-common">Suscribase</button>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--subscribe-area end-->
    
    
    <!--google-map-->
    <script src="https://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.22&amp;key=AIzaSyChs2QWiAhnzz0a4OEhzqCXwx_qA9ST_lE"></script>
    <script>
        google.maps.event.addDomListener(window, 'load', init);
        function init() {
            var mapOptions = {
                zoom: 11,
                scrollwheel: true,
                center: new google.maps.LatLng(40.6700, -73.9400), // New York

                styles: 
                    [
                        {
                            "featureType": "all",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "saturation": 36
                                },
                                {
                                    "color": "#333333"
                                },
                                {
                                    "lightness": 40
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "visibility": "on"
                                },
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 17
                                },
                                {
                                    "weight": 1.2
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#dedede"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 29
                                },
                                {
                                    "weight": 0.2
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 18
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                },
                                {
                                    "lightness": 19
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#e9e9e9"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        }
                    ]
            };
            var mapElement = document.getElementById('googleMap');

            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6700, -73.9400),
                map: map
            });
        }
    </script>

   @include('layouts.footer')