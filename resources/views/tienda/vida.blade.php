
    <!--header-area end-->
    @include('layouts.master')
    
    

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-vida">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/vida.png" style="width:16%">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle" > — Seguro de Vida — </h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                        <input type='hidden' name='seguro' value='Vida' />
                                        <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;">  
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/vida.png" style="width:16%">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle" > — Seguro de Vida — </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                              <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                            </div>
                            <div>
                                <input type='hidden' name='seguro' value='Vida' />
                                <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    <!--banner-area end-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" class="font-weight-bold">Seguro de Vida:  Un apoyo para ti y tu familia en momentos inesperados</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">Adquirir un seguro de vida no debe dejarse para después: la protección, la tranquilidad y el respaldo económico para tu familia no tiene precio.  Se trata de proteger el patrimonio de tus seres queridos. Es prever y pensar en el futuro.
                        Diligencia tus datos 100% en línea en solo minutos y diseñemos juntos un seguro de vida a tu medida!</p>
                    </div>
                </div>
            </div>
           
            <!--div class="row">
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/vida/vida_digital.jpg" alt="promo"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <h3 style="font-weight: 800; margin-top: 5px; text-align:center">Seguro de Vida Digital</h3></br>
                    <div class="row">
                        <div class="col-lg-12 col-xs-10" style="padding: 0 0 0 30px;">
                            <p style="font-size: 16px;">Ahora puedes asegurarte y cuidar la calidad de vida de tus seres queridos de la manera más rápida y sencilla: 100 % en línea desde tu computador o celular.</p>
                            <p style="font-size: 16px;">¡Elige tu plan (con valores asegurados de 10, 20 o 40 millones para tus beneficiarios) y diligencia tus datos en solo minutos! Nuestro seguro te brinda:</p>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-2 col-xs-2" style="padding:0; text-align: center;">
                            <img src="assets/images/dinero.png" style="width:40%">
                        </div>
                        <div class="col-lg-10 col-xs-10" style="padding:0;">
                            <p style="font-size: 16px;">Respaldo económico en caso de accidente, enfermedad y muerte.</p>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-2 col-xs-2" style="padding:0; text-align: center;">
                            <img src="assets/images/asesor.png" style="width:40%">
                        </div>
                        <div class="col-lg-10 col-xs-10" style="padding:0; margin: 7px 0px;">
                            <p style="font-size: 16px;">Orientación virtual médica, psicológica y laboral. </p>
                        </div>
                    </div>
                    
                </div>
            </div-->


            <!--div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>EL SEGURO DE VIDA ES PARA MI, </h2>
                        <h2>PORQUE LO MAS IMPORTANTE SOY YO</h2>
                    </div>
                    <div class="row mt-30">
                        <div class="col-sm-6">

                            <div class="sin-service style-2 text-center">
                                <h4>¿Que pasaría si me diagnostican una enfermedad grave?</h4>
                                <h4>¿si sufro un accidente y no puedo recuperarme?</h4>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="sin-service style-2 text-center">
                                <h4>¿invalidez por enfermedad o accidente?</h4>
                                <h4>¿como mantener la calidad de vida de mi familia y la mía?</h4>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="sin-service style-2 text-left">
                                <div style="text-align:center"><img src="assets/images/mensaje-vida.png" alt="seguro-vida" style="width:55%" /></div>                                
                            </div>
                        </div>
                         <div class="col-sm-6">
                            <div class="sin-service style-2 text-left">
                                <h4>¿invalidez por enfermedad o accidente?</h4>
                                <p>Una solución de vida es continuar mi vida normal y la de mi familia, nuestra poliza esta diseñada  para cubrir  todas las necesidades y garantizar  la tranquilidad  de nuestros clientes en cualquier eventualidad</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <h3>Beneficios</h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            <div class="sin-service style-2 text-left">
                                <ul>
                                    <li>Cáncer</li>
                                    <li>Enfermedades Graves.</li>
                                    <li>Muerte Accidental.</li>
                                </ul> 
                            </div>
                        </div>
                        <div class="col-lg-5 col-xs-12">
                            <div class="sin-service style-2 text-left">
                                <ul>
                                    <li>Invalidez por Accidente y Enfermedad</li>
                                    <li>Gastos de Curación por Accidente (fracturas, quemaduras)</li>
                                    <li>Renta diaria por Accidente</li>
                                </ul> 
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-12">
                            <div class="sin-service style-2 text-left">
                                <ul>
                                    <li>Auxilio por Accidente</li>
                                    <li>Auxilio Exequias</li>
                                </ul> 
                            </div>
                        </div>
                         
                    </div>
                    <div class="row">                   
                        <div class="section-title style-4 text-left pt-10">
                            <a href="{{ url('contactenos') }}"><h2>Contáctanos</h2></a>
                            <p> 
                                <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/> Viviana Melo +57 316 6825935
                            </p>
                        </div>
                    </di>
                </div>
            </div-->
        </div>
    </div>
    <!--blog-area start-->
    <!--service-area start-->
    <div class="service-area mt-minus-100">
        <div class="container" style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Elige tu Plan</h3>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 0px 5px;">
                    <div class="sin-service" style="text-align: justify;">
                        <img src="assets/images/vida/vida_uno.jpg" alt="seguro vida">
                         <div class="text-center">
                            <h4 style="font-weight: 700;">Plan Vida Renta</h4>
                        </div>
                        <div style="height:110px">
                            <p style="padding: 0 10px;">Elige el Plan Vida Renta y protege el futuro de tus seres queridos para que en caso de fallecimiento o incapacidad total y permanente, reciban un renta mensual y no les falte nada.</p></br></br>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('vida_renta') }}" class="readmore"  style="padding: 0 10px;"> + Info. </a>
                        </div>
                        
                    </div> 
                   
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 0px 5px;">
                    <div class="sin-service" style="text-align: justify;">
                        
                        <img src="assets/images/vida/vida_tres.jpg" alt="promo">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Plan Vida Más</h4>
                        </div>
                        <div style="height:110px">
                            <p style="padding: 0 10px;">Adquiere tu seguro Plan Vida Más mientras cuentas con protección y respaldo ante cualquier eventualidad.</p>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('vida_mas') }}" class="readmore"  style="padding: 0 10px;"> + Info. </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 0px 5px;">
                    <div class="sin-service" style="text-align: justify;">
                        <img src="assets/images/vida/vida_dos.jpg" alt="promo">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Enfermedades Graves</h4>
                        </div>
                        <div style="height:110px">
                            <p style="padding: 0 10px;">En caso de sufrir una enfermedad de alto costo, cuenta con un capital de respaldo para ti y los tuyos.</p></br></br></br></br>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('enfermedades_graves') }}" class="readmore"  style="padding: 0 10px;"> + Info. </a>
                        </div>
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 0px 5px;">
                    <div class="sin-service" style="text-align: justify;">
                        <img src="assets/images/vida/vida_cinco.jpg" alt="promo" >
                         <div class="text-center">
                            <h4 style="font-weight: 700;">Plan Vida a la Medida</h4>
                        </div>
                        <div style="height:110px">
                            <p style="padding: 0 10px;">Arma tu plan con las mejores opciones para ti y tu familia ante invalidez, muerte accidental, gastos de curación por accidente, renta diaria por hospitalización, enfermedades graves, cáncer, auxilio de cáncer, auxilio de exequias y desempleo.</p>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('vida_medida') }}" class="readmore"  style="padding: 0 10px;"> + Info. </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 0px 5px;">
                    <div class="sin-service" style="text-align: justify;">
                        <img src="assets/images/vida/vida_cuatro.jpg" alt="salud">
                         <div class="text-center">
                            <h4 style="font-weight: 700;">Plan Protege tu Vida</h4>
                        </div>
                        <div style="height:110px">
                            <p style="padding: 0 10px;">Cuidar más de ti y ganar en tranquilidad son dos objetivos en los que avanzas con la adquisición de Plan Protege tu Vida.</p></br></br></br>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('protege_vida') }}" class="readmore"  style="padding: 0 10px;"> + Info. </a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!--service-area end-->
    <div class="row">
        <br/>
    </div>
    <!--faq-area start-->
    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Para qué sirve un Seguro de Vida?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Si te estás haciendo esta pregunta, debes saber que un Seguro de Vida te permite asegurar un monto de dinero que se convierte en respaldo económico para tus seres queridos, en caso de que les llegues a faltar. Así garantizas que, ante una pérdida, ellos puedan recibir ese dinero, conservar su calidad de vida y continuar con la consecución de sus sueños. ¡Pero no es el único caso en que sirve el seguro!</p>
                                    <p>También es una gran ayuda si llegas a sufrir alguna enfermedad grave, porque este Seguro de Vida Digital te entrega un dinero que sirve como apoyo para pagar tratamientos o cirugías que se deriven de la enfermedad, de manera que tus finanzas no sufran un impacto alto ante una situación como esa. Adicional, tu familia cuenta con un auxilio para correr con los gastos funerarios que genere la muerte del asegurado.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Quiénes son los beneficiarios de ley en un Seguro de Vida?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Son las personas que, de acuerdo con la ley, están designadas para recibir el respaldo económico de tu plan en caso de que mueras, siempre y cuando no hayas designado a nadie (o si por alguna causa tu designación no puede ejecutarse). El 50 % del dinero se entrega al cónyuge o compañero permanente y el otro 50 % a los herederos.</p>
                                    <p>Si no tienes cónyuge, el 100 % del dinero se paga a los hijos. Si no tienes hijos, el dinero se paga a tus padres. En caso de que no tengas hijos ni padres, se distribuye el 50 % de los herederos entre y cónyuge y hermanos. Si no tienes hermanos, ese 50 % de los herederos se distribuye entre cónyuges y sobrinos, y si no tienes sobrinos esa herencia se entrega al ICBF.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Cómo reclamar un Seguro de Vida?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Es sencillo tu proceso de reclamación para este Seguro de Vida Digital: solo debes comunicarte a la linea de asistencia de la aseguradora que te respalda , Allí te solicitarán los documentos que debes enviar al correo electrónico que te indiquen, ¡y listo!.</p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿Qué me cubre?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-basic">
                                        <li>Muerte por cualquier causa.</li>
                                        <li>Muerte accidental.</li>
                                        <li>Incapacidad total o permanente.</li>
                                        <li>Renta diaria por hospitalización por cualquier causa.</li>
                                        <li>Renta diaria por hospitalización en unidad de cuidados extensivos.</li>
                                        <li>Diagnóstico de enfermedades graves.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Qué no me cubre?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-basic">
                                        <li>Enfermedades diagnosticadas con anterioridad a la vigencia del seguro.</li>
                                        <li>Suicidio o intento de suicidio dentro de los primeros doce (12) meses de vigencia del seguro.</li>
                                        <li>Actos delictivos de acuerdo a la ley penal, en los que participe directa o indirectamente el asegurado.</li>
                                        <li>Radiaciones ionizantes o contaminación por radioactividad de combustible nuclear o de cualquier residuo nuclear.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        ¿Cual es la edad minima y maxima para adquirir un seguro de vida?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Se puede adquirir desde los 18 años hasta los 70 años</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">¡ Diseñemos juntos un seguro de vida a tu medida ! </h3> 
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')

    <div class="row">
        <br/><br/>
    </div>

   @include('layouts.footer')
    <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>
