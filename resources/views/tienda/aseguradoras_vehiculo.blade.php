
    <!--header-area end-->
    @include('layouts.master')

    <!--blog-area start-->

    
    <div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="https://www.segurossura.com.co/paginas/pago-express.aspx#/Pagos" target="_blank" >
                                    
                                        <img src="assets/images/blog/sura 480x420.jpg" alt="blog-image">

                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>Sura</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #888&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000518888</p>
                                        </div>
                                    </div>
                                    

    <div class="container" style="padding: 0 4px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-1 col-xs-8" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-row" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="color: #000;">Sura</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Soat</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Viaje</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-7 col-xs-9" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                <ul class="list-basic">
                <p>Suramericana S.A. es una de las mayores empresas latinoamericana en la Industria de Seguros, con presencia en nueve países, donde se presenta como Seguros SURA. 
                También en Colombia participa en el sector de seguridad social, mediante EPS SURA, ARL SURA, Dinámica y Consultoría en Gestión de Riesgos, 
                entre otros. Es reconocida por su experiencia de más de 75 años en el mercado. </p>
                </br>                
                     <div style="text-align:center">
                        <a href="https://www.segurossura.com.co/paginas/pago-express.aspx#/Pagos" target="_blank" class="btn-common" role="button">
                            <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                        </a>
                    </div>
                </div>

                
               
                <div class="tab-pane fade" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Soat Suramericana</h3>
                    </br>
                    <p>compra con nosotros el SOAT</p>
                    <div style="text-align:center">
                                 <a href="https://www.suraenlinea.com/soat/sura/seguro-obligatorio?asesor=13205" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>SOAT SURA</strong></span>
                                </a>
                        </div>
                </div>
                <div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Homicidio</li>
                        <li>Incapacidad total y permanente por accidente</li>
                        <li>Incapacidad permanente parcial por accidente</li>
                        <li>Desmembración accidental</li>
                        <li>Gastos médicos por accidente</li>
                        <li>Renta diaria por hospitalización por accidente</li>
                        <li>Renta por incapacidad total temporal</li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos" role="tabpanel" aria-labelledby="pills-documentos-tab">
                    <h3 style="margin-top: 0;">Documentos</h3>
                    <p dir="ltr">Conoce en las condiciones generales del Seguro Exequial, las exclusiones y más detalles de este producto.</p>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_DE _CANCELACION.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i> Condiciones generales</a>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_DE _CANCELACION.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i> Cancelación del Seguro Exequial</a>
                        </div>  
                    </div>  
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md- col-xs-5" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link active" id="pills-profile-mobile-tab" data-toggle="pill" href="#pills-profile-mobile" role="tab" aria-controls="pills-profile-mobile" aria-selected="false" style="color: #000;">Sura</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Cobertura Básica</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas Adicionales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
               
                <div class="tab-pane fade show active" id="pills-profile-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Por qué tener este seguro</h3>
                    <p>Cuando en un evento accidental la persona que genera ingresos en una familia muere o pierde la capacidad de desarrollar un trabajo remunerado, hay un deterioro significativo en la calidad de vida de todos los miembros de este grupo familiar, ya que los ingresos se ven mermados y los gastos generalmente se incrementan, especialmente, cuando quien sufre el accidente queda en estado de incapacidad o invalidez.</p>
                    <p>El seguro de Accidentes Personales es una herramienta que permite evitar este tipo de situaciones, pues en caso de no tener esta protección, la familia tendría que recurrir al gasto de los ahorros, pedir dinero a familiares o amigos, endeudarse con el sistema financiero formal o informal, reducir los gastos de la familia para ajustar el presupuesto a la nueva realidad o buscar otras fuentes de ingresos, entre otros.</p>
                </div>
                <div class="tab-pane fade" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Cobertura Básica</h3>
                    </br>
                    <p>Muerte Accidental</p>
                    <h3 style="margin-top: 0;">Condiciones Generales</h3>
                    <p>Las condiciones generales de los Seguros Su Futuro Seguro MAPFRE definen los requisitos que deben cumplir los asegurados para beneficiarse de todas las garantías establecidas en el acuerdo.</p>
                    
                </div>
                <div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Homicidio</li>
                        <li>Incapacidad total y permanente por accidente</li>
                        <li>Incapacidad permanente parcial por accidente</li>
                        <li>Desmembración accidental</li>
                        <li>Gastos médicos por accidente</li>
                        <li>Renta diaria por hospitalización por accidente</li>
                        <li>Renta por incapacidad total temporal</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                </div>
                
            </div>
            

        </div>

    </div>














                                   
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingTOne">
                                        <h5 class="mb-0">
                                            <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>Se diferencia por contar con una oferta multisegmento, 
                                                multicanal y multiregión para acompañar a sus más de 17 millones de clientes con soluciones de seguros y gestión de tendencias y riesgos, 
                                                que brindan bienestar y competitividad sostenibles a las personas y organizaciones. Es filial de Grupo SURA, que cuenta con el 81,1 % 
                                                de la sociedad. También participa la alemana Munich Re, con 18,9 % de propiedad.
                                            </br>
                                            9 Paises
                                            </br>
                                            17.6 millones De clientes
                                            </br>
                                            17.745 Colaboradores
                                            </br>
                                            22 mil Asesores
                                            </p>
                                            
                                        </div>
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                    </div>

                    <!--div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/aig 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>AIG</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #***&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> **********</p>
                                        </div>
                                    </div>
                                    <p>AIG-Metropolitana de Seguros y Reaseguros S.A. es parte de AIG, compañía líder en seguros a nivel mundial con más de 90 años de experiencia y servicio al cliente. Nuestra principal fortaleza radica en los 40.000 empleados que combinan el alcance global, con la capacidad para atender y servir a nuestros clientes en más de 160 países y jurisdicciones. </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingTwo">
                                        <h5 class="mb-0">
                                            <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>También en Colombia participa en el sector de seguridad social, mediante EPS SURA, ARL SURA, Dinámica y Consultoría en Gestión de Riesgos, 
                                                entre otros. Es reconocida por su experiencia de más de 75 años en el mercado. Se diferencia por contar con una oferta multisegmento, 
                                                multicanal y multiregión para acompañar a sus más de 17 millones de clientes con soluciones de seguros y gestión de tendencias y riesgos, 
                                                que brindan bienestar y competitividad sostenibles a las personas y organizaciones. Es filial de Grupo SURA, que cuenta con el 81,1 % 
                                                de la sociedad. También participa la alemana Munich Re, con 18,9 % de propiedad.Nos especializamos en seguros industriales y personales con más de 500 productos y servicios innovadores, un profundo conocimiento y excelencia en gestión de siniestros y una gran solidez financiera. Como resultado, más de 88 millones de clientes en todo el mundo confían en nosotros para satisfacer sus necesidades.
                                                </br>
                                                AIG-Metropolitana de Seguros y Reaseguros S.A. tiene 44 años de presencia en Ecuador. Nuestro objetivo es brindar a nuestros clientes acceso a una atención personalizada y con profesionales que entienden sus necesidades particulares, con todo el respaldo y los recursos de un líder mundial.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div-->

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/allianz 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>Allianz</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #265 &nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000513500</p>
                                        </div>
                                    </div>

                                    <p>Colseguros fue la primera aseguradora fundada en Colombia en 1874, y una de las empresas más grandes en el país, con 145 años al servicio de los colombianos.
                                        Allianz es uno de los aseguradores más grandes del mundo, atendiendo más de 85 millones de clientes, todo gracias al esfuerzo que desde 1890 seguimos poniendo día a día por entregarle la tranquilidad a cada uno de los que nos ponen su confianza en nosotros.
                                        
                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingThree">
                                        <h5 class="mb-0">
                                            <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>En Allianz Colombia seguiremos comprometidos con asegurar el futuro de cada colombiano, ofreciéndole soluciones que se ajusten a todas sus necesidades.<br/>Desde 1890, en todo el mundo, en Allianz hemos estado trabajando duro para asegurar la vida de las personas y dar coraje a nuestros clientes para lo que está por venir.
                                                Somos actuarios, asesores y agentes de servicio; ingenieros, abogados y expertos técnicos; somos hijas e hijos, madres y padres, contadores, inversores y empresarios, y juntos estamos dando forma a nuestra industria.
                                                Porque sabemos lo importante que es tener un socio justo a su lado que brinde soluciones sólidas y sostenibles, nos esforzamos por hacerlo bien, con pasión, todos los días.

                                                </br>
                                                
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://www.allianz.co/clientes/todos-los-clientes/pagos.html" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/axa colpatria 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                     <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>Axa Colpatria</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #247&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000512620</p>
                                        </div>
                                    </div>

                                    <p>Somos un equipo humano que se reinventa constantemente para satisfacer las necesidades de Seguros Generales, Seguros de Vida, Salud, Riesgos Laborales y Ahorro de los colombianos.
                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingFour">
                                        <h5 class="mb-0">
                                            <a href="#collapseFour" class="btn btn-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>AXA COLPATRIA es la unión entre la aseguradora más importante a nivel mundial, AXA, compañía de seguros número 1 a nivel mundial, con presencia en 59 países y alrededor de 103 millones de clientes en el mundo y Seguros Colpatria, compañía con 57 años de experiencia en el mercado colombiano, cuyo capital sólido y reputado le ha permitido destacarse en las líneas de negocio de Seguros Generales, Seguros de Vida, Salud, Riesgos Laborales y Capitalizadora.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://www.arl-colpatria.co/pagosenlinea/opcionespago" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/bolivar 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>Seguros Bolivar</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #322&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000123322</p>
                                        </div>
                                    </div>
                                    
                                    <p>Somos una aseguradora colombiana que apoya el desarrollo del país; dando seguridad a nuestros clientes con soluciones ajustadas a sus necesidades.
                                        <br/>
                                        ENRIQUECER LA VIDA CON INTEGRIDAD 
                                        En Seguros Bolívar este propósito se hace latente en la medida que enriquecemos la vida con integridad porque protegemos y restablecemos la vida cotidiana de las familias y empresas colombianas generando tranquilidad.
                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingFive">
                                        <h5 class="mb-0">
                                            <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>PRINCIPIOS Y VALORES <br/> La esencia de nuestra cultura tiene como centro el ser humano, y a partir de este eje, se ponen en práctica los principios y valores, la misión y el propósito superior.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://www.segurosbolivar.com/RecaudosElectronicos/faces/muestrapagos.jspx/pages/layout/consultUser.action" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/estado 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>Seguros del estado</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #388&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000123010</p>
                                        </div>
                                    </div>

                                    <p>MEGA (Meta Estratégica Grande y Ambiciosa)
                                        <br/>
                                        Para el año 2021 nuestra aseguradora conservará su posición como segunda compañía del mercado, registrando un índice combinado igual o menor al 98%, que le permita fortalecer su posición patrimonial. Así mismo, se ofrecerán mejores experiencias a los clientes internos y externos, fortaleciendo la calidad de nuestros servicios a través de la transformación digital.<br/> 
                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingSix">
                                        <h5 class="mb-0">
                                            <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>Valores<br/>
                                            Es obligación de los colaboradores de Seguros del Estado S.A. y Seguros de Vida del Estado S.A. anteponer el cumplimiento de los principios y valores éticos a la consecución de las metas comerciales y de observar las normas que regulan las relaciones entre sí y con los clientes.
                                            <br/> Respeto - Lealtad - Rectitud - Honestidad - Transparencia - Igualdad - Disciplina - Responsabilidad

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://www.segurosdelestado.com/pages/Tips" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/hdi 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>HDI</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #204&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000121206</p>
                                        </div>
                                    </div>
                                    
                                    <p>HDI Seguros es una empresa multicanal y multiproducto, enfocada en la protección de las personas, las familias y las empresas. Tiene presencia en las principales ciudades del país: Bogotá, Barranquilla, Cartagena, Montería, Bucaramanga, Medellín, Manizales, Armenia, Pereira, Ibagué, Neiva y Cali, con 27 oficinas, alrededor de 1300 asesores de seguros y cuenta con más de 30.000 puntos de pago a nivel nacional.
                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingSeven">
                                        <h5 class="mb-0">
                                            <a href="#collapseSeven" class="btn btn-link" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>Historia Colombia <br/>
                                            En diciembre de 1937, un grupo de aseguradoras británicas fundaron la Compañía de Seguros LA ANDINA, siendo ésta la segunda aseguradora más antigua de Colombia. En el año 1994, el accionista mayoritario de Royal Insurance Company, decide vender su participación accionaria al Grupo Generali, de origen italiano, grupo que operaba en Colombia desde 1952, con la Compañía Granadina de Seguros S.A. y desde 1957 con la Compañía Granadina de Seguros de Vida S.A.
                                            En 1996, Seguros La Andina se fusiona con Granadina de Seguros S.A. dando origen a Generali Colombia Seguros Generales S.A. y Granadina de Seguros de Vida S.A. cambió su denominación social por Generali Colombia Vida Compañía de Seguros S.A., operando de ésta manera hasta el año 2018, cumpliendo así con más de 80 años de operación.


                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://www.hdi.com.co/pagos.aspx" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/liberty 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>Liberty Seguros</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #224&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000117224</p>
                                        </div>
                                    </div>
                                    
                                    <p>En los últimos 100 años, Liberty se ha comprometido a ayudar a las personas a preservar y proteger lo que ganan, construyen, poseen y valoran. Al mirar hacia el futuro, recordamos los hitos de un siglo que fueron posibles gracias a nuestros empleados, socios y, lo que es más importante, a nuestros clientes.
                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingEigth">
                                        <h5 class="mb-0">
                                            <a href="#collapseEigth" class="btn btn-link" data-toggle="collapse" data-target="#collapseEigth" aria-expanded="false" aria-controls="collapseEigth">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseEigth" class="collapse" aria-labelledby="headingEigth" data-parent="#accordion">
                                        <div class="card-body">
                                            <p><span style="font-weight: bold;">Responsabilidad social</span>
                                                <br/>
                                                Tenemos una responsabilidad en ayudar a vivir vidas más seguras y protegidas
                                                En Liberty Seguros creemos que el compromiso con nuestros clientes es tan importante como nuestro impacto en las comunidades; por eso cada año movilizamos en todo el mundo más de 50 millones de dólares de inversión a la comunidad que se ven reflejados en tiempo, dinero y recursos.
                                                <br/><span style="font-weight: bold;">
                                                Nuestro aporte</span>
                                                <br/>
                                                Sirviendo con Liberty
                                                <br/>
                                                Servir con Liberty: en 2018 construimos y entregamos 14 hogares para familias colombianas en condición de vulnerabilidad y compartimos con los niños del barrio Los Cerezos en Soacha en una jornada de educación para la paz
                                                
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://aplicaciones.libertyseguros.co/caja_external/faces/start?_adf.ctrl-state=weoa5vevo_3" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/mapfre 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>MAPRFE</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #624&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000977025</p>
                                        </div>
                                    </div>
                                    
                                    <p>Desde nuestra llegada al país, hace más de 30 años, estamos garantizando la tranquilidad de los colombianos, asumiendo riesgos con clara vocación de servicio, permanencia y estabilidad, apoyados en grandes inversiones en tecnología, infraestructura y fomento social. 
                                        
                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingNine">
                                        <h5 class="mb-0">
                                            <a href="#collapseNine" class="btn btn-link" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>Nuestro amplio portafolio de productos, servicios y valores agregados, sumado a una extensa red de oficinas a nivel nacional, talento humano altamente capacitado, permanente espíritu de innovación a todo nivel y el firme propósito de fomentar difundir la cultura del seguro del país nos convierte en una excelente opción aseguradora en el mercado. 
                                                <br/>
                                                <span style="font-weight: bold;">Responsabilidad social</span>
                                                <br/>
                                                Nuestras actividades aseguradoras se desarrollan a través de 2 sociedades aseguradoras: MAPFRE SEGUROS GENERALES DE COLOMBIA S.A y MAPFRE COLOMBIA VIDA SEGUROS S.S. Otras empresas del grupo MAPFRE en Colombia son: CREDIMAPFRE, MAPFRE SERVICIOS EXEQUIALES, ANDI ASISTENCIA, MAPFRE RE, SOLUNION, CESVI COLOMBIA.
                                                <br/><span style="font-weight: bold;">
                                                Principales Magnitudes:</span> <br/>
                                                ●     1.8 millones de clientes asegurados<br/>
                                                ●     198.305 Autos asegurados<br/>
                                                ●     114.295 Negocios y casas aseguradas<br/>
                                                ●     970 colaboradores <br/>
                                                ●     139 oficinas <br/>
                                                ●     1.150 empleados <br/>
                                                ●     2.619 intermediarios <br/>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://cotiza.mapfre.com.co/pagosWeb/vista/paginas/noFilterIniPagosPublico.jsf" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/mundial 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>Mundial de seguros</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #935&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000111935</p>
                                        </div>
                                    </div>
                                    
                                    <p>Seguros de Vida S.A.<br/>
                                        Se crea la Compañía Internacional de Seguros de Vida S.A., dedicada exclusivamente a la comercialización de seguros de personas.
                                        Desde su fundación, Seguros Mundial ha forjado su imagen de solidez y seriedad en el mercado asegurador, lo que le ha permitido participar de manera competitiva en todos los negocios inherentes a su actividad.

                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingTen">
                                        <h5 class="mb-0">
                                            <a href="#collapseTen" class="btn btn-link" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>Entendiendo la apertura del mercado asegurador como una oportunidad para crecer, progresar y prestar un mejor servicio a cada uno de sus clientes, Seguros Mundial diseñó y desarrolló un Plan Estratégico que determinó como estrategia la especialización en los ramos de Cumplimiento, Judiciales, Aviación, SOAT, Vida Grupo Educativo, Accidentes Personales y Seguro de Crédito.
                                                <br/>
                                                El Plan Estratégico de Seguros Mundial está integrado al Sistema de Gestión de Calidad que nos permite trabajar sistemáticamente para mejorar los procesos de la organización, garantizando la satisfacción de las necesidades y expectativas de nuestros clientes bajo los más altos estándares de calidad y servicio.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://recaudos.mundialseguros.com.co/" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/previsora 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>Previsora</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #345&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000910554</p>
                                        </div>
                                    </div>
                                    
                                    <p>La Previsora S.A. es una sociedad de economía mixta del orden nacional, sometida al régimen de las empresas industriales y comerciales del Estado. Cuenta con personería jurídica y autonomía administrativa, está vinculada al Ministerio de Hacienda y Crédito Público y es vigilada por la Superintendencia Financiera de Colombia.
                                       
                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingEleven">
                                        <h5 class="mb-0">
                                            <a href="#collapseEleven" class="btn btn-link" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>En la actualidad, La Previsora S.A. está constituida como una de las principales entidades del sector asegurador colombiano, contando con un amplio portafolio de productos en ramos de seguros generales, patrimoniales y de personas y prestando sus servicios con una amplia presencia nacional.
                                                <br/>
                                                La Previsora es una aseguradora estatal colombiana que se creó en 1954. La compañía, que está vinculada con el Ministerio de Hacienda y Crédito Público, ingresó al sector privado en 1990. La Previsora opera en los segmentos de accidentes en el trabajo, vida individual y rentas vitalicias, vida grupal, accidentes personales y gastos funerarios. La aseguradora era conocida como Previsora Vida hasta el 2008. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="http://200.24.63.5/portal2/previsora3/previpagos/" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="single-blog style-3 mb-60">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="blog-thumb">
                                    <a href="#"><img src="assets/images/blog/sbs 480x420.jpg" alt="blog-image"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 p-0">
                                <div class="blog-desc text-left">
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-6">
                                            <h3>SBS</h3>
                                        </div>
                                        <div class="col-lg-6 col-xs-6" style="text-align:right">
                                            <p style="font-weight:800;"><span  style="color:#e2032c">Linea Asistencia:</span> #360&nbsp&nbsp&nbsp -&nbsp&nbsp&nbsp<span  style="color:#e2032c">Fijo:</span> 018000522244</p>
                                        </div>
                                    </div>
                                    
                                    <p><span style="font-weight: bold;">Nuestra Misión</span><br/>
                                       Tus sueños y proyectos nos inspiran para asegurar tu futuro con soluciones ágiles, justas y amigables.<br/>
                                       <span style="font-weight: bold;">Nuestra Visión</span><br/>
                                       Ser reconocidos como la Compañía de seguros más justa, transparente y amigable, destacándose en innovación y servicio, generando valor para clientes, aliados, colaboradores y la comunidad
                                    </p>
                                </div>
                                
                                <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify">
                                    <div class="card-header faq-heading" id="headingTwelve">
                                        <h5 class="mb-0">
                                            <a href="#collapseTwelve" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                Leer Mas
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>Aseguradora Solidaria de Colombia fue reconocida como la Primera Mejor Empresa para trabajar en Colombia para las Mujeres en el ranking que desarrolló el instituto Great Place To Work, así mismo, recibió el galardón como la Segunda Mejor Empresa para trabajar en América Latina y como Segunda Mejor Empresa para trabajar en Colombia
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://checkout.payulatam.com/invoice-collector/#/login/661764" target="_blank" class="btn-common" role="button">
                                    <span class="fl-button-text"><strong>PAGO EN LÍNEA</strong></span>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                
            </div>
        </div>
    </div>
    <!--blog-area end-->
   
      <div class="row">
        <br/><br/>
    </div>
    <!--subscribe-area start-->
    <!--div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="subscribe-form">
                        <h3>Escribenos</h3>
                        <p>Le enviaremos noticias mensuales </p>
                        <input type="email" placeholder="Su Email" />
                        <button class="btn-common">Suscribase</button>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--subscribe-area end-->
    
    
    <!--google-map-->
    <script src="https://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.22&amp;key=AIzaSyChs2QWiAhnzz0a4OEhzqCXwx_qA9ST_lE"></script>
    <script>
        google.maps.event.addDomListener(window, 'load', init);
        function init() {
            var mapOptions = {
                zoom: 11,
                scrollwheel: true,
                center: new google.maps.LatLng(40.6700, -73.9400), // New York

                styles: 
                    [
                        {
                            "featureType": "all",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "saturation": 36
                                },
                                {
                                    "color": "#333333"
                                },
                                {
                                    "lightness": 40
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "visibility": "on"
                                },
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 17
                                },
                                {
                                    "weight": 1.2
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#dedede"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 29
                                },
                                {
                                    "weight": 0.2
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 18
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                },
                                {
                                    "lightness": 19
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#e9e9e9"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        }
                    ]
            };
            var mapElement = document.getElementById('googleMap');

            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6700, -73.9400),
                map: map
            });
        }
    </script>

   @include('layouts.footer')