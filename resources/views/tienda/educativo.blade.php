
    <!--header-area end-->
    @include('layouts.master')
    
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-educativo">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/educativo.png" style="width:16%">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle" > — Seguro Educativo — </h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                        <input type='hidden' name='seguro' value='Educativo' />
                                        <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;">  
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -30px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/educativo.png" style="width:16%">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle" > — Seguro Educativo — </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                <input type='hidden' name='seguro' value='Educativo' />
                                <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
   <!--banner-area end-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Seguro Educativo: Planea  desde  hoy el proyecto educativo de tus  hijos y seres  queridos.</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">El seguro educativo permite, a través de una planeación bien estructurada de las necesidades futuras de gasto en educación de sus seres queridos, asegurar la formación académica.</p>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/banners/educativo_uno.jpg" alt="promo"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <ul class="list-basic"  style="margin-top: 90px" >
                        <li id="listado">Asegura el colegio, la universidad y/o el posgrado de quien desees.</li>
                        <li id="listado">Garantiza  la  educación  universitaria y posgrado de  tus hijos y seres queridos.</li>
                        <li id="listado">Cuentas con un seguro de vida que te garantizará alcanzar tu meta de estudio en caso de muerte o incapacidad.</li>
                        <li id="listado">Puedes disponer de un auxilio educativo semestral, en caso de  fallecimiento o  incapacidad total y permanente  del asegurado.</li>
                        <li id="listado">Puedes destinarlo para estudios en Colombia o en el exterior.</li>
                        <li id="listado">Flexibilidad en el diseño del plan, con base a tu capacidad economica.</li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
     <div class="row">
        <br/>
    </div>

   
    <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Condiciones Generales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h3 style="margin-top: 0;">Beneficios</h3>
                    <p>Los seguros Educativos te permiten anticipar los costos que conlleva a la educación universitaria de tus hijos o beneficiarios, gracias a la cual podrán tener un futuro lleno de éxitos. Con esta póliza podrás adelantarte al futuro a través de una cómoda y flexible inversión, y además te aprovecharás de todos los beneficios y ventajas que ofrece.</p>
                    <ul>
                        <li>Con esta póliza podrás beneficiarte de importantes valores de rescate y contarás con ciertos valores asegurados garantizados aunque el valor futuro de la matrícula sea menor e incluso cuando el beneficiario estudie en el extranjero.</li>
                        <li>Contratando su  SEGURO  EDUCATIVO.</li>
                        
                    </ul>

                    <h3>Los beneficios de la póliza SU FUTURO SEGURO son:</h3>
                        
                     <ul>
                        <li>Renta educativa universitaria: esta póliza te asegura el pago de la carrera universitaria que elija el beneficiario, sin tener en cuenta el lugar o la fecha en que se curse, y siempre en función del valor seleccionado.</li>
                        <li>Fallecimiento por cualquier motivo: en el caso de que el asegurado fallezca antes del vencimiento del contrato, esta póliza asume los pagos correspondientes hasta que el beneficiario comience la universidad.</li>
                        <li>Invalidez total y permanente: el beneficio es idéntico al anterior, aunque la causa sea invalidez total y permanente del asegurado.</li>
                    </ul>

                </div>
                <div class="tab-pane fade" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Condiciones Generales</h3>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <br/>
                            <a href="assets/documentos/CONDICIONES_GENERALES_SEGURO_EDUCATIVO.pdf" class="btn btn-default pull-left" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Condiciones generales</a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Coberturas</h3>
                    <h4>Renta educativa universitaria</h4>
                    <p>Pagaremos, de acuerdo con el valor que hayas seleccionado, la carrera universitaria escogida por el beneficiario independientemente del lugar y la fecha en que la curse.</p>
                    </br>
                    <h4>Fallecimiento por cualquier causa</h4>
                    <p>En caso de fallecimiento por cualquier causa del asegurado, pagaremos el colegio al beneficiario hasta su entrada en la universidad, conforme con los valores contratados.</p>
                    </br>
                    <h4>Invalidez total y permanente</h4>
                    <p>En caso de invalidez total y permanente del asegurado, pagaremos el colegio al beneficiario hasta su entrada en la Universidad, conforme con los valores contratados.</p>
                    <ul>
                        <li>Este seguro garantiza los costos relacionados con la educación superior de los hijos o parientes del titular de la póliza.</li>
                        <li>Para beneficiarse del amparo de fallecimiento, la edad mínima del asegurado debe ser de 18 años y la máxima, 70 años.</li>
                        <li>En el caso del amparo de invalidez total y permanente, la edad mínima de ingreso a la póliza como asegurado es de 18 años y la máxima, 60 años.</li>
                        <li>El beneficiario podrá utilizar esta póliza para realizar sus estudios en una Institución de Educación Superior dentro del territorio colombiano o en el extranjero.</li>
                        <li>Si lo desea, el beneficiario puede cambiar de carrera universitaria o de centro, siempre que la nueva elección esté dentro de los parámetros autorizados.</li>
                        <li>También se incluye la formación religiosa y la relacionada con aviación o instituciones militares.</li>
                        <li>El rescate se puede efectuar después del primer año de vigencia y siempre que la prima de la póliza haya sido pagada.</li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos" role="tabpanel" aria-labelledby="pills-documentos-tab">
                    <h3 style="margin-top: 0;">Documentos</h3>
                    <p dir="ltr">Conoce en las condiciones generales del Seguro Exequial, las exclusiones y más detalles de este producto.</p>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_DE _CANCELACION.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i> Condiciones generales</a>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_DE _CANCELACION.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i> Cancelación del Seguro Exequial</a>
                        </div>  
                    </div>  
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-home-mobile" role="tab" aria-controls="pills-home-mobile" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-profile-mobile-tab" data-toggle="pill" href="#pills-profile-mobile" role="tab" aria-controls="pills-profile-mobile" aria-selected="false" style="color: #000;">Condiciones Generales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home-mobile" role="tabpanel" aria-labelledby="pills-home-mobile-tab">
                    <h3 style="margin-top: 0;">Beneficios</h3>
                    <p>Los seguros Educativos te permiten anticipar los costos que conlleva a la educación universitaria de tus hijos o beneficiarios, gracias a la cual podrán tener un futuro lleno de éxitos. Con esta póliza podrás adelantarte al futuro a través de una cómoda y flexible inversión, y además te aprovecharás de todos los beneficios y ventajas que ofrece.</p>
                    <ul>
                        <li>Con esta póliza podrás beneficiarte de importantes valores de rescate y contarás con ciertos valores asegurados garantizados aunque el valor futuro de la matrícula sea menor e incluso cuando el beneficiario estudie en el extranjero.</li>
                        <li>Contratando su  SEGURO  EDUCATIVO.</li>
                        
                    </ul>

                    <h3>Los beneficios de la póliza SU FUTURO SEGURO son:</h3>
                        
                     <ul>
                        <li>Renta educativa universitaria: esta póliza te asegura el pago de la carrera universitaria que elija el beneficiario, sin tener en cuenta el lugar o la fecha en que se curse, y siempre en función del valor seleccionado.</li>
                        <li>Fallecimiento por cualquier motivo: en el caso de que el asegurado fallezca antes del vencimiento del contrato, esta póliza asume los pagos correspondientes hasta que el beneficiario comience la universidad.</li>
                        <li>Invalidez total y permanente: el beneficio es idéntico al anterior, aunque la causa sea invalidez total y permanente del asegurado.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-profile-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Condiciones Generales</h3>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/CONDICIONES_GENERALES_SEGURO_EDUCATIVO.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Condiciones generales</a>
                        </div>
                    </div>
                </div>
                
                <div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Coberturas</h3>
                    <h4>Renta educativa universitaria</h4>
                    <p>Pagaremos, de acuerdo con el valor que hayas seleccionado, la carrera universitaria escogida por el beneficiario independientemente del lugar y la fecha en que la curse.</p>
                    </br>
                    <h4>Fallecimiento por cualquier causa</h4>
                    <p>En caso de fallecimiento por cualquier causa del asegurado, pagaremos el colegio al beneficiario hasta su entrada en la universidad, conforme con los valores contratados.</p>
                    </br>
                    <h4>>Invalidez total y permanente</h4>
                    <p>En caso de invalidez total y permanente del asegurado, pagaremos el colegio al beneficiario hasta su entrada en la Universidad, conforme con los valores contratados.</p>
                    <ul>
                        <li>Este seguro garantiza los costos relacionados con la educación superior de los hijos o parientes del titular de la póliza.</li>
                        <li>Para beneficiarse del amparo de fallecimiento, la edad mínima del asegurado debe ser de 18 años y la máxima, 70 años.</li>
                        <li>En el caso del amparo de invalidez total y permanente, la edad mínima de ingreso a la póliza como asegurado es de 18 años y la máxima, 60 años.</li>
                        <li>El beneficiario podrá utilizar esta póliza para realizar sus estudios en una Institución de Educación Superior dentro del territorio colombiano o en el extranjero.</li>
                        <li>Si lo desea, el beneficiario puede cambiar de carrera universitaria o de centro, siempre que la nueva elección esté dentro de los parámetros autorizados.</li>
                        <li>También se incluye la formación religiosa y la relacionada con aviación o instituciones militares.</li>
                        <li>El rescate se puede efectuar después del primer año de vigencia y siempre que la prima de la póliza haya sido pagada.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                
            </div>
            

        </div>

    </div>
    <div class="row">
        <br/><br/>
    </div>
   
    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Cuál es el mejor seguro educativo en Colombia?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>El que te permite estructurar de acuerdo a  tus requerimientos  y planes de estudio que se persigan, por lo que el mejor seguro educativo debe ser lo suficientemente flexible para adaptarse a cualquier circunstancia,brindando   tranquilidad a  tu vida.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Comó adquirir un seguro estudiantil?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Adquirir un seguro educativo es un proceso sencillo; solo se debe consultar a nuestra  agencia  de seguro,  cual es la mejor opción que   se adapte a las necesidades del tomador, el perfil del beneficiario y al presupuesto disponible.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Por qué tener un seguro educativo?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Sirve para ahorrar dinero y garantizar que tus hijos al crecer reciban una formación universitaria de calidad, en universidades privadas que estén ubicadas dentro del territorio colombiano o en el extranjero.</p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿Quién paga el seguro estudiantil?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>
                                        Cualquier persona con parentesco con el beneficiario, puede ejercer como asegurado y pagar la póliza de seguros para estudios universitarios. Por tal motivo, el beneficiario o los beneficiarios pueden ser tus hijos, tus sobrinos, tus nietos, tu hermano menor, tu ahijado, el hijo de tu amigo, el hijo de un empleado.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Qué cubre el seguro estudiantil?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <p>El seguro educativo depende directamente de la póliza contratada; sin embargo, la mayoría de las aseguradoras ofrecen a los clientes que desean garantizar la educación de sus hijos las siguientes coberturas: </p>
                                    <ul class="list-basic">
                                        <li>Renta educativa universitaria hasta finalizar la carrera</li>
                                        <li>Renta educativa general por invalidez</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        ¿Cómo funciona un seguro educativo?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Quién tiene hijos, en ocasiones se pregunta por el futuro: ¿Qué sucedera si en el futuro estoy desempleado o si ocurriera un hecho inesperado y mis hijos desean estudiar en la universidad.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Aseguramos los proyectos educativos desde que son sueños.</h4>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    <div class="row">
        <br/><br/>
    </div>
    <!--banner-area end-->
    <!--blog-area start-->
    <!--div class="blog-area mt-100 sm-mt-80">
        <div class="container">
             <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>SEGURO EDUCATIVO</h2>
                    </div>
                </div>
                
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="blog-details"   >

                       <div class="blog-details-text">
                        <br/><br/>
                            <h3 style="text-align:center">Alguna vez se ha preguntado!</h3>
                            <h4 style="text-align:center">¿Podré pagar la educación de mis hijos?</h4>
                            <h4 style="text-align:center">¿Contasré con los recursos para hacerlo?</h4>
                            <br/><br/>
                            <h4>Asegura el futuro de tus hijos desde ahora</h4>
                            <p>El producto RENTA EDUCATIVA te permite asegurar el futuro de tus hijos desde $565.000 mensuales en un plan de 5 años. </p>
                            <h4>Beneficios:</h4>
                            
                            <ul style="list-style-type: circle;">
                                <li>Devolucion del 100% ahorrado mas rentabilidad en caso de no llevarse a cabo el plan.</li>
                                <li>Cobertura primaria, secundaria y universidad en caso de fallecimiento o diagnostico de incapacidad total del tomador de la poliza.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div style="text-align:center">
                        <img src="assets/images/el-estudio-tu-mejor-opcion.jpg" alt="blog-image">
                    </div>
                </div>
            </div>
            <br/><br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>SEGURO EDUCATIVO</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div style="text-align:center">
                        <img src="assets/images/blog/educativo.png" alt="blog-image">
                    </div>
                    <br/>
                    <div class="blog-details">

                        <div class="blog-details-text">
                            <p>La educación ha sido herramienta indispensable para llegar lejos, y seguirá siéndolo para las futuras generaciones. 
                                Por eso, hemos diseñado una Póliza que te permite anticiparte al mañana y mediante una cómoda y flexible inversión, 
                                puedes asegurar desde hoy la educación universitaria de tus hijos o beneficiarios, de acuerdo a tus posibilidades presentes.</p>
                            
                            <h3>Beneficios y Ventajas</h3>
                            
                            <p>Los seguros Su Futuro Seguro te permiten anticipar los costos que conllevará la educación universitaria de tus hijos o beneficiarios, 
                                gracias a la cual podrán tener un futuro lleno de éxitos. Con esta póliza podrás adelantarte al futuro a través de una cómoda y flexible 
                                inversión, y además te aprovecharás de todos los beneficios y ventajas que ofrece.</p>
                            <ul style="list-style-type: circle;">
                                <li>Con esta póliza podrás beneficiarte de importantes valores de rescate y contarás con ciertos valores asegurados garantizados aunque 
                                    el valor futuro de la matrícula sea menor e incluso cuando el beneficiario estudie en el extranjero.</li>
                                <li>Contratando Su Futuro Seguro, la alta rentabilidad está asegurada.</li>
                                
                            </ul>

                            <h3>Los beneficios de la póliza SU FUTURO SEGURO son:</h3>
                                
                             <ul style="list-style-type: circle;">
                                <li>Renta educativa universitaria: esta póliza te asegura el pago de la carrera universitaria que elija el beneficiario, sin tener en cuenta el lugar o la fecha en que se curse, y siempre en función del valor seleccionado.</li>
                                <li>Fallecimiento por cualquier motivo: en el caso de que el asegurado fallezca antes del vencimiento del contrato, esta póliza asume los pagos correspondientes hasta que el beneficiario comience la universidad.</li>
                                <li>Invalidez total y permanente: el beneficio es idéntico al anterior, aunque la causa sea invalidez total y permanente del asegurado.</li>
                            </ul>
                        </div>
                   
                    </div>
                
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="blog-details">

                        <div class="blog-details-text">
                            
                            <h3>Coberturas</h3>
                            <p><span style="font-weight: 800; color: red">Renta educativa universitaria</span>
                            <p>Pagaremos, de acuerdo con el valor que hayas seleccionado, la carrera universitaria escogida por el beneficiario independientemente del lugar y la fecha en que la curse.</p>
                            
                            <p><span style="font-weight: 800; color: red">Fallecimiento por cualquier causa</span>
                            <p>En caso de fallecimiento por cualquier causa del asegurado, pagaremos el colegio al beneficiario hasta su entrada en la universidad, conforme con los valores contratados.</p>
                            
                            <p><span style="font-weight: 800; color: red">Invalidez total y permanente</span>
                            <p>En caso de invalidez total y permanente del asegurado, pagaremos el colegio al beneficiario hasta su entrada en la Universidad, conforme con los valores contratados.</p>
                            <ul style="list-style-type: circle;">
                                <li>Este seguro garantiza los costes relacionados con la educación superior de los hijos o parientes del titular de la póliza.</li>
                                <li>Para beneficiarse del amparo de fallecimiento, la edad mínima del asegurado debe ser de 18 años y la máxima, 70 años.</li>
                                <li>En el caso del amparo de invalidez total y permanente, la edad mínima de ingreso a la póliza como asegurado es de 18 años y la máxima, 60 años.</li>
                                <li>El beneficiario podrá utilizar esta póliza para realizar sus estudios en una Institución de Educación Superior dentro del territorio colombiano o en el extranjero.</li>
                                <li>Si lo desea, el beneficiario puede cambiar de carrera universitaria o de centro, siempre que la nueva elección esté dentro de los parámetros autorizados.</li>
                                <li>También se incluye la formación religiosa y la relacionada con aviación o instituciones militares.</li>
                                <li>El rescate se puede efectuar después del primer año de vigencia y siempre que la prima de la póliza haya sido pagada.</li>
                            </ul>
                            
                            <br/>
                            <h3>Condiciones Generales</h3>
                            <p>Las condiciones generales de los Seguros Su Futuro Seguro MAPFRE definen los requisitos que deben cumplir los asegurados para beneficiarse de todas las garantías establecidas en el acuerdo.</p>
                            <ul style="list-style-type: circle;">
                                <li>Es uno de los seguros más económicos del mercado</li>
                                <li>Se pueden asegurar perros y gatos</li>
                                <li>Fácil de adquirir</li>
                                <li>Ofrece la cobertura de responsabilidad civil en salarios mínimos según la necesidad del cliente</li>
                                <li>La cobertura de muerte accidental va hasta 1.000.000</li>
                            </ul>
                           
                        </div>
                   
                    </div>
                
                </div>
            </div>
            <div class="row">                   
                <div class="section-title style-4 text-left pt-10">
                    <h2>Contáctanos</h2>
                    <p>  
                        <img src="assets/images/icons/telefono.png" alt="coberturas" style="width:4%"/> 4851105 Extención 737-785 &nbsp;&nbsp;&nbsp;
                        <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/>3206964550 &nbsp;&nbsp;&nbsp;
                        <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/>3046126643 &nbsp;&nbsp;&nbsp;
                        <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/>312 893 2013 &nbsp;&nbsp;&nbsp;
                    </p>
                </div>
            </div>
            <br/>


        </div>
    </div-->
    <!--blog-area end-->
    
   @include('layouts.footer')

   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>