
    <!--header-area end-->
    @include('layouts.master')
    
    
    <!--page-banner-area start-->
    <div class="page-banner-area bg-2">
        <div class="container">
            <div class="row align-items-center height-400">
                <div class="col-lg-12">
                    <div class="page-banner-text text-white text-center">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--page-banner-area end-->
    <div>
        <br/><br/><br/>
    </div>

    <div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>Gracias por contactarnos, pronto uno de nuestros asesores lo atendera.</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 text-center">
                     <div class="contact-info">
                        <div class="single-contact-info">
                            <h4><i class="fa fa-phone"></i>Teléfono</h4>
                            <p>Fijo: +57 (2) 485 11 05</p>
                            <p>Celular: +57 (2) 485 11 05</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12  text-center">
                     <div class="contact-info">
                        <div class="single-contact-info">
                            <h4><i class="fa fa-envelope"></i>Email</h4>
                            <p>servicioalcliente@prontoyseguros.com</p>
                            <p>+57 317 790 2646</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--contact-area start-->
    <div>
        <br/><br/><br/><br/>
    </div>
    <!--contact-area end-->

   @include('layouts.footer')