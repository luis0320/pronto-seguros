
    @include('layouts.master')
    @php
        $listado = $data->body->cotizaciones;
    @endphp
    
    <div class="benefit-area sm-mt-65">
       
        <div class="container"   style="padding: 0 20px;">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                        <div class="card-header" style="color:#fff">
                            <h3 class="text-center font-weight-light my-4" id="linea_tittle">Seguros todo riesgo</h3>
                        </div>
                        <div class="card-body">
                            <div class="col-md-12">
                                    <h4 class="text-center" id="linea_tittle">Axa Colpatria</h4>
                                </div>
                                @foreach($listado as $listado)
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                             <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Daños a terceros:</span> xxxxxxxxxx</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Prima con Iva :</span>
                                                @php
                                                    print $listado->valorTotalPrima;
                                                @endphp
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Daños a terceros:</span> xxxxxxxxxx</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p style="font-size:16px;line-height: 25px;"><span style="font-weight:bold">Daños a terceros:</span> xxxxxxxxxx</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">¡Compártenos tus datos y te ayudamos a adquerir tu Seguro Todo Riesgo! </h3>
                </div>
            </div>
        </div>
    </div>
      <!--contact-area start-->
    <div class="contact-area" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <br/><br/>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="contact-info">
                        <h3 style="font-size: 24px; margin-top: 0">Contáctanos</h3></br>
                        <!--h4 style="margin-top: 0">Resuelve tus dudas o cuéntanos sobre tu experiencia</h4-->
                        <div class="single-contact-info">
                            <h4><i class="fa fa-map-marker"></i>Dirección</h4>
                            <p>Cra. 66a No 10-15 Br .Limonar<br/>
                                Cali, Colombia</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-phone"></i>Teléfono</h4>
                            <p>Fijo: +602 4851105 Ext 706</p>
                            <p>Celular: 312 893 2013</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-envelope"></i>Email</h4>
                            <p>jefesoat@prontoyseguros.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="contact-form style-3">
                            
                            <form action="{{ route('enviar-email') }}" method="post" id="form-contact" accept-charset="utf-8">
                            {!! csrf_field() !!}
                            <div class="row">
                                
                                <div class="col-lg-6">
                                    <input type="text" name="nombres" placeholder="Nombre" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="apellidos" placeholder="Apellido" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="telefono" placeholder="Teléfono" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="email" placeholder="Email" required/>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="asunto" placeholder="Motivo Consulta" required/>
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="mensaje" placeholder="Mensaje" required></textarea>
                                </div>
                                <div class="col-lg-12" style="text-align: right;">
                                    <button class="btn-common" id="form-submit">Enviar Mensaje</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--div class="col-lg-12 col-md-12">
                    <p>Resuelve tus dudas o cuéntanos sobre tu experiencia</p>
                </div-->
            </div>
        </div>
    </div>
    <div class="benefit-area mt-80 ">
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Promesa de Servicio</h3>
                    <p>Resuelve tus dudas o cuéntanos sobre tu experiencia. En las próximas 24 horas recibirás nuestra llamada o si quieres información inmediata </br>nos puedes contactar a nuestro e-mail, WhatsApp o asesor virtual.</P>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div>
    @include('layouts.footer')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/javascript/jquery.blockUI.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            jQuery("form#todoriesgo").on("submit", function (e) {
                e.preventDefault();
                //$("button.registrarse").prop("disabled", true);
                jQuery.ajax({
                    url: "{{ route('consumo-riesgo') }}",
                    type: "post",
                    dataType: "json",
                    data: jQuery(this).serialize(),
                    beforeSend: function () {
                        jQuery.blockUI({ 
                            message: '', 
                            css: { 
                                border: "0",
                                background: "transparent"
                            },
                            overlayCSS:  { 
                                backgroundColor: "#fff",
                                opacity:         0.6, 
                                cursor:          "wait" 
                            },
                            baseZ: 10000
                        });
                    },
                    success: function (data) {
                        jQuery.unblockUI();
                       if(data.ok) {
                           window.location.href = "{{ url('inicio') }}";
                       } else {
                           alert(data.mensaje);
                           $("button.registrarse").prop("disabled", false);
                       }
                    }
                });
            });
        });
    </script>
