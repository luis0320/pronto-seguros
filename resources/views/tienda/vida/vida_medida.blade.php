
    <!--header-area end-->
    @include('layouts.master')
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row">
            <div class="col-lg-12 col-xs-12 text-center">
                <h2 >Plan Vida a la Medida</h2>
            </div>
        </div>
    </div>     
    <div class="row">
        <br/><br/>
    </div>
     <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>                    
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-dos-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Planes</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-proce-tab" data-toggle="pill" href="#pills-proce" role="tab" aria-controls="pills-proce" aria-selected="true" style="color: #000;">Procedimiento en caso de siniestros</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h2 style="margin-top: 0;">Beneficios</h2>
                    <p>Con Plan Vida a la Medida tienes la posibilidad de:</p>

                    <ul class="list-basic">
                        <li>Respaldo para tu cónyuge, padres o hijos.</li>
                        <li>Puedes pagar mensualmente a través del débito automático de tu cuenta de ahorros o corriente o con tu Tarjeta de Crédito Bancolombia Visa, MasterCard o American Express.</li>
                        <li>Estás protegido dentro y fuera del país.</li>
                        <li>Te respaldamos en caso de que sufras una invalidez, pérdida e inutilización, como consecuencia de un accidente o enfermedad.</li>
                        <li>Si te hospitalizan por cualquier causa (excepto enfermedades y accidentes preexistentes), cuentas con una renta diaria por hospitalización desde $50.000 hasta $100.000 diarios con un día de deducible.</li>
                        <li>Protección y respaldo para gastos de curación por un accidente.</li>
                    </ul>
                </div>
                
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2 style="margin-top: 0;">Características</h2>
                    <p>Puedes asignar la cantidad de beneficiarios que desees, teniendo en cuenta que la suma de los porcentajes no puede superar el 100%.</p>

                    <ul class="list-basic">
                        <li>Esta solución aplica para personas entre los 18 y 57 años de edad.</li>
                        <li>El valor asegurado que escojas en la cobertura de Vida es independiente del valor de los anexos, excepto en la cobertura de Enfermedades Graves para la cual debes tener un valor asegurado de $30.000.000 en Vida.</li>
                        <li>Crecimiento anual del valor asegurado al IPC.</li>
                    </ul>

                    <h3>Tarifa</h3>
                    <p>Desde $ 8,340. Este valor podrá variar según el valor asegurado, las coberturas adicionales, la edad y el género del asegurado.</p>
                </div>

                <div class="tab-pane fade" id="pills-dos" role="tabpanel" aria-labelledby="pills-dos-tab">
                    <h2 style="margin-top: 0;">Planes</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>La tarifa del seguro varía según la edad que tengas y las coberturas adicionales que elijas. Esta información la puedes consultar en la línea de atención al cliente de SURA (en Bogotá, Cali y Medellín al 437 8888 y para el resto del país, sin ningún costo: 01800 0518888).</li>
                        <li>Es un seguro que se vende solo a través de un canal de llamadas de salida (cuando un asesor te contacta).</li>
                        <li>El valor asegurado que puedes contratar en la cobertura de Vida va desde $ 15.000.000 hasta $200.000.000 y el acumulado máximo, como resultado de la suma de varios planes, no debe exceder de $250.000.000.</li>
                        <li>Puedes escoger las coberturas de invalidez, pérdida o inutilización como consecuencia de un accidente o enfermedad, indemnización por muerte accidental con valores asegurados que varían desde $20.000.000 hasta $200.000.000.</li>
                        <li>Para la cobertura de cáncer contarás con un respaldo de $20.000.000 y un auxilio de cáncer por $3.000.000.</li>
                        <li>El valor asegurado en la cobertura de gastos de curación es de $1.000.000.</li>
                        <li>En caso de que fallezcas, tus beneficiarios tendrán un auxilio exequial de $3.000.000, $5.000.000 o $7.000.000 para los gastos funerarios.</li>
                    </ul>

                    <p>Contamos con estos y más planes. Llámanos o chatea con nosotros para darte una asesoría personalizada y ayudarte a elegir la mejor opción de cobertura y valor asegurado de acuerdo a tus necesidades.</p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>
                </div>

                <div class="tab-pane fade" id="pills-proce" role="tabpanel" aria-labelledby="pills-proce-tab">
                    <h2 style="margin-top: 0;">Procedimiento en caso de siniestros</h2>
                    <p dir="ltr">En caso de un siniestro, cubierto por este seguro, tú o tus beneficiarios deberán solicitar el pago de las indemnizaciones a las que tengas derecho, acreditando su ocurrencia.</p>
                    <p dir="ltr">Puedes comunicarte por alguna de las siguientes opciones:</p>
                    <ul class="list-basic" dir="ltr">
                        <li>Líneas de atención al cliente de SURA: en Bogotá, Cali o Medellín al 437 88 88 o en el resto del país 01 8000 51 88 88.</li>
                        <li>Desde tu celular, marcando al #888.</li>
                        <li>Sucursal Telefónica Bancolombia: en Bogotá 343 00 00, Medellín 510 90 00, Cali 554 05 05, Barranquilla 361 88 88 o desde el resto del país 01 800 0912345.</li>
                        <li>Por estos mismos medios, te indicarán los documentos que debes presentar para soportar la reclamación.</li>
                    </ul>
                </div>

                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión del seguro</h2>

                    <h3>¿Cómo radicar novedades del seguro?</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                    </ul>
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-home-mobile" role="tab" aria-controls="pills-home-mobile" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>                    
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-profile-mobile-tab" data-toggle="pill" href="#pills-profile-mobile" role="tab" aria-controls="pills-profile-mobile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-dos-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Planes</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-proce-mobile-tab" data-toggle="pill" href="#pills-proce-mobile" role="tab" aria-controls="pills-proce-mobile" aria-selected="true" style="color: #000;">Procedimiento en caso de siniestros</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contac-mobilet-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home-mobile" role="tabpanel" aria-labelledby="pills-home-mobile-tab">
                    <h2 style="margin-top: 0;">Beneficios</h2>
                    <p>Con Plan Vida a la Medida tienes la posibilidad de:</p>

                    <ul class="list-basic">
                        <li>Respaldo para tu cónyuge, padres o hijos.</li>
                        <li>Puedes pagar mensualmente a través del débito automático de tu cuenta de ahorros o corriente o con tu Tarjeta de Crédito Bancolombia Visa, MasterCard o American Express.</li>
                        <li>Estás protegido dentro y fuera del país.</li>
                        <li>Te respaldamos en caso de que sufras una invalidez, pérdida e inutilización, como consecuencia de un accidente o enfermedad.</li>
                        <li>Si te hospitalizan por cualquier causa (excepto enfermedades y accidentes preexistentes), cuentas con una renta diaria por hospitalización desde $50.000 hasta $100.000 diarios con un día de deducible.</li>
                        <li>Protección y respaldo para gastos de curación por un accidente.</li>
                    </ul>
                </div>
                
                <div class="tab-pane fade" id="pills-profile-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h2 style="margin-top: 0;">Características</h2>
                    <p>Puedes asignar la cantidad de beneficiarios que desees, teniendo en cuenta que la suma de los porcentajes no puede superar el 100%.</p>

                    <ul class="list-basic">
                        <li>Esta solución aplica para personas entre los 18 y 57 años de edad.</li>
                        <li>El valor asegurado que escojas en la cobertura de Vida es independiente del valor de los anexos, excepto en la cobertura de Enfermedades Graves para la cual debes tener un valor asegurado de $30.000.000 en Vida.</li>
                        <li>Crecimiento anual del valor asegurado al IPC.</li>
                    </ul>

                    <h3>Tarifa</h3>
                    <p>Desde $ 8,340. Este valor podrá variar según el valor asegurado, las coberturas adicionales, la edad y el género del asegurado.</p>
                </div>

                <div class="tab-pane fade" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-dos-mobile-tab">
                    <h2 style="margin-top: 0;">Planes</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>La tarifa del seguro varía según la edad que tengas y las coberturas adicionales que elijas. Esta información la puedes consultar en la línea de atención al cliente de SURA (en Bogotá, Cali y Medellín al 437 8888 y para el resto del país, sin ningún costo: 01800 0518888).</li>
                        <li>Es un seguro que se vende solo a través de un canal de llamadas de salida (cuando un asesor te contacta).</li>
                        <li>El valor asegurado que puedes contratar en la cobertura de Vida va desde $ 15.000.000 hasta $200.000.000 y el acumulado máximo, como resultado de la suma de varios planes, no debe exceder de $250.000.000.</li>
                        <li>Puedes escoger las coberturas de invalidez, pérdida o inutilización como consecuencia de un accidente o enfermedad, indemnización por muerte accidental con valores asegurados que varían desde $20.000.000 hasta $200.000.000.</li>
                        <li>Para la cobertura de cáncer contarás con un respaldo de $20.000.000 y un auxilio de cáncer por $3.000.000.</li>
                        <li>El valor asegurado en la cobertura de gastos de curación es de $1.000.000.</li>
                        <li>En caso de que fallezcas, tus beneficiarios tendrán un auxilio exequial de $3.000.000, $5.000.000 o $7.000.000 para los gastos funerarios.</li>
                    </ul>

                    <p>Contamos con estos y más planes. Llámanos o chatea con nosotros para darte una asesoría personalizada y ayudarte a elegir la mejor opción de cobertura y valor asegurado de acuerdo a tus necesidades.</p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>
                </div>

                <div class="tab-pane fade" id="pills-proce-mobile" role="tabpanel" aria-labelledby="pills-proce-mobile-tab">
                    <h2 style="margin-top: 0;">Procedimiento en caso de siniestros</h2>
                    <p dir="ltr">En caso de un siniestro, cubierto por este seguro, tú o tus beneficiarios deberán solicitar el pago de las indemnizaciones a las que tengas derecho, acreditando su ocurrencia.</p>
                    <p dir="ltr">Puedes comunicarte por alguna de las siguientes opciones:</p>
                    <ul class="list-basic" dir="ltr">
                        <li>Líneas de atención al cliente de SURA: en Bogotá, Cali o Medellín al 437 88 88 o en el resto del país 01 8000 51 88 88.</li>
                        <li>Desde tu celular, marcando al #888.</li>
                        <li>Sucursal Telefónica Bancolombia: en Bogotá 343 00 00, Medellín 510 90 00, Cali 554 05 05, Barranquilla 361 88 88 o desde el resto del país 01 800 0912345.</li>
                        <li>Por estos mismos medios, te indicarán los documentos que debes presentar para soportar la reclamación.</li>
                    </ul>
                </div>

                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión del seguro</h2>

                    <h3>¿Cómo radicar novedades del seguro?</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                    </ul>
                </div>
            </div>
            
        </div>

    </div>
    <div class="row">
        <br/>
    </div>

    <!--faq-area start-->
    <div class="faq-area ">
        <div class="container">
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        Información relacionada
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body" style="padding: 0;">
                                    <div class="col-sm-12">
                                        <h3 dir="ltr">¿Cómo cancelar el producto?</h3>

                                        <p dir="ltr">Puedes comunícate a la línea de atención de SURA en Bogotá, Cali o Medellín al 437 88 88 o en el resto del país 01 8000 51 88 88.</p>

                                        <h3 dir="ltr">Causales de terminación</h3>

                                        <p dir="ltr">Este seguro termina por las siguientes causas:</p>

                                        <ul class="list-basic" dir="ltr">
                                            <li>Por mora en el pago de la prima o saldo insuficiente en la cuenta de ahorros, corriente o por falta de cupo en la tarjeta de crédito.</li>
                                            <li>Cuando lo solicites a SURA por medio escrito o a través de la línea de atención.</li>
                                            <li>Cuando SURA pague la indemnización por la cobertura de herencia.</li>
                                            <li>Cuando SURA te lo informe por escrito, mínimo con 10 días de antelación.</li>
                                            <li>Al finalizar la vigencia, cuando cumplas 80 años.</li>
                                        </ul>

                                        <h3 dir="ltr">Canales de atención posventa</h3>

                                        <p dir="ltr">Cuando adquieras tu producto puedes usar alguno de los siguientes canales y con gusto te atenderemos:</p>

                                        <ul class="list-basic">
                                            <li>Teléfono: +57 (2) 485 11 05</li>
                                            <li>Celular: +57 313 680 49 88</li>
                                            <li>WhatsApp: +57 313 680 49 88</li>
                                            <li>Email: servicioalcliente@prontoyseguros.com</li>
                                        </ul>
                                        <h3><a href="{{ url('contactenos') }}" title="Contactenos">Contactenos </a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>s
    <div class="row">
        <br/>
    </div>
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>

