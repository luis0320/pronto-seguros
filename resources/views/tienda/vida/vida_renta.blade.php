
    <!--header-area end-->
    @include('layouts.master')
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row">
            <div class="col-lg-12 col-xs-12 text-center">
                <h2 >Plan Vida Renta</h2>
            </div>
        </div>
    </div>     
    <div class="row">
        <br/><br/>
    </div>
     <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Planes</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h2 style="margin-top: 0;">Beneficios del producto</h2>
                    <h3>Siempre puedes contar con nosotros</h3>

                    <ul class="list-basic">
                        <li>Podrás asegurarte, asegurar a tu cónyuge o tus empleados.</li>
                        <li>Excelente alternativa para padres con hijos en edades escolares o universitarios, igualmente aplica muy bien para personas mayores de 40 años pues el costo del seguro no depende de la edad, ni del género.</li>
                        <li>Para afiliarte no requieres exámenes médicos ni otro tipo de trámites. La adquisición de este producto es voluntaria y su expedición es inmediata. Solo es necesario responder en forma precisa y firmar la declaración de asegurabilidad.</li>
                        <li>Complemento ideal para la brecha pensional.&nbsp;</li>
                    </ul>

                </div>
                <div class="tab-pane fade" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2 style="margin-top: 0;">Planes</h2>
                    <h3 dir="ltr">Elige el plan que se adapte a tus necesidades</h3>

                    <p dir="ltr">Elige entre tres opciones durante cinco, diez o quince años:</p>

                    <ul class="list-basic" dir="ltr">
                        <li>Plan A: aseguras una mensualidad de $1.200.000.</li>
                        <li>Plan B: aseguras una mensualidad de $800.000.</li>
                        <li>Plan C: aseguras una mensualidad de $400.000.</li>
                    </ul>
                    <p>Contamos con estos y más planes. Llámanos o chatea con nosotros para darte una asesoría personalizada y ayudarte a elegir la mejor opción de cobertura y valor asegurado de acuerdo a tus necesidades.</p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2 style="margin-top: 0;">Características</h2>
                    <h3>Características del Plan Vida Renta</h3>

                    <ul class="list-basic" >
                        <li>Realizar los pagos mensuales, por medio de débito automático a tu cuenta de ahorros o corriente, o con tu Tarjeta de Crédito Bancolombia Visa, MasterCard o American Express.</li>
                        <li>Edad de ingreso para Clientes Bancolombia entre los 18 y 69 años. La cobertura por muerte va hasta los 80 años y la cobertura por incapacidad total y permanente hasta los 75 años. Protección y respaldo a tus beneficiarios menores de 69 años para Gastos de Curación (Si alguno llegase a sufrir un accidente e incurre en gastos por concepto de honorarios profesionales a médicos u odontólogos graduados, hospitalización, cirugía, ayudas diagnósticas, tratamiento en la clínica u hospital, alimentación, enfermería, fisioterapia y elementos ortopédicos y ambulancia, para cada uno la cobertura es de $1.000.000 al año.) El cliente queda asegurado en el momento de tomar la póliza, siempre y cuando haya existido el primer débito a su cuenta o tarjeta.</li>
                        <li>Los beneficiarios reciben un valor mensual controlando el gasto inmediato de la suma asegurada.</li>
                        <li>Renovación automática sin necesidad de trámites adicionales.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión del seguro</h2>
                    <h3>Cómo solicitar tu Plan Vida Renta</h3>

                    <p >Para obtener este producto debes ser un cliente Bancolombia (tener cuenta de ahorro, cuenta corriente o tarjeta de crédito). Acércate a nuestra sucursal, donde uno de nuestros asesores te orientará para elegir el producto que más se adapte a tus necesidades, diligenciar la póliza, cumplir con las condiciones, leer y aceptar la declaración de asegurabilidad.</p>

                    <h3>¿Cómo es el proceso de devolución de primas?</h3>

                    <p>En caso de existir devolución de primas, la aseguradora procede a hacer el reembolso del dinero a tu cuenta de ahorros o corriente, o abonarlo a tu tarjeta de crédito. También puedes reclamar tu cheque en cualquier sucursal de Suramericana.</p>

                    <h3>¿Cuál es el procedimiento en caso de siniestros?</h3>

                    <p>En caso de ocurrir algún evento cubierto en la póliza, tú o tus beneficiarios deben reportarlo cuanto antes.</p>


                    <ul class="list-basic">
                        <li>Muerte del asegurado.</li>
                        <li>Muerte accidental del asegurado.</li>
                        <li>Incapacidad total y permanente del asegurado.</li>
                        <li>Por gastos de curación por accidente del beneficiario.</li>
                    </ul>

                    <h3>¿Cómo radicar novedades del seguro?</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                    </ul>
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-home-mobile" role="tab" aria-controls="pills-home-mobile" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Planes</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-profile-mobile-tab" data-toggle="pill" href="#pills-profile-mobile" role="tab" aria-controls="pills-profile-mobile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home-mobile" role="tabpanel" aria-labelledby="pills-home-mobile-tab">
                    <h2 style="margin-top: 0;">Beneficios del producto</h2>
                    <h3>Siempre puedes contar con nosotros</h3>

                    <ul class="list-basic">
                        <li>Podrás asegurarte, asegurar a tu cónyuge o tus empleados.</li>
                        <li>Excelente alternativa para padres con hijos en edades escolares o universitarios, igualmente aplica muy bien para personas mayores de 40 años pues el costo del seguro no depende de la edad, ni del género.</li>
                        <li>Para afiliarte no requieres exámenes médicos ni otro tipo de trámites. La adquisición de este producto es voluntaria y su expedición es inmediata. Solo es necesario responder en forma precisa y firmar la declaración de asegurabilidad.</li>
                        <li>Complemento ideal para la brecha pensional.&nbsp;</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h2 style="margin-top: 0;">Planes</h2>
                    <h3 dir="ltr">Elige el plan que se adapte a tus necesidades</h3>

                    <p dir="ltr">Elige entre tres opciones durante cinco, diez o quince años:</p>

                    <ul class="list-basic" dir="ltr">
                        <li>Plan A: aseguras una mensualidad de $1.200.000.</li>
                        <li>Plan B: aseguras una mensualidad de $800.000.</li>
                        <li>Plan C: aseguras una mensualidad de $400.000.</li>
                    </ul>
                    <p>Contamos con estos y más planes. Llámanos o chatea con nosotros para darte una asesoría personalizada y ayudarte a elegir la mejor opción de cobertura y valor asegurado de acuerdo a tus necesidades.</p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-profile-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h2 style="margin-top: 0;">Características</h2>
                    <h3>Características del Plan Vida Renta</h3>

                    <ul class="list-basic" >
                        <li>Realizar los pagos mensuales, por medio de débito automático a tu cuenta de ahorros o corriente, o con tu Tarjeta de Crédito Bancolombia Visa, MasterCard o American Express.</li>
                        <li>Edad de ingreso para Clientes Bancolombia entre los 18 y 69 años. La cobertura por muerte va hasta los 80 años y la cobertura por incapacidad total y permanente hasta los 75 años. Protección y respaldo a tus beneficiarios menores de 69 años para Gastos de Curación (Si alguno llegase a sufrir un accidente e incurre en gastos por concepto de honorarios profesionales a médicos u odontólogos graduados, hospitalización, cirugía, ayudas diagnósticas, tratamiento en la clínica u hospital, alimentación, enfermería, fisioterapia y elementos ortopédicos y ambulancia, para cada uno la cobertura es de $1.000.000 al año.) El cliente queda asegurado en el momento de tomar la póliza, siempre y cuando haya existido el primer débito a su cuenta o tarjeta.</li>
                        <li>Los beneficiarios reciben un valor mensual controlando el gasto inmediato de la suma asegurada.</li>
                        <li>Renovación automática sin necesidad de trámites adicionales.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión del seguro</h2>
                    <h3>Cómo solicitar tu Plan Vida Renta</h3>

                    <p >Para obtener este producto debes ser un cliente Bancolombia (tener cuenta de ahorro, cuenta corriente o tarjeta de crédito). Acércate a nuestra sucursal, donde uno de nuestros asesores te orientará para elegir el producto que más se adapte a tus necesidades, diligenciar la póliza, cumplir con las condiciones, leer y aceptar la declaración de asegurabilidad.</p>

                    <h3>¿Cómo es el proceso de devolución de primas?</h3>

                    <p>En caso de existir devolución de primas, la aseguradora procede a hacer el reembolso del dinero a tu cuenta de ahorros o corriente, o abonarlo a tu tarjeta de crédito. También puedes reclamar tu cheque en cualquier sucursal de Suramericana.</p>

                    <h3>¿Cuál es el procedimiento en caso de siniestros?</h3>

                    <p>En caso de ocurrir algún evento cubierto en la póliza, tú o tus beneficiarios deben reportarlo cuanto antes.</p>


                    <ul class="list-basic">
                        <li>Muerte del asegurado.</li>
                        <li>Muerte accidental del asegurado.</li>
                        <li>Incapacidad total y permanente del asegurado.</li>
                        <li>Por gastos de curación por accidente del beneficiario.</li>
                    </ul>

                    <h3>¿Cómo radicar novedades del seguro?</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                    </ul>
                </div>
            </div>
            
        </div>

    </div>
    <div class="row">
        <br/>
    </div>

    <!--faq-area start-->
    <div class="faq-area ">
        <div class="container">
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        Información relacionada
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body" style="padding: 0;">
                                    <div class="col-sm-12">
                                        <h3>Incrementos y exclusiones</h3>

                                        <ul class="list-basic">
                                            <li><strong>Incremento anual de la suma asegurada</strong><br>
                                            La suma asegurada se incrementará cada año, a partir de la fecha de inicio de la vigencia del seguro, con base en el Índice de Precios al Consumidor (IPC) de los últimos doce meses conocidos, certificados por el DANE. En el momento de afectarse el amparo del seguro, Suramericana liquidará la suma a pagar, con base en el valor alcanzado a la fecha de la última renovación.</li>
                                        </ul>

                                        <h3>Plazos y términos</h3>

                                        <p><a href="#" title="Conoce los plazos y términos de este producto" target="_blank">Conoce los plazos y términos de este producto. </a></p>

                                        <h3>¿Cómo cancelar el seguro?</h3>

                                        <p>Para la cancelación del seguro Plan Vida Renta, el tomador o asegurado debe autorizar por escrito la cancelación de la póliza y llevar la carta a una oficina de Bancolombia.</p>

                                        <h3>Causales de terminación</h3>

                                        <ul class="list-basic">
                                            <li>Por mora en el pago de la prima.</li>
                                            <li>Cuando el tomador así lo determine.</li>
                                            <li>Al fallecimiento del asegurado.</li>
                                            <li>Por el pago del amparo de incapacidad total y permanente.</li>
                                            <li>Para el amparo básico de vida, al finalizar la vigencia de la póliza en la cual el asegurado cumpla los 80 años de edad. Para el amparo de incapacidad total y permanente al finalizar la vigencia de la póliza en la cual el asegurado cumpla 70 años de edad. En el caso del amparo de curación, al finalizar la vigencia en que el asegurado cumple 39 años.</li>
                                            <li>Salvo el amparo de vida, los demás amparos podrán ser revocados unilateralmente por Suramericana, mediante el envío de una comunicación escrita dirigida al asegurado, con por lo menos diez días hábiles de antelación a la terminación efectiva del amparo.</li>
                                        </ul>

                                        <h3>Más información</h3>

                                        <ul class="list-basic">
                                            <li>Teléfono: +57 (2) 485 11 05</li>
                                            <li>Celular: +57 313 680 49 88</li>
                                            <li>WhatsApp: +57 313 680 49 88</li>
                                            <li>Email: servicioalcliente@prontoyseguros.com</li>
                                        </ul>
                                        <!--h3><a href="{{ url('contactenos') }}" title="Contactenos">Contactenos </a></h3-->
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65">
        <div class="container"  style="padding: 0 20px;">

            <div class="row" style="background-color: #eaeaea;">
                <div class="col-lg-5 col-sm-6" style="padding: 0;">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/vida/simulador.jpg" alt="promo">
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" >
                    <div class="text-center"  id="soat_items">
                        <h3 style="font-weight: 800; margin-top: 80px; text-align:center">Simula tu plan</h3>
                    </div>
                    <div class="text-center"  id="soat_items_mobile">
                        <h3 style="font-weight: 800; margin-top: 5px; text-align:center">Simula tu plan</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-xs-12" style="padding: 0 0 0 30px;">
                            <p style="font-size: 16px;">En Pronto y Seguros queremos que analices mejor tus posibilidades. Usa nuestro simulador para conocer la periodicidad y el plan ideales para ti:</p>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-12 col-xs-12" style="padding:0; text-align: center;">
                            <a href="#" class="btn-common" role="button" >
                                <span class="fl-button-text"><strong>Simulador</strong></span>
                            </a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>

