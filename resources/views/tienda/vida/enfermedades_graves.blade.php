
    <!--header-area end-->
    @include('layouts.master')
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row">
            <div class="col-lg-12 col-xs-12 text-center">
                <h2>Plan Cobertura Enfermedades Graves</h2>
            </div>
        </div>
    </div>     
    <div class="row">
        <br/><br/>
    </div>
     <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-renta-beneficios-tab" data-toggle="pill" href="#pills-renta-beneficios" role="tab" aria-controls="pills-renta-beneficios" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-renta-caracteristicas-tab" data-toggle="pill" href="#pills-renta-caracteristicas" role="tab" aria-controls="pills-renta-caracteristicas" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-renta-planes-tab" data-toggle="pill" href="#pills-renta-planes" role="tab" aria-controls="pills-renta-planes" aria-selected="true" style="color: #000;">Planes</a>
                    </li>
                    
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-renta-gestion-tab" data-toggle="pill" href="#pills-renta-gestion" role="tab" aria-controls="pills-renta-gestion" aria-selected="false" style="color: #000; text-align: left;">Solicitud y gestión de tu plan cobertura enfermedades graves</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-renta-beneficios" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h2 style="margin-top: 0;">Beneficios del producto</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>Tienes la posibilidad de elegir entre 7 planes, de acuerdo con tu necesidad.</li>
                        <li>El valor a pagar varía de acuerdo con el plan seleccionado, la edad y el sexo.</li>
                        <li>Tienes renovación automática de la póliza cada año.</li>
                        <li>Puedes asignar los beneficiarios que quieras, teniendo en cuenta que la suma de los porcentajes de todos los beneficiarios debe sumar 100%.</li>
                        <li>El capital de respaldo que recibes puedes usarlo como una jubilación anticipada, o una provisión para tu cuidado personal en caso de quedar con alguna limitación.</li>
                    </ul>
                </div>

                <div class="tab-pane fade" id="pills-renta-caracteristicas" role="tabpanel" aria-labelledby="pills-renta-caracteristicas-tab">
                    <h2 style="margin-top: 0;">Características</h2>

                    <ul class="list-basic" dir="ltr">
                        <li>El ingreso es para personas entre los 18 y 57 años de edad.</li>
                        <li>La cobertura de enfermedades graves es hasta los 69 años. El amparo de vida hasta los 80 años y a dicha edad, se ofrece la conversión a otro seguro de vida individual.</li>
                        <li>El máximo que se puede adquirir son $400 millones en la cobertura de enfermedades graves, los cuales pueden ser expedidos en varias pólizas.</li>
                        <li>Este seguro&nbsp; indemniza el valor asegurado alcanzado hasta la fecha de diagnóstico de una de las enfermedades y procedimientos quirúrgicos contemplados en la póliza, siempre y cuando el asegurado sobreviva por lo menos treinta (30) días posteriores al diagnóstico o cirugía.<strong> Las enfermedades y procedimientos quirúrgicos amparados son: </strong>infarto del miocardio, enfermedad cerebrovascular, insuficiencia renal crónica, esclerosis múltiple y revascularización coronaria (By Pass) y cáncer (consulta las exclusiones o los tipos de cáncer que se incluyen <a href="/wcm/connect/www.grupobancolombia.com15880/e0932f0a-d558-4b03-ba00-93bd46a928bc/Plan+Cobertura+Enfermedades+Graves.pdf?MOD=AJPERES&amp;CVID=m6kYBFP" target="_blank">aquí.</a></li>
                        <li><strong>Terminación amparo de enfermedades graves: </strong>en los casos que se haga el pago del 100% del valor indemnizable del amparo de enfermedades graves, se producirá la terminación del mismo y el asegurado continuará con el amparo de vida.</li>
                        <li>La suma asegurada se incrementará anualmente a partir de la fecha de inicio de la vigencia del seguro, con base en el índice de precios al consumidor, IPC, de los últimos doce (12) meses conocidos certificados por el DANE.</li>
                    </ul>
                </div>

                <div class="tab-pane fade" id="pills-renta-planes" role="tabpanel" aria-labelledby="pills-renta-planes-tab">
                    <h2 style="margin-top: 0;">Planes</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>Vida $10 millones y en enfermedades graves $50 millones.</li>
                        <li>Vida $12 millones y en enfermedades graves $60 millones.</li>
                        <li>Vida $16 millones y en enfermedades graves $80 millones.</li>
                        <li>Vida $20 millones y en enfermedades graves $100 millones.</li>
                        <li>Vida $25 millones y en enfermedades graves $125 millones.</li>
                        <li>Vida $30 millones y en enfermedades graves $150 millones.</li>
                        <li>Vida $35 millones y en enfermedades graves $175 millones.</li>
                    </ul>

                    
                    <p>Contamos con estos y más planes. Llámanos o chatea con nosotros para darte una asesoría personalizada y ayudarte a elegir la mejor opción de cobertura y valor asegurado de acuerdo a tus necesidades.</p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>
                </div>
                
                <div class="tab-pane fade" id="pills-renta-gestion" role="tabpanel" aria-labelledby="pills-renta-gestion-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión de tu plan cobertura enfermedades graves</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>Este es un producto exclusivo de televentas, es decir que el asesor te contacta y te ofrece el producto.</li>
                        <li>Para iniciar el proceso de venta debes aceptar la grabación de la llamada, informar los datos generales del titular, datos del asegurado, amparos, valores asegurados, beneficiarios, cumplir con las condiciones de edad y aceptar la declaración de asegurabilidad, de esta manera quedas asegurado.</li>
                        <li>Debes ser cliente&nbsp; Bancolombia y tener una cuenta de ahorro, corriente o tarjeta de crédito.</li>
                        <li>Con el acompañamiento de uno de nuestros asesores comerciales de la línea, puedes conocer las características y beneficios específicos de cada uno de nuestros planes, buscando el que más se adapte a tus necesidades.</li>
                    </ul>

                    <p dir="ltr">Ante un caso inesperado, realiza los siguientes pasos para reclamar tu <strong>Plan Cobertura Enfermedades Graves de Sura:</strong></p>

                    <ul class="list-basic" dir="ltr">
                        <li>Verifica las coberturas de tu seguro, es importante que tú y tus beneficiarios conozcan la póliza y sus coberturas.</li>
                        <li>Comunícate con la línea de atención de Sura, con el fin de recibir orientación y el acompañamiento de nuestros expertos.
                        <ul class="list-basic" dir="ltr">
                            <li>Medellín, Cali y Bogotá: 437 88 88</li>
                            <li>Resto del país: 01 8000 518 888 o desde tu celular: #888</li>
                        </ul>
                        </li>
                        <li>Presenta la siguiente documentación:
                        <ul class="list-basic" dir="ltr">
                            <li>Formulario de reclamación.</li>
                            <li>Fotocopia del documento de identidad del asegurado.</li>
                            <li>Historia clínica completa del asegurado.</li>
                        </ul>
                        </li>
                        <li>Debes incluir los siguientes documentos para cada enfermedad o padecimiento:<br>
                        <strong>Cáncer: </strong>resultado de la anatomía patológica (biopsia).<br>
                        <strong>Infarto del miocardio:</strong> electrocardiogramas (lectura y trazado), resultado de medición de enzimas cardiacas y troponinas.<br>
                        <strong>Enfermedad Cerebro Vascular:</strong> certificado del médico especialista.<br>
                        <strong>Insuficiencia renal crónica:</strong> certificado del médico especialista<br>
                        <strong>Esclerosis Múltiple: </strong>certificado del médico especialista, enfermedad cerebro vascular.<br>
                        <strong>Revascularización coronaria (By–pass):</strong> descripción quirúrgica del procedimiento.</li>
                        <li><strong>Trasplante de órgano mayor:</strong> descripción quirúrgica del procedimiento, traumatismo mayor de cabeza, certificación de un neurólogo sobre el trastorno de la función cerebral y resultado de test neurológico (TAC o Resonancias Nuclear Magnéticas).</li>
                    </ul>
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-renta-beneficios-mobile-tab" data-toggle="pill" href="#pills-renta-beneficios-mobile" role="tab" aria-controls="pills-renta-beneficios-mobile" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-renta-caracteristicas-mobile-tab" data-toggle="pill" href="#pills-renta-caracteristicas-mobile" role="tab" aria-controls="pills-renta-caracteristicas-mobile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-renta-planes-mobile-tab" data-toggle="pill" href="#pills-renta-planes-mobile" role="tab" aria-controls="pills-renta-planes-mobile" aria-selected="true" style="color: #000;">Planes</a>
                    </li>
                    
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-renta-gestion-mobile-tab" data-toggle="pill" href="#pills-renta-gestion-mobile" role="tab" aria-controls="pills-renta-gestion-mobile" aria-selected="false" style="color: #000; text-align: left;">Solicitud y gestión de tu plan cobertura enfermedades graves</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-renta-beneficios-mobile" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h2 style="margin-top: 0;">Beneficios del producto</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>Tienes la posibilidad de elegir entre 7 planes, de acuerdo con tu necesidad.</li>
                        <li>El valor a pagar varía de acuerdo con el plan seleccionado, la edad y el sexo.</li>
                        <li>Tienes renovación automática de la póliza cada año.</li>
                        <li>Puedes asignar los beneficiarios que quieras, teniendo en cuenta que la suma de los porcentajes de todos los beneficiarios debe sumar 100%.</li>
                        <li>El capital de respaldo que recibes puedes usarlo como una jubilación anticipada, o una provisión para tu cuidado personal en caso de quedar con alguna limitación.</li>
                    </ul>
                </div>

                <div class="tab-pane fade" id="pills-renta-caracteristicas-mobile" role="tabpanel" aria-labelledby="pills-renta-caracteristicas-tab">
                    <h2 style="margin-top: 0;">Características</h2>

                    <ul class="list-basic" dir="ltr">
                        <li>El ingreso es para personas entre los 18 y 57 años de edad.</li>
                        <li>La cobertura de enfermedades graves es hasta los 69 años. El amparo de vida hasta los 80 años y a dicha edad, se ofrece la conversión a otro seguro de vida individual.</li>
                        <li>El máximo que se puede adquirir son $400 millones en la cobertura de enfermedades graves, los cuales pueden ser expedidos en varias pólizas.</li>
                        <li>Este seguro&nbsp; indemniza el valor asegurado alcanzado hasta la fecha de diagnóstico de una de las enfermedades y procedimientos quirúrgicos contemplados en la póliza, siempre y cuando el asegurado sobreviva por lo menos treinta (30) días posteriores al diagnóstico o cirugía.<strong> Las enfermedades y procedimientos quirúrgicos amparados son: </strong>infarto del miocardio, enfermedad cerebrovascular, insuficiencia renal crónica, esclerosis múltiple y revascularización coronaria (By Pass) y cáncer (consulta las exclusiones o los tipos de cáncer que se incluyen <a href="/wcm/connect/www.grupobancolombia.com15880/e0932f0a-d558-4b03-ba00-93bd46a928bc/Plan+Cobertura+Enfermedades+Graves.pdf?MOD=AJPERES&amp;CVID=m6kYBFP" target="_blank">aquí.</a></li>
                        <li><strong>Terminación amparo de enfermedades graves: </strong>en los casos que se haga el pago del 100% del valor indemnizable del amparo de enfermedades graves, se producirá la terminación del mismo y el asegurado continuará con el amparo de vida.</li>
                        <li>La suma asegurada se incrementará anualmente a partir de la fecha de inicio de la vigencia del seguro, con base en el índice de precios al consumidor, IPC, de los últimos doce (12) meses conocidos certificados por el DANE.</li>
                    </ul>
                </div>

                <div class="tab-pane fade" id="pills-renta-planes-mobile" role="tabpanel" aria-labelledby="pills-renta-planes-tab">
                    <h2 style="margin-top: 0;">Planes</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>Vida $10 millones y en enfermedades graves $50 millones.</li>
                        <li>Vida $12 millones y en enfermedades graves $60 millones.</li>
                        <li>Vida $16 millones y en enfermedades graves $80 millones.</li>
                        <li>Vida $20 millones y en enfermedades graves $100 millones.</li>
                        <li>Vida $25 millones y en enfermedades graves $125 millones.</li>
                        <li>Vida $30 millones y en enfermedades graves $150 millones.</li>
                        <li>Vida $35 millones y en enfermedades graves $175 millones.</li>
                    </ul>

                    
                    <p>Contamos con estos y más planes. Llámanos o chatea con nosotros para darte una asesoría personalizada y ayudarte a elegir la mejor opción de cobertura y valor asegurado de acuerdo a tus necesidades.</p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>
                </div>
                
                <div class="tab-pane fade" id="pills-renta-gestion-mobile" role="tabpanel" aria-labelledby="pills-renta-gestion-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión de tu plan cobertura enfermedades graves</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>Este es un producto exclusivo de televentas, es decir que el asesor te contacta y te ofrece el producto.</li>
                        <li>Para iniciar el proceso de venta debes aceptar la grabación de la llamada, informar los datos generales del titular, datos del asegurado, amparos, valores asegurados, beneficiarios, cumplir con las condiciones de edad y aceptar la declaración de asegurabilidad, de esta manera quedas asegurado.</li>
                        <li>Debes ser cliente&nbsp; Bancolombia y tener una cuenta de ahorro, corriente o tarjeta de crédito.</li>
                        <li>Con el acompañamiento de uno de nuestros asesores comerciales de la línea, puedes conocer las características y beneficios específicos de cada uno de nuestros planes, buscando el que más se adapte a tus necesidades.</li>
                    </ul>

                    <p dir="ltr">Ante un caso inesperado, realiza los siguientes pasos para reclamar tu <strong>Plan Cobertura Enfermedades Graves de Sura:</strong></p>

                    <ul class="list-basic" dir="ltr">
                        <li>Verifica las coberturas de tu seguro, es importante que tú y tus beneficiarios conozcan la póliza y sus coberturas.</li>
                        <li>Comunícate con la línea de atención de Sura, con el fin de recibir orientación y el acompañamiento de nuestros expertos.
                        <ul class="list-basic" dir="ltr">
                            <li>Medellín, Cali y Bogotá: 437 88 88</li>
                            <li>Resto del país: 01 8000 518 888 o desde tu celular: #888</li>
                        </ul>
                        </li>
                        <li>Presenta la siguiente documentación:
                        <ul class="list-basic" dir="ltr">
                            <li>Formulario de reclamación.</li>
                            <li>Fotocopia del documento de identidad del asegurado.</li>
                            <li>Historia clínica completa del asegurado.</li>
                        </ul>
                        </li>
                        <li>Debes incluir los siguientes documentos para cada enfermedad o padecimiento:<br>
                        <strong>Cáncer: </strong>resultado de la anatomía patológica (biopsia).<br>
                        <strong>Infarto del miocardio:</strong> electrocardiogramas (lectura y trazado), resultado de medición de enzimas cardiacas y troponinas.<br>
                        <strong>Enfermedad Cerebro Vascular:</strong> certificado del médico especialista.<br>
                        <strong>Insuficiencia renal crónica:</strong> certificado del médico especialista<br>
                        <strong>Esclerosis Múltiple: </strong>certificado del médico especialista, enfermedad cerebro vascular.<br>
                        <strong>Revascularización coronaria (By–pass):</strong> descripción quirúrgica del procedimiento.</li>
                        <li><strong>Trasplante de órgano mayor:</strong> descripción quirúrgica del procedimiento, traumatismo mayor de cabeza, certificación de un neurólogo sobre el trastorno de la función cerebral y resultado de test neurológico (TAC o Resonancias Nuclear Magnéticas).</li>
                    </ul>
                </div>
            </div>
            
        </div>

    </div>
   
    <div class="row">
        <br/>
    </div>
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>

