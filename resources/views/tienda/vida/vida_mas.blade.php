
    <!--header-area end-->
    @include('layouts.master')
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row">
            <div class="col-lg-12 col-xs-12 text-center">
                <h2 >Plan Vida Mas</h2>
            </div>
        </div>
    </div>     
    <div class="row">
        <br/><br/>
    </div>
     <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h2 style="margin-top: 0;">Beneficios del producto</h2>
                    <h3 dir="ltr">Siempre puedes contar con nosotros</h3>
                    <p dir="ltr">Vida Más se puede activar en los casos en los que el seguro de vida es a valor constante. Aplica para varios de los planes de créditos de Consumo, Comerciales, Redescuento y Microcréditos.&nbsp; El valor asegurado será el valor del desembolso inicial del crédito multiplicado por dos (2) y permanece constante durante toda la vigencia del mismo. Esta opción se activa a razón de la voluntad del tomador. También puede ser cancelada cuando el Cliente así lo indique (sin cancelar la parte correspondiente al seguro obligatorio). Los Clientes que adquieren vida más tienen los siguientes beneficios:</p>
                    <ul class="list-basic" dir="ltr">
                        <li>Solicitas el producto sin necesidad de trámites ni documentos adicionales al seguro de vida de deudores.</li>
                        <li>Cuentas con cobertura ante muerte e incapacidad total y permanente.</li>
                        <li>Tienes auxilio funerario hasta por $1.000.000.</li>
                        <li>Pagas cada mes tu prima dentro de la cuota del crédito, sin costo de financiación ni trámites adicionales.</li>
                        <li>La póliza se renueva de forma automática durante la vigencia del crédito.</li>
                    </ul>
                </div>
               
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2 style="margin-top: 0;">Características del producto</h2>
                    <h3>Características del Plan Vida Más</h3>

                    <ul class="list-basic" dir="ltr">
                        <li>En caso de que ocurra alguno de los eventos que cubre este seguro, la deuda es saldada y se paga el valor adicional a ti o a tus beneficiarios.</li>
                        <li>Esta es una póliza colectiva de vida voluntaria para los clientes Bancolombia, con crédito de consumo o libre inversión atados a la póliza de vida deudor.</li>
                        <li>La empresa aseguradora es Suramericana y el valor asegurado será el doble del monto desembolsado.</li>
                        <li>En caso de fallecimiento del asegurado, tus beneficiarios pueden contar con auxilio funerario.</li>
                    </ul>   
                </div>

                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión del seguro</h2>
                    <h3 dir="ltr">Cómo solicitar tu Plan Vida Más</h3>

                    <p dir="ltr">El Plan Vida Más lo podrás adquirir al momento de firmar la documentación para el desembolso de tu crédito de consumo o de libre inversión. Pregúntale a tu asesor.</p>

                    <h3 dir="ltr">¿Cuál es el procedimiento en caso de siniestros?</h3>

                    <p dir="ltr">En caso de ocurrir algún evento cubierto en la póliza, debes reportarlo cuanto antes a tu contacto comercial de Bancolombia.</p>

                    <p dir="ltr">Recuerda proporcionar en las Sucursales Físicas la documentación, necesaria para el pago de la indemnización en caso de</p>

                    <ul class="list-basic" dir="ltr">
                        <li>Muerte del asegurado.</li>
                        <li>Muerte accidental del asegurado.</li>
                        <li>Incapacidad total y permanente del asegurado.</li>
                    </ul>

                    <p dir="ltr">El contacto comercial le enviará toda la documentación al corredor de seguros encargado de presentar la reclamación ante la aseguradora.</p>

                    

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                    </ul>
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-home-mobile" role="tab" aria-controls="pills-home-mobile" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-profile-mobile-tab" data-toggle="pill" href="#pills-profile-mobile" role="tab" aria-controls="pills-profile-mobile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home-mobile" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h2 style="margin-top: 0;">Beneficios del producto</h2>
                    <h3 dir="ltr">Siempre puedes contar con nosotros</h3>
                    <p dir="ltr">Vida Más se puede activar en los casos en los que el seguro de vida es a valor constante. Aplica para varios de los planes de créditos de Consumo, Comerciales, Redescuento y Microcréditos.&nbsp; El valor asegurado será el valor del desembolso inicial del crédito multiplicado por dos (2) y permanece constante durante toda la vigencia del mismo. Esta opción se activa a razón de la voluntad del tomador. También puede ser cancelada cuando el Cliente así lo indique (sin cancelar la parte correspondiente al seguro obligatorio). Los Clientes que adquieren vida más tienen los siguientes beneficios:</p>
                    <ul class="list-basic" dir="ltr">
                        <li>Solicitas el producto sin necesidad de trámites ni documentos adicionales al seguro de vida de deudores.</li>
                        <li>Cuentas con cobertura ante muerte e incapacidad total y permanente.</li>
                        <li>Tienes auxilio funerario hasta por $1.000.000.</li>
                        <li>Pagas cada mes tu prima dentro de la cuota del crédito, sin costo de financiación ni trámites adicionales.</li>
                        <li>La póliza se renueva de forma automática durante la vigencia del crédito.</li>
                    </ul>
                </div>
               
                <div class="tab-pane fade" id="pills-profile-mobile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2 style="margin-top: 0;">Características del producto</h2>
                    <h3>Características del Plan Vida Más</h3>

                    <ul class="list-basic" dir="ltr">
                        <li>En caso de que ocurra alguno de los eventos que cubre este seguro, la deuda es saldada y se paga el valor adicional a ti o a tus beneficiarios.</li>
                        <li>Esta es una póliza colectiva de vida voluntaria para los clientes Bancolombia, con crédito de consumo o libre inversión atados a la póliza de vida deudor.</li>
                        <li>La empresa aseguradora es Suramericana y el valor asegurado será el doble del monto desembolsado.</li>
                        <li>En caso de fallecimiento del asegurado, tus beneficiarios pueden contar con auxilio funerario.</li>
                    </ul>   
                </div>
                
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión del seguro</h2>
                    <h3 dir="ltr">Cómo solicitar tu Plan Vida Más</h3>

                    <p dir="ltr">El Plan Vida Más lo podrás adquirir al momento de firmar la documentación para el desembolso de tu crédito de consumo o de libre inversión. Pregúntale a tu asesor.</p>

                    <h3 dir="ltr">¿Cuál es el procedimiento en caso de siniestros?</h3>

                    <p dir="ltr">En caso de ocurrir algún evento cubierto en la póliza, debes reportarlo cuanto antes a tu contacto comercial de Bancolombia.</p>

                    <p dir="ltr">Recuerda proporcionar en las Sucursales Físicas la documentación, necesaria para el pago de la indemnización en caso de</p>

                    <ul class="list-basic" dir="ltr">
                        <li>Muerte del asegurado.</li>
                        <li>Muerte accidental del asegurado.</li>
                        <li>Incapacidad total y permanente del asegurado.</li>
                    </ul>

                    <p dir="ltr">El contacto comercial le enviará toda la documentación al corredor de seguros encargado de presentar la reclamación ante la aseguradora.</p>

                    

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                    </ul>
                </div>
            </div>
            
        </div>

    </div>
    <div class="row">
        <br/>
    </div>

    <!--faq-area start-->
    <div class="faq-area ">
        <div class="container">
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        Información relacionada
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body" style="padding: 0;">
                                    <div class="col-sm-12">
                                        <h3 dir="ltr">¿Cómo cancelar el seguro?</h3>

                                        <p dir="ltr">Para la cancelación del Seguro Vida Más debes estar al día en tus pagos y ponerte en contacto con la línea de atención al cliente o acercarte a las oficinas de Sufi.</p>

                                        <h3 dir="ltr">Causales de terminación</h3>

                                        <ul class="list-basic" dir="ltr">
                                            <li>Por solicitud del cliente.</li>
                                            <li>La aseguradora revoca la póliza.</li>
                                            <li>El cliente termina su crédito o leasing.</li>
                                        </ul>

                                        <h3 dir="ltr">Canales de atención posventa</h3>
                                        <ul class="list-basic">
                                            <li>Teléfono: +57 (2) 485 11 05</li>
                                            <li>Celular: +57 313 680 49 88</li>
                                            <li>WhatsApp: +57 313 680 49 88</li>
                                            <li>Email: servicioalcliente@prontoyseguros.com</li>
                                        </ul>
                                        <h3><a href="{{ url('contactenos') }}" title="Contactenos">Contactenos </a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="row">
        <br/>
    </div>
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>

