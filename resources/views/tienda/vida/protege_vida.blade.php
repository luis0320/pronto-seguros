
    <!--header-area end-->
    @include('layouts.master')
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row">
            <div class="col-lg-12 col-xs-12 text-center">
                <h2>Plan Protege tu Vida</h2>
            </div>
        </div>
    </div>     
    <div class="row">
        <br/><br/>
    </div>
     <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Planes</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h2 style="margin-top: 0;">Beneficios del producto</h2>

                    <p dir="ltr">Pensando en proteger tu vida Creamos estas herramientas pensando en tu seguidad y la de tu familia las cuales puedes conoces a continuación.</p>

                    <p dir="ltr"><strong>Beneficios:</strong></p>
                    <p dir="ltr"><strong>1.</strong>&nbsp;Orientación telefónica en temas relacionados con:</p>
                    <ul class="list-basic" dir="ltr">
                        <li>Inquietudes pediátricas.</li>
                        <li>Inquietudes médicas.</li>
                        <li>Asesoría psicológica.</li>
                        <li>Asesoría jurídica en temas relacionados con asuntos notariales, policiales, familiares, laborales, civiles, comerciales y administrativos.</li>
                        <li>Asesoría nutricional.</li>
                    </ul>

                    <p dir="ltr"><strong>2.</strong> <span>Descuentos en farmacia a la hora de comprar tus medicamentos</span></p>
                    <p dir="ltr"><strong>3.</strong> <span>Acceso a revistas y diarios virtual PASALAPAGINA.COM</span></p>
                    <p dir="ltr"><strong>4.</strong> <span>Descuentos en curos virtuales en inglés en open english y Wall Street.</span></p>
                    <p dir="ltr"><strong>5.</strong> <span>Gratis 1 clase de cocina con chef en vivo online personalizada.</span></p>
                    <p dir="ltr"><strong>6.</strong> <span>Descuentos en gimnasios.</span></p>
                    <p><strong>Para acceder a estos servicios contactanos:</strong></p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>

                </div>
                
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2 style="margin-top: 0;">Características</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>Realizar los pagos mensuales, por medio de débito automático a tu cuenta de ahorros o corriente, o con tu Tarjeta de Crédito Bancolombia Visa, MasterCard o American Express, además el seguro se renovará auténticamente cada año.</li>
                        <li>Edad de ingreso y permanencia: Personas entre los 18 y 65 años de edad. Tener en cuenta que para afiliarte no requieres exámenes médicos ni otro tipo de trámite.</li>
                        <li>La adquisición de este producto es voluntaria y su expedición es inmediata. Solo es necesario que respondas de forma precisa y que firmes la declaración de asegurabilidad.</li>
                        <li>Podrás elegir entre planes con los valores asegurados desde $40 millones, el valor máximo que puedes adquirir es de $400 millones. Para lo anterior puedes asignar los beneficiarios que quieras, pero la suma de los porcentajes de todos ellos debe ser 100%.</li>
                        <li>La suma asegurada se incrementa anualmente con base en el índice de precios al consumidor (IPC).</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2 style="margin-top: 0;">Planes</h2>
                    <h3 dir="ltr">Elige el plan que se adapte a tus necesidades</h3>

                    <p dir="ltr">Elige entre tres opciones durante cinco, diez o quince años:</p>

                    <ul class="list-basic" dir="ltr">
                        <li>Plan A: aseguras una mensualidad de $1.200.000.</li>
                        <li>Plan B: aseguras una mensualidad de $800.000.</li>
                        <li>Plan C: aseguras una mensualidad de $400.000.</li>
                    </ul>
                    <p>Contamos con estos y más planes. Llámanos o chatea con nosotros para darte una asesoría personalizada y ayudarte a elegir la mejor opción de cobertura y valor asegurado de acuerdo a tus necesidades.</p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión del seguro</h2>
                    <h3 dir="ltr">¿Cómo solicitar tu Plan Protege tú Vida?</h3>

                    <p dir="ltr">Para obtener este producto debes ser cliente Bancolombia (tener cuenta de ahorro, cuenta corriente o tarjeta de crédito) Acércate a nuestra Sucursal Física, donde un contacto comercial te orientará para elegir el plan que más se adapta a tus necesidades, diligenciar la póliza, cumplir con las condiciones, leer y aceptar la declaración de asegurabilidad.</p>

                    <h3 dir="ltr">¿Cómo es el proceso de devolución de primas?</h3>

                    <p dir="ltr">En caso de existir devolución de primas, la aseguradora procede a hacer el reembolso del dinero a tu cuenta de ahorros o corriente, o abonarlo a tu tarjeta de crédito. También puedes reclamar tu cheque en cualquier sucursal de Suramericana.</p>

                    <h3 dir="ltr">¿Cuál es el procedimiento en caso de reclamaciones y novedades del seguro?</h3>

                    <p dir="ltr">En caso de ocurrir algún evento cubierto en la póliza, tú o tus beneficiarios deben reportarlo cuanto antes.</p>

                    <p dir="ltr">Si lo deseas, puedes comunicarte desde la comodidad de tu casa a la línea de atención, la cual posee un horario de atención las 24 horas.</p>

                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>

                    <p dir="ltr">Allí te orientaran en cuanto a los documentos necesarios para proceder con la reclamación, te informaran el correo al cual debes dirigir los documentos y se llevará trazabilidad de tu caso para que este no se pierda y podamos responderte de una manera oportuna.</p>

                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-home-mobile" role="tab" aria-controls="pills-home-mobile" aria-selected="true" style="color: #000;">Beneficios del producto</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-profile-mobile-tab" data-toggle="pill" href="#pills-profile-mobile" role="tab" aria-controls="pills-profile-mobile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Planes</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home-mobile" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h2 style="margin-top: 0;">Beneficios del producto</h2>

                    <p dir="ltr">Pensando en proteger tu vida Creamos estas herramientas pensando en tu seguidad y la de tu familia las cuales puedes conoces a continuación.</p>

                    <p dir="ltr"><strong>Beneficios:</strong></p>
                    <p dir="ltr"><strong>1.</strong>&nbsp;Orientación telefónica en temas relacionados con:</p>
                    <ul class="list-basic" dir="ltr">
                        <li>Inquietudes pediátricas.</li>
                        <li>Inquietudes médicas.</li>
                        <li>Asesoría psicológica.</li>
                        <li>Asesoría jurídica en temas relacionados con asuntos notariales, policiales, familiares, laborales, civiles, comerciales y administrativos.</li>
                        <li>Asesoría nutricional.</li>
                    </ul>

                    <p dir="ltr"><strong>2.</strong> <span>Descuentos en farmacia a la hora de comprar tus medicamentos</span></p>
                    <p dir="ltr"><strong>3.</strong> <span>Acceso a revistas y diarios virtual PASALAPAGINA.COM</span></p>
                    <p dir="ltr"><strong>4.</strong> <span>Descuentos en curos virtuales en inglés en open english y Wall Street.</span></p>
                    <p dir="ltr"><strong>5.</strong> <span>Gratis 1 clase de cocina con chef en vivo online personalizada.</span></p>
                    <p dir="ltr"><strong>6.</strong> <span>Descuentos en gimnasios.</span></p>
                    <p><strong>Para acceder a estos servicios contactanos:</strong></p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>

                </div>
                
                <div class="tab-pane fade" id="pills-profile-mobile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2 style="margin-top: 0;">Características</h2>
                    <ul class="list-basic" dir="ltr">
                        <li>Realizar los pagos mensuales, por medio de débito automático a tu cuenta de ahorros o corriente, o con tu Tarjeta de Crédito Bancolombia Visa, MasterCard o American Express, además el seguro se renovará auténticamente cada año.</li>
                        <li>Edad de ingreso y permanencia: Personas entre los 18 y 65 años de edad. Tener en cuenta que para afiliarte no requieres exámenes médicos ni otro tipo de trámite.</li>
                        <li>La adquisición de este producto es voluntaria y su expedición es inmediata. Solo es necesario que respondas de forma precisa y que firmes la declaración de asegurabilidad.</li>
                        <li>Podrás elegir entre planes con los valores asegurados desde $40 millones, el valor máximo que puedes adquirir es de $400 millones. Para lo anterior puedes asignar los beneficiarios que quieras, pero la suma de los porcentajes de todos ellos debe ser 100%.</li>
                        <li>La suma asegurada se incrementa anualmente con base en el índice de precios al consumidor (IPC).</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2 style="margin-top: 0;">Planes</h2>
                    <h3 dir="ltr">Elige el plan que se adapte a tus necesidades</h3>

                    <p dir="ltr">Elige entre tres opciones durante cinco, diez o quince años:</p>

                    <ul class="list-basic" dir="ltr">
                        <li>Plan A: aseguras una mensualidad de $1.200.000.</li>
                        <li>Plan B: aseguras una mensualidad de $800.000.</li>
                        <li>Plan C: aseguras una mensualidad de $400.000.</li>
                    </ul>
                    <p>Contamos con estos y más planes. Llámanos o chatea con nosotros para darte una asesoría personalizada y ayudarte a elegir la mejor opción de cobertura y valor asegurado de acuerdo a tus necesidades.</p>
                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2 style="margin-top: 0;">Solicitud y gestión del seguro</h2>
                    <h3 dir="ltr">¿Cómo solicitar tu Plan Protege tú Vida?</h3>

                    <p dir="ltr">Para obtener este producto debes ser cliente Bancolombia (tener cuenta de ahorro, cuenta corriente o tarjeta de crédito) Acércate a nuestra Sucursal Física, donde un contacto comercial te orientará para elegir el plan que más se adapta a tus necesidades, diligenciar la póliza, cumplir con las condiciones, leer y aceptar la declaración de asegurabilidad.</p>

                    <h3 dir="ltr">¿Cómo es el proceso de devolución de primas?</h3>

                    <p dir="ltr">En caso de existir devolución de primas, la aseguradora procede a hacer el reembolso del dinero a tu cuenta de ahorros o corriente, o abonarlo a tu tarjeta de crédito. También puedes reclamar tu cheque en cualquier sucursal de Suramericana.</p>

                    <h3 dir="ltr">¿Cuál es el procedimiento en caso de reclamaciones y novedades del seguro?</h3>

                    <p dir="ltr">En caso de ocurrir algún evento cubierto en la póliza, tú o tus beneficiarios deben reportarlo cuanto antes.</p>

                    <p dir="ltr">Si lo deseas, puedes comunicarte desde la comodidad de tu casa a la línea de atención, la cual posee un horario de atención las 24 horas.</p>

                    <ul class="list-basic">
                        
                        <li>Teléfono: +57 (2) 485 11 05</li>
                        <li>Celular: +57 313 680 49 88</li>
                        <li>WhatsApp: +57 313 680 49 88</li>
                        <li>Email: servicioalcliente@prontoyseguros.com</li>
                        <li><a href="{{ url('nosotros') }}"  style="font-weight:800">Tienes Dudas? Contáctanos!</a></li>
                    </ul>

                    <p dir="ltr">Allí te orientaran en cuanto a los documentos necesarios para proceder con la reclamación, te informaran el correo al cual debes dirigir los documentos y se llevará trazabilidad de tu caso para que este no se pierda y podamos responderte de una manera oportuna.</p>

                </div>
            </div>
            
        </div>

    </div>
    <div class="row">
        <br/>
    </div>

    <!--faq-area start-->
    <div class="faq-area ">
        <div class="container">
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        Información relacionada
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body" style="padding: 0;">
                                    <div class="col-sm-12">
                                        <h3>Incrementos y exclusiones</h3>

                                        <ul class="list-basic">
                                            <li><strong>Incremento anual de la suma asegurada</strong><br>
                                            La suma asegurada se incrementará cada año, a partir de la fecha de inicio de la vigencia del seguro, con base en el Índice de Precios al Consumidor (IPC) de los últimos doce meses conocidos, certificados por el DANE. En el momento de afectarse el amparo del seguro, Suramericana liquidará la suma a pagar, con base en el valor alcanzado a la fecha de la última renovación.</li>
                                        </ul>

                                        <h3>Plazos y términos</h3>

                                        <p><a href="#" title="Conoce los plazos y términos de este producto" target="_blank">Conoce los plazos y términos de este producto. </a></p>

                                        <h3>¿Cómo cancelar el seguro?</h3>

                                        <p>Para la cancelación del seguro Plan Vida Renta, el tomador o asegurado debe autorizar por escrito la cancelación de la póliza y llevar la carta a una oficina de Bancolombia.</p>

                                        <h3>Causales de terminación</h3>

                                        <ul class="list-basic">
                                            <li>Por mora en el pago de la prima.</li>
                                            <li>Cuando el tomador así lo determine.</li>
                                            <li>Al fallecimiento del asegurado.</li>
                                            <li>Por el pago del amparo de incapacidad total y permanente.</li>
                                            <li>Para el amparo básico de vida, al finalizar la vigencia de la póliza en la cual el asegurado cumpla los 80 años de edad. Para el amparo de incapacidad total y permanente al finalizar la vigencia de la póliza en la cual el asegurado cumpla 70 años de edad. En el caso del amparo de curación, al finalizar la vigencia en que el asegurado cumple 39 años.</li>
                                            <li>Salvo el amparo de vida, los demás amparos podrán ser revocados unilateralmente por Suramericana, mediante el envío de una comunicación escrita dirigida al asegurado, con por lo menos diez días hábiles de antelación a la terminación efectiva del amparo.</li>
                                        </ul>

                                        <h3>Más información</h3>

                                        <ul class="list-basic">
                                            <li>Teléfono: +57 (2) 485 11 05</li>
                                            <li>Celular: +57 313 680 49 88</li>
                                            <li>WhatsApp: +57 313 680 49 88</li>
                                            <li>Email: servicioalcliente@prontoyseguros.com</li>
                                        </ul>
                                        <h3><a href="{{ url('contactenos') }}" title="Contactenos">Contactenos </a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="row">
        <br/>
    </div>
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>

