@include('layouts.masteradmin')

            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Soat</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
                            <li class="breadcrumb-item active">Cotizaciones</li>
                        </ol>
                        <!--div class="card mb-4">
                            <div class="card-body">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>.</div>
                        </div-->
                        <div class="card mb-4">
                            <div class="card-header" style="color:#fff"><i class="fas fa-table mr-1"></i>Clientes que cotizan soat</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>placa</th>
                                                <th>Fecha de <br> Vencimiento</th>
                                                <th>Total</th>
                                                <th>Descuento</th>
                                                <th>Ahora</th>
                                                <th>Marca</th>
                                                <th>Linea</th>
                                                <th>Modelo</th>
                                                <th>Cedula</th>
                                                <th>Celular</th>
                                                <th>Correo</th>
                                                <th>Fecha de consulta</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cotizacionsoats as $cotizacionsoat)
                                            <tr>
                                                <td>{{ $cotizacionsoat->id }}</td>
                                                <td>{{ $cotizacionsoat->placa }}</td>
                                                <td>{{ $cotizacionsoat->fecha }}</td>
                                                <td>{{ $cotizacionsoat->total }}</td>
                                                <td>{{ $cotizacionsoat->descuento }}</td>
                                                <td>{{ $cotizacionsoat->preciodes }}</td>
                                                <td>{{ $cotizacionsoat->marca }}</td>
                                                <td>{{ $cotizacionsoat->linea }}</td>
                                                <td>{{ $cotizacionsoat->modelo }}</td>
                                                <td>{{ $cotizacionsoat->cedula }}</td>
                                                <td>{{ $cotizacionsoat->celular }}</td>
                                                <td>{{ $cotizacionsoat->correo }}</td>
                                                <td>{{ $cotizacionsoat->fecha_creacion }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>

      
    </body>
</html>
