
    <!--header-area end-->
    @include('layouts.master')
<!--page-banner-area start-->
    <div class="page-banner-area bg-nosotros">
        <div class="container">
            <div class="row align-items-center height-800">
                <div class="col-lg-12">
                    <div class="page-banner-text text-center mt-minus-10">
                        <h2>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--banner-area end-->
    
    <!--story-area start-->
    <!--div class="company-story-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-12 img-100p">
                    <img src="assets/images/about/1.jpg" alt="" />
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="about-text">
                    
                        <h2>Quienes Somos</h2>
                        
                        <p>Brindamos servicios profesionales y soluciones integrales para el manejo eﬁciente de los riesgos, 
                           en busca de la satisfacción oportuna de las necesidades de nuestros clientes actuales y potenciales, 
                            soportados en un grupo humano competente y altamente comprometido en la búsqueda de un mejoramiento continuo 
                            del sistema de gestión de calidad.</p>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--story-area end-->
    
    <!--about-area start-->
    <div class="about-area mt-85 sm-mt-35">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title text-center">
                        <h3 class="font-weight-bold" id="linea_tittle">Por qué elegirnos</h3>
                        <p>Brindamos servicios profesionales con gran experiencia y conocimiento en soluciones integrales para el manejo eﬁciente de los riesgos, asegurando una excelente garantía de acompañamiento y atención personalizada. ¡Confía en nosotros, estaremos siempre a tu lado!</p>
                    </div>
                </div>
            </div>
            <!--div class="row mt-55 sm-mt-35">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="sin-service style-2">
                        <i class="ti-clipboard"></i>
                        <h3>Información relevante</h3>
                        <p>xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="sin-service style-2">
                        <i class="ti-truck"></i>
                        <h3>Información relevante</h3>
                        <p>xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx.</p>
                    </div>
                </div>
                <div class="col-lg-4 d-lg-block col-md-6 d-md-none col-sm-12">
                    <div class="sin-service style-2">
                        <i class="ti-cup"></i>
                        <h3>Información relevante</h3>
                        <p>xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx.</p>
                    </div>
                </div>
            </div-->
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <!--about-area end-->
    <div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">¿Por qué preferirnos?</h3>
                </div>
            </div>
            <br/>
            <div class="row">
                
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;">
                    <div class="sin-service" style="padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/companias.png" style="width:35%; margin-top: 33px;height: 84px;">
                        <p style="font-size:14px;text-align: justify; height: 57px; margin-top: 20px;"><br/>Contamos con el respaldo de aseguradoras a nivel nacional</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12"style="padding: 0px 10px;">
                    <div class="sin-service" style=" padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/asesor2.png" style="width:35%; margin-top: 33px;height: 84px;">
                        <p style="font-size:14px;text-align: justify;  height: 57px; margin-top: 20px;"><br/>Asesoría personalizada en todo momento.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;">
                    <div class="sin-service" style="padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/planes.png" style="width:35%; margin-top: 33px;">
                        <p style="font-size:14px;text-align: justify; margin-top: 20px;"><br/>Contamos con un amplio portafolio de servicios, que se ajustan a tus necesidades.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;">
                    <div class="sin-service" style="padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/mapa.png" style="width:35%; margin-top: 33px;">
                        <p style="font-size:14px;text-align: justify; margin-top: 20px;"><br/>Desde la comodidad de tu casa, a un solo click.</p> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 ">
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Promesa de Servicio</h3>
                    <p>Resuelve tus dudas o cuéntanos sobre tu experiencia. En las próximas 24 horas recibirás nuestra llamada o si quieres información inmediata </br>nos puedes contactar a nuestro e-mail, WhatsApp o asesor virtual.</P>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <!--counterup-area start-->
    <!--div class="counterUp-area sm-mt-10">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="single-counter">
                        <p class="count-number count1">90</p>
                        <h4>Proyectos completados</h4>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="single-counter">
                        <p class="count-number count2">120</p>
                        <h4>Clientes felices</h4>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="single-counter">
                        <p class="count-number count3">50</p>
                        <h4>Aliados</h4>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="single-counter">
                        <p class="count-number count4">240</p>
                        <h4>Otro dato a mostrar</h4>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--counterup-area end-->
    
    <!--team-area start-->
    <!--div class="team-area mt-80 sm-mt-60">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 offset-sm-2">
                    <div class="section-title text-center">
                        <h2>Nuestro Equipo</h2>
                    </div>
                </div>
            </div>
            <div class="row mt-55 sm-mt-15">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="single-team">
                        <div class="team-thumb img-100p">
                            <img src="assets/images/team/1.jpg" alt="" />
                            <a href="javascript:void(0);"><i class="ti-plus"></i></a>
                        </div>
                        <div class="team-desc">
                            <h4>Jaqueline Murillas</h4>
                            <small>Gerente General</small>
                        </div>
                        <div class="social-icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="single-team">
                        <div class="team-thumb img-100p">
                            <img src="assets/images/team/2.jpg" alt="" />
                            <a href="javascript:void(0);"><i class="ti-plus"></i></a>
                        </div>
                        <div class="team-desc">
                            <h4>Ollie Schneider</h4>
                            <small>Director general</small>
                        </div>
                        <div class="social-icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="single-team">
                        <div class="team-thumb img-100p">
                            <img src="assets/images/team/3.jpg" alt="" />
                            <a href="javascript:void(0);"><i class="ti-plus"></i></a>
                        </div>
                        <div class="team-desc">
                            <h4>Alex Manning</h4>
                            <small>Desarrollador</small>
                        </div>
                        <div class="social-icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="single-team">
                        <div class="team-thumb img-100p">
                            <img src="assets/images/team/4.jpg" alt="" />
                            <a href="javascript:void(0);"><i class="ti-plus"></i></a>
                        </div>
                        <div class="team-desc">
                            <h4>Juan Rodriguez</h4>
                            <small>Gerente Financiero</small>
                        </div>
                        <div class="social-icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--team-area end-->
    
    <!--testimonial-area start-->
    <!--div class="testimonial-area bg-3 overlay mt-80 sm-mt-60">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 offset-sm-2">
                    <div class="section-title text-white text-center z-index">
                        <h2>Lo que dice nuestros clientes</h2>
                    </div>
                </div>
            </div>
            <div class="row mt-55">
                <div class="col-sm-12">
                    <div class="testimonial-items style-2 carousel-two arrow-none z-index">
                        <div class="single-testimonial style-2 style-3">
                            <img src="assets/images/testimonial/2.jpg" alt="" />
                            <h4>Russell Stephens</h4>
                            <small>Empresa</small>
                            <div class="testimonial-text">
                                <p>xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx .</p>
                            </div>
                        </div>
                        <div class="single-testimonial style-2 style-3">
                            <img src="assets/images/testimonial/3.jpg" alt="" />
                            <h4>Kiwhi Leonard</h4>
                            <small>Persona</small>
                            <div class="testimonial-text">
                                <p>xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx.</p>
                            </div>
                        </div>
                        <div class="single-testimonial style-2 style-3">
                            <img src="assets/images/testimonial/1.jpg" alt="" />
                            <h4>Nancy Franklin</h4>
                            <small>Auto</small>
                            <div class="testimonial-text">
                                <p>xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx .</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--testimonial-area end-->
    
    
   
      @include('layouts.footer')
