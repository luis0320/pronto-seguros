
    <!--header-area end-->
    @include('layouts.master')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-cumplimiento">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/cumplimiento.png" style="width:16%; margin-top:15px">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle" >- Seguro de Cumplimiento -</h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                    </div>
                                    <div>
                                        <input type='hidden' name='seguro' value='Cumplimiento' />
                                        <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;"> 
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/cumplimiento.png" style="width:16%; margin-top:15px">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle" > — Seguro de Cumplimiento — </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                            </div>
                            <div>
                                <input type='hidden' name='seguro' value='Cumplimiento' />
                                <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>

    <!--Estandar-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold">Póliza de cumplimiento</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">La póliza de Seguros de Cumplimiento es un contrato de seguros celebrado entre una compañía de seguros y un tomador, el cual para la compañía de seguros es un afianzado o asegurado, este contrato de seguros con la compañía garantiza el cumplimiento por parte del tomador de una obligación o obligaciones contenidas en la ley.</p>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/vida/cumplimiento.jpg" alt="promo"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <br/>
                    <ul class="list-basic" style="margin-top:15px" >
                        <li id="listado">Esta póliza te garantiza el buen uso del anticipo.</li>
                        <li id="listado">La asesoría técnica de la compañía que emite la póliza de cumplimiento.</li>
                        <li id="listado">El cumplimiento del contrato  que asegure el cumplimiento de un proyecto.</li>
                        <li id="listado">A la  hora  de auditar  a tu proveedor, la opinión experta de la  aseguradora, es una garantía de seriedad.</li>
                        <li id="listado">Al  solicitar  a  tus proveedores  este tipo de pólizas  resguardar sus intereses comerciales  y su patrimonio.</li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                   
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Cobertura Básica</a>
                    </li>
                    <!--li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas Adicionales</a>
                    </li-->
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Cobertura Básica</h3>
                    <p>Cubre los perjuicios patrimoniales que sufra directamente el contratante o receptor de la oferta, quien ostenta la calidad de asegurado de la presente póliza.</p>
                    
                </div>
                <!--div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Buen manejo del anticipo (Cubre a la entidad asegurada por los perjuicios derivados de la no inversión del anticipo y apropiación indebida del anticipo.)</li>
                        <li>Devolución de pagos anticipados</li>
                        <li>Pagos de salarios prestaciones sociales legales</li>
                        <li>Estabilidad y calidad de obra</li>
                        <li>Calidad y correcto funcionamiento</li>
                    </ul>
                    
                </div-->
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Cobertura Básica</a>
                    </li>
                    <!--li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas Adicionales</a>
                    </li-->
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Cobertura Básica</h3>
                    <p>Cubre los perjuicios patrimoniales que sufra directamente el contratante o receptor de la oferta, quien ostenta la calidad de asegurado de la presente póliza.</p>
                </div>
                <!--div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Buen manejo del anticipo (Cubre a la entidad asegurada por los perjuicios derivados de la no inversión del anticipo y apropiación indebida del anticipo.)</li>
                        <li>Devolución de pagos anticipados</li>
                        <li>Pagos de salarios prestaciones sociales legales</li>
                        <li>Estabilidad y calidad de obra</li>
                        <li>Calidad y correcto funcionamiento</li>
                    </ul>
                </div-->
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                
            </div>
            

        </div>

    </div>
    <div class="row">
        <br/>
    </div>
    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Que  es  una  póliza  de  cumplimiento?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Es una póliza que protege el patrimonio del contratante ante los incumplimientos imputables del deudor.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Que  cubre una póliza  de cumplimiento?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Cubre los perjuicios y daños como consecuencia del incumplimiento por parte del afianzado.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Como  se  calcula  la prima de una póliza  de cumplimiento?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>El  valor de la póliza de cumplimiento se toma un porcentaje del valor de los  contratos  y este será el valor  que tendrá que pagar según la vigencia del contrato  para cubrir los riesgos de incumplimiento.</p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿Donde  se  compra  una póliza  de cumplimiento?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>A traves de  Pronto y Seguros, ofrecemos  pólizas  de cumplimiento de acuerdo  a sus  necesidades con las las aseguradoras  las cuales son las únicas autorizadas para ofrecer seguros de cumplimiento, a las personas o empresas interesadas en contratar este tipo de seguros.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Como  se  hace  efectiva  una póliza  de cumplimiento?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-basic">
                                    <li>Se  presenta  el reclamo  ante  el proveedor  contratista.</li>
                                    <li>Notifica  a la  aseguradora.</li>
                                    <li>La  compañía  aseguradora  se comunica  con el afectado  y  le  suministra las  instrucciones  para  continuar con los soportes  de la reclamación.
                                        <ul class="list-basic">
                                            <li>Primero: Presentación del reclamo ante la contratista.</li>
                                            <li>Segundo: Notificación a la aseguradora.</li>
                                            <li>Tercero: Esperar.</li>
                                        </ul>
                                    </li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!--div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        ¿Definir pregunta?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Definir</p>
                                </div>
                            </div>
                        </div-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Revisemos el contrato para cotizar la mejor opción. </h3> 
                    
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    <div class="row">
        <br/><br/>
    </div>
    <!--Fin Estandar-->
    
    <!--page-banner-area start-->
    <!--div class="page-banner-area bg-cumplimiento">
        <div class="container">
            <div class="row align-items-center height-400">
                <div class="col-lg-12">
                    <div class="page-banner-text text-center text-white">
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--banner-area end-->
    <!--blog-area start-->
    <!--div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>Póliza de cumplimiento</h2>
                    </div>
                    <div class="row mt-30">
                        <div class="col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <p>La Póliza De Cumplimiento fue creada en Colombia por la Ley 225 del 1938.</p>
                                <p>Se define como un contrato celebrado entre una compañía y un contratista.</p>
                                <p>Este seguro ampara los daños patrimoniales en la medida  que pretende el restablecimiento del patrimonio económico del contratante (ASEGURADO) por causa del incumplimiento del contrato por parte del tomador del seguro o contratista.</p>
                                <p>El Asegurado es la persona a la que se le garantiza cubrir el perjuicio en caso de incumplimiento de los contratistas.</p>
                            </div>
                            <div class="section-title style-4 text-right pt-10">
                                <a href="{{ url('contactenos') }}"><h2>Contáctanos</h2></a>
                                <p> <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/> Viviana Melo +57 316 6825935</p>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
             <br/> 
             <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" style="border: 3px ; border-style: ridge;">
                    <div class="blog-details">
                        <div class="section-title style-4 text-center pt-10">
                            <h2>Coberturas</h2>
                        </div>
                        <div class="section-title style-4 text-left pt-10">
                            <p>Ampara durante la vigencia de un año los siguiente eventos:</p>
                        </div>

                    </div>
                </div>
                
                <div class="col-lg-6 col-md-12 col-sm-12" style="border: 3px ; border-style: ridge;">
                    <div class="blog-details">
                        <div class="section-title style-4 text-left pt-10">
                            <h3>Cobertura Básica </h3>
                        </div>
                        <div class="blog-details-text">
                            <ul>
                                <li>Seriedad en la oferta ( Cubre al asegurado frente al incumplimiento del tomador del contrato)</li>
                                <li>Cumplimiento del contrato</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12" style="border: 3px; border-style: ridge; ">
                    <div class="blog-details">
                        <div class="section-title style-4 text-left pt-10">
                            <h3>Cobertura Adicionales </h3>
                        </div>
                        <div class="blog-details-text">
                            <ul>
                                <li>Buen manejo del anticipo (Cubre a la entidad asegurada por los perjuicios derivados de la no inversión del anticipo y apropiación indebida del anticipo.)</li>
                                <li>Devolución de pagos anticipados</li>
                                <li>Pagos de salarios prestaciones sociales legales</li>
                                <li>Estabilidad y calidad de obra</li>
                                <li>Calidad y correcto funcionamiento</li>
                            </ul>
                        </div>
                    </div>
                </div> 
            </div>

        </div>
    </div>

    <br/-->
    <!--blog-area end-->
    
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>