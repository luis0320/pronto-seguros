
    <!--header-area end-->
    @include('layouts.master')
    
    <!--page-banner-area start-->
    <!--div class="page-banner-area bg-construccion">
        <div class="container">
            <div class="row align-items-center height-400">
                <div class="col-lg-12">
                    <div class="page-banner-text text-center text-white">
                    </div>
                </div>
            </div>
        </div>
    </div-->

    <!--story-area start-->
    <div class="company-story-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-12 img-100p">
                    <img src="assets/images/construccion.png" alt="" />
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>Póliza de construcción</h2>
                    </div>
                    <div class="row mt-30">
                        <div class="col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <p>Esta póliza ampara los daños ocurridos durante la construcción ó montaje en el predio donde se realiza la obra, siempre que dichos daños sucedan de manera súbita e imprevista.</p>
                                <p>Cubre la construcción de toda clase de obras civiles tales como: edificios, túneles, puentes represas, carreteras, etc. Proporciona amplia protección tanto contra daños sufridos por la obra, incluyendo el equipo de construcción del contratista y/o maquinaria de construcción, como contra reclamos de terceros por daños a sus bienes o a sus personas, cuando éstos ocurran como consecuencia de los trabajos de construcción mencionados.</p>
                            </div>
                            <div class="section-title style-4 text-right pt-10">
                                <a href="{{ url('contactenos') }}"><h2>Contáctanos</h2></a>
                                <p> <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/> Viviana Melo +57 316 6825935</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--banner-area end-->
    <!--blog-area start-->
    <div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>Póliza de construcción</h2>
                    </div>
                    <div class="row mt-30">
                        <div class="col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <p>Esta póliza ampara los daños ocurridos durante la construcción ó montaje en el predio donde se realiza la obra, siempre que dichos daños sucedan de manera súbita e imprevista.</p>
                                <p>Cubre la construcción de toda clase de obras civiles tales como: edificios, túneles, puentes represas, carreteras, etc. Proporciona amplia protección tanto contra daños sufridos por la obra, incluyendo el equipo de construcción del contratista y/o maquinaria de construcción, como contra reclamos de terceros por daños a sus bienes o a sus personas, cuando éstos ocurran como consecuencia de los trabajos de construcción mencionados.</p>
                            </div>
                            <div class="section-title style-4 text-right pt-10">
                                <a href="{{ url('contactenos') }}"><h2>Contáctanos</h2></a>
                                <p> <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/> Viviana Melo +57 316 6825935</p>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
             <br/> 
             <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" style="border: 3px ; border-style: ridge;">
                    <div class="blog-details">
                        <div class="section-title style-4 text-center pt-10">
                            <h2>Coberturas</h2>
                        </div>
                        <div class="section-title style-4 text-left pt-10">
                            <p>Ampara durante la vigencia de un año los siguiente eventos:</p>
                        </div>

                    </div>
                </div>
                
                <div class="col-lg-6 col-md-12 col-sm-12" style="border: 3px ; border-style: ridge;">
                    <div class="blog-details">
                        <div class="section-title style-4 text-left pt-10">
                            <h3>Cobertura Básica </h3>
                        </div>
                        <div class="blog-details-text">
                            <ul>
                                <li>Inexperiencia, descuido y actos mal intencionados cometidos por los empleados del asegurado</li>
                                <li>Sustracción con violencia</li>
                                <li>Rayo</li>
                                <li>Incendio y explosión</li>
                                <li>Corto circuito</li>
                                <li>Caída de aeronaves</li>
                                <li>Daños materiales que sufran los bienes asegurados durante la construcción, por cualquier causa que no esté expresamente excluida y que no pudiera ser cubierta bajo los amparos opcionales.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12" style="border: 3px; border-style: ridge; ">
                    <div class="blog-details">
                        <div class="section-title style-4 text-left pt-10">
                            <h3>Cobertura Adicionales </h3>
                        </div>
                        <div class="blog-details-text">
                            <ul>
                                <li>Terremoto, temblor de tierra y erupción volcánica</li>
                                <li>Asonada, motín, conmoción civil o popular, huelga y actos mal intencionados de terceros y terrorismo</li>
                                <li>Inundaciones y riesgos geotécnicos</li>
                                <li>Mantenimiento</li>
                                <li>Responsabilidad civil extracontractual daños materiales</li>
                                <li>Responsabilidad civil extracontractual daños personales</li>
                                <li>Propiedades existentes del asegurado, o propiedades que quedan bajo el cuidado, la custodia o la supervisión del asegurado</li>
                                <li>Propiedades adyacentes</li>
                                <li>Gastos por remoción de escombros</li>
                                <li>Gastos para flete aéreo y gastos adicionales por horas extras, trabajo nocturno, trabajo en días feriados flete expreso</li>
                            </ul>
                        </div>
                    </div>
                </div> 
            </div>

        </div>
    </div>

    <br/>
    <!--blog-area end-->
    
   @include('layouts.footer')