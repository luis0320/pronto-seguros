
    <!--header-area end-->
    @include('layouts.master')
    <style>
        .row:before, .row:after {display: none !important;}
        
    </style>
    <!--banner-area start-->
    <!--div class="banner-area bg-1 overlay">
        <div class="container">
            <div class="row align-items-center height-800 pb-111">
                <div class="col-sm-12">
                    <div class="banner-text text-center">
                        <h2>Pronto y Seguros</h2>
                        
                        <a class="venobox video-play" data-gall="gall-video" data-autoplay="true" data-vbtype="video" href="https://youtu.be/YnNL3_4drC4">
                        <i class="fa fa-play"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin: 15px 0px;">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>

        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
            <div class="page-banner-area bg-bannercinco">
                    <div class="row align-items-center height-400 pb-300">
                        <div class="col-sm-5 col-lg-5"  id="banner_riesgo">
                            <div class="banner-text text-center" style="margin:15px 50px ;">                                
                                <h2 style="color: #fff !important">Seguro <br/>Todo Riesgo</h2>
                                </br>
                                <!--input class="" style="padding: 12px;width: 158px;" matinput="" maxlength="6" id="mat-input-0" placeholder="PLACA" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"-->
                                <a href="{{ url('todo-riesgo3') }}" style="width: 158px;" class="btn-common">Cotiza ya</a>
                            </div>
                        </div>

                        <div class="col-sm-12 col-lg-12"  id="banner_riesgo_mobile">
                            <div class="banner-text text-center" style="margin-bottom: -100px;">                                
                                <!--input class="" style="padding: 4px;width: 100px;" matinput="" maxlength="6" id="mat-input-0" placeholder="PLACA" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"-->
                                <a href="{{ url('todo-riesgo3') }}" style="height: 32px; line-height: 35px; width: 100px;" class="btn-common">Cotiza ya</a>
                                <h2 style="font-size: 20px; color: #fff !important; margin-top: 0;line-height: 28px;">Seguro Todo Riesgo</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item ">
                <div class="page-banner-area bg-banneruno">
                    <div class="row align-items-center height-400 pb-300">
                        <div class="col-sm-4" id="banner_vida">
                            <div class="banner-text text-center" style="margin-bottom: -100px;">
                                <h2 style="color: #fff !important">Seguro </br>de Vida</h2>
                                </br>
                                <a href="{{ url('vida') }}"  class="btn-common">Cotiza aquí</a>
                            </div>
                        </div>


                        <div class="col-sm-12" id="banner_vida_mobile">
                            <div class="banner-text text-center" style="margin: 48px 24px; ;">
                                <h2 style="font-size: 24px;color: #fff !important">Seguro de Vida</h2>
                                <a href="{{ url('vida') }}"  class="btn-common">Cotiza aquí</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="page-banner-area bg-bannerdos">
                    <div class="row align-items-center height-400 pb-300">
                        <div class="col-sm-7" id="banner_hogar">
                            <div class="banner-text text-center">
                            </div>
                        </div>
                        <div class="col-sm-5" id="banner_hogar">
                            <div class="banner-text text-center" style="margin:40px 24px ;">
                                <h2 style="color: #fff !important">Seguro de Hogar</h2>
                                </br>
                                <a href="{{ url('hogar') }}"  class="btn-common">Cotiza aquí</a>
                            </div>
                        </div>
                        <div class="col-xs-5" id="banner_hogar_mobile">
                            <div class="banner-text text-center">
                            </div>
                        </div>

                        <div class="col-xs-7" id="banner_hogar_mobile">
                            <div class="banner-text text-center" style="margin: 40px 24px;">
                                <h2 style="font-size: 22px;color: #fff !important">Seguro de hogar</h2>
                                <a href="{{ url('hogar') }}"  class="btn-common">Cotiza aquí</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="width:8%">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next" style="width:8%">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!--banner-area end-->
    <!--about-area start-->
    <div class="about-area mt-85 sm-mt-30">
       
    <div class="footer-widget">
        <h3 style="margin-bottom:0px; text-align: center;">Trabajamos con las mejores aseguradoras</h3>
        <!--style="font-family: 'Delius Swash Caps', cursive;"--><br>
    </div>
            

    
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class="footer-widget" style="box-shadow:1px 1px 10px #7b7a7a">
                
                    <div class="instagram-imgages" style="text-align: center;">
                    <MARQUEE BEHAVIOR=ALTERNATE>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/sura.png" alt="sura" style="" /></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/solidaria.png" alt="estado" /></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/sbs.png" alt="sbs"/></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/previsora.png" alt="previsora"/></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/mapfre.png" alt="mapfre"/></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/liberty.png" alt="liberty"/></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/hdi.png" alt="hdi" style="width:70px" /></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/bolivar.png" alt="bolivar"/></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/axa_colpatria.png" alt="axa"/></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/Allianz.png" alt="allianz" style=""/></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/colmena.png" alt="colmena" style=""/></a>
                        <a href="#" style="border-right: 1px solid #ccc;"><img src="assets/images/blog/seguros_del_estado.png" alt="seguros del estado" style=""/></a>
                        </MARQUEE>
                    </div>
               
                </div>
            </div>
        </div>
  
                    
    
    </div>
    <div class="service-area mt-minus-100 sm-mt-80">
        <div class="container">
            <div class="row ">
                <div class="col-md-10 offset-md-1 col-sm-12">
                    <div class="section-title style-2">
                        <h3  id="linea_tittle">Tenemos los mejores seguros para ti</h3>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--service-area start-->
    <div class="service-area mt-minus-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service text-justify">
                        <img  id="seguros_cuadros_home" src="assets/images/blog/autos 370x240.png" alt="cotizar">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Seguro de Vehículo</h4>
                        </div>
                        <div id="seguros_cuadros">
                            <p>Tenemos las mejores opciones para acompañarte en tu camino, cotiza tu póliza y maneja tranquilo. Cubrimos el 100% de tu vehículo. </p>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('todo-riesgo') }}" class="readmore">Cotizar</a>
                        </div>
                        
                    </div> 
                   
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service text-justify" >
                        <img id="seguros_cuadros_home" src="assets/images/blog/soat_banner.jpg" alt="promo" >
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Soat</h4>
                        </div>
                        <div id="seguros_cuadros">
                            <p > No tenemos ese servicio establecido para la compra del soat. </p>
                            <p > Paginas donde puedes compar el soat en + info. </p>
                        </div>
                        
                        <div class="text-center">
                            <a href="{{ url('soat') }}" class="readmore">+ Info</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service text-justify">
                        <img  id="seguros_cuadros_home" src="assets/images/blog/vida_banner.jpg" alt="promo">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Seguro de Vida</h4>
                        </div>
                        <div id="seguros_cuadros">
                            <p>Por  que todo  puede cambiar  en un instante, piensa en ti y en los tuyos. Un seguro de vida es una garantía para que puedas afrontar algunas circunstancias complicadas con la mayor tranquilidad y apoyo posible.</p>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('vida') }}" class="readmore">+ Info</a>
                        </div>
                        
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service text-justify">
                        
                        <img id="seguros_cuadros_home" src="assets/images/blog/exequial.jpg" alt="promo">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Seguro Exequial</h4>
                        </div>
                        <div id="seguros_cuadros">
                            <p>Cuando fallece un ser querido es importante no preocuparse por cuestiones económicas y tener un seguro que garantice todos los gastos funerarios desde la velación hasta el destino final con el acompañamiento del asesor familiar.</p>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('exequial') }}" class="readmore">+ Info</a>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service text-justify">
                        <img id="seguros_cuadros_home" src="assets/images/blog/salud2_banner.jpg" alt="salud">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Seguro de Salud</h4>
                        </div>
                        <div id="seguros_cuadros">
                            <p>La mejor decisión para cuidar tu salud y la de tu familia. Con un Seguro de Salud permaneces tranquilo porque sabes que estás protegido en todo momento. </p>
                        </div>
                        
                        <div class="text-center">
                            <a href="{{ url('salud') }}" class="readmore">+ Info</a>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service text-justify">
                        <img id="seguros_cuadros_home" src="assets/images/blog/educacion_banner.jpg" alt="educacion">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Seguro Educativo</h4>
                        </div>
                        <div id="seguros_cuadros">
                            <p>La educación ha sido herramienta indispensable para llegar lejos y seguirá siéndolo para las futuras generaciones. Puedes asegurar desde hoy tu educación universitaria o la de tus hijos y beneficiarios, de acuerdo a tus posibilidades presentes.</p>
                        </div>
                        
                        <div class="text-center">
                            <a href="{{ url('educativo') }}" class="readmore">+ Info</a>
                        </div>
                        
                    </div>
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service text-justify">
                        <img id="seguros_cuadros_home" src="assets/images/blog/accidentes_banner.jpg" alt="accidentes">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Accidentes Personas</h4>
                        </div>
                        <div id="seguros_cuadros">
                            <p >Con nuestro seguro de accidentes personales tienes la tranquilidad de garantizar para ti y tu familia un soporte financiero en caso de sufrir alguna lesión, incapacidad o de fallecer a causa de un accidente.</p>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('personal') }}" class="readmore">+ Info</a>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service text-justify">
                        <img id="seguros_cuadros_home" src="assets/images/blog/hogar_banner.jpg" alt="hogar">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Seguro Hogar</h4>
                        </div>
                        <div id="seguros_cuadros">
                            <p>Sabemos que tu vivienda y lo que tienes dentro de ella lo has conseguido con esfuerzo, por eso protegemos tu hogar contra todo lo que le pueda pasar. Brindamos una protección completa contra incendio, terremoto, AMIT y otros riesgos para que vivas tranquilo. </p>
                        </div>
                        
                        <div class="text-center">
                            <a href="{{ url('hogar') }}" class="readmore">+ Info</a>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service text-justify">
                        <img id="seguros_cuadros_home" src="assets/images/blog/mascotas_banner.jpg" alt="mascotas">
                        <div class="text-center">
                            <h4 style="font-weight: 700;">Seguro Mascotas</h4>
                        </div>
                        <div id="seguros_cuadros">
                            <p>Tu mascota es única e importante ¡Protégela! Asegura al integrante favorito de la familia y hazlo disfrutar de beneficios únicos para él. Desde peluquería, asesoría veterinaria y paseos, hasta el respaldo si causa un accidente o fallece.</p>
                        </div>
                        
                        <div class="text-center">
                            <a href="{{ url('mascotas') }}" class="readmore">+ Info</a>
                        </div>
                        
                    </div>
                </div>
                <!--div class="col-lg-3 col-md-3 col-sm-6 col-xs-6" style="padding: 0px 5px;">
                    <div class="sin-service">
                        <img src="assets/images/blog/otros.jpg" alt="salud">
                        <h4 style="font-weight: 700;">Otros seguros</h4>
                        <p id="seguros">xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx .</br>&nbsp;</p></br>
                        <a href="{{ url('educativo') }}" class="readmore">+ Info</a>
                    </div>
                </div-->
            </div>
        </div>
    </div>
    <!--service-area end-->
    <div class="about-area mt-85 sm-mt-30">
        <div class="container">
            <div class="row ">
                <div class="col-md-10 offset-md-1 col-sm-12">
                    <div class="section-title style-2">
                        <h3 id="linea_tittle" >Seguros Empresas</h3>
                        <br/>
                        <!--style="font-family: 'Delius Swash Caps', cursive;"-->
                    </div>
                </div>
                <div class="col-lg-5 col-sm-12 text-center" style="margin: auto;">
                </div>
            </div>
        </div>
    </div>

     <div class="page-banner-area bg-bannerempresa">
        <div class="container">
            <div class="row align-items-center height-800">

                <div class="col-xs-6 text-center" id="empresas">
                    <div class="banner-text text-center" style="margin:-200px 200px ;position: absolute">
                        <a href="{{ url('pyme') }}"  class="btn-common-banner" style="border-radius: 72px;font-size: 11px; color: white;width: 245px;">
                            Seguro Multiriesgo Pyme
                        </a>
                        <br/><br/>
                        <a href="{{ url('cumplimiento') }}"  class="btn-common-banner" style=" border-radius: 72px;font-size: 11px; color: white;width: 245px;">
                            Póliza de cumplimiento
                        </a>
                        <br/><br/>
                        <a href="{{ url('maquinaria') }}"  class="btn-common-banner" style=" border-radius: 72px;font-size: 11px; color: white;width: 245px;">
                            Todo riesgo equipo y maquinaria
                        </a>
                        <br/><br/>
                        <a href="{{ url('construccion') }}"  class="btn-common-banner" style="border-radius: 72px;font-size: 11px; color: white;width: 245px;">
                            Todo riesgo construccion
                        </a>
                        <br/><br/>
                        <a href="{{ url('civil') }}"  class="btn-common-banner" style=" border-radius: 72px; font-size: 11px; color: white;width: 245px;">
                            Seguro de responsabilidad civil
                        </a>
                        <br/><br/>
                        <a href="{{ url('carga') }}"  class="btn-common-banner" style=" border-radius: 72px; font-size: 11px; color: white;width: 245px;">
                            Transporte mercancías/carga
                        </a>
                    </div>
                </div>
                
                 <div class="col-xs-6" id="empresas_mobile">
                    <div class="banner-text text-center" style="margin:-90px 2px ;position: absolute">
                        <a href="{{ url('pyme') }}"  class="btn-common-banner" style="border-radius: 72px;font-size: 9px; color: white;width: 175px; height: 30px; line-height: 30px; padding:0">
                            Seguro Multiriesgo Pyme
                        </a>
                        <br/>
                        <a href="{{ url('cumplimiento') }}"  class="btn-common-banner" style=" border-radius: 72px;font-size: 9px; color: white;width: 175px; height: 30px; line-height: 30px; padding:0">
                            Póliza de cumplimiento
                        </a>
                        <br/>
                        <a href="{{ url('maquinaria') }}"  class="btn-common-banner" style=" border-radius: 72px;font-size: 9px; color: white;width: 175px; height: 30px; line-height: 30px; padding:0">
                            Todo riesgo equipo y maquinaria
                        </a>
                        <br/>
                        <a href="{{ url('construccion') }}"  class="btn-common-banner" style="border-radius: 72px;font-size: 9px; color: white;width: 175px; height: 30px; line-height: 30px; padding:0">
                            Todo riesgo construcción
                        </a>
                        <br/>
                        <a href="{{ url('civil') }}"  class="btn-common-banner" style=" border-radius: 72px;font-size: 9px; color: white;width: 175px; height: 30px; line-height: 30px; padding:0">
                            Seguro de responsabilidad civil
                        </a>
                        <br/>
                        <a href="{{ url('carga') }}"  class="btn-common-banner" style=" border-radius: 72px;font-size: 9px; color: white;width: 175px; height: 30px; line-height: 30px; padding:0">
                            Transporte mercancías/carga
                        </a>
                    </div>
                </div>

                <!--div class="col-xs-7" id="banner_hogar_mobile">
                    <div class="banner-text text-center" style="margin: -5px 30px; position: absolute;">
                        <h2 style="font-size: 24px;">Seguro de hogar</h2>
                        <a href="{{ url('hogar') }}"  class="btn-common">Compra aquí</a>
                    </div>
                </div-->
            </div>
        </div>
    </div>
    <!--blog-area start-->
    <!--div class="blog-area mt-62 sm-mt-58">
        <div class="container-fluid">
            <div class="row mt-42">
                <div class="col-lg-6 col-sm-12 single-blog style-3 style-4 sm-mb-0 p-0">
                    <div class="row" style="margin-top: 80px;">
                        <div class="col-sm-6">
                            <div class="blog-thumb">
                                <a href="#"><img src="assets/images/audi.png" alt="blog-image"></a>
                               
                            </div>
                        </div>
                        <div class="col-sm-6 p-0">
                            <div class="blog-desc text-center">
                                <h3 style="font-size: 30px;">SOAT</h3>
                                <p>Adquiere tu SOAT de manera rapida y segura, adicionalmente tendras descuentos de hasta un 10% </p>
                                <!--input class="" style="padding: 9px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Placa" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0"->
                                <a href="{{ url('soat') }}" class="btn-common">Comprar</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 single-blog style-3 style-4 sm-mb-0 p-0">
                    <div class="row" style="margin-top: 80px;">
                        <div class="col-sm-6">
                            <div class="blog-thumb">
                                <a href="#"><img src="assets/images/camioneta.png" alt="blog-image"></a>
                               
                            </div>
                        </div>
                        <div class="col-sm-6 p-0">
                            <div class="blog-desc text-center">
                                <h3 style="font-size: 30px;">Poliza todo riesgo</h3>
                                <p>Cotiza, compara y compra al mejor precio y sin salir de tu hogar. Nosotros te cuidamos </p>
                                <input class="" style="padding: 9px;" matinput="" maxlength="6" id="mat-input-0" placeholder="Placa" aria-invalid="true" aria-required="false" aria-describedby="mat-error-0">
                                <a href="#" class="btn-common">Cotiza</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <div  id="pago_linea">
    </div>
    <div class="blog-area mt-100 sm-mt-80" >
        
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                <div class="card single-faq style-2" style="margin: 0px 25px 0 25px; text-align:center">     
                        <br/>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-5 col-md-4 col-sm-12 text-center">
                            </div>
                            <div class="col-lg-7 col-md-8 col-sm-12 text-left">
                                <h3 class="mb-0">Pago En Linea</h3>
                                <br/>
                                <p>Hacer tus pagos desde la comodidad de tu casa nunca fue tan fácil.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-md-4 col-sm-12 text-center">
                                <img src="assets/images/banners/pago_linea.jpg" alt="blog-image" style="width:100%">
                            </div>
                            <div class="col-lg-5 col-md-8 col-sm-12 text-center"  id="estilo_borde">
                                
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://www.segurossura.com.co/paginas/pago-express.aspx#/Pagos" target="_blank" >
                                            <img src="assets/images/blog/sura.png" alt="blog-image" style="height: 70px;">
                                        </a>
                                        <a href="https://www.segurossura.com.co/paginas/pago-express.aspx#/Pagos" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://www.allianz.co/clientes/todos-los-clientes/pagos.html" target="_blank">
                                            <img src="assets/images/blog/Allianz.png" alt="blog-image" style="height: 70px;"></a>
                                        <a href="https://www.allianz.co/clientes/todos-los-clientes/pagos.html" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://www.axacolpatria.co/pagosenlinea/opcionespago" target="_blank">
                                            <img src="assets/images/blog/axa_colpatria.png" alt="blog-image" style="height: 70px;"></a>
                                        <a href="https://www.axacolpatria.co/pagosenlinea/opcionespago" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://www.segurosbolivar.com/RecaudosElectronicos/faces/muestrapagos.jspx/pages/layout/consultUser.action" target="_blank">
                                            <img src="assets/images/blog/bolivar.png" alt="blog-image" style="height: 70px;"></a>
                                        <a href="https://www.segurosbolivar.com/RecaudosElectronicos/faces/muestrapagos.jspx/pages/layout/consultUser.action" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="#" target="_blank" >
                                            <img src="assets/images/blog/colmena.png" alt="blog-image" style="height: 70px;">
                                        </a>
                                        <a href="#" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://extranet.hdi.com.co/Front_PaymentModule" target="_blank" >
                                            <img src="assets/images/blog/hdi.png" alt="blog-image" style="height: 70px;">
                                        </a>
                                        <a href="https://extranet.hdi.com.co/Front_PaymentModule" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://portal.cliente.libertyseguros.co/" target="_blank" >
                                            <img src="assets/images/blog/liberty.png" alt="blog-image" style="height: 70px;">
                                        </a>
                                        <a href="https://portal.cliente.libertyseguros.co/" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://cotiza.mapfre.com.co/pagosWeb/vista/paginas/noFilterIniPagosPublico.jsf" target="_blank" >
                                            <img src="assets/images/blog/mapfre.png" alt="blog-image" style="height: 70px;">
                                        </a>
                                        <a href="https://cotiza.mapfre.com.co/pagosWeb/vista/paginas/noFilterIniPagosPublico.jsf" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                </div>
                                </br>  

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="#" target="_blank" >
                                            <img src="assets/images/blog/solidaria.png" alt="blog-image" style="height: 70px;">
                                        </a>
                                        <a href="https://www.solipagosonline.com.co/UIPasarelaPagos/" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://www.previsora.gov.co/previpagos" target="_blank" >
                                            <img src="assets/images/blog/previsora.png" alt="blog-image" style="height: 70px;">
                                        </a>
                                        <a href="https://www.previsora.gov.co/previpagos" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://checkout.payulatam.com/invoice-collector/#/login/661764" target="_blank" >
                                            <img src="assets/images/blog/sbs.png" alt="blog-image" style="height: 70px;">
                                        </a>
                                        <a href="https://checkout.payulatam.com/invoice-collector/#/login/661764" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                     <div class="col-lg-3 col-md-4 col-xs-6">
                                        <a href="https://www.segurosdelestado.com/pages/Tips" target="_blank" >
                                            <img src="assets/images/blog/seguros_del_estado.png" alt="blog-image"  style="height: 70px;">
                                        </a>
                                        <a href="https://www.segurosdelestado.com/pages/Tips" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>

                                </div>
                                <br/><br/>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-xs-12 text-left">
                                        <p>Quieres obtener mas información de las aseguradoras da clic 
                                        <a href="{{ url('aseguradoras') }}" role="button" >
                                            <span class="fl-button-text"><strong> AQUI</strong></span>
                                        </a></p>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-lg-2 col-md-8 col-sm-12 text-center"  style="margin:auto">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <h3>Financiera Exxtra</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <a href=" https://www.avalpaycenter.com/wps/portal/portal-de-pagos/web/pagos-aval/resultado-busqueda/realizar-pago?idConv=7282" target="_blank" >
                                            <img src="assets/images/blog/exxtra.png" alt="blog-image" style="">
                                        </a>
                                        <a href=" https://www.avalpaycenter.com/wps/portal/portal-de-pagos/web/pagos-aval/resultado-busqueda/realizar-pago?idConv=7282" target="_blank" class="btn-common" role="button" >
                                            <span class="fl-button-text" style="font-size: 9px;"><strong>PAGAR</strong></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/><br/>
            </div>  
            <br/><br/><br/>
        </div>
    <!--project-area start-->
    <!--div class="project-area mt-40 sm-mt-minus-10">
        <div class="container-fluid">
            <div class="row">
                    <div class="col-md-8 offset-md-2 col-sm-12">
                    <div class="section-title text-center">
                        <h2>Nuestros clientes</h2>
                    </div>
                </div>
            </div>
            <div class="row mt-65 sm-mt-40 ">
                <div class="col-lg-3 col-sm-6 p-0">
                    <div class="single-project">
                        <div class="project-thumb">
                            <img src="assets/images/blog/sura 480x420.jpg" alt=""/>
                        </div>
                        <div class="project-desc">
                            <a class="venobox" data-gall="myGallery" href="assets/images/blog/sura 480x420.jpg"><i class="ti-fullscreen"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 p-0">
                    <div class="single-project">
                        <div class="project-thumb">
                            <img src="assets/images/blog/aig 480x420.jpg" alt=""/>
                        </div>
                        <div class="project-desc">
                            <a class="venobox" data-gall="myGallery" href="assets/images/blog/aig 480x420.jpg"><i class="ti-fullscreen"></i></a>
                            <h3>XXXXXXXXX XXXXXXX</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 p-0">
                    <div class="single-project">
                        <div class="project-thumb">
                            <img src="assets/images/blog/allianz 480x420.jpg" alt=""/>
                        </div>
                        <div class="project-desc">
                            <a class="venobox" data-gall="myGallery" href="assets/images/blog/allianz 480x420.jpg"><i class="ti-fullscreen"></i></a>
                            <h3>XXXXXXXXX XXXXXXX</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 p-0">
                    <div class="single-project">
                        <div class="project-thumb">
                            <img src="assets/images/blog/axa colpatria 480x420.jpg" alt=""/>
                        </div>
                        <div class="project-desc">
                            <a class="venobox" data-gall="myGallery" href="assets/images/blog/axa colpatria 480x420.jpg"><i class="ti-fullscreen"></i></a>
                            <h3>XXXXXXXXX XXXXXXX</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div-->
    </div>
    <!--div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">¿Por qué preferirnos?</h3>
                </div>
            </div>
            <br/>
            <div class="row">
                
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;">
                    <div class="sin-service" style="padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/companias.png" style="width:35%; margin-top: 33px;height: 84px;">
                        <p style="font-size:14px;text-align: justify; height: 57px; margin-top: 20px;"><br/>Contamos con el respaldo de aseguradoras a nivel nacional</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12"style="padding: 0px 10px;">
                    <div class="sin-service" style=" padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/asesor2.png" style="width:35%; margin-top: 33px;height: 84px;">
                        <p style="font-size:14px;text-align: justify;  height: 57px; margin-top: 20px;"><br/>Asesoría personalizada en todo momento.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;">
                    <div class="sin-service" style="padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/planes.png" style="width:35%; margin-top: 33px;">
                        <p style="font-size:14px;text-align: justify; margin-top: 20px;"><br/>Contamos con un amplio portafolio de servicios, que se ajustan a tus necesidades.</p> 
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12" style="padding: 0px 10px;">
                    <div class="sin-service" style="padding: 0 12px; background-color: #eaeaea; height: 270px; ">
                        <img src="assets/images/icon/mapa.png" style="width:35%; margin-top: 33px;">
                        <p style="font-size:14px;text-align: justify; margin-top: 20px;"><br/>Desde la comodidad de tu casa, a un solo click.</p> 
                    </div>
                </div>
            </div>
        </div>
    </div-- >
    <div class="row">
        <br/>
    </div-->
    <!--project-area end-->
    

   @include('layouts.footer')