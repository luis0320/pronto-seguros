
    <!--header-area end-->
    @include('layouts.master')
    
    <!--page-banner-area start-->
    <!--div class="page-banner-area bg-8">
        <div class="container">
            <div class="row align-items-center height-400">
                <div class="col-lg-12">
                    <div class="page-banner-text text-center text-white">
                        <h2>Vida</h2>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--banner-area end-->
    <!--blog-area start-->
      <div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2 id="linea_tittle">Política de Tratamiento de Datos Personales <br>
                            Pronto y seguros del valle Ltda.</h2>
                    </div>
                    <div class="row mt-30">
                        <div class="col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <p>Con el presente documento ponemos a su disposición la Política de Protección de Datos Personales, en cumplimiento de lo dispuesto en   los artículos 15 y 20 de la Constitución Política, la Ley 1581 de 2012 y el Decreto 1377 de 2013 que reglamento esta última.</p>
 
                                <!--p>Solicito su amable colaboración de reportarlo al siguiente correo  <span style="color:blue">servicioalcliente@prontoyseguros.com </span></p>
                                <p>Para manejo de cancelaciones de negocios nuevos estos seran manejados directamente por nuestra asesora ALEXANDRA MORALES por favor enviar la información al siguiente correo <span style="color:blue">cancelaciones@prontoyseguro.com</span></p-->  
                            </div>
                        </div>
                    </div>

                    <div class="section-title style-4 text-center pt-10">
                        <div style="height: auto; background-color: #eaeaea; border-radius: 19px;" class="col-lg-12 col-xs-12">
                            <div class="col-lg-1 col-xs-12" style="padding:0 10px; text-align: center;" id="soat_items_mobile">
                                <img src="assets/images/verificar.png" style="width:15%; margin: 5px 0px;">
                            </div>
                            <div class="col-lg-1 col-xs-12" style="padding:0 10px; text-align: center;" id="soat_items">
                                <img src="assets/images/verificar.png" style="width:60%; margin: 14px 0px;">
                            </div>
                            <div class="col-lg-10 col-xs-12" style="padding:0;">
                                <p style="font-size: 16px; text-align: justify; padding: 0 10px;"> <h3 id="linea_tittle">Ahora puedes radicar la cancancelación por nuestra pagina</h3></p>
                            </div>
                        </div>
                       
                        <div class="section-title style-4 text-center pt-10">
                        &nbsp;
                        </div>
                            <div style="padding: 5px;">
                                <a href="{{ route('scancelaciones.create') }}" class="btn-common" target='_blank'>
                                    Radicar Cancelacion
                                </a>
                            </div>
                            
                            <div class="sin-service style-2">
                                <a href="assets/documentos/FORMATO_DE_CANCELACION.pdf" class="btn btn-default pull-center" target="_blank"><i class="fa fa-download"></i> Descargar modelo de cancelación</a>
                                                
                            </div>
                        </div>
                                       
                 

                    <div class="section-title style-4 text-center pt-10">
                        <h3 id="linea_tittle">Nota de aclaración.</h3>
                    </div>
                    <div class="row mt-30">
                        <div class="col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <p>Si su póliza fue financiada con alguna de las financieras con convenio para el financiamiento de pólizas de seguros.</p>
                                <p>La devolución de prima por el tiempo no causado se ara a la   compañía que financio su póliza..</p>
                                <p>Si su pago fue  de contado ,  el valor de la  prima  no causado  se devolviera  a su nombre  por favor  firmar formato de solicitud  de devolución de prima anexando  certificación de cuenta.</p>
                                <p>Si su póliza tiene beneficiario oneroso es decir  Prenda  a  alguna entidad  financiera  como  BANCO  DE  OCCIDENTE  O FINESSA  o cualquier otra.</p>
 
                                <!--p>Solicito su amable colaboración de reportarlo al siguiente correo  <span style="color:blue">servicioalcliente@prontoyseguros.com </span></p>
                                <p>Para manejo de cancelaciones de negocios nuevos estos seran manejados directamente por nuestra asesora ALEXANDRA MORALES por favor enviar la información al siguiente correo <span style="color:blue">cancelaciones@prontoyseguro.com</span></p-->  
                            </div>
                        </div>
                    </div>
                </div>     
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-left pt-10">
                        <div class="row mt-30">
                            <div class="col-sm-12">
                                <div class="sin-service style-2 text-left">
                                    <h3>Su solicitud de cancelación debe  adjuntar los  siguientes documentos:</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="row mt-30">
                        <div class="col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <ul>
                                    <li>Carta solicitando la cancelación</li>
                                    <li>Formato de devolución de primas a su favor si su pago fue de contado</li>
                                    <li>Paz y Salvo de la compañía financiera    o beneficiario oneroso.</li>
                                    
                                </ul>
                                <p>Tenga en cuenta que sin estos documentos la compañía no puede realizar su solicitud de cancelación</p>
                                <p>Recuerde   todos estos documentos los puede enviar a los correos.</p>
                                <ul>
                                    <li><span style="color:blue">EXPEDICION@PRONTOYSEGUROS.COM</span></li>
                                    <li><span style="color:blue">CARTERA@PRONTOYSEGUROS.COM</span></li>
                                    <li><span style="color:blue">GERENCIA@PRONTOYSEGUROS.COM</span></li>
                                </ul>
                                <p>O llamarnos a los teléfonos</p>
                                <ul>
                                    <li>316 242 24 95</li>
                                    <li>317 852 08 65</li>
                                    <li>321 642 54 82</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Escribenos si requieres apoyo en la cancelación. </h3> 
                    
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    <div class="row">
        <br/><br/>
    </div>
    <!--blog-area end-->
    
   @include('layouts.footer')