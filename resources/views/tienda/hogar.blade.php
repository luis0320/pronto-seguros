
    <!--header-area end-->
    @include('layouts.master')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-hogar">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/hogar.png" style="width:16%; margin-top:15px">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle" > — Seguro de Hogar — </h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                    </div>
                                    <div>
                                        <input type='hidden' name='seguro' value='Hogar' />
                                        <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;">  
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/hogar.png" style="width:16%; margin-top:15px">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle" > — Seguro de Hogar — </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                            </div>
                            <div>
                                <input type='hidden' name='seguro' value='Hogar' />
                                <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
   <!--banner-area end-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">

            <div class="row">

                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold">Póliza de hogar: protección y bienestar a tu alcance</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">El seguro de hogar es un seguro multirriesgo, que ampara, bajo un mismo contrato los principales riesgos que afectan al patrimonio, amparando los daños materiales, terremoto, incendios, hurto, la responsabilidad civil que pueda derivarse del normal funcionamiento del día a día.</p>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/vida/hogar.jpg" alt="promo"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <ul class="list-basic"  style="margin-top: 17px" >
                        <li id="listado">Aseguramos valor  comercial casa, apartamento o  edificio</li>
                        <li id="listado">No aplica  Infraseguro</li>
                        <li id="listado">Sin restricción por  antigüedad</li>
                        <li id="listado">Sin restricción por  ubicación</li>
                        <li id="listado">Sin Inspección</li>
                        <li id="listado">Cobertura  a Celulares</li>
                        <li id="listado">Cobertura en  responsabilidad Civil</li>
                        <li id="listado">Entregamos  de inmediato  su póliza</li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                   
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Coberturas</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Asistencias</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="color: #000;">Otros beneficios</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-documentos-tab" data-toggle="pill" href="#pills-documentos" role="tab" aria-controls="pills-documentos" aria-selected="true" style="color: #000;">Formato Cotización</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Coberturas </h3>
                    
                    <ul class="list-basic">
                        <li>En equipos móviles, portátiles  y Joyas( Bajo  políticas  de suscripción)</li>
                        <li>Tiene  una  cobertura  en caso  de un accidente que provoque la muerte, invalidez, desmembración o pérdida de la vida</li>
                        <li>También incluye  reembolso por gastos  médicos</li>
                        <li>Incluye a la  familia y a los empleados  domésticos.</li>
                        <li>Protección  extendida  al hogar</li>
                        <li>Daños  estéticos, gastos  adicionales  para  recomposición estética del  bien luego de un siniestro.<br/>Ejemplo: Luego  de la  rotura  de un  tubo, el siniestro te cubre la zona dañada, pero no  encontraste  la misma baldosa y requieres  remodelar  el área  no AFECTADA,  la  compañía te da hasta  el 50% del valor  del  siniestro con un mínimo de 2 smmlv</li>
                        <li>Incendio, caída directa de rayo</li>
                        <li>Explosión</li>
                        <li>Daños por agua</li>
                        <li>Anegación</li>
                        <li>Huracán, vientos fuertes, granizo, impacto de objetos, Caída de aeronaves</li>
                        <li>Huelga, motín, asonada, conmoción civil y actos mal intencionados de terceros</li>
                        <li>Terremoto, maremoto o tsunami, erupción volcánica o temblor.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Asistencias</h3>

                    <h4 style="margin-top: 0;">Asistencia  domiciliarias</h4>
                    <ul class="none">
                        <li>Plomeria 30 SMDLV</li>
                        <li>Electricidad 30 SMDLV</li>
                        <li>Cerrajeria 30 SMDLV</li>
                        <li>Vidrieria 30 SMDLV</li>
                    </ul>
                    <h4 style="margin-top: 0;">Asistencia Plus</h4>
                    <ul class="none">
                        <li>Gastos de mudanza y bodegaje 30 SMDLV</li>
                        <li>Gastos de Hotel 20 SMDLV</li>
                        <li>Interrupción de viaje 20 SMDLV</li>
                        <li>Celaduria 30 SMDLV</li>
                        <li>Inundación 30 SMDLV</li>
                        <li>Lavado de cortinas y alfombras 30 SMDLV</li>
                        <li>Orientación Jurídica Sin limite</li>
                        <li>Asistencia domiciliaria  sin limites</li>
                        <li>Servicio de Ambulancia</li>
                        <li>Acompañamiento en citas  médicas</li>
                        <li>Translado materno</li>
                        <li>Asistencia especial jornada de aseo</li>
                        <li>Retiro de escombros</li>
                        <li>Profesionales en cada</li>
                    </ul>
                    <div class="col-md-12 col-xs-12 text-center">
                        <a href="assets/documentos/HOGAR.pdf" class="btn btn-default" target="_blank" style="width: 250px; "><i class="fa fa-download"></i>Hogar Liberty</a>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h3 style="margin-top: 0;">Otros beneficios</h3>
                    <ul class="list-basic">
                        <li>Solo es  necesario que relacions  contenidos de alto valor</li>
                        <li>Cobertura  sin valor de Infraseguro</li>
                        <li>Aseguramos teléfonos celulares por  medio  de su póliza de hogar </li>
                        <li>Menores deducibles  en Amit, Sustracción y Equipos electrónicos</li>
                        <li>No se  requiere  inspección para  tomar la póliza.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos" role="tabpanel" aria-labelledby="pills-documentos-tab" style="height: 230px;">
                    <h3 style="margin-top: 0;">Formato Cotización</h3>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_HOGAR.xlsx" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Formato cotización seguro HOGAR</a>
                        </div>  
                    </div>  
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Cobertura</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Asistencias</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="pills-home-mobile-tab" data-toggle="pill" href="#pills-home-mobile" role="tab" aria-controls="pills-home-mobile" aria-selected="true" style="color: #000;">Otros beneficios</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-documentos-tab" data-toggle="pill" href="#pills-documentos-mobile" role="tab" aria-controls="pills-documentos" aria-selected="true" style="color: #000;">Formato Cotización</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Coberturas </h3>
                    
                    <ul class="list-basic">
                        <li>En equipos móviles, portátiles  y Joyas( Bajo  políticas  de suscripción)</li>
                        <li>Tiene  una  cobertura  en caso  de un accidente que provoque la muerte, invalidez, desmembración o pérdida de la vida</li>
                        <li>También incluye  reembolso por gastos  médicos</li>
                        <li>Incluye a la  familia y a los empleados  domésticos.</li>
                        <li>Protección  extendida  al hogar</li>
                        <li>Daños  estéticos, gastos  adicionales  para  recomposición estética del  bien luego de un siniestro.<br/>Ejemplo: Luego  de la  rotura  de un  tubo, el siniestro te cubre la zona dañada, pero no  encontraste  la misma baldosa y requieres  remodelar  el área  no AFECTADA,  la  compañía te da hasta  el 50% del valor  del  siniestro con un mínimo de 2 smmlv</li>
                        <li>Incendio, caída directa de rayo</li>
                        <li>Explosión</li>
                        <li>Daños por agua</li>
                        <li>Anegación</li>
                        <li>Huracán, vientos fuertes, granizo, impacto de objetos, Caída de aeronaves</li>
                        <li>Huelga, motín, asonada, conmoción civil y actos mal intencionados de terceros</li>
                        <li>Terremoto, maremoto o tsunami, erupción volcánica o temblor.</li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Asistencias</h3>

                    <h4 style="margin-top: 0;">Asistencia  domiciliarias</h4>
                    <ul class="none">
                        <li>Plomeria 30 SMDLV</li>
                        <li>Electricidad 30 SMDLV</li>
                        <li>Cerrajeria 30 SMDLV</li>
                        <li>Vidrieria 30 SMDLV</li>
                    </ul>
                    <h4 style="margin-top: 0;">Asistencia Plus</h4>
                    <ul class="none">
                        <li>Gastos de mudanza y bodegaje 30 SMDLV</li>
                        <li>Gastos de Hotel 20 SMDLV</li>
                        <li>Interrupción de viaje 20 SMDLV</li>
                        <li>Celaduria 30 SMDLV</li>
                        <li>Inundación 30 SMDLV</li>
                        <li>Lavado de cortinas y alfombras 30 SMDLV</li>
                        <li>Orientación Jurídica Sin limite</li>
                        <li>Asistencia domiciliaria  sin limites</li>
                        <li>Servicio de Ambulancia</li>
                        <li>Acompañamiento en citas  médicas</li>
                        <li>Translado materno</li>
                        <li>Asistencia especial jornada de aseo</li>
                        <li>Retiro de escombros</li>
                        <li>Profesionales en cada</li>
                    </ul>
                    <div class="col-md-12 col-xs-12 text-center">
                        <a href="assets/documentos/HOGAR.pdf" class="btn btn-default" target="_blank" style="width: 250px; "><i class="fa fa-download"></i>Hogar Liberty</a>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-home-mobile" role="tabpanel" aria-labelledby="pills-home-mobile-tab">
                    <h3 style="margin-top: 0;">Otros beneficios</h3>
                    <ul class="list-basic">
                        <li>Solo es  necesario que relacions  contenidos de alto valor</li>
                        <li>Cobertura  sin valor de Infraseguro</li>
                        <li>Aseguramos teléfonos celulares por  medio  de su póliza de hogar </li>
                        <li>Menores deducibles  en Amit, Sustracción y Equipos electrónicos</li>
                        <li>No se  requiere  inspección para  tomar la póliza.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos-mobile" role="tabpanel" aria-labelledby="pills-documentos-tab">
                    <h3 style="margin-top: 0;">Formato Cotización</h3>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_HOGAR.xlsx" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Formato cotización seguro HOGAR</a>
                        </div>  
                    </div>    
                </div>
                
            </div>
            

        </div>

    </div>
    <div class="row">
        <br/>
    </div>
    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Por que valor puedo asegurar mi casa, apartamento o local?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Valor  comercial,   se ingresa  el valor comercial suministrado por  el asegurado.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Cuánto cuesta  un  seguro  de hogar?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>El  costo  de  un seguro  de  hogar  es de acuerdo al valor  asegurado  de la  vivienda, apartamento o local, influye  las  áreas  construidas, el tipo de construcción  y  la antigüedad  por  lo general  inicia con una prima desde $180.000 mil  pesos  más iva.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Que cubre el seguro de hogar ?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Con este seguro se protege la vivienda y sus contenidos ante la ocurrencia de diversos riesgos descritos en la póliza; brinda una cobertura básica en caso de incendio, terremoto, hurto, explosión, daños por agua y vientos fuertes, entre otros. Este seguro permite proteger el hogar ante los riesgos a los que está expuesto y garantiza que, en caso de incendio, hurto o cualquier otro hecho asegurado se podrán reponer los bienes adquiridos con tanto esfuerzo.</p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿Cómo  funciona un  seguro  de hogar?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Es   un  contrato  formalizado  por  escrito entre  un asegurado y un asegurador,  el asegurado se obliga  con el asegurador  a que por el pago de una prima, el asegurador  en caso de que se produzca un riesgo o un acontecimiento  indemnice a el asegurado  de acuerdo al valor  asegurado por su vivienda , casa, apartamento o local.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Cual  es el mejor  seguro  de hogar?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Hay  varias  compañías  importantes  en Colombia  que tienen muy buenas  cobertura  que dan  tranquilidad  a nuestros  clientes..</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        ¿Que  pasa  si  dejo  de  pagar el seguro  de hogar?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>La  cobertura del contrato  se  suspenderá, a partir  de la  fecha  del impago.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Garantiza la tranquilidad de tu familia teniendo siempre un techo. </h3> 
                    <h4 style="padding: 0 10px; text-align: center;">Escoge el plan que más se acomoda a tus necesidades y no esperes para asegurar tu tranquilidad.</h4>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    <div class="row">
        <br/><br/>
    </div>
     
    <!--page-banner-area start-->
    <!--div class="page-banner-area bg-hogar">
        <div class="container">
            <div class="row align-items-center height-400">
                <div class="col-lg-12">
                    <div class="page-banner-text text-center text-white">
                        <h2>Hogar</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="benefit-area mt-80 sm-mt-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="section-title style-4 text-center pt-10">
                        <h2>Póliza de hogar</h2>
                    </div>
                    <div class="row mt-30">
                        <div class="col-sm-12">
                            <div class="sin-service style-2 text-left">
                                <p>El seguro de hogar es un seguro multirriesgo, que ampara, bajo un mismo contrato los principales riesgos que afectan al patrimonio, amparando los daños materiales, terremoto, incendios, hurto, la responsabilidad civil que pueda derivarse del normal funcionamiento del día a día.</p>
                            </div>
                            <div class="section-title style-4 text-right pt-10">
                                <a href="{{ url('contactenos') }}"><h2>Contáctanos</h2></a>
                                <p> <img src="assets/images/icons/celular.png" alt="coberturas" style="width:7%"/> Viviana Melo +57 316 6825935</p>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
             <br/> 
             <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" style="border: 3px ; border-style: ridge;">
                    <div class="blog-details">
                        <div class="section-title style-4 text-center pt-10">
                            <h2>Coberturas</h2>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6 col-md-12 col-sm-12" style="border: 3px ; border-style: ridge;">
                    <div class="blog-details">
                        <div class="section-title style-4 text-left pt-10">
                            <h3>Cobertura Básica </h3>
                        </div>
                        <div class="blog-details-text">
                            <ul>
                                <li>Incendio, caída directa de rayo</li>
                                <li>Explosión</li>
                                <li>Daños por agua</li>
                                <li>Anegación</li>
                                <li>Huracán, vientos fuertes, granizo, impacto de objetos, Caída de aeronaves</li>
                                <li>Huelga, motín, asonada, conmoción civil y actos mal intencionados de terceros</li>
                                <li>Terremoto, maremoto o tsunami, erupción volcánica o temblor.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12" style="border: 3px; border-style: ridge; ">
                    <div class="blog-details">
                        <div class="section-title style-4 text-left pt-10">
                            <h3>Cobertura Adicionales </h3>
                        </div>
                        <div class="blog-details-text">
                            <ul>
                                <li>Sustracción con violencia para contenidos</li>
                                <li>Equipo eléctrico y electrónico</li>
                                <li>Responsabilidad civil extracontractual familiar</li>
                                <li>Asistencia al hogar</li>
                                <li>Plomería</li>
                                <li>Cerrajería</li>
                                <li>Rotura de vidrios</li>
                                <li>Electricidad</li>
                                <li>Celaduría</li>
                                <li>Gastos de hospedaje</li>
                                <li>Gastos de mudanza</li>
                                <li>Orientación jurídica</li>
                            </ul>
                        </div>
                    </div>
                </div> 
            </div>

        </div>
    </div>

    <br/-->

   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>