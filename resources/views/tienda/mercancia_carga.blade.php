
    <!--header-area end-->
    @include('layouts.master')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-carga">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/transporte.png" style="width:16%; margin-top:15px">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle" > Seguro Transporte de Mercancias</h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                    </div>
                                    <div>
                                        <input type='hidden' name='seguro' value='Transporte de Mercancias' />
                                        <input type='hidden' name='contacto' value='directorcomercial1@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;"> 
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/transporte.png" style="width:16%; margin-top:15px">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle" > Seguro Transporte de Mercancias </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                            </div>
                            <div>
                                <input type='hidden' name='seguro' value='Transporte de Mercancias' />
                                <input type='hidden' name='contacto' value='directorcomercial1@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>


    <!--Estandar-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold">Póliza de transporte de mercancias</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">La póliza para transportadores de carga, terrestre, aéreos, marítimo, asegura automáticamente todos los despachos realizados por la empresa transportadora de carga</p>
                        <p style="font-size:16px;line-height: 25px;">Cuenta con los siguientes riesgos: perdidas y / o daños materiales de los bienes asegurados, que se produzca con ocasión de su transporte.</p>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/vida/mercancia.jpg" alt="promo" style="height: 390px;" >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <p>El seguro para Empresas de Transporte Terrestre de Carga de incluye los siguientes beneficios y ventajas:</p>
                    <ul class="list-basic">
                        <li id="listado">Es la póliza que permite proteger las operaciones en el mercado internacional de una forma eficiente y adecuada en casos de pérdida total de la carga, falta de entrega o perjuicios por huelga.</li>
                        <li id="listado">Se reducen los factores de riesgo durante la operación logística y el transporte de mercancías.</li>
                        <li id="listado">Es una póliza flexible que ofrece al asegurado la posibilidad de añadir las coberturas de Saqueo o Avería particular.</li>
                    </ul>
                    <p>Asimismo, los asegurados pueden acceder a coberturas adicionales como:</p>
                     <ul class="list-basic">
                        <li id="listado">Responsabilidad civil extracontractual a la que puede verse expuesto el transportista por daños a terceros durante la movilización de la mercancía.</li>
                        <li id="listado">Riesgos personales que pueda sufrir el conductor.</li>
                        <li id="listado">Daños o pérdidas que pueda sufrir la mercancía.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                   
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Cobertura Básica</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas Adicionales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Cobertura Básica</h3>
                    
                    <ul class="list-basic">
                        <li>Falta de Entrega: ampara la no entrega por extravió, hurto común, hurto calificado según su definición legal de uno o más bultos completos (contenido y empaques) del despacho de conformidad con el contrato de transporte.</li>
                        <li>Avería Particular: ampara los daños a las constitución física de la mercancía ( al contenido, no a su empaque) que sean consecuencia de maltrato, oxidación, contaminación, ruptura, abolladuras, derrames, etc.</li>
                        <li>Saqueo: ampara la sustracción parcial o total del contenido y la sustracción de algunas partes integrantes de los bienes asegurados cuando no tengan empaque.</li>
                        <li>Huelga: ampara las perdidas y / o daños materiales de las mercancías transportadas, causados directamente por huelga, suspensión de hechos de labores, suspensión por cierre patronal, disturbios de trabajo, asonada, motín, conmoción civil o popular y actos terroristas.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Algunas compañías como valor agregado dan apoyo en la administración del riesgo</li>
                        <li>Selección de vehículos y conductores</li>
                        <li>Escoltas</li>
                        <li>Control de mercancías</li>
                        <li>Transporte de mercancías peligrosas</li>
                        <li>Horario de rodamiento</li>
                        <li>Parqueaderos y sitios para pernotar</li>
                        <li>Manuales de carga y seguridad</li>
                        <li>Instrucciones para el generador de carga</li>
                        <li>Transporte de bienes personales y menajes domestico</li>
                        <li>Plan de prevención integral para transportadores</li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Cobertura Básica</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Coberturas Adicionales</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Cobertura Básica</h3>
                    
                    <ul class="list-basic">
                        <li>Falta de Entrega: ampara la no entrega por extravió, hurto común, hurto calificado según su definición legal de uno o más bultos completos (contenido y empaques) del despacho de conformidad con el contrato de transporte.</li>
                        <li>Avería Particular: ampara los daños a las constitución física de la mercancía ( al contenido, no a su empaque) que sean consecuencia de maltrato, oxidación, contaminación, ruptura, abolladuras, derrames, etc.</li>
                        <li>Saqueo: ampara la sustracción parcial o total del contenido y la sustracción de algunas partes integrantes de los bienes asegurados cuando no tengan empaque.</li>
                        <li>Huelga: ampara las perdidas y / o daños materiales de las mercancías transportadas, causados directamente por huelga, suspensión de hechos de labores, suspensión por cierre patronal, disturbios de trabajo, asonada, motín, conmoción civil o popular y actos terroristas.</li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Coberturas Adicionales</h3>
                    <ul class="list-basic">
                        <li>Algunas compañías como valor agregado dan apoyo en la administración del riesgo</li>
                        <li>Selección de vehículos y conductores</li>
                        <li>Escoltas</li>
                        <li>Control de mercancías</li>
                        <li>Transporte de mercancías peligrosas</li>
                        <li>Horario de rodamiento</li>
                        <li>Parqueaderos y sitios para pernotar</li>
                        <li>Manuales de carga y seguridad</li>
                        <li>Instrucciones para el generador de carga</li>
                        <li>Transporte de bienes personales y menajes domestico</li>
                        <li>Plan de prevención integral para transportadores</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                
            </div>
            

        </div>

    </div>
    <div class="row">
        <br/>
    </div>
    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Es  obligatorio contratar una póliza  de transporte de mercancía?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Es es necesario saber que existe una obligación por ley de cubrir una serie de responsabilidades en relación a la mercancía transportada, desde el momento en que esta es cargada en el medio de transporte correspondiente hasta la recepción de la misma en el punto de destino.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Qué  tipos  de pólizas  de transporte  existen.?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                     <ul class="list-basic">
                                        <li>Póliza flotante: cuando se moviliza continuamente carga y en volúmenes importantes.</li>
                                        <li>Póliza específica: para embarques menores y de poca regularidad. Cualquier póliza de seguro individual contratada.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Cuál es la cobertura de indemnización  por los seguros de transporte?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>En caso de que las mercancías sufran cualquier tipo de daño o robo durante cualquier etapa de la movilización, la compañía aseguradora contratada se encargará de indemnizar al propietario de los productos, siempre y cuando se trate de incidentes que estén cubiertos y reflejados en el contrato de la póliza.</p>
                                    <p>Asimismo, si se produjera una pérdida total, la indemnización cubrirá el valor de las mercancías en el lugar y momento en que se llevó a cabo la carga, además de los gastos derivados de la entrega al transportista y el importe total correspondiente al seguro de transporte de mercancías.</p>
                                    <p>Los riesgos  que  cubren las  compañías de seguros son: </p>
                                    <ul class="list-basic">
                                        <li>Roturas o deterioros</li>
                                        <li>Robo</li>
                                        <li>Carga y Descarga</li>
                                        <li>Riesgos extraordinarios</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿Cómo puedo contratar UNA PÓLIZA de transporte de mercancías?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>La contratación de una póliza de transporte es fundamental para que la mercancía sea transportada de una manera segura y  en caso de que suceda cualquier evento súbito.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Qué no cubre el seguro?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <h4>No cubren:</h4>
                                    <ul class="list-basic">
                                        <li>Conducta dolosa, culpa grave del asegurado.</li>
                                        <li>Negligencia en conducción por parte del operador del transporte.</li>
                                        <li>Insuficiencia o inapropiado embalaje.</li>
                                        <li>Vicio propio.</li>
                                        <li>Pérdida, daño o gastos causados por demora.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        ¿Cómo reclamo al seguro ante un siniestro?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Documentación mínima que debe presentarse:</p>
                                    <ul class="list-basic">
                                        <li>Póliza de seguro</li>
                                        <li>Carta de reclamación.</li>
                                        <li>Factura comercial, Lista de empaque y Copia de los documentos de transporte.</li>
                                        <li>Cartas  formales a los posibles responsables del daño y/o pérdida. A través de éstas, el beneficiario, responsabilizará a las terceras partes que hayan intervenido en la operación.</li>
                                        <li>Fotografías de la carga dañada.</li>
                                        <li>Denunció ante las  autoridades (en caso de robo).</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Diseñamos  junto el seguro  ideal para  proteger el transporte  de tu mercancía.</h3> 
                    
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    <div class="row">
        <br/><br/>
    </div>
    <!--Fin Estandar-->
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>