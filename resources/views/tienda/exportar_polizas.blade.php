@include('layouts.masteradmin')
@extends('layouts.app')


@section('content')
    <section class="content container-fluid">
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item"><a href="moduloadministrador">Inicio</a></li>
                        <li class="breadcrumb-item active">Exportar</li>
                    </ol>
                    <!--div class="card mb-4">
                        <div class="card-body">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>.</div>
                    </div-->
                    <div class="card mb-4">
                        <div class="card-header" style="color:#fff"><i class="fas fa-table mr-1"></i>Importar polizas</div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="form-group">
                                    <label>Exportar Archivo Excel    </label>
                                    <a href="{{ route('exportar-excel') }}" class="btn btn-primary">Exportar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/scripts.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>

@endsection
</body>
</html>

