
    <!--header-area end-->
    @include('layouts.master')
    
    

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-exequial">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/exequial.png" style="width:16%; margin-top: 15px;">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle" > — Seguro Exequial — </h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                        <input type='hidden' name='seguro' value='Exequial' />
                                        <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;"> 
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/exequial.png" style="width:16%; margin-top: 10px;">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle" > — Seguro Exquial — </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                <input type='hidden' name='seguro' value='Exequial' />
                                <input type='hidden' name='contacto' value='generales@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
   <!--banner-area end-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Seguro Exequial: Apoyo en el día más difícil</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">Bríndale a tu familia tranquilidad y apoyo en el momento que más lo necesiten. Con el seguros de exequias que cubre los gastos funerarios o ser querido.</p>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/banners/exequial_1.jpg" alt="exequial"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <ul class="list-basic" dir="ltr" style="margin-top: 35px">
                        <li id="listado">El servicio funerario se presta al asegurado y a las personas que figuran como beneficiarios designados en la póliza.</li>
                        <li id="listado">Facilidad para adquirir el producto, sin exámenes médicos u otros trámites.</li>
                        <li id="listado">No se requiere historia médica para su contratación.</li>
                        <li id="listado">Traslado del cuerpo a cualquier parte del país.</li>
                        <li id="listado">Programas individuales adaptados a las necesidades de cada persona o grupo familiar.</li>
                    </ul>
                </div>
            </div>
            <!--br/>
            <div class="row">
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/vida/vida_digital.jpg" alt="promo"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <h3 style="font-weight: 800; margin-top: 5px; text-align:center">Seguro Exequial</h3></br>
                    <div class="row">
                        <div class="col-lg-12 col-xs-10" style="padding: 0 0 0 30px;">
                            <p style="font-size: 16px;">Ahora puedes asegurarte y cuidar la calidad de vida de tus seres queridos de la manera más rápida y sencilla: 100 % en línea desde tu computador o celular.</p>
                            <p style="font-size: 16px;">¡Elige tu plan (con valores asegurados de 10, 20 o 40 millones para tus beneficiarios) y diligencia tus datos en solo minutos! Nuestro seguro te brinda:</p>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-2 col-xs-2" style="padding:0; text-align: center;">
                            <img src="assets/images/dinero.png" style="width:40%">
                        </div>
                        <div class="col-lg-10 col-xs-10" style="padding:0;">
                            <p style="font-size: 16px;">Respaldo económico en caso de accidente, enfermedad y muerte.</p>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-2 col-xs-2" style="padding:0; text-align: center;">
                            <img src="assets/images/asesor.png" style="width:40%">
                        </div>
                        <div class="col-lg-10 col-xs-10" style="padding:0; margin: 7px 0px;">
                            <p style="font-size: 16px;">Orientación virtual médica, psicológica y laboral. </p>
                        </div>
                    </div>
                    
                </div>
            </div-->
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                   
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Planes y tarifas</a>
                    </li>
                    <!--li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Tarifas</a>
                    </li-->
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="color: #000;">Otros beneficios</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-documentos-tab" data-toggle="pill" href="#pills-documentos" role="tab" aria-controls="pills-documentos" aria-selected="true" style="color: #000;">Documentos</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                
                <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Características</h3>
                    <ul class="list-basic" dir="ltr">
                        <li>La forma de pago es trimestral o anual, con débito a la cuenta de ahorros, corriente o Tarjeta de Crédito Visa, Mastercard o todas las tarjetas.</li>
                        <li>La edad mínima de ingreso es 18 años y la edad máxima 60 y 79, años (la persona debe estar con alguien de su grupo familiar).</li>
                        <li>La edad de permanencia es hasta los 70 años.</li>
                        <li>Padres hasta con 80 años, si están en el grupo asegurado conformado por: asegurado principal, cónyuge e hijos. Para los hijos aplica hasta los 30 años de edad.</li>
                        <li>No necesitas realizar trámites médicos.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Planes y tarifas</h3>
                    <embed src="assets/documentos/PRODUCTO_EXEQUIAS.pdf" type="application/pdf" width="100%" height="670px" />
                </div>
                <!--div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h2 style="margin-top: 0;">Tarifas</h2>
                    
                </div-->
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li><a href="https://api.whatsapp.com/send?phone=573128932013" target="_blank" style="font-family:Arial Display"><!--i class="fa fa-whatsapp"></i--> Whatsapp: +57 312 893 2013  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/images/W5.png" alt="" style="width:21px; margin: -1px -22px; position: absolute" /></a></li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h3 style="margin-top: 0;">Otros beneficios</h3>
                    <ul class="list-basic" dir="ltr">
                       <li>La posibilidad de acudir a cualquier funeraria, según tu preferencia.</li>
                        <li>Orientación telefónica las 24 horas del día, los 365 días del año.</li>
                        <li>La alternativa de escoger el grupo familiar y el plan que más se adapte a tus necesidades. Este es un producto flexible.</li>
                        <li>Cobertura en gastos de repatriación con el Plan A.</li>
                        <li>Pago por reembolso.</li>
                        <li>Permanencia en el amparo de gastos exequiales de forma indefinida. Además, su cobertura es inmediata por muerte por accidente, homicidio o suicidio.</li>
                        
                        
                    </ul>

                </div>
                <div class="tab-pane fade" id="pills-documentos" role="tabpanel" aria-labelledby="pills-documentos-tab">
                    <h3 style="margin-top: 0;">Documentos</h3>
                    <p dir="ltr">Condiciones generales y  solicitud para expedir el seguro Exequias.</p>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/CONDICIONES_GENERALES_SEGURO_EXEQUIAL.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Condiciones generales</a>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/Formulario_Seguro_Exequias_Colmena.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Solicitud Exequias</a>
                        </div>  
                    </div>  
                </div>
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link active" id="pills-profile-mobile-tab" data-toggle="pill" href="#pills-profile-mobile" role="tab" aria-controls="pills-profile-mobile" aria-selected="false" style="color: #000;">Características</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Planes y tarifas</a>
                    </li>
                    <!--li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Tarifas</a>
                    </li-->
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="pills-home-mobile-tab" data-toggle="pill" href="#pills-home-mobile" role="tab" aria-controls="pills-home-mobile" aria-selected="true" style="color: #000;">Otros beneficios</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-documentos-tab" data-toggle="pill" href="#pills-documentos-mobile" role="tab" aria-controls="pills-documentos" aria-selected="true" style="color: #000;">Documentos</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                
                <div class="tab-pane fade show active" id="pills-profile-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Características</h3>
                    <ul class="list-basic" dir="ltr">
                        <li>La forma de pago es trimestral o anual, con débito a la cuenta de ahorros, corriente o Tarjeta de Crédito Visa, Mastercard o American Express.</li>
                        <li>La edad mínima de ingreso es 18 años y la edad máxima 65 (la persona debe estar con alguien de su grupo familiar).</li>
                        <li>La edad de permanencia es hasta los 70 años.</li>
                        <li>Padres hasta con 80 años, si están en el grupo asegurado conformado por: asegurado principal, cónyuge e hijos. Para los hijos aplica hasta los 30 años de edad.</li>
                        <li>No necesitas realizar trámites médicos.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Planes y tarifas</h3>
                    <div class="row">
                        <div class="col-md-12 col-xs-12 text-center">
                            <a href="assets/documentos/PRODUCTO_EXEQUIAS.pdf" class="btn btn-default" target="_blank" style="width: 250px; "><i class="fa fa-download"></i> Planes y tarifas</a>
                        </div>  
                    </div>
                </div>
                <!--div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Tarifas</h3>
                    
                </div-->
                
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-home-mobile" role="tabpanel" aria-labelledby="pills-home-mobile-tab">
                    <h3 style="margin-top: 0;">Otros beneficios</h3>
                    <ul class="list-basic" dir="ltr">
                        <li>La posibilidad de acudir a cualquier funeraria, según tu preferencia.</li>
                        <li>Orientación telefónica las 24 horas del día, los 365 días del año, en la Línea de Atención al Cliente de Suramericana.</li>
                        <li>La alternativa de escoger el grupo familiar y el plan que más se adapte a tus necesidades. Este es un producto flexible.</li>
                        <li>Cobertura en gastos de repatriación con el Plan A.</li>
                        <li>Pago por reembolso.</li>
                        <li>Permanencia en el amparo de gastos exequiales de forma indefinida. Además, su cobertura es inmediata por muerte por accidente, homicidio o suicidio.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos-mobile" role="tabpanel" aria-labelledby="pills-documentos-tab">
                    <h3 style="margin-top: 0;">Documentos</h3>
                    <p dir="ltr">Condiciones generales y  solicitud para expedir el seguro Exequias.</p>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/CONDICIONES_GENERALES_SEGURO_EXEQUIAL.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Condiciones generales</a>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/Formulario_Seguro_Exequias_Colmena.pdf" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i> Solicitud Exequias</a>
                        </div>  
                    </div>  
                </div>
            </div>
            

        </div>

    </div>
    <!--div class="faq-area ">
        <div class="container">
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOneInfo" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOneInfo" style="font-size: 16px;">
                                        Información relacionada
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOneInfo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body" style="padding: 0;">
                                    <div class="col-sm-12">
                                        <h3 dir="ltr">¿Cómo cancelar el producto?</h3>

                                        <p dir="ltr">Llamanos o escribenos una carta para la cancelación de la póliza o diligencia el formato de novedades.</p>

                                        <h3 dir="ltr">Causales de terminación</h3>

                                        <ul class="list-basic" dir="ltr">
                                            <li>Por mora en el pago de la prima.</li>
                                            <li>Cuando el tomador así lo determine.</li>
                                            <li>Por voluntad de cualquiera de las partes contratantes mediante aviso escrito dado a la otra. Si la decisión la toma Suramericana, tal aviso se dará con una anticipación no inferior a diez días hábiles a la fecha de revocatoria.</li>
                                            <li>Cuando el tomador o asegurado principal deje de pertenecer al grupo asegurado.</li>
                                        </ul>

                                        <h3 dir="ltr">Canales de atención posventa</h3>

                                        <p dir="ltr">Cuando adquieras tu producto puedes usar alguno de los siguientes canales y con gusto te atenderemos:</p>


                                        <h3>Más información</h3>

                                        <ul class="list-basic">
                                            <li>Teléfono: +57 (2) 485 11 05</li>
                                            <li>Celular: +57 313 680 49 88</li>
                                            <li>WhatsApp: +57 313 680 49 88</li>
                                            <li>Email: servicioalcliente@prontoyseguros.com</li>
                                        </ul>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div-->

    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Que cubre este seguro? 
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Gastos exequiales a quien demuestre haber pagado los gastos, o los servicios funerales de cualquiera de los asegurados.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Cuales son los gastos exequiales cubiertos?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                     <ul class="list-basic">
                                        <li>Diligencias y trámites legales ante las autoridades competentes, para la inhumación o cremación</li>
                                        <li>Cofre de corte lineal. Traslado de la persona fallecida desde su residencia o clínica a las salas de velación, iglesia y al parque cementerio.</li>
                                        <li>(preparación del cuerpo). </li>
                                        <li>Utilización de las Salas de velación a nivel nacional, hasta por veinticuatro (24) horas</li>
                                        <li>Traslado en buseta para los acompañantes (25 personas sólo Bogotá).</li>
                                        <li>Celebración del servicio religioso. Entrega de un libro de asistencia a las honras. </li>
                                        <li>Espacio en Parque Cementerio por 4 años en arrendamiento a nivel nacional donde exista esta modalidad (Derechos de Parque Cementerio), o servicio de Cremación (Urna para las cenizas).</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Quién me entrega el certificado de defunción?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Lo entrega el médico tratante y es responsabilidad del familiar del fallecido.</p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        ¿Tiene traslado nacional?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Si alguno de los asegurados fallece en Colombia, se reembolsará el costo de los trámites y traslado del cuerpo a la ciudad donde se preste el servicio funerario..</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Tiene repatriación?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Si alguno de los asegurados fallece durante los primeros 60 dias de su estancia en el exterior, se reembolsan los gastos de trámite y costos del traslado del cuero hasta colombia.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingSix">
                                <h5 class="mb-0">
                                    <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"  style="font-size: 16px;">
                                        ¿Si el asegurado o quien paga la prima fallece, sigue asegurado el grupo familiar o todo termina?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse text-justify" aria-labelledby="headingSix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>En caso  de  que ocurra  el fallecimiento del asegurado principal la   compañía  cubrirá  la  prima del grupo  familiar  durante  los tres primeros meses de su muerte sin superar en ningún caso  el término de la  vigencia.</p>
                                    <p>¡Ten  presente, cualquiera  de los  asegurados dependientes que sea  mayor de edad, podrá reemplazarte  con el fin de evitar que la  póliza  termine por  falta  de pago, para  ello el asegurado deberá notificar a la  compañía  de ser el nuevo asegurado de su  grupo familiar, y todos  seguirán  cubiertos ante cualquier eventualidad.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/><br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Escoge el plan que más se acomoda a tus necesidades y no esperes para asegurar tu tranquilidad y la de tus seres queridos. </h3> 
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    <div class="row">
        <br/><br/>
    </div>
   @include('layouts.footer')
    <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>
