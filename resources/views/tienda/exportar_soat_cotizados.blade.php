@extends('layouts.app')
@include('layouts.masteradmin')


    @section('content')
   
      
 
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="../moduloadministrador">Inicio</a></li>
                    <li class="breadcrumb-item active">Soat cotizados</li>
                </ol>        
                <div class="card-body">
                    <div id="layoutAuthentication">
                        <div id="layoutAuthentication_content">
                            <main>
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-30">
                                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                                <div class="card-body">
                                                    
                                                    <form action="{{ route('exportar-soat') }}" method="post" id="form-contact" accept-charset="utf-8">
                                                        {!! csrf_field() !!}
                                                        
                                                        <div class="form-row">
                                                            <div class="col-md-20">
                                                                <div class="form-group">
                                                                    <label class="small mb-1" for="name">Exportar soat cotizados por fecha de vencimiento</label>
                                                                    
                                                                </div>
                                                            </div>   
                                                        </div>                                   
                                                        <div class="form-row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="form-group">
                                                                    <label class="small mb-1" for="Hasta">Desde</label>
                                                                        <input type="date" name="mes_start" id="mes_start" class="form-control" >
                                                                    </div>
                                                                </div>
                                                            </div>        
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="form-group">
                                                                        <label class="small mb-1" for="Desde">Hasta</label>
                                                                        <input type="date" name="mes_end" id="mes_end" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>      
                                                        </div>
                                                        <div class="form-group mt-4 mb-0">
                                                            <!--a class="btn btn-primary btn-block" href="{{ url('administrador') }}">Crear la cuenta</a-->
                                                            
                                                            <button type="submit" class="btn btn-primary">Exportar</button>
                                                        </div>
                                                    </form>
                                                            
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>   
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Pronto y Seguros 2020</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>


    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/scripts.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/assets/demo/datatables-demo.js') }}"></script>
      
    </body>
</html>






@endsection




