
    <!--header-area end-->
    @include('layouts.master')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-banner-area bg-pyme">
                    <div class="row align-items-center height-800 pb-111" >
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 text-center mt-70" id="banner_soat" >
                            <div class="banner-text" style="background:#fff; border-radius: 20px;">
                                <img src="assets/images/icon/pyme.png" style="width:16%; margin-top: 15px;">
                                <h2 class="mt-1" style="font-size:43px">Cotiza ya</h2>
                                <h3 class="mt-1" id="linea_tittle" > — Seguro Multiriesgo PYME — </h3>
                                <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="nombres" placeholder="Nombre y apellido" required  oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="text" style="padding: 9px;width: 225px;" name="email" placeholder="Email" required  oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                                    </div>
                                    <div style="padding: 5px;">
                                        <input type="tel" style="padding: 9px;width: 225px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                                    </div>
                                    <div style="padding: 0 15px;">
                                        <p style="font-size: 20px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                                    </div>
                                    <div>
                                        <input type='hidden' name='seguro' value='Multiriesgo PYME' />
                                        <input type='hidden' name='contacto' value='directorcomercial1@prontoyseguros.com' />
                                    </div>
                                    <div style="padding: 5px;"> 
                                        <button class="btn-common" id="form-submit">Cotiza aquí</button>
                                    </div> 
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center" id="banner_soat" >
                        </div>
                        <div class="col-sm-8" id="banner_soat">
                            <div class="banner-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area mt-100 sm-mt-80"  id="banner_soat_mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-center">
                </div>
                <div class="col-xs-8 text-center" style="background:#eaeaea;margin: -70px 0 0 0; border-radius: 20px;">
                    <div class="banner-text"style="">
                        <img src="assets/images/icon/pyme.png" style="width:16%; margin-top: 15px;">
                        <h3 class="mt-1" style="font-weight: 700;">Cotiza ya</h3>
                        <h4 id="linea_tittle" > — Seguro Multiriesgo PYME — </h4>
                        <form action="{{ route('email-seguro') }}" method="post" id="form-contact" accept-charset="utf-8">
                        {!! csrf_field() !!}
                            <div style="padding: 5px;">
                                <input type="text" style="padding: 9px;width: 183px;" name="nombres" placeholder="Nombre y apellido" required   oninvalid="setCustomValidity('Por favor agregar tu Nombre y Apellido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="email" style="padding: 9px;width: 183px;" name="email" placeholder="Email" required   oninvalid="setCustomValidity('Por favor agregar un Email valido')" onchange="try{setCustomValidity('')}catch(e){}" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
                            </div>
                            <div style="padding: 5px;">
                                <input type="tel" style="padding: 9px;width: 183px;" name="telefono" placeholder="Celular" minlength="7" maxlength="10" required oninvalid="setCustomValidity('Por favor agregar un número de Celular valido')" onchange="try{setCustomValidity('')}catch(e){}"/>
                            </div>
                            <div style="padding: 5px;">
                                <p style="font-size: 15px;color: #646567;">¡Protégete de imprevistos y disfruta la vida sin límites! </p>   
                            </div>
                            <div>
                                <input type='hidden' name='seguro' value='Multiriesgo PYME' />
                                <input type='hidden' name='contacto' value='directorcomercial1@prontoyseguros.com' />
                            </div>
                            <div style="padding: 5px;">  
                                <button class="btn-common" id="form-submit">Cotiza aquí</button>
                            </div> 
                        </form>
                    </div>
                </div>
                <div class="col-xs-2 text-center" id="banner_soat_mobile">
                </div>
            </div>
        </div>
    </div>
   
    
    <!--Estandar-->
    <div class="benefit-area sm-mt-65">
        <div class="container"  style="padding: 0 20px;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold">Póliza de multiriesgo - PYME</h3>
                    <div class="sin-service style-2 text-justify">
                        <p style="font-size:16px;line-height: 25px;">Cobertura diseñada especialmente para la pequeña y mediana empresa, protege las propiedades y el patrimonio micro empresarial en caso de ocurrencia de diversos eventos que afecten su estabilidad y correcto funcionamiento.</p>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="font-weight-bold" id="linea_tittle">Beneficios</h3>
                </div>
                <div class="col-lg-5 col-sm-6" style="padding: 0;"  id="soat_items">
                    <div class="section-title style-4 text-center "  style="background-color: #eaeaea;">
                        <img src="assets/images/vida/multiriesgo.jpg" alt="promo"  >
                    </div>
                </div>
                <div class="col-lg-7 col-sm-6" style="background-color: #eaeaea;">
                    <br/>
                    <ul class="list-basic" style="margin-top:35px" >
                        <li id="listado">Se asegura  tanto el predio como los   bienes,el capital, contenidos, según  la  necesidad  y tipo de negocio de cada empresario.</li>
                        <li id="listado">En  caso de siniestro se cubren diferentes  gastos según  la  póliza  contratada.</li>
                        <li id="listado">Se  cubre la remoción de  escombros, extinción, reposición de archivos.</li>
                        <li id="listado">Tiene  asistencia eléctrica, custodia de bienes, traslados.</li>
                        <li id="listado">Incluyen  amparos  adicionales, como  huelga, daños internos, hurto etc.</li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="container" style="padding: 0 7px;">
        <div class="row"  id="seguros">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                   
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-dos" role="tab" aria-controls="pills-dos" aria-selected="true" style="color: #000;">Cobertura</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Otras Coberturas</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-documentos-tab" data-toggle="pill" href="#pills-documentos" role="tab" aria-controls="pills-documentos" aria-selected="true" style="color: #000;">Formato Cotización</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent" style="margin: 0px -24px; border-left: 1px solid #e2032b;">
                <div class="tab-pane fade show active" id="pills-dos" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 style="margin-top: 0;">Coberturas</h3>
                    <h4>Amparos básicos, todo riesgo Daño material.</h4>
                    <ul class="list-basic">
                        <li>Incendio y/o rayo, explosion, extensión de amparos(índice variable)</li>
                        <li>Terremoto, temblor y/o erupción volcánica/maremoto(Índice variable)</li>
                        <li>HMACC AMIT(Índice variable)</li>
                        <li>Equipos médicos y / científicos especializados</li>
                        <li>Equipos móviles y portátiles(Índice variable en contenidos)</li>
                        <li>Herramientas</li>
                        <li>Obras de Arte</li>
                        <li>Valores de tránsito</li>
                        <li>Dineros en efectivo y títulos valores</li>
                        <li>Índice variable</li>
                        <li>Lucro cesante por Rotura (si se toma el amparo)</li>
                        <li>Manejo (infidelidad de empleados)</li>
                        <li>Responsabilidad Civil Extracontractual</li>
                        <li>Accidentes personales</li>
                        <li>Asistencias.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-tarifas" role="tabpanel" aria-labelledby="pills-tarifas-tab">
                    <h3 style="margin-top: 0;">Otras Coberturas</h3>
                    <h4>En el  amparo  de responsabilidad Civil PLO</h4>
                    <p>Predio labores y operaciones.</p>
                    <ul class="list-basic">
                        <li>Responsabilidad Civil patronal</li>
                        <li>Responsabilidad Civil parqueaderos</li>
                        <li>Responsabilidad Civil contratistas y subcontratistas</li>
                        <li>Responsabilidad civil productos (servitecas, talleres, laboratorios, alimentos)</li>
                        <li>Responsabilidad vehiculos propios y  no propios</li>
                        <li>Bienes bajo cuidado, tenencia y control</li>
                        <li>Costos de Procesos</li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>
                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos" role="tabpanel" aria-labelledby="pills-documentos-tab" style="height: 230px;">
                    <h3 style="margin-top: 0;">Formato Cotización</h3>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_PYME.xlsx" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Formato cotización seguro PYME</a>
                        </div>  
                    </div>  
                </div>
                
            </div>

        </div>


        <div class="row"  id="seguros_mobile">
            <div class="tab-content col-md-3 col-xs-12" style="z-index: 1; margin-top: 5px;">
                <ul class="nav nav-pills mb-3 flex-column" id="pills-tab" role="tablist" style="font-size:16px; text-align: justify;">
                    
                    <li class="nav-item active" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-mobile-tab" data-toggle="pill" href="#pills-dos-mobile" role="tab" aria-controls="pills-dos-mobile" aria-selected="true" style="color: #000;">Cobertura</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-tarifas-tab" data-toggle="pill" href="#pills-tarifas-mobile" role="tab" aria-controls="pills-tarifas" aria-selected="true" style="color: #000;">Otros Coberturas</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-contact-mobile-tab" data-toggle="pill" href="#pills-contact-mobile" role="tab" aria-controls="pills-contact-mobile" aria-selected="false" style="color: #000;">Solicitud y gestión del seguro</a>
                    </li>
                    <li class="nav-item" style="margin-left: 0px;">
                        <a class="nav-link" id="pills-home-documentos-tab" data-toggle="pill" href="#pills-documentos-mobile" role="tab" aria-controls="pills-documentos" aria-selected="true" style="color: #000;">Formato Cotización</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content col-md-9 col-xs-12" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-dos-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Coberturas</h3>
                    <h4>Amparos básicos, todo riesgo Daño material.</h4>
                    <ul class="list-basic">
                        <li>Incendio y/o rayo, explosion, extensión de amparos(índice variable)</li>
                        <li>Terremoto, temblor y/o erupción volcánica/maremoto(Índice variable)</li>
                        <li>HMACC AMIT(Índice variable)</li>
                        <li>Equipos médicos y / científicos especializados</li>
                        <li>Equipos móviles y portátiles(Índice variable en contenidos)</li>
                        <li>Herramientas</li>
                        <li>Obras de Arte</li>
                        <li>Valores de tránsito</li>
                        <li>Dineros en efectivo y títulos valores</li>
                        <li>Índice variable</li>
                        <li>Lucro cesante por Rotura (si se toma el amparo)</li>
                        <li>Manejo (infidelidad de empleados)</li>
                        <li>Responsabilidad Civil Extracontractual</li>
                        <li>Accidentes personales</li>
                        <li>Asistencias.</li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="pills-tarifas-mobile" role="tabpanel" aria-labelledby="pills-profile-mobile-tab">
                    <h3 style="margin-top: 0;">Otras Coberturas</h3>
                    <h4>En el  amparo  de responsabilidad Civil PLO</h4>
                    <p>Predio labores y operaciones.</p>
                    <ul class="list-basic">
                        <li>Responsabilidad Civil patronal</li>
                        <li>Responsabilidad Civil parqueaderos</li>
                        <li>Responsabilidad Civil contratistas y subcontratistas</li>
                        <li>Responsabilidad civil productos (servitecas, talleres, laboratorios, alimentos)</li>
                        <li>Responsabilidad vehiculos propios y  no propios</li>
                        <li>Bienes bajo cuidado, tenencia y control</li>
                        <li>Costos de Procesos</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact-mobile" role="tabpanel" aria-labelledby="pills-contact-mobile-tab">
                    <h3 style="margin-top: 0;">Solicitud y gestión del seguro</h3>

                    <p>Para solicitar novedades relacionadas con información del producto, atención de reclamos y cancelaciones, puedes comunicarte con las siguientes líneas de atención:</p>

                    <ul class="list-basic">
                        <li>Teléfono: +57 (2) 4851105 Ext 771 - 705 - 706</li>
                        <li>Celular: +57 3216425482</li>
                        <li>WhatsApp: +57 3216425482</li>
                        <li>Email: generales@prontoyseguros.com</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-documentos-mobile" role="tabpanel" aria-labelledby="pills-documentos-tab">
                    <h3 style="margin-top: 0;">Formato Cotización</h3>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <a href="assets/documentos/FORMATO_PYME.xlsx" class="btn btn-default pull-right" target="_blank" style="width: 300px;"><i class="fa fa-download"></i>Formato cotización seguro PYME</a>
                        </div>  
                    </div>    
                </div>
                
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="faq-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="section-title style-2 text-center">
                        <h3 id="linea_tittle">Resuelve tus dudas</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12">
                    <div id="accordion">
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne" style="font-size: 16px;">
                                        ¿Que ampara un seguro Pyme?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show text-justify" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Protege las pequeñas, medianas y grandes empresas, (Pyme) ante los riesgos a los que están expuestas.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingTwo">
                                <h5 class="mb-0">
                                    <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  style="font-size: 16px;">
                                        ¿Que es un seguro Multirriesgo?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse text-justify" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Es un seguro  que protege a las pequeñas y medianas empresas (pymes) ante aquellos riesgos a los que están expuestas; ampara los bienes muebles (contenidos) e inmuebles (planta física) cuando estos sufran daños a causa de un evento cubierto por la póliza.El seguro tiene una cobertura básica para los bienes en caso de incendio, explosión y daños por agua, entre otros. Adicionalmente, se brindan coberturas opcionales para otros eventos como terremoto, actos mal intencionados de terceros, hurto, lucro cesante, rotura de maquinaria o responsabilidad ante terceros.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingThree">
                                <h5 class="mb-0">
                                    <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="font-size: 16px;">
                                        ¿Qué es una póliza Modular?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse text-justify" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Este seguro es ideal para empresarios con negocios como restaurantes, peluquerías, servitecas, ferreterías, cacharrerías, droguerías y se puede modular por amparos.</p>
                                </div>
                            </div>
                        </div>
                         <div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFour">
                                <h5 class="mb-0">
                                    <a href="#collapsefour" class="btn btn-link" data-toggle="collapse" aria-expanded="true" aria-controls="collapsefour" style="font-size: 16px;">
                                        Que es amit y hmacc?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse text-justify" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>HMACC : huelga, motín, asonada, conmoción civil - AMIT : Actos mal intencionados de terceros.</p>
                                </div>
                            </div>
                        </div>
                        <!--div class="card single-faq">
                            <div class="card-header faq-heading" id="headingFive">
                                <h5 class="mb-0">
                                    <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"  style="font-size: 16px;">
                                        ¿Qué significa  Hmc Cap?
                                        <i class="fa fa-plus-circle pull-right" aria-hidden="true"></i>
                                        <i class="fa fa-minus-circle pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse text-justify" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Terremoto, maremoto, temblor o erupción volcánica</p>
                                </div>
                            </div>
                        </div-->
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
    <div class="benefit-area mt-80 sm-mt-65" >
        <div class="container" style="background-color: #eaeaea;">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 id="linea_tittle" style="padding: 0 10px; text-align: center;">Protege tu patrimonio empresarial. </h3> 
                    
                </div>
            </div>
        </div>
    </div>
    @include('layouts.contactenos')
    <div class="row">
        <br/><br/>
    </div>
    <!--Fin Estandar-->

    <!--blog-area end-->
    
   @include('layouts.footer')
   <script type="text/javascript">
        $('#myTab a').on('click', function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
        $('.collapse').collapse()
    </script>