
    <!--header-area end-->
    @include('layouts.master')
    
    
    <!--page-banner-area start-->
    <div class="page-banner-area bg-2">
        <div class="container">
            <div class="row align-items-center height-400">
                <div class="col-lg-12">
                    <div class="page-banner-text text-white text-center">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--page-banner-area end-->
    
    <!--contact-area start-->
    <div class="contact-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="contact-info">

                        <h3>Contáctenos</h3>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-map-marker"></i>Dirección</h4>
                            <p>Cra. 66a No 10-15 Br .Limonar<br/>
                                Cali, Colombia</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-phone"></i>Teléfono</h4>
                            <p>Fijo: +602 4851105 Ext 706</p>
                            <p>Celular: 312 893 2013</p>
                        </div>
                        <div class="single-contact-info">
                            <h4><i class="fa fa-envelope"></i>Email</h4>
                            <p>servicioalcliente@prontoyseguros.com</p>
                            <p>ayuda@prontoyseguros.com</p>
                        </diV>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 sm-mt-75">
                    <div class="contact-form style-3">
                        <form action="{{ route('enviar-email') }}" method="post" id="form-contact" accept-charset="utf-8">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" name="nombres" placeholder="Nombre" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="apellidos" placeholder="Apellido" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="telefono" placeholder="Teléfono" required/>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="email" placeholder="Email" required/>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="asunto" placeholder="Motivo Consulta" required/>
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="mensaje" placeholder="Mensaje" required></textarea>
                                </div>
                                <div class="col-lg-12" style="text-align: right;">
                                    <button class="btn-common" id="form-submit">Enviar Mensaje</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <br/><br/>
    </div>
    <div class="blog-area mt-100 sm-mt-80" style="border:solid 2px #e0030e;text-align: center; color:#e0030e">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 text-center">
                    <div class="logo">
                        <a href="{{ url('inicio') }}"><img src="assets/images/logo.png" alt="logo"></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 col-sm-12" >
                    <div class="blog-details">
                        <div class="section-title style-4 text-center">
                            <h2 style="color:#e0030e; margin-top: 4px;">Oficina principal</h2>
                            <h4 style="color:#e0030e">Lunes a Viernes 8:00 am - 5:00 pm</h4>
                            <h4 style="color:#e0030e">Sábados 9:00 am A 1:00 pm</h4>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <!--contact-area end-->
    <!--div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    
                    <div class="row">
                        <div class="col-lg-12 p-0">
                            <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify; width: 95%;">
                                <div class="card-header faq-heading" id="headingTOne">
                                    <h5 class="mb-0">
                                        <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                            Cali - Calle novena
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10">
                                                <p>Calle 9 #39-00 </p>
                                                <p>Cel: 316 3112464</p>
                                                <p>PBX: 485 1105 – Ext. 790</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12 p-0">
                            <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify; width: 95%;">
                                <div class="card-header faq-heading" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a href="#collapseTwo" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                            Cali - Calle 70N sameco
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10">
                                                <p>Calle 70N # 2a – 270</p>
                                                <p>Celular: 314 791 4894</p>
                                                <p>PBX: (2)485 1105 – Ext. 780</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12 p-0">
                            <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify; width: 95%;">
                                <div class="card-header faq-heading" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a href="#collapseThree" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                            Cali - Av. Pasoancho
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10">
                                                <p>Calle 13 # 77 – 109</p>
                                                <p>Celular: 321 636 7921</p>
                                                <p>PBX: (2)485 1105 – Ext. 786</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div-->

    <!--div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">                    
                    <div class="row">
                        <div class="col-lg-12 p-0">
                            <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify; width: 95%;">
                                <div class="card-header faq-heading" id="headingTOne">
                                    <h5 class="mb-0">
                                        <a href="#collapseFour" class="btn btn-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                            Cali - Avenida vásquez cobo
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10">
                                                <p>Avenida Vásquez Cobo #34N – 54 </p>
                                                <p>PBX: (2)485 1105 – Ext. 739</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12 p-0">
                            <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify; width: 95%;">
                                <div class="card-header faq-heading" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a href="#collapseFive" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                            Pasto
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10">
                                                <p>Cra 33 # 1A-56</p>
                                                <p>LOCAL 22 CENTRO CIAL EL VERGEL OFICINA 102</p>
                                                <p>Teléfono: (2) 485 11 05 Ext: 770</p>
                                                <p>Celular: 317 371 1339 Celular: 300 402 3458</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12 p-0">
                            <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify; width: 95%;">
                                <div class="card-header faq-heading" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a href="#collapseSix" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                            Popayan - Alkamotor
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10">
                                                <p>CALLE 23 Norte # 9-26 Esquina</p>
                                                <p>Celular: 311 323 4848</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    
    <div class="blog-area mt-100 sm-mt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    
                    <div class="row">
                        <div class="col-lg-12 p-0">
                            <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify; width: 95%;">
                                <div class="card-header faq-heading" id="headingTOne">
                                    <h5 class="mb-0">
                                        <a href="#collapseSeven" class="btn btn-link" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                            Palmira - Concesionario centro motors
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10">
                                                <p>Calle 42 No. 32b-05 </p>
                                                <p>Celular: 317 635 11 67</p>
                                                <p>PBX: (2) 233 97 13 EXT 201</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12 p-0">
                            <div class="card single-faq style-2" style="margin: 0px 27px 0 0px; padding:0% 5%; text-align:Justify; width: 95%;">
                                <div class="card-header faq-heading" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a href="#collapseEight" class="btn btn-link" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                                Tulua - Concesionario centro motors
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10">
                                                <p>Carrera 40 No. 26-50</p>
                                                <p>Celular: 316 297 6133</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div-->
    <div>
        </br></br>
    </div>  
    <!--google-map area start-->
    <div class="google-map-area mt-80 sm-mt-75 xs-mt-50">
        <iframe src="https://maps.google.com/maps?q=Pronto+y+seguros&amp;iwloc=near&amp;output=embed" width="100%" height="400px" frameborder="0" style="border:0"></iframe>
        <!--div id="googleMap" class="gmap-two"></div-->
    </div>
    <!--google-map area end-->

    <!--subscribe-area start-->
    <!--div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="subscribe-form">
                        <h3>Escribenos</h3>
                        <p>Le enviaremos noticias mensuales </p>
                        <input type="email" placeholder="Su Email" />
                        <button class="btn-common">Suscribase</button>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!--subscribe-area end-->
    
    
    <!--google-map-->
    <script src="https://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.22&amp;key=AIzaSyChs2QWiAhnzz0a4OEhzqCXwx_qA9ST_lE"></script>
    <script>
        google.maps.event.addDomListener(window, 'load', init);
        function init() {
            var mapOptions = {
                zoom: 11,
                scrollwheel: true,
                center: new google.maps.LatLng(3.43722, -76.5225), 

                styles: 
                    [
                        {
                            "featureType": "all",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "saturation": 36
                                },
                                {
                                    "color": "#333333"
                                },
                                {
                                    "lightness": 40
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "visibility": "on"
                                },
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 17
                                },
                                {
                                    "weight": 1.2
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#dedede"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 29
                                },
                                {
                                    "weight": 0.2
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 18
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                },
                                {
                                    "lightness": 19
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#e9e9e9"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        }
                    ]
            };
            var mapElement = document.getElementById('googleMap');

            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6700, -73.9400),
                map: map
            });
        }
    </script>

   @include('layouts.footer')