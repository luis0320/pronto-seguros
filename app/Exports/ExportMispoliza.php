<?php

namespace App\Exports;

use App\Models\Mispoliza;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExportMispoliza implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Mispoliza::all();
    }
}
