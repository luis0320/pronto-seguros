<?php

namespace App\Exports;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\Cotizacionsoat;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExportSoatCotizados implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function collection()
    {

        
        // $fi = $request->mes_start.' 00.00.01';
        //$ff = $request->mes_end.' 23.59.59';
      
        //$fi = $request->mes_start;
        //$ff = $request->mes_end;
        $fi = $this->request->mes_start;
        $ff = $this->request->mes_end;

        return Cotizacionsoat::select('*')
        ->whereBetween('fecha',[$fi,$ff])
        ->orderBy('fecha','DESC')    
        ->get();


        //return Cotizacionsoat::all();
        //return back()->with(compact('cotizacotizados'));

    }
}
