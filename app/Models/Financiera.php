<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Financiera
 *
 * @property $id
 * @property $nit
 * @property $digito
 * @property $nombre
 * @property $telefono
 * @property $correo
 * @property $activo
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Financiera extends Model
{
    
    static $rules = [
		'nit' => 'required',
		'digito' => 'required',
		'nombre' => 'required',
		'telefono' => 'required',
		'correo' => 'required',
		'activo' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nit','digito','nombre','telefono','correo','activo'];



}
