<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Scancelacione
 *
 * @property $id
 * @property $estadotramite
 * @property $fechaacancelar
 * @property $celular
 * @property $email
 * @property $placa
 * @property $observacion
 * @property $mc_idmcancelaciones
 * @property $c_idcompanias
 * @property $f_idfinancieras
 * @property $u_idusuarios
 * @property $b_idbeneficiarios
 * @property $filecarta
 * @property $filelevantamiento
 * @property $filedevolucion
 * @property $filecedula
 * @property $filetarjetadepropiedad
 * @property $filesarlaft
 * @property $filepoliza
 * @property $fileotros
 * @property $fileotros1
 * @property $created_at
 * @property $updated_at
 * @property $observacion
 * @property $ob_idcancelaciones
 *
 * @property Compania $compania
 * @property Financiera $financiera
 * @property Motivcancelacione $motivcancelacione
 * @property Estado $estado
 * @property Beneficiario $beneficiario  
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Scancelacione extends Model
{
    
    static $rules = [
		'estadotramite' => 'required',
		'fechaacancelar' => 'required',
		'celular' => 'required',
		'email' => 'required',
		'observacion' => 'bail',
		'mc_idmcancelaciones' => 'required',
		'c_idcompanias' => 'required',
		'f_idfinancieras' => 'required',
		'u_idusuarios' => 'required',
		'b_idbeneficiarios' => 'required',
        'filecarta' => 'bail',
        'filelevantamiento'=> 'bail',
        'filedevolucion'=> 'bail',
        'filecedula'=> 'bail',
        'filetarjetadepropiedad'=> 'bail',
        'filesarlaft'=> 'bail',
        'filepoliza'=> 'bail',
        'fileotros'=> 'bail',
        'fileotros1'=> 'bail',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['estadotramite','fechaacancelar','celular','email','placa','observacion','mc_idmcancelaciones','c_idcompanias','f_idfinancieras','u_idusuarios','b_idbeneficiarios','filecarta','filelevantamiento','filedevolucion','filecedula','filetarjetadepropiedad','filesarlaft','filepoliza','fileotros','fileotros1'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function compania()
    {
        return $this->hasOne('App\Models\Compania', 'id', 'c_idcompanias');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function financiera()
    {
        return $this->hasOne('App\Models\Financiera', 'id', 'f_idfinancieras');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function motivcancelacione()
    {
        return $this->hasOne('App\Models\Motivcancelacione', 'id', 'mc_idmcancelaciones');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function estado()
    {
        return $this->hasOne('App\Models\Estado', 'id', 'estadotramite');
    }
        /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function beneficiario()
    {
        return $this->hasOne('App\Models\Beneficiario', 'id', 'b_idbeneficiarios');
    }
    
    

}
