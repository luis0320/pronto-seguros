<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Mispoliza
 *
 * @property $id
 * @property $Poliza
 * @property $Anexo
 * @property $Nota
 * @property $FecNot
 * @property $Desde
 * @property $Hasta
 * @property $Ej
 * @property $CodCli
 * @property $NitCC
 * @property $NombreCliente
 * @property $Compania
 * @property $Rm
 * @property $Ramo
 * @property $PrimNeta
 * @property $GastoExp
 * @property $IVA
 * @property $PrimTot
 * @property $Comi
 * @property $Comision
 * @property $PrimPropi
 * @property $ComiPropi
 * @property $Ca
 * @property $Placas
 * @property $Proc01
 * @property $Pro1
 * @property $Proc1
 * @property $Proc02
 * @property $Pro2
 * @property $Proc2
 * @property $NombreProced
 * @property $Tv
 * @property $Us
 * @property $Tomador
 * @property $Beneficiario
 * @property $Observaciones
 * @property $Sub
 * @property $TP
 * @property $Ap
 * @property $FecNac
 * @property $Tel1Aseg
 * @property $Tel2Aseg
 * @property $CelAsegur
 * @property $DirAsegur
 * @property $EmailAsegur
 * @property $Ciudad
 * @property $Depto
 * @property $FecRbo
 * @property $Rferencia
 * @property $Zn
 * @property $VrAsegPoliz
 * @property $VrAsegur
 * @property $Mode
 * @property $CodFase
 * @property $Marca
 * @property $Clase
 * @property $Tipo
 * @property $Motor
 * @property $SerieChs
 * @property $Servicio
 * @property $Cilindraj
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Mispoliza extends Model
{
    
    static $rules = [
		'Poliza' => 'required',
		'Anexo' => 'required',
		'Nota' => 'required',
		'FecNot' => 'required',
		'Desde' => 'required',
		'Hasta' => 'required',
		'Ej' => 'required',
		'CodCli' => 'required',
		'NitCC' => 'required',
		'NombreCliente' => 'required',
		'Compania' => 'required',
		'Rm' => 'required',
		'Ramo' => 'required',
		'PrimNeta' => 'required',
		'GastoExp' => 'required',
		'IVA' => 'required',
		'PrimTot' => 'required',
		'Comi' => 'required',
		'Comision' => 'required',
		'PrimPropi' => 'required',
		'ComiPropi' => 'required',
		'Ca' => 'required',
		'Placas' => 'required',
		'Proc01' => 'required',
		'Pro1' => 'required',
		'Proc1' => 'required',
		'Proc02' => 'required',
		'Pro2' => 'required',
		'Proc2' => 'required',
		'NombreProced' => 'required',
		'Tv' => 'required',
		'Us' => 'required',
		'Tomador' => 'required',
		'Beneficiario' => 'required',
		'Observaciones' => 'required',
		'Sub' => 'required',
		'TP' => 'required',
		'Ap' => 'required',
		'FecNac' => 'required',
		'Tel1Aseg' => 'required',
		'Tel2Aseg' => 'required',
		'CelAsegur' => 'required',
		'DirAsegur' => 'required',
		'EmailAsegur' => 'required',
		'Ciudad' => 'required',
		'Depto' => 'required',
		'FecRbo' => 'required',
		'Rferencia' => 'required',
		'Zn' => 'required',
		'VrAsegPoliz' => 'required',
		'VrAsegur' => 'required',
		'Mode' => 'required',
		'CodFase' => 'required',
		'Marca' => 'required',
		'Clase' => 'required',
		'Tipo' => 'required',
		'Motor' => 'required',
		'SerieChs' => 'required',
		'Servicio' => 'required',
		'Cilindraj' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Poliza','Anexo','Nota','FecNot','Desde','Hasta','Ej','CodCli','NitCC','NombreCliente','Compania','Rm','Ramo','PrimNeta','GastoExp','IVA','PrimTot','Comi','Comision','PrimPropi','ComiPropi','Ca','Placas','Proc01','Pro1','Proc1','Proc02','Pro2','Proc2','NombreProced','Tv','Us','Tomador','Beneficiario','Observaciones','Sub','TP','Ap','FecNac','Tel1Aseg','Tel2Aseg','CelAsegur','DirAsegur','EmailAsegur','Ciudad','Depto','FecRbo','Rferencia','Zn','VrAsegPoliz','VrAsegur','Mode','CodFase','Marca','Clase','Tipo','Motor','SerieChs','Servicio','Cilindraj'];



}
