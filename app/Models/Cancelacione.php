<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cancelacione
 *
 * @property $id
 * @property $motivo
 * @property $aseguradora
 * @property $fechadecancelacion
 * @property $cedula
 * @property $nombre
 * @property $beneficiario
 * @property $metododepago
 * @property $estado
 * @property $fecharegistro
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Cancelacione extends Model
{
    
    static $rules = [
		'motivo' => 'required',
		'aseguradora' => 'required',
		'fechadecancelacion' => 'required',
		'cedula' => 'required',
		'nombre' => 'required',
		'beneficiario' => 'required',
		'metododepago' => 'required',
		'estado' => 'required',
		'fecharegistro' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['motivo','aseguradora','fechadecancelacion','cedula','nombre','beneficiario','metododepago','estado','fecharegistro'];



}
