<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Observacione
 *
 * @property $id
 * @property $observacion
 * @property $ob_idestado
 * @property $ob_idcancelaciones
 * @property $u_idusuarios
 * @property $created_at
 * @property $updated_at
 *
 * @property Estado $estado
 * @property User $user
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Observacione extends Model
{
    
    static $rules = [
		'observacion' => 'required',
		'ob_idestado' => 'required',
		'ob_idcancelaciones' => 'required',
		'u_idusuarios' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['observacion','ob_idestado','ob_idcancelaciones','u_idusuarios'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function estado()
    {
        return $this->hasOne('App\Models\Estado', 'id', 'ob_idestado');
    }


        /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function User()
    {
        return $this->hasOne('App\Models\Estado', 'id', 'u_idusuarios');
    }

}
