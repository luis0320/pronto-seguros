<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Activosoattarifa
 *
 * @property $id
 * @property $tarifa
 * @property $activ
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Activosoattarifa extends Model
{
    
    static $rules = [
		'tarifa' => 'required',
		'activ' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tarifa','activ'];



}
