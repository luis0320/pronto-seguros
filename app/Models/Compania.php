<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Compania
 *
 * @property $id
 * @property $nit
 * @property $digito
 * @property $nombre
 * @property $asistencia
 * @property $asistenciafijo
 * @property $activo
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Compania extends Model
{
    
    static $rules = [
		'nit' => 'required',
		'digito' => 'required',
		'nombre' => 'required',
		'asistencia' => 'required',
		'asistenciafijo' => 'required',
		'activo' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nit','digito','nombre','asistencia','asistenciafijo','activo'];



}
