<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cotizacionsoat
 *
 * @property $id
 * @property $placa
 * @property $fecha
 * @property $total
 * @property $marca
 * @property $linea
 * @property $modelo
 * @property $cedula
 * @property $correo
 * @property $celular1
 * @property $descuento
 * @property $preciodes
 * @property $celular
 * @property $codigoreferido
 * @property $fecha_creacion
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Cotizacionsoat extends Model
{
    
    static $rules = [
		'placa' => 'required',
		'fecha' => 'required',
		'total' => 'required',
		'marca' => 'bail',
		'linea' => 'bail',
		'modelo' => 'bail',
		'cedula' => 'bail',
		'correo' => 'bail',
		'celular1' => 'bail',
		'descuento' => 'bail',
		'preciodes' => 'bail',
		'celular' => 'bail',
		'codigoreferido' => 'bail',
		'fecha_creacion' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['placa','fecha','total','marca','linea','modelo','cedula','correo','celular1','descuento','preciodes','celular','codigoreferido','fecha_creacion'];



}
