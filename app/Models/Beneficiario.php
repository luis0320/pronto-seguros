<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Beneficiario
 *
 * @property $id
 * @property $nit
 * @property $digito
 * @property $benefnombre
 * @property $activo
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Beneficiario extends Model
{
    
    static $rules = [
		'nit' => 'required',
		'digito' => 'required',
		'benefnombre' => 'required',
		'activo' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nit','digito','benefnombre','activo'];



}
