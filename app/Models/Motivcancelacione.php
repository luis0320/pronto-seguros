<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Motivcancelacione
 *
 * @property $id
 * @property $motivnombre
 * @property $activo
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Motivcancelacione extends Model
{
    
    static $rules = [
		'motivnombre' => 'required',
		'activo' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['motivnombre','activo'];



}
