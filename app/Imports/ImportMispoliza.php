<?php

namespace App\Imports;

use App\Models\Mispoliza;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportMispoliza implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Mispoliza([

            //
            'Poliza' => $row[0],
            'Anexo' => $row[1],
            'Nota' => $row[2],
            'FecNot' => $row[3],
            'Desde' => $row[4],
            'Hasta' => $row[5],
            'Ej' => $row[6],
            'CodCli' => $row[7],
            'NitCC' => $row[8],
            'NombreCliente' => $row[9],
            'Compania' => $row[10],
            'Rm' => $row[11],
            'Ramo' => $row[12],
            'PrimNeta' => $row[13],
            'GastoExp' => $row[14],
            'IVA' => $row[15],
            'PrimTot' => $row[16],
            'Comi' => $row[17],
            'Comision' => $row[18],
            'PrimPropi' => $row[19],
            'ComiPropi' => $row[20],
            'Ca' => $row[21],
            'Placas' => $row[22],
            'Proc01' => $row[23],
            'Pro1' => $row[24],
            'Proc1' => $row[25],
            'Proc02' => $row[26],
            'Pro2' => $row[27],
            'Proc2' => $row[28],
            'NombreProced' => $row[29],
            'Tv' => $row[30],
            'Us' => $row[31],
            'Tomador' => $row[32],
            'Beneficiario' => $row[33],
            'Observaciones' => $row[34],
            'Sub' => $row[35],
            'TP' => $row[36],
            'Ap' => $row[37],
            'FecNac' => $row[38],
            'Tel1Aseg' => $row[39],
            'Tel2Aseg' => $row[40],
            'CelAsegur' => $row[41],
            'DirAsegur' => $row[42],
            'EmailAsegur' => $row[43],
            'Ciudad' => $row[44],
            'Depto' => $row[45],
            'FecRbo' => $row[46],
            'Rferencia' => $row[47],
            'Zn' => $row[48],
            'VrAsegPoliz' => $row[49],
            'VrAsegur' => $row[50],
            'Mode' => $row[51],
            'CodFase' => $row[52],
            'Marca' => $row[53],
            'Clase' => $row[54],
            'Tipo' => $row[55],
            'Motor' => $row[56],
            'SerieChs' => $row[57],
            'Servicio' => $row[58],
            'Cilindraj' => $row[59],

        ]);
    }
}
