<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/license-plate',
        '/user',
        '/operation-car',
        '/payment',
        '/multicash',
        '/new-insurance-policy-budget',
        '/purchase',
        '/cotizar-poliza',
        '/imprimir-cotizacion',
        '/emision-poliza'
    ];
}
