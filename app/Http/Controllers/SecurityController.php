<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class SecurityController extends Controller
{
    private $endpointPrueba = 'https://pre.transfiriendo.com:448/PortaliSoatSEApi/api/Account/Token';
    private $usernamePrueba = 'PV VIRTUAL';
    private $passwordPrueba = '!QAZxsw2';
    private $client_secretPrueba = 'kNzZ!PxQyDoS';


    private $endpoint = 'https://www.esbus.transfiriendo.com/PortaliSoatSEApi/api/Account/Token';
    private $username = 'DANIELA.MAZO';
    private $password = 'Daheny85*';
    private $client_id = 'iSoatAPIV2';
    private $client_secret = '49-!C@YVlk_*';

    public function getToken() {
        $body = [
            'grant_type' => 'password',
            'username' => $this->username,
            'password' => $this->password,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret
        ];
        $client = new Client();
        $response = $client->request('POST',
            $this->endpoint, ['form_params' => $body]
        );
        // dd($body);
        $responseFormated = json_decode($response->getBody()->getContents());
        return $responseFormated->access_token;
    }
    public function getTokenPrueba() {
        $body = [
            'grant_type' => 'password',
            'username' => $this->usernamePrueba,
            'password' => $this->passwordPrueba,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secretPrueba
        ];
        $client = new Client();
        $response = $client->request('POST',
            $this->endpointPrueba, ['form_params' => $body]
        );
        // dd($body);
        $responseFormated = json_decode($response->getBody()->getContents());
        return $responseFormated->access_token;
    }
}
