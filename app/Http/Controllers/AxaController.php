<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use MP;
use DB;
use App\Conten;
use App\Banner;
use App\Support\Collection;
use App\User;
use Auth;
use Mail;
use App\Mail\UsuarioCreado;
use App\Mail\EmailContacto;


class AxaController extends Controller
{

    private $endpoint = 'http://localhost:8085';
    private $canal = 2;

    public function policyQuote(Request $request)
    {
        $client = new Client();
        $response = $client->request('GET', $this->endpoint . '/services/ConsultarPolizaAuto?identificacion='.$request->input('identificacion').'&tipoIdentificacion='.$request->input('tipoIdentificacion').'&estadoCivil='.$request->input('estadoCivil').'&fechaNacimiento='.$request->input('fechaNacimiento').'&genero='.$request->input('genero').'&primerApellido='.$request->input('primerApellido').'&primerNombre='.$request->input('primerNombre').'&placa='.$request->input('placa').'&identificacionEmpresa='.$request->input('identificacionEmpresa').'&codigoDistribuidor='.$request->input('codigoDistribuidor').'&modelo='.$request->input('modelo').'&fasecoldaMarcaId='.$request->input('fasecoldaMarcaId').'&fasecoldaModeloId='.$request->input('fasecoldaModeloId').'&monto='.$request->input('monto').'&codigoDivipola='.$request->input('codigoDiviPola').'&detalleMontoVehiculo='.$request->input('detalleMontoVehiculo').'&esNuevo='.$request->input('esNuevo'));

        $response = $client->request('GET', $this->endpoint . '/services/ConsultarPolizaAuto?identificacion=94332124&tipoIdentificacion=1&estadoCivil=1&fechaNacimiento=1975-06-20&genero=M&primerApellido=Cicery&primerNombre=yhefferson&placa=JPK254&identificacionEmpresa=9008374397&codigoDistribuidor=50372&modelo=2020&fasecoldaMarcaId=064&fasecoldaModeloId=01051&monto=35900000&codigoDivipola=05001&detalleMontoVehiculo=0&esNuevo=false');
        $data = json_decode($response->getBody()->getContents());
        dd($data);
        return view('tienda.listado_todoriesgo')->with(compact('data'));
    }

    public function printPolicy(Request $request)
    {
        $client = new Client();
        $response = $client->request('GET', $this->endpoint . '/services/ImprimirCotizacionAuto?printBinary=true&JustRecordQuote=false&canal='. $this->canal .'&idCorrelacionConsumidor='.$request->input('idCorrelacionConsumidor').'&tempId='.$request->input('tempId').'&email='.$request->input('email').'&nombreCompleto='.$request->input('nombreCompleto'));
        return response()->json(['data'=> json_decode($response->getBody()->getContents()), 'success' => 'success'], 200);
    }

    public function policyEmission(Request $request)
    {
        $client = new Client();
        $response = $client->request('GET', $this->endpoint . '/services/EmitirCotizacionAuto?canal='. $this->canal .'&idCorrelacionConsumidor='.$request->input('idCorrelacionConsumidor').'&beneficiarioIndicador='.$request->input('beneficiarioIndicador').'&beneficiarioNumero='.$request->input('beneficiarioNumero').'&beneficiarioTipoCorreo='.$request->input('beneficiarioTipoCorreo').'&beneficiarioEmail='.$request->input('beneficiarioEmail').'&beneficiarioConstruccionNr='.$request->input('beneficiarioConstruccionNr').'&beneficiarioDireccion='.$request->input('beneficiarioDireccion').'&beneficiarioCodigoDivipola='.$request->input('beneficiarioCodigoDivipola').'&beneficiarioIdentificacion='.$request->input('beneficiarioIdentificacion').'&beneficiarioTipoIdentificacion='.$request->input('beneficiarioTipoIdentificacion').'&beneficiarioFechaNacimiento='.$request->input('beneficiarioFechaNacimiento').'&beneficiarioGenero='.$request->input('beneficiarioGenero').'&beneficiarioPrimerApellido='.$request->input('beneficiarioPrimerApellido').'&beneficiarioPrimerNombre='.$request->input('beneficiarioPrimerNombre').'&tomadorcelularPersonalIndicador='.$request->input('tomadorcelularPersonalIndicador').'&tomadorcelularPersonalNumero='.$request->input('tomadorcelularPersonalNumero').'&tomadorEmail='.$request->input('tomadorEmail').'&tomadorDireccion='.$request->input('tomadorDireccion').'&tomadorCodigoDivipola='.$request->input('tomadorCodigoDivipola').'&tomadorIdentificacion='.$request->input('tomadorIdentificacion').'&tomadorTipoIdentificacion='.$request->input('tomadorTipoIdentificacion').'&tomadorFechaNacimiento='.$request->input('tomadorFechaNacimiento').'&tomadorGenero='.$request->input('tomadorGenero').'&tomadorPrimerApellido='.$request->input('tomadorPrimerApellido').'&tomadorPrimerNombre='.$request->input('tomadorPrimerNombre').'&aseguradoCelularPersonalIndicador='.$request->input('aseguradoCelularPersonalIndicador').'&aseguradoCelularPersonalNumero='.$request->input('aseguradoCelularPersonalNumero').'&aseguradoEmail='.$request->input('aseguradoEmail').'&aseguradoConstruccionNr='.$request->input('aseguradoConstruccionNr').'&aseguradoDireccion='.$request->input('aseguradoDireccion').'&aseguradoCodigoDivipola='.$request->input('aseguradoCodigoDivipola').'&aseguradoIdentificacion='.$request->input('aseguradoIdentificacion').'&aseguradoTipoIdentificacion='.$request->input('aseguradoTipoIdentificacion').'&aseguradoFechaNacimiento='.$request->input('aseguradoFechaNacimiento').'&aseguradoGenero='.$request->input('aseguradoGenero').'&aseguradoPrimerApellido='.$request->input('aseguradoPrimerApellido').'&aseguradoPrimerNombre='.$request->input('aseguradoPrimerNombre').'&observaciones='.$request->input('observaciones').'&variablesProcesoTempId='.$request->input('variablesProcesoTempId').'&esBeneficiarioAsegurado='.$request->input('esBeneficiarioAsegurado').'&esTomadorAsegurado='.$request->input('esTomadorAsegurado').'&planDePago='.$request->input('planDePago').'&codigoAlizanza='.$request->input('codigoAlizanza').'&codigoRamoAlianza='.$request->input('codigoRamoAlianza').'&codigoPuntoVentaAlianza='.$request->input('codigoPuntoVentaAlianza').'&numeroPropuesta='.$request->input('numeroPropuesta').'&placa='.$request->input('placa').'&numeroMotor='.$request->input('numeroMotor').'&numeroChasis='.$request->input('numeroChasis').'');
        return response()->json(['data'=> json_decode($response->getBody()->getContents()), 'success' => 'success'], 200);
    }

}
