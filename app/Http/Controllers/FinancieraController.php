<?php

namespace App\Http\Controllers;

use App\Models\Financiera;
use Illuminate\Http\Request;

/**
 * Class FinancieraController
 * @package App\Http\Controllers
 */
class FinancieraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $financieras = Financiera::paginate();

        return view('financiera.index', compact('financieras'))
            ->with('i', (request()->input('page', 1) - 1) * $financieras->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $financiera = new Financiera();
        return view('financiera.create', compact('financiera'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Financiera::$rules);

        $financiera = Financiera::create($request->all());

        return redirect()->route('financieras.index')
            ->with('success', 'Financiera created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $financiera = Financiera::find($id);

        return view('financiera.show', compact('financiera'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $financiera = Financiera::find($id);

        return view('financiera.edit', compact('financiera'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Financiera $financiera
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Financiera $financiera)
    {
        request()->validate(Financiera::$rules);

        $financiera->update($request->all());

        return redirect()->route('financieras.index')
            ->with('success', 'Financiera updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $financiera = Financiera::find($id)->delete();

        return redirect()->route('financieras.index')
            ->with('success', 'Financiera deleted successfully');
    }
}
