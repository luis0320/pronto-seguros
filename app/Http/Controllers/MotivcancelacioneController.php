<?php

namespace App\Http\Controllers;

use App\Models\Motivcancelacione;
use Illuminate\Http\Request;

/**
 * Class MotivcancelacioneController
 * @package App\Http\Controllers
 */
class MotivcancelacioneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $motivcancelaciones = Motivcancelacione::paginate();

        return view('motivcancelacione.index', compact('motivcancelaciones'))
            ->with('i', (request()->input('page', 1) - 1) * $motivcancelaciones->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $motivcancelacione = new Motivcancelacione();
        return view('motivcancelacione.create', compact('motivcancelacione'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Motivcancelacione::$rules);

        $motivcancelacione = Motivcancelacione::create($request->all());

        return redirect()->route('motivcancelaciones.index')
            ->with('success', 'Motivcancelacione created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $motivcancelacione = Motivcancelacione::find($id);

        return view('motivcancelacione.show', compact('motivcancelacione'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $motivcancelacione = Motivcancelacione::find($id);

        return view('motivcancelacione.edit', compact('motivcancelacione'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Motivcancelacione $motivcancelacione
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Motivcancelacione $motivcancelacione)
    {
        request()->validate(Motivcancelacione::$rules);

        $motivcancelacione->update($request->all());

        return redirect()->route('motivcancelaciones.index')
            ->with('success', 'Motivcancelacione updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $motivcancelacione = Motivcancelacione::find($id)->delete();

        return redirect()->route('motivcancelaciones.index')
            ->with('success', 'Motivcancelacione deleted successfully');
    }
}
