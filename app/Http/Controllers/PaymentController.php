<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use MP;
use DB;
use App\Conten;
use App\Banner;
use App\Support\Collection;
use Auth;
use Mail;


class PaymentController extends Controller
{

    private $companyIndentifyPrueba = 'SEPVV';
    private $payPrueba = 'https://pre.irecaudocoe.transfiriendo.com:456/IRecaudoCoe/WebForms/Pago/Views/GenerarTransaccion.aspx?input=';
    private $wsdlPrueba = 'https://pre.irecaudocoe.transfiriendo.com:456/Irecaudocoe/WebServices/Public/PublicServices.asmx?wsdl';

    
    private $companyIndentify = 'PSEVALLE';
    private $wsdl = 'https://www.irecaudocoe.transfiriendo.com:444/Irecaudocoe/WebServices/Public/PublicServices.asmx?wsdl';
    private $pay = 'https://www.irecaudocoe.transfiriendo.com:444/IRecaudoCoe/WebForms/Pago/Views/GenerarTransaccion.aspx?input=';

    public function purchase($dataPay, $data)
    {
        $currentDate = date(DATE_ATOM, mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
        $name = $data["Contact"]["FirstName"] . ' ' . $data["Contact"]["FirstName1"] . ' ' . $data["Contact"]["LastName"] . ' ' . $data["Contact"]["LastName1"];
        $reference = $this->registerOrder($currentDate, $dataPay->TotalWithDiscountAmount, $dataPay->InsurancePolicyNumber, $data);
        $data = '<ProcesarReferenciasRq>
            <PersonaDeudora>
                <Nombre>'.$name.'</Nombre>
                <TipoIdentificacion>'. $data["Contact"]["DocumentTypeId"] .'</TipoIdentificacion>
                <NumeroIdentificacion>'. $data["Contact"]["DocumentNumber"] .'</NumeroIdentificacion>
            </PersonaDeudora>
            <IdentificadorCompania>' . $this->companyIndentify . '</IdentificadorCompania>
            <TransaccionId>' . $reference . '</TransaccionId>
            <Referencias>
                <Referencia>
                    <Cabecera>
                        <NumeroReferencia>'. $data["Vehicle"]["NumberPlate"] .' - '.$dataPay->InsurancePolicyNumber.'</NumeroReferencia>
                        <NumeroReferenciaOrigen>'.$dataPay->InsurancePolicyNumber.'</NumeroReferenciaOrigen>
                        <Moneda>COP</Moneda>
                        <ImporteTotal>'.$dataPay->TotalWithDiscountAmount.'</ImporteTotal>
                        <ImporteSubtotal>'.$dataPay->TotalWithDiscountAmount.'</ImporteSubtotal>
                        <ImporteIva>0</ImporteIva>
                        <FechaEmision>'. date('Y-m-d', strtotime($dataPay->FromValidateDate)).'</FechaEmision>
                        <FechaVencimiento>'.date('Y-m-d', strtotime($dataPay->FromValidateDate)).'</FechaVencimiento>
                    </Cabecera>
                </Referencia>
            </Referencias>
            <Extras>
                <Ref2>860.009.578-6</Ref2>
                <Ref4>PRONTO SEGUROS API|'.$dataPay->InsurancePolicyNumber.'</Ref4>
                <Ref5>'. $data["Vehicle"]["NumberPlate"] .'|||51903|'. $data["Contact"]["DocumentNumber"] .'</Ref5>
            </Extras>
        </ProcesarReferenciasRq>';
        try {
            $client = new \SoapClient($this->wsdl, ["trace" => 1]);
            $result = $client->ProcesarReferenciasCoe(['pProcesarReferenciasRq' => $data, 'pClaveControl' => sha1($this->companyIndentify . '1')]);
            return simplexml_load_string($result->ProcesarReferenciasCoeResult)->IdentificadorTransaccion;


        } catch (SoapFault $e) {
            echo $e->getMessage();
        }
    }

    public function registerOrder($payDate, $value, $insurancePolicyNumber,$data)
    {
        $insert_payment = DB::table('purchases')->insertGetId([
            'pay_date' => $payDate,
            'email' => $data["Vehicle"]["NumberPlate"],
            'total_purchase' => $value,
            'insurance_policy_number' => $insurancePolicyNumber
        ]);

        DB::table('purchases')->where('id', $insert_payment)->update(['reference' => 'soat_' . $insert_payment]);
        return 'soat_' . $insert_payment;
    }

    public function multicash(Request $request)
    {
        $data = simplexml_load_string($request->input('multicash'));
        try {
            DB::table('purchases')->where('reference', $data->BoletaPago->NumeroReferencia)
                ->update([
                    'detail' => $request->input('multicash'),
                    'update_date' => $data->BoletaPago->FechaPago,
                    'pay_date' => $data->BoletaPago->FechaPago,
                    'total_purchase' => $data->BoletaPago->ImporteTotal,
                ]);
            return response()->json(['success' => 'success'], 200);
        } catch (Exception $e) {
            return response()->json(['error' => 'fail', 'message' => $e->getMessage()], $e->getCode());
        }
    }


    public function purchasePrueba($dataPay, $data)
    {
        $currentDate = date(DATE_ATOM, mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
        $name = $data["Contact"]["FirstName"] . ' ' . $data["Contact"]["FirstName1"] . ' ' . $data["Contact"]["LastName"] . ' ' . $data["Contact"]["LastName1"];
        $reference = $this->registerOrder($currentDate, $dataPay->TotalWithDiscountAmount, $dataPay->InsurancePolicyNumber, $data);
        $data = '<ProcesarReferenciasRq>
            <PersonaDeudora>
                <Nombre>'.$name.'</Nombre>
                <TipoIdentificacion>'. $data["Contact"]["DocumentTypeId"] .'</TipoIdentificacion>
                <NumeroIdentificacion>'. $data["Contact"]["DocumentNumber"] .'</NumeroIdentificacion>
            </PersonaDeudora>
            <IdentificadorCompania>' . $this->companyIndentifyPrueba . '</IdentificadorCompania>
            <TransaccionId>' . $reference . '</TransaccionId>
            <Referencias>
                <Referencia>
                    <Cabecera>
                        <NumeroReferencia>'. $data["Vehicle"]["NumberPlate"] .' - '.$dataPay->InsurancePolicyNumber.'</NumeroReferencia>
                        <NumeroReferenciaOrigen>'.$dataPay->InsurancePolicyNumber.'</NumeroReferenciaOrigen>
                        <Moneda>COP</Moneda>
                        <ImporteTotal>'.$dataPay->TotalWithDiscountAmount.'</ImporteTotal>
                        <ImporteSubtotal>'.$dataPay->TotalWithDiscountAmount.'</ImporteSubtotal>
                        <ImporteIva>0</ImporteIva>
                        <FechaEmision>'. date('Y-m-d', strtotime($dataPay->FromValidateDate)).'</FechaEmision>
                        <FechaVencimiento>'.date('Y-m-d', strtotime($dataPay->FromValidateDate)).'</FechaVencimiento>
                    </Cabecera>
                </Referencia>
            </Referencias>
            <Extras>
                <Ref2>860.009.578-6</Ref2>
                <Ref4>PRONTO SEGUROS API|'.$dataPay->InsurancePolicyNumber.'</Ref4>
                <Ref5>'. $data["Vehicle"]["NumberPlate"] .'|||51903|'. $data["Contact"]["DocumentNumber"] .'</Ref5>
            </Extras>
        </ProcesarReferenciasRq>';
        try {
            $client = new \SoapClient($this->wsdlPrueba, ["trace" => 1]);
            $result = $client->ProcesarReferenciasCoe(['pProcesarReferenciasRq' => $data, 'pClaveControl' => sha1($this->companyIndentifyPrueba . '1')]);
            return simplexml_load_string($result->ProcesarReferenciasCoeResult)->IdentificadorTransaccion;


        } catch (SoapFault $e) {
            echo $e->getMessage();
        }
    }
}
