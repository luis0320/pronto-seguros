<?php

namespace App\Http\Controllers;

use App\Models\Activosoattarifa;
use Illuminate\Http\Request;

/**
 * Class ActivosoattarifaController
 * @package App\Http\Controllers
 */
class ActivosoattarifaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activosoattarifas = Activosoattarifa::paginate();

        return view('activosoattarifa.index', compact('activosoattarifas'))
            ->with('i', (request()->input('page', 1) - 1) * $activosoattarifas->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activosoattarifa = new Activosoattarifa();
        return view('activosoattarifa.create', compact('activosoattarifa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Activosoattarifa::$rules);

        $activosoattarifa = Activosoattarifa::create($request->all());

        return redirect()->route('activosoattarifas.index')
            ->with('success', 'Activosoattarifa created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activosoattarifa = Activosoattarifa::find($id);

        return view('activosoattarifa.show', compact('activosoattarifa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activosoattarifa = Activosoattarifa::find($id);

        return view('activosoattarifa.edit', compact('activosoattarifa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Activosoattarifa $activosoattarifa
     * @return \Illuminate\Http\Response
     */

     
    public function update(Request $request, Activosoattarifa $activosoattarifa)
    {
        request()->validate(Activosoattarifa::$rules);

        $activosoattarifa->update($request->all());

        return redirect()->route('activosoattarifas.index')
            ->with('success', 'Activosoattarifa updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $activosoattarifa = Activosoattarifa::find($id)->delete();

        return redirect()->route('activosoattarifas.index')
            ->with('success', 'Activosoattarifa deleted successfully');
    }
}
