<?php

namespace App\Http\Controllers;

use App\Models\Observacione;
use App\Models\Estado;
use App\User;
use Illuminate\Http\Request;

/**
 * Class ObservacioneController
 * @package App\Http\Controllers
 */
class ObservacioneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $observaciones = Observacione::paginate();
        

        return view('observacione.index', compact('observaciones'))
            ->with('i', (request()->input('page', 1) - 1) * $observaciones->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $observacione = new Observacione();
        return view('observacione.create', compact('observacione'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Observacione::$rules);

        $observacione = Observacione::create($request->all());

        return redirect()->route('scancelaciones.index')
            ->with('success', 'Observacione created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $observacione = Observacione::find($id);

        return view('observacione.show', compact('observacione'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $observacione = Observacione::find($id);

        return view('observacione.edit', compact('observacione'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Observacione $observacione
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Observacione $observacione)
    {
        request()->validate(Observacione::$rules);

        $observacione->update($request->all());

        return redirect()->route('observaciones.index')
            ->with('success', 'Observacione updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $observacione = Observacione::find($id)->delete();

        return redirect()->route('observaciones.index')
            ->with('success', 'Observacione deleted successfully');
    }
}
