<?php

require_once("lib/nusoap.php");



//definir el wsdl
$wsdl="https://tmms.axacolpatria.co:3275/AXAColpatria/GestionPolizas/CotizacionPolizaAuto/GestionPolizaAuto.svc?Wsdl";
$cliente=new nusoap_client($wsdl,true);
?>
<pre>
<?php var_dump($cliente); ?>
</pre>
<?php
 $params = [
            'HEADER' => [
                'canal' =>2,//opcional
                'idCorrelacionConsumidor' => 'cac9b3e2-d5d1-4d82-971e-79b7477879a7',//opcional
                'idTransaccion' => 1,//opcional
                'peticionFecha' => '2020-09-18T19:23:12.217Z',//opcional
                'usuario'=> 'Carlos'//opcional
            ],
            'BODY' => [
                'cotizacion' => [
					'subproducto' => [
						'producto' => [
							'identificacionProducto' => [
                                'idProducto'=> 203010//opcional
                            ]
						],
						
					],
					'cliente' => [
                        'persona'=>[
                            'identificacion'=>[
                                'identificacion'=>80057038,//requerido
                                'tipoIdentificacion'=>1//requerido
                            ],
                            'estadoCivil'=>2,//opcional
                            'fechaNacmiento'=>'05/04/1980',//requerido
                            'genero'=> 'M',//requerido
                            'primerApellido'=>'Reyes',//opcional
                            'primerNombre'=>'Victor'//opcional
                        ],
												
                    ],
                    'distribuidor'=>[
                        'empresa' => [
                            'identificacion'=>102030,//opcional
                            'datosBasicos' => [
                                'razonSocial'=>1//opcional
                            ]                           
                        ],
                        "codigoDistribuidor"=>101010,//requerido
                        

                    ],
                    'contrato'=>[
                        'tipodeContrato'=>'1',//opcional
                        'cobertura'=>[
                        'limite'=>[
                            'limite'=>'',//opcional
                            ]     
                        ],
                        'vehiculo'=>[
                            'Placa'=>'zyx 481',//requerido
                            'NumeroMotor'=>40151515,//opcional
                            'NumeroChasis'=>202020,//opcional
                            'Linea'=>'Sedan',//opcional
                            'Modelo'=>'2014',//requerido
                            'Color'=>7,//opcional
                            'Servicio'=>15,//opcional
                            'FasecoldaMarcaID'=>'xyz123',//requerido
                            'FasecoldaModeloID'=>'abc123',//requerido
                            'Monto'=>[
                                'moneda'=>'pesos', //opcional
                                'monto'=>30000000//requerido
                            ]
                        ]
                    ],
                    
					'CodigoDivipola' => 05001,//requerido
					'IdAcuerdoNegocio' => 17,//requerido
					'ValidacionEventos' => true,//opcional
					'IdZonaClasificacion' => 4,//requerido
					'DetalleMontoVehiculo' => 0,//opcional
					'EsNuevo' => false,//opcional
                ],
            ]
           
        ];

$respuesta= $cliente->call('CotizarPolizaAuto',$params);


?>
