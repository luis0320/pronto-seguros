<?php

namespace App\Http\Controllers;

use App\Models\CotizacionSoat;
use Illuminate\Http\Request;

/**
 * Class CotizacionSoatController
 * @package App\Http\Controllers
 */
class CotizacionSoatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cotizacionSoats = CotizacionSoat::paginate();

        return view('cotizacion-soat.index', compact('cotizacionSoats'))
            ->with('i', (request()->input('page', 1) - 1) * $cotizacionSoats->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cotizacionSoat = new CotizacionSoat();
        return view('cotizacion-soat.create', compact('cotizacionSoat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(CotizacionSoat::$rules);

        $cotizacionSoat = CotizacionSoat::create($request->all());

        return redirect()->route('cotizacion-soats.index')
            ->with('success', 'CotizacionSoat created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cotizacionSoat = CotizacionSoat::find($id);

        return view('cotizacion-soat.show', compact('cotizacionSoat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cotizacionSoat = CotizacionSoat::find($id);

        return view('cotizacion-soat.edit', compact('cotizacionSoat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  CotizacionSoat $cotizacionSoat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CotizacionSoat $cotizacionSoat)
    {
        request()->validate(CotizacionSoat::$rules);

        $cotizacionSoat->update($request->all());

        return redirect()->route('cotizacion-soats.index')
            ->with('success', 'CotizacionSoat updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $cotizacionSoat = CotizacionSoat::find($id)->delete();

        return redirect()->route('cotizacion-soats.index')
            ->with('success', 'CotizacionSoat deleted successfully');
    }
}
