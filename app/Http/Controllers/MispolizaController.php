<?php

namespace App\Http\Controllers;

use App\Models\Mispoliza;
use DB;
use MP;
use Illuminate\Http\Request;

/**
 * Class MispolizaController
 * @package App\Http\Controllers
 */
class MispolizaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $perfil = auth()->user()->perfil;
        $usuario = auth()->user()->cliente_id;
      

        $cedula = DB::table('clientes')
        ->select('identificacion','id')
        ->where('id',$usuario)->first();

        
        //dd($cedula->identificacion);
        
        $mispolizas = Mispoliza::where('NitCC',($cedula->identificacion))
        ->orderBy('id','DESC')
        ->paginate();
        

        return view('mispoliza.index', compact('mispolizas','cedula'))
            ->with('i', (request()->input('page', 1) - 1) * $mispolizas->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mispoliza = new Mispoliza();
        return view('mispoliza.create', compact('mispoliza'));
    }

 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Mispoliza::$rules);

        $mispoliza = Mispoliza::create($request->all());

        return redirect()->route('mispolizas.index')
            ->with('success', 'Mispoliza created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $perfil = auth()->user()->perfil;
        $usuario = auth()->user()->cliente_id;

        $mispoliza = Mispoliza::find($id);

        $cedula = DB::table('clientes')
        ->select('identificacion','id')
        ->where('id',$usuario)->first();

        return view('mispoliza.show', compact('mispoliza','cedula'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mispoliza = Mispoliza::find($id);

        return view('mispoliza.edit', compact('mispoliza'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Mispoliza $mispoliza
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mispoliza $mispoliza)
    {
        request()->validate(Mispoliza::$rules);

        $mispoliza->update($request->all());

        return redirect()->route('mispolizas.index')
            ->with('success', 'Mispoliza updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $mispoliza = Mispoliza::find($id)->delete();

        return redirect()->route('mispolizas.index')
            ->with('success', 'Mispoliza deleted successfully');
    }
}
