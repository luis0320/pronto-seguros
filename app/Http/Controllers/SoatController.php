<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use MP;
use DB;
use App\Conten;
use App\Banner;
use App\Support\Collection;
use Auth;
use Mail;
use GuzzleHttp\Client;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SecurityController;
use Opis\Closure\SecurityException;


class SoatController extends Controller
{
    // DEV
    private $endpointPrueba = 'https://pre.transfiriendo.com:448/SOATNETSEAPI';
    private $method_payPrueba = 'https://pre.irecaudocoe.transfiriendo.com:456/IRecaudoCoe/WebForms/Pago/Views/GenerarTransaccion.aspx?input=';

    // PROD
    private $endpoint = 'https://www.esbus.transfiriendo.com/SOATNETSEAPI';
    private $method_pay = 'https://www.irecaudocoe.transfiriendo.com:444/IRecaudoCoe/WebForms/Pago/Views/GenerarTransaccion.aspx?input=';

    private $client = 'iSoatAPIV2';
    private $authorization = 'bearer';
    private $data_vehiculo = array();
    private $data_descuento = array();

    public function licensePlate(Request $request)
    {

        $security = new SecurityController();

        $token = $security->getToken();
        //dd('entra');
        $currentDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
        $futureDate = date('Y-m-d', mktime(23, 59, 59, date("m"), date("d"), date("Y") + 1));
        if ($request->isMethod('get')) {
            return redirect('/soat');
        }
        $client = new Client();

        $response = $client->request('GET', $this->endpoint . '/api/v3/Vehicle/GetVehicle?NumberPlate=' . $request->input('plate'), [
            'headers' => [
                'Client' => [$this->client],
                'Authorization' => [$this->authorization . ' ' . $token],
            ]
        ]);

        // modificacion json_response
        $res_decode = json_decode($response->getBody()->getContents());
        $dataCar = $res_decode->Data;
        if ($dataCar == null) {
            $message = 'Algo sucedio, intenta de nuevo';
            return view('tienda.soat_error')->with(compact('message'));
        }


        if($dataCar->ChasisNumber == null){
            $chasis = '0'; 
        }else{
            $chasis = ''.$dataCar->ChasisNumber;
        }


        //dd('/api/V3/InsurancePolicy/CalculateExistInsurancePolicy?NumberPlate='.$request->input('plate').'&MotorNumber='.$dataCar->MotorNumber.'&ChasisNumber='.$dataCar->ChasisNumber.'&Vin='. $dataCar->Vin.'&CountryId='.$dataCar->CountryId.'&FromValidateDate='.$currentDate.'&DueValidateDate='.$futureDate.'&ServiceTypeId='.$dataCar->ServiceTypeId.'&Operation=1&Locked=False');

        $responsePolicy = $client->request('GET',
            $this->endpoint.'/api/V3/InsurancePolicy/CalculateExistInsurancePolicy?NumberPlate='.$request->input('plate').'&MotorNumber='.$dataCar->MotorNumber.'&ChasisNumber='.$chasis.'&Vin='. $dataCar->Vin.'&CountryId='.$dataCar->CountryId.'&FromValidateDate='.$currentDate.'&DueValidateDate='.$futureDate.'&ServiceTypeId='.$dataCar->ServiceTypeId.'&Operation=1&Locked=False', [
                'headers' => [
                    'Client' => [$this->client],
                    'Authorization' => [$this->authorization . ' ' . $token],
                ]
            ]);

        
        
        $dataPolicy = json_decode($responsePolicy->getBody()->getContents())->Data;
        $data = [
            'car' => $dataCar,
            'policy' => ['valid_less_two_months' => false, 'valid_two_months' => false, 'data' => $dataPolicy]
        ];

        //dd($dataPolicy);
        //dd($data['car']);
        //dd($data['car']->NewTariff);

        //Validar si aplica tarifa de descuento
            
        $validateEngine = 1;
        if ($dataCar->FuelTypeId == 5){
            $validateEngine = 2;
        }
        
        $responseTariff = $client->request('GET',
                $this->endpoint .'/api/v3/Vehicle/GetVehicleTariff?VehicleClassId='.$dataCar->VehicleClassId.'&VehicleClassMinistryId='.$dataCar->VehicleClassMinistryId.'&VehicleYear='.$dataCar->VehicleYear.'&CylinderCapacity='.$dataCar->CylinderCapacity.'&PassengerCapacity='.$dataCar->PassengerCapacity.'&LoadCapacity='.$dataCar->LoadCapacity.'&EnginePowerTypeId='.$validateEngine.'&DocumentType='.$request->input('documenttypeid').'&DocumentNumber='.$request->input('DocumentNumber').'&NumberPlate='.$request->input('plate').'&FuelTypeld='.$request->input('FuelTypeld').'', [
                    'headers' => [
                        'Client' => [$this->client],
                        'Authorization' => [$this->authorization . ' ' . $token],
                    ]
                ]);

        $resTatiffDecode = json_decode($responseTariff->getBody()->getContents())->Data;

        //Fin de la validacion
        
        //dd($resTatiffDecode->TotalWithDiscountAmount);
        //dd($resTatiffDecode->ElectricDiscountFormatted);
        //dd($resTatiffDecode);
       

        if ($dataPolicy->Message && $dataPolicy->Message != '') {
            $date = date('Y-m-d');
            $toDay = new \DateTime($date);
            $dateCalculate = new \DateTime($dataPolicy->FromValidateDate);
            $diff = $toDay->diff($dateCalculate);
            if (($diff->y <= 1 && $diff->m > 1) || $diff->y >= 1) {
                $data['policy']['valid_two_months'] = true;
            } else {
                $data['policy']['valid_less_two_months'] = true;
            }
        }

        //dd($data['car']->GetVehicleTariff->DiscountAmount);

        $tienedescuento = $data['car']->NewTariff;
        $dueValidateDate = $dataPolicy->FromValidateDate;
        $tipoDoc = DB::table('tipo_documento')->select(['*'])->get();
        $tipoVeh = DB::table('tipos_vehiculos')->select(['*'])->get();
        $ciudades = DB::table('ciudades')->select(['*'])->where('municipio', '!=', '')->OrderBy('municipio')->get();
        $rut = DB::table('tipo_rut')->select(['*'])->get();
        $ciiu = DB::table('tipo_ciiu')->select(['*'])->get();
        $direccion = DB::table('direcciones')->select(['*'])->get();
        $tipo_combustible = DB::table('fuel_types')->select(['*'])->where('id', $dataCar->FuelTypeId)->pluck('descripcion')->first();
        $rut_nacional = $rut;
        $ciiu_nacional = $ciiu;
       

        //dd($data['car']->NewTariff);
        //dd($data['car']->NewTariff->TotalFormatted);
        //dd($data['car']->NewTariff->TariffCode);
        
        
        if(empty($data['car']->NewTariff)){
            $totalprecio = 0;
            $totaldescuento = 0;
            $totaldescontado = 0;
            $errorrunt = 0;
        }else{
            $totalprecio = $data['car']->NewTariff->TotalFormatted;
            $totaldescuento = $resTatiffDecode->ElectricDiscountFormatted;
            $totaldescontado = $resTatiffDecode->TotalWithDiscountAmountFormatted;
            $errorrunt = 1;
        }


        // se guarda cotizacion del soat;
        $insert_coti_soat = DB::table('cotizacionsoats')->insertGetId([
            'placa' => $data['car']->NumberPlate,
            'fecha' => $dataPolicy->FromValidateDate,
            'total' => $totalprecio,
            'marca' => $data['car']->BrandName,
            'linea' => $data['car']->VehicleLineDescription,
            'modelo' => $data['car']->VehicleYear,
            'cedula' => $request->input('DocumentNumber'), 
            'correo' => $request->input('email'), 
            'descuento' => $totaldescuento, 
            'preciodes' => $totaldescontado,  
            'celular' => $request->input('Celular'),                   
            'fecha_creacion' => date('Y-m-d'),
        ]);

        $last_cost = DB::table('cotizacionsoats')->select(['id'])->get()->last();
        // FIN se guarda cotizacion del soat;

        $activotarifa = DB::table('activosoattarifas')->select(['activ'])->where('tarifa', $resTatiffDecode->TariffCode)->get()->last();

        return view('tienda.soat_licenseplate')->with(compact('data', 'dueValidateDate', 'tipoDoc', 'tipoVeh', 'ciudades', 'rut', 'ciiu', 'direccion', 'rut_nacional', 'ciiu_nacional', 'tipo_combustible', 'tienedescuento','resTatiffDecode','errorrunt','last_cost','activotarifa'));
    }

    public function operationCar(Request $request)
    {
        $security = new SecurityController();
        $token = $security->getToken();
        if ($request->input('operation') == 2) {
            $codeRunt = DB::table('vehicleministry')->where('vehicleclassministryid', $request->input('code'))->pluck('vehicleministrycoderunt')->first();
            $client = new Client();
            $response = $client->request('GET',
                $this->endpoint . '/api/v3/Vehicle/GetNationalOperationCardData?NationalOperationCardId=2&VehicleClassMinistryCode=' . $codeRunt . '&NumberPlate=' . $request->input('plate'), [
                    'headers' => [
                        'Client' => [$this->client],
                        'Authorization' => [$this->authorization . ' ' . $token],
                    ]
                ]);
            $res_decode = json_decode($response->getBody()->getContents());
            $data = $res_decode->Data;
            $responseTariff = $client->request('GET',
                $this->endpoint . '/api/v3/Vehicle/GetVehicleTariff?VehicleClassId=' . $data->VehicleClassId . '&VehicleClassMinistryId=' . $data->VehicleClassMinistryId . '&VehicleYear=' . $request->input('year') . '&CylinderCapacity=' . $request->input('cylinder') . '&PassengerCapacity=' . $request->input('passenger') . '&LoadCapacity=' . $request->input('load') . '', [
                    'headers' => [
                        'Client' => [$this->client],
                        'Authorization' => [$this->authorization . ' ' . $token],
                    ]
                ]);
            $resTatiffDecode = json_decode($responseTariff->getBody()->getContents());

               

            if ($resTatiffDecode->Data) {
                $dataTariff = $resTatiffDecode->Data;
                $total = $dataTariff->TotalFormatted;
                $totaldescuento = $dataTariff->TotalWithDiscountAmountFormatted;
                $descuento = $dataTariff->DiscountAmountFormatted;
                $VehicleClassMinistryId = $data->VehicleClassMinistryId;
                $VehicleClassId = $data->VehicleClassId;
                $returnHTML = view('tienda.soat_pricenew', compact('total', 'totaldescuento', 'descuento'))->render();
                $variables = view('tienda.soat_variables', compact('VehicleClassMinistryId', 'VehicleClassId'))->render();
                return response()->json(['success' => 'success', 'html' => $returnHTML, 'variables' => $variables], 200);
            }
            if ($resTatiffDecode->Data == null) {

                $message = $resTatiffDecode->Message;
                return response()->json(['error' => 'error', 'html' => $message], 404);
            }
        }

        $total = $request->input('total');
        $totaldescuento = $request->input('totaldescuento');
        $descuento = $request->input('descuento');
        $returnHTML = view('tienda.soat_pricenew', compact('total', 'totaldescuento', 'descuento'))->render();
        return response()->json(['success' => 'success', 'html' => $returnHTML], 200);

         //dd($data['car']->GetVehicleTariff->DiscountAmount);
    }

    public function typeDocument(Request $request)
    {
        $typeDocument = $request->documentType;
        $returnHTML = view('tienda.soat_typedocument', compact('typeDocument'))->render();
        return response()->json(['success' => 'success', 'html' => $returnHTML], 200);
    }

    public function newInsurancePolicyBudget(Request $request)
    {
        $security = new SecurityController();
        $token = $security->getToken();
        $cilindraje = $request->input('cylindercapacity');
        if ($request->input('cilindraje')) {
            $cilindraje = $request->input('cilindraje');
        }
        $name = $request->input('dir1') . ' ' . $request->input('dir2') . ' ' . $request->input('dir3') . ' ' . $request->input('dir4') . ' ' . $request->input('dir5');
        $idstate = DB::table('ciudades')->select(['stateId'])->where('id', $request->input('cityid'))->first()->stateId;
        $date = date('Y-m-d', strtotime($request->input('dueValidateDate')));
        $dateSum = $date;
        $documentNumber = $request->input('documentnumber');
        $lastName = $request->input('lastname');
        $firstName = $request->input('firstname');
        $firstName1 = $request->input('firstname1');
        $lastName1 = $request->input('lastname1');
        if ($request->input('documenttypeid') == 3) {
            $documentNumber = $request->input('documentnumber') . $request->input('digVerificacion');
            $lastName = $request->input('firstname');
            $firstName = '';
            $firstName1 = '';
            $lastName1 = '';
        }
        $body = [
            'Contact' => [
                'Address' => [
                    'CityId' => $request->input('cityid'),
                    'Name' => $name,
                    'StateId' => $idstate,
                ],
                'CiiuId' => $request->input('ciiuid'),
                'Cellular' => $request->input('phone'),
                'DocumentNumber' => $documentNumber,
                'DocumentTypeId' => $request->input('documenttypeid'),
                'Email' => $request->input('email'),
                'FirstName' => $firstName,
                'FirstName1' => $firstName1,
                'LastName' => $lastName,
                'LastName1' => $lastName1,
                'Phone' => $request->input('phone'),
            ],
            'FromValidateDate' => $dateSum,
            'RegimenTypeId' => $request->input('regimentypeid'),
            'Rutid' => $request->input('rutid'),
            'SendPolicy' => [
                'Address' => $name,
                'Cellular' => $request->input('phone'),
                'CityId' => $request->input('cityid'),
                'Email' => $request->input('email'),
            ],
            'SystemSource' => 10,
            'ChangeEmisionType' => 1,
            'Vehicle' => [
                'BrandId' => $request->input('brandid'),
                'ChasisNumber' => $request->input('chasisnumber'),
                'CylinderCapacity' => $cilindraje,
                'LoadCapacity' => $request->input('loadcapacity'),
                'MotorNumber' => $request->input('motornumber'),
                'NumberPlate' => $request->input('numberplate'),
                'PassengerCapacity' => $request->input('passengercapacity'),
                'ServiceTypeId' => $request->input('servicetypeid'),
                'VIN' => $request->input('vin'),
                'VehicleClassId' => $request->input('vehicleclassid'),
                'VehicleClassMinistryId' => $request->input('vehicleclassministryid'),
                'VehicleLineDescription' => $request->input('vehiclelinedescription'),
                'VehicleLineId' => $request->input('vehiclelineid'),
                'VehicleYear' => $request->input('vehicleyear'),
                'FuelTypeId' => $request->input('fueltypeid'),
                'enginePowerTypeId' => $request->input('fueltypeid'),
                'vehiclebodytypeid' => $request->input('vehiclebodytypeid'),
            ]
        ];

        //dd($body);

        $client = new Client();


        $response = $client->request('POST',
            $this->endpoint . '/api/v3/InsurancePolicy/NewInsurancePolicyBudget', [
                'body' => json_encode($body),
                'headers' => [
                    'Client' => [$this->client],
                    'Authorization' => [$this->authorization . ' ' . $token],
                    'Content-Type' => 'application/json',
                ]
            ]);
            
        //dd($response);   
            
        $res_decode = json_decode($response->getBody()->getContents());
        //dd($res_decode); 

        $data = $res_decode->Data;

        //dd($data); 

        if ($data == null) {
            $message = $res_decode->Message;
            return view('tienda.soat_error')->with(compact('message'));
        }

        // se guarda codigo referido cotizacion del soat;  
        DB::table('cotizacionsoats')->where('id', $request->input('codigorefe'))
                ->update([
                    'codigoreferido' => $request->input('last_cost'),                
                ]);
        // FIN se guarda cotizacion del soat;



        $pay = new PaymentController();
        
        $urlpayment = $pay->purchase($data, $body);
        return redirect()->away($this->method_pay . $urlpayment);
    }

    public function userData(Request $request)
    {
        $security = new SecurityController();
        $token = $security->getToken();
        $client = new Client();
        $response = $client->request('GET', $this->endpoint . '/api/v3/Contact/GetContact?DocumentType=' . $request->input('documentType') . '&DocumentNumber=' . $request->input('documentNumber'), [
            'headers' => [
                'Client' => [$this->client],
                'Authorization' => [$this->authorization . ' ' . $token],
            ]
        ]);
        $res_decode = json_decode($response->getBody()->getContents());
        $data = $res_decode->Data;
        $returnHTML = view('tienda.soat_userdata', compact('data'))->render();
        return response()->json([
            'ok' => true,
            'html' => $returnHTML
        ]);
        $returnHTML = view('tienda.soat_userdata', compact('data'));
        return view('tienda.soat_userdata', compact('data'));
    }


    public function licensePlateprueba(Request $request)
    {

        $security = new SecurityController();

        $token = $security->getTokenPrueba();
        //dd('entra');
        $currentDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
        $futureDate = date('Y-m-d', mktime(23, 59, 59, date("m"), date("d"), date("Y") + 1));
        if ($request->isMethod('get')) {
            return redirect('/soat');
        }
        $client = new Client();

        $response = $client->request('GET', $this->endpointPrueba . '/api/v3/Vehicle/GetVehicle?NumberPlate=' . $request->input('plate'), [
            'headers' => [
                'Client' => [$this->client],
                'Authorization' => [$this->authorization . ' ' . $token],
            ]
        ]);

        // modificacion json_response
        $res_decode = json_decode($response->getBody()->getContents());
        $dataCar = $res_decode->Data;
        if ($dataCar == null) {
            $message = 'Algo sucedio, intenta de nuevo';
            return view('tienda.soat_error')->with(compact('message'));
        }


        if($dataCar->ChasisNumber == null){
            $chasis = '0'; 
        }else{
            $chasis = ''.$dataCar->ChasisNumber;
        }


        //dd('/api/V3/InsurancePolicy/CalculateExistInsurancePolicy?NumberPlate='.$request->input('plate').'&MotorNumber='.$dataCar->MotorNumber.'&ChasisNumber='.$dataCar->ChasisNumber.'&Vin='. $dataCar->Vin.'&CountryId='.$dataCar->CountryId.'&FromValidateDate='.$currentDate.'&DueValidateDate='.$futureDate.'&ServiceTypeId='.$dataCar->ServiceTypeId.'&Operation=1&Locked=False');

        $responsePolicy = $client->request('GET',
            $this->endpointPrueba.'/api/V3/InsurancePolicy/CalculateExistInsurancePolicy?NumberPlate='.$request->input('plate').'&MotorNumber='.$dataCar->MotorNumber.'&ChasisNumber='.$chasis.'&Vin='. $dataCar->Vin.'&CountryId='.$dataCar->CountryId.'&FromValidateDate='.$currentDate.'&DueValidateDate='.$futureDate.'&ServiceTypeId='.$dataCar->ServiceTypeId.'&Operation=1&Locked=False', [
                'headers' => [
                    'Client' => [$this->client],
                    'Authorization' => [$this->authorization . ' ' . $token],
                ]
            ]);

        
        
        $dataPolicy = json_decode($responsePolicy->getBody()->getContents())->Data;
        $data = [
            'car' => $dataCar,
            'policy' => ['valid_less_two_months' => false, 'valid_two_months' => false, 'data' => $dataPolicy]
        ];

        //dd($dataPolicy);
        //dd($data['car']);
        //dd($data['car']->NewTariff);

        //Validar si aplica tarifa de descuento
            
        $validateEngine = 1;
        if ($dataCar->FuelTypeId == 5){
            $validateEngine = 2;
        }
        
        $responseTariff = $client->request('GET',
                $this->endpoint .'/api/v3/Vehicle/GetVehicleTariff?VehicleClassId='.$dataCar->VehicleClassId.'&VehicleClassMinistryId='.$dataCar->VehicleClassMinistryId.'&VehicleYear='.$dataCar->VehicleYear.'&CylinderCapacity='.$dataCar->CylinderCapacity.'&PassengerCapacity='.$dataCar->PassengerCapacity.'&LoadCapacity='.$dataCar->LoadCapacity.'&EnginePowerTypeId='.$validateEngine.'&DocumentType='.$request->input('documenttypeid').'&DocumentNumber='.$request->input('DocumentNumber').'&NumberPlate='.$request->input('plate').'&FuelTypeld='.$request->input('FuelTypeld').'', [
                    'headers' => [
                        'Client' => [$this->client],
                        'Authorization' => [$this->authorization . ' ' . $token],
                    ]
                ]);

        $resTatiffDecode = json_decode($responseTariff->getBody()->getContents())->Data;

        //Fin de la validacion
        
        //dd($resTatiffDecode->TotalWithDiscountAmount);
        //dd($resTatiffDecode->ElectricDiscountFormatted);
        //dd($resTatiffDecode);
       

        if ($dataPolicy->Message && $dataPolicy->Message != '') {
            $date = date('Y-m-d');
            $toDay = new \DateTime($date);
            $dateCalculate = new \DateTime($dataPolicy->FromValidateDate);
            $diff = $toDay->diff($dateCalculate);
            if (($diff->y <= 1 && $diff->m > 1) || $diff->y >= 1) {
                $data['policy']['valid_two_months'] = true;
            } else {
                $data['policy']['valid_less_two_months'] = true;
            }
        }

        //dd($data['car']->GetVehicleTariff->DiscountAmount);

        $tienedescuento = $data['car']->NewTariff;
        $dueValidateDate = $dataPolicy->FromValidateDate;
        $tipoDoc = DB::table('tipo_documento')->select(['*'])->get();
        $tipoVeh = DB::table('tipos_vehiculos')->select(['*'])->get();
        $ciudades = DB::table('ciudades')->select(['*'])->where('municipio', '!=', '')->OrderBy('municipio')->get();
        $rut = DB::table('tipo_rut')->select(['*'])->get();
        $ciiu = DB::table('tipo_ciiu')->select(['*'])->get();
        $direccion = DB::table('direcciones')->select(['*'])->get();
        $tipo_combustible = DB::table('fuel_types')->select(['*'])->where('id', $dataCar->FuelTypeId)->pluck('descripcion')->first();
        $rut_nacional = $rut;
        $ciiu_nacional = $ciiu;
       

        //dd($data['car']->NewTariff);
        //dd($data['car']->NewTariff->TotalFormatted);
        //dd($data['car']->NewTariff->TariffCode);
        
        
        if(empty($data['car']->NewTariff)){
            $totalprecio = 0;
            $totaldescuento = 0;
            $totaldescontado = 0;
            $errorrunt = 0;
        }else{
            $totalprecio = $data['car']->NewTariff->TotalFormatted;
            $totaldescuento = $resTatiffDecode->ElectricDiscountFormatted;
            $totaldescontado = $resTatiffDecode->TotalWithDiscountAmountFormatted;
            $errorrunt = 1;
        }


        // se guarda cotizacion del soat;
        $insert_coti_soat = DB::table('cotizacionsoats')->insertGetId([
            'placa' => $data['car']->NumberPlate,
            'fecha' => $dataPolicy->FromValidateDate,
            'total' => $totalprecio,
            'marca' => $data['car']->BrandName,
            'linea' => $data['car']->VehicleLineDescription,
            'modelo' => $data['car']->VehicleYear,
            'cedula' => $request->input('DocumentNumber'), 
            'correo' => $request->input('email'), 
            'descuento' => $totaldescuento, 
            'preciodes' => $totaldescontado,  
            'celular' => $request->input('Celular'),                   
            'fecha_creacion' => date('Y-m-d'),
        ]);

        $last_cost = DB::table('cotizacionsoats')->select(['*'])->get()->last();
        // FIN se guarda cotizacion del soat;

        $activotarifa = DB::table('activosoattarifas')->select(['activ'])->where('tarifa', $resTatiffDecode->TariffCode)->get()->last();

        return view('tienda.soat_licenseplateprueba')->with(compact('data', 'dueValidateDate', 'tipoDoc', 'tipoVeh', 'ciudades', 'rut', 'ciiu', 'direccion', 'rut_nacional', 'ciiu_nacional', 'tipo_combustible', 'tienedescuento','resTatiffDecode','errorrunt','last_cost','activotarifa'));
    }

    public function newInsurancePolicyBudgetPrueba(Request $request)
    {
        $security = new SecurityController();
        $token = $security->getTokenPrueba();
        $cilindraje = $request->input('cylindercapacity');
        if ($request->input('cilindraje')) {
            $cilindraje = $request->input('cilindraje');
        }
        $name = $request->input('dir1') . ' ' . $request->input('dir2') . ' ' . $request->input('dir3') . ' ' . $request->input('dir4') . ' ' . $request->input('dir5');
        $idstate = DB::table('ciudades')->select(['stateId'])->where('id', $request->input('cityid'))->first()->stateId;
        $date = date('Y-m-d', strtotime($request->input('dueValidateDate')));
        $dateSum = $date;
        $documentNumber = $request->input('documentnumber');
        $lastName = $request->input('lastname');
        $firstName = $request->input('firstname');
        $firstName1 = $request->input('firstname1');
        $lastName1 = $request->input('lastname1');
        if ($request->input('documenttypeid') == 3) {
            $documentNumber = $request->input('documentnumber') . $request->input('digVerificacion');
            $lastName = $request->input('firstname');
            $firstName = '';
            $firstName1 = '';
            $lastName1 = '';
        }
        $body = [
            'Contact' => [
                'Address' => [
                    'CityId' => $request->input('cityid'),
                    'Name' => $name,
                    'StateId' => $idstate,
                ],
                'CiiuId' => $request->input('ciiuid'),
                'Cellular' => $request->input('phone'),
                'DocumentNumber' => $documentNumber,
                'DocumentTypeId' => $request->input('documenttypeid'),
                'Email' => $request->input('email'),
                'FirstName' => $firstName,
                'FirstName1' => $firstName1,
                'LastName' => $lastName,
                'LastName1' => $lastName1,
                'Phone' => $request->input('phone'),
            ],
            'FromValidateDate' => $dateSum,
            'RegimenTypeId' => $request->input('regimentypeid'),
            'Rutid' => $request->input('rutid'),
            'SendPolicy' => [
                'Address' => $name,
                'Cellular' => $request->input('phone'),
                'CityId' => $request->input('cityid'),
                'Email' => $request->input('email'),
            ],
            'SystemSource' => 10,
            'ChangeEmisionType' => 1,
            'Vehicle' => [
                'BrandId' => $request->input('brandid'),
                'ChasisNumber' => $request->input('chasisnumber'),
                'CylinderCapacity' => $cilindraje,
                'LoadCapacity' => $request->input('loadcapacity'),
                'MotorNumber' => $request->input('motornumber'),
                'NumberPlate' => $request->input('numberplate'),
                'PassengerCapacity' => $request->input('passengercapacity'),
                'ServiceTypeId' => $request->input('servicetypeid'),
                'VIN' => $request->input('vin'),
                'VehicleClassId' => $request->input('vehicleclassid'),
                'VehicleClassMinistryId' => $request->input('vehicleclassministryid'),
                'VehicleLineDescription' => $request->input('vehiclelinedescription'),
                'VehicleLineId' => $request->input('vehiclelineid'),
                'VehicleYear' => $request->input('vehicleyear'),
                'FuelTypeId' => $request->input('fueltypeid'),
                'enginePowerTypeId' => $request->input('fueltypeid'),
                'vehiclebodytypeid' => $request->input('vehiclebodytypeid'),
            ]
        ];

        //dd($body);

        $client = new Client();


        $response = $client->request('POST',
            $this->endpointPrueba. '/api/v3/InsurancePolicy/NewInsurancePolicyBudget', [
                'body' => json_encode($body),
                'headers' => [
                    'Client' => [$this->client],
                    'Authorization' => [$this->authorization . ' ' . $token],
                    'Content-Type' => 'application/json',
                ]
            ]);
            
        //dd($response);   
            
        $res_decode = json_decode($response->getBody()->getContents());
        //dd($res_decode); 

        $data = $res_decode->Data;

        //dd($data); 

        if ($data == null) {
            $message = $res_decode->Message;
            return view('tienda.soat_error')->with(compact('message'));
        }

        // se guarda codigo referido cotizacion del soat;  
        DB::table('cotizacionsoats')->where('id', $request->input('codigorefe'))
                ->update([
                    'codigoreferido' => $request->input('last_cost'),                
                ]);
        // FIN se guarda cotizacion del soat;



        $pay = new PaymentController();
        
        $urlpayment = $pay->purchasePrueba($data, $body);
        return redirect()->away($this->method_payPrueba. $urlpayment);
    }

}
