<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use MP;
use DB;
use App\Conten;
use App\Banner;
use App\Support\Collection;
use App\User;
use Auth;
use Mail;
use App\Mail\UsuarioCreado;
use App\Mail\EmailContacto;
use SoapClient;

class BaseSoapController extends Controller {
    
    protected static $options;
    protected static $context;
    protected static $wsdl;

    public static function setWsdl($service)
    {
        return self::$wsdl = $service;
    }

    public static function getWsdl()
    {
        return self::$wsdl;
    }

    public static function generateContext(){
        self::$options = [
            'http'=>[
                'user_agent' => 'PHPSoapClient',
                'protocol_version' => 1.0,
                'header' => "Transfer-Encoding: chunked\r\n",
            ]
        ];
        return self::$context = stream_context_create(self::$options);
    }    

    public function loadXmlStringArray($xmlString){
        $array = (array) @simplexml_load_string($xmlString);

        if(!$array){
            $array =(array) @json_decode($xmlString, true);
        }
        else
        {
            $array =(array) @json_decode(json_encode($array), true);
        }
        return $array;

    }

}
