<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use MP;
use DB;
use App\Conten;
use App\Banner;
use App\Support\Collection;
use App\User;
use Auth;
use Mail;
use App\Mail\UsuarioCreado;
use App\Mail\EmailContacto;
use App\Mail\EmailCancelacion;
use App\Mail\EmailExequial;
use App\Mail\EmailRenovacion;
use App\Mail\EmailRenovacionA;
use SoapClient;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportMispoliza;
use App\Exports\ExportSoatCotizados;
use App\Imports\ImportMispoliza;


class HomeController extends Controller {
    
    const NRO_RESULTADOS = '20';
    

    
    public function inicio()
    {
        return view('tienda.index');
    }
    
    public function moduloadministrador()
    {
        return view('admin.administrador');
    }

    public function contactenos()
    {
        return view('tienda.contactenos');
    }

    public function policitaprotecciondedatosp()
    {
        return view('tienda.politica-de-proteccion-de-datos-personales');
    }


    public function colmena()
    {
        return view('tienda.colmena');
    }

    public function nosotros(Request $request)
    {
        /*$clientes = DB::table('clientes')
                        ->select(['*'])
                        ->where('identificacion', '=', '81818181')
                        ->get();
       */
        //dd($clientes);
        return view('tienda.nosotros');

    }

    public function aseguradoras()
    {
        return view('tienda.aseguradoras_vehiculo');
    }
    public function soat()
    {
        $tipoDoc = DB::table('tipo_documento')->select(['*'])->get();
        return view('tienda.soat')->with(compact('tipoDoc'));
        //return view('tienda.soat');
    }
    public function soatsuramericana()
    {
        return view('tienda.soat_sura');
    }
    public function soatTest()
    {
        $tipoDoc = DB::table('tipo_documento')->select(['*'])->get();

        return view('tienda.soat-test')->with(compact('tipoDoc'));
    }
    public function salud()
    {
        return view('tienda.salud');
    }
    public function educativo()
    {
        return view('tienda.educativo');
    }
    public function exequial()
    {
        return view('tienda.exequial');
    }
    public function vida()
    {
        return view('tienda.vida');
    }
    public function vida_renta()
    {
        return view('tienda.vida.vida_renta');
    }
    public function vida_medida()
    {
        return view('tienda.vida.vida_medida');
    }
    public function vida_mas()
    {
        return view('tienda.vida.vida_mas');
    }
    public function protege_vida()
    {
        return view('tienda.vida.protege_vida');
    }
    public function enfermedades_graves()
    {
        return view('tienda.vida.enfermedades_graves');
    }
    public function hogar()
    {
        return view('tienda.hogar');
    }
    public function cancelaciones()
    {
        return view('tienda.cancelaciones');
    }
    public function admincancelaciones()
    {
        return view('tienda.admincancelaciones');
    }

    public function personal()
    {
        return view('tienda.personal');
    }
     public function cotizar_todoriesgo()
    {
        return view('tienda.cotizar_todoriesgo');
    }
    public function mascotas()
    {
        return view('tienda.mascotas');
    }
    public function pyme()
    {
        return view('tienda.multiriesgo_pyme');
    }
    public function cumplimiento()
    {
        return view('tienda.cumplimiento');
    }
    public function maquinaria()
    {
        return view('tienda.equipo_maquinaria');
    }
    public function construccion()
    {
        return view('tienda.construccion');
    }
    public function civil()
    {
        return view('tienda.responsabilidad_civil');
    }
    public function carga()
    {
        return view('tienda.mercancia_carga');
    }

    public function riesgo()
    {
        return view('tienda.todo_riesgo');
    }

    public function riesgo3()
    {
        return view('tienda.todo_riesgo3');
    }

    public function exportar_soat_cotizados()
    {
        return view('tienda.exportar_soat_cotizados');
    }

    

    public function informacion_todoriesgo()
    {
        $ciudades = DB::table('ciudades')->select(['*'])->get();
        $zonas = DB::table('zona')->select(['*'])->get();
        $identificacion = DB::table('identificacion')->select(['*'])->get();
        $estado_civil = DB::table('estado_civil')->select(['*'])->get();
        return view('tienda.informacion_todoriesgo')->with(compact('ciudades','zonas','identificacion','estado_civil'));
    }
    /*Inicio Administrador*/
    public function administrador(Request $request)
    {
        $user = new User();
        //dd($request->password);
        $user_existente = User::where('email', $request->email)->first();
        
        if($user_existente) {
            $password = $user_existente->password;
            if(password_verify($request->password, $password))
            {
                // /return view('admin.administrador');
                if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) 
                {
                    return response()->json(['ok' => true, 'mensaje' => '']);
                } else {
                    return response()->json(['ok' => false, 'mensaje' => 'Email o contraseña incorrectos']);
                }
                return response()->json(['ok' => true, 'mensaje' => '']);
            }
            else
            {
                //$response = ['ok' => false, 'mensaje' => 'La contraseña es incorrecta por favor verifique nuevamente'];
                return response()->json(['ok' => false, 'mensaje' => 'La contraseña es incorrecta por favor verifique nuevamente']);
                //return redirect()->back()->with($response);
            }
            //$response = ['ok' => false, 'mensaje' => 'No se puede crear el usuario ya que existe otro usuario registrado con este correo electrónico'];
            //return redirect()->back()->with($response);
        }
        else
        {
            return response()->json(['ok' => false, 'mensaje' => 'No existe el email en nuestro sistema por favor verifique e intente nuevamente']);
            //$response = ['ok' => false, 'mensaje' => 'No existe el email en nuestro sistema por favor verifique e intente nuevamente'];
            //return redirect()->back()->with($response);   
        }
        
        //return view('admin.administrador');
    }
    public function login()
    {
        return view('admin.login');
    }
    public function registrarse()
    {
        $mensaje='';
        return view('admin.registrarse')->with(compact('mensaje'));
        
    }
    public function recuperarcontrasena()
    {
        return view('admin.recuperarcontraseña');
    }
    public function graficos()
    {
        return view('admin.graficos');
    }
    public function tablas()
    {
        //dd('entra');
        $envios_email = DB::table('email_cotizaciones')->select(['*'])->get();
        return view('admin.tablas')->with(compact('envios_email'));
    }

    public function reporte_usuarios()
    {
        //dd('entra');
        $users = DB::table('users')->select(['*'])->get();
        return view('admin.reporte_usuarios')->with(compact('users'));
    }

    public function detalle_usuario(Request $request)
    {
       
        $email = $request->email;
        //dd($email);
        $usuario = DB::table('users')->select(['*'])->where('email',$email)->first();
        return view('admin.editar_usuario')->with(compact('usuario'));
    }

    public function detalle_mensaje(Request $request)
    {
       
        $id = $request->idEmail;
        //dd($email);
        $envios_email = DB::table('email_cotizaciones')->select(['*'])->where('id',$id)->first();
        return view('admin.seguimiento_email')->with(compact('envios_email'));
    }

    public function actualizacionusuario(Request $request)
    {
        //dd('entra');
        $cliente_id = auth()->user()->cliente_id;
    
        $usuario = DB::table('clientes')->select(['*'])->where('id',$cliente_id)->first();
        
        return view('admin.actualizacion')->with(compact('usuario'));
    }
    
    public function actualizarusuarioadmin(Request $request)
    {
        
        DB::table('users')->where('email', $request->email_reg)
                ->update([
                    'name' => $request->firtsname,
                    'perfil' => $request->perfil,
                    'estado' => $request->estado,
                ]);

    
        
        //return view('admin.actualizacion')->with(compact('usuario'));
        return response()->json(['ok' => true, 'mensaje' => 'Datos actualizados correctamente']);
    }

    public function actualizarmensajeemail(Request $request)
    {
        
        DB::table('email_cotizaciones')->where('id', $request->id)
                ->update([
                    'observaciones' => $request->observaciones,
                ]);
        return response()->json(['ok' => true, 'mensaje' => 'Datos actualizados correctamente']);
    }

    public function actualizarusuario(Request $request)
    {
        
        DB::table('users')->where('email', $request->email_reg)
                ->update([
                    'name' => $request->firtsname,
                    'password' => bcrypt($request->password),
                ]);

        DB::table('clientes')->where('email', $request->email_reg)
                ->update([
                    'razon_social' => $request->firtsname,
                    'tipo_ident' => $request->typeIdent,
                    'identificacion' => $request->identification,
                    'direccion' => $request->adress,
                    'telefono1' => $request->phone,
                ]);
    
        
        //return view('admin.actualizacion')->with(compact('usuario'));
        return response()->json(['ok' => true, 'mensaje' => 'Datos actualizados correctamente']);
    }



    /*Fin Administrador*/




    public function servicios()
    {
        $params = array();
        $url = "https://www.banguat.gob.gt/variables/ws/TipoCambio.asmx?wsdl";
        //$params = array("moneda" => "2");
        try{

            
            /*self::setWsdl("https://www.banguat.gob.gt/variables/ws/TipoCambio.asmx?wsdl");
            $this->service = InstanceSoapClient::init();*/

            $client = new SoapClient ($url, $params);
            //dd('okokoko');
            $fecha =$client->VariablesDisponibles()->VariablesDisponiblesResult->Variables->Variable;

            //dd($client->__getTypes());
            //$fecha = $client->Variables()->VariablesResult->Variables->Variable;
            //$cambio = $client->TipoCambioDia()->TipoCambioDiaResult->CambioDolar->VarDolar->referencia;
            dd($fecha[0]);

        }catch(SoapFault $fault){
            echo '<br>'.$fault;

        }
    
    }

    
    public function mailrenovaciones(Request $request)
    {

   
        Mail::to($request->email)
        ->bcc([
            'negociosweb@prontoyseguros.com',
        ],[
            'web',                    
        ])->send(new EmailRenovacion($request));
            //Mail::to('luis0320@gmail.com')->send(new EmailCancelacion($request));

            
        return back()->with('message','Hemos recibido el correo solicitando la renovacion de su poliza');
    }

    public function mailrenoautos(Request $request)
    {

   
        Mail::to($request->email)
        ->bcc([
            'negociosweb@prontoyseguros.com',
        ],[
            'web',                    
        ])->send(new EmailRenovacionA($request));
            //Mail::to('luis0320@gmail.com')->send(new EmailCancelacion($request));

            
        return back()->with('message','Hemos recibido el correo solicitando la renovacion de su poliza');
    }

    public function cotizarsoatsura(Request $request)
    {

        $insert_coti_soat = DB::table('cotizacionsoats')->insertGetId([
            'placa' => $data['car']->NumberPlate,
            'fecha' => $dataPolicy->FromValidateDate,
            'total' => $data['car']->NewTariff->TotalFormatted,
            'marca' => $data['car']->BrandName,
            'linea' => $data['car']->VehicleLineDescription,
            'modelo' => $data['car']->VehicleYear,            
            'fecha_creacion' => date('Y-m-d'),
        ]);
    }

    public function registro(Request $request)
    {
        
        $user = DB::table('users')->select(['*'])->where('email', $request->email)->first();
        
        if($user)
        {
            return response()->json(['ok' => false, 'mensaje' => 'Cliente con el email registrado ya existe por favor validar la información, ó digite su usuario y contraseña']);
        }
        else
        {
            $cliente = DB::table('clientes')->select(['*'])->where('identificacion', $request->identification)->first();
              //dd($cliente);prueba
            if(!$cliente)
            {

                $codigo = 'PyS'.Str::random(8);

                $insert_cliente = DB::table('clientes')->insertGetId([
                    'razon_social' => $request->firtsname . ' ' . $request->lastname,
                    'tipo_ident' =>$request->typeIdent,
                    'identificacion' => $request->identification,
                    'codigo' => $codigo,
                    'email' => $request->email,
                    'estado' => '1',
                    'fecha_creacion' => date('Y-m-d'),
                    'fecha_modificacion' => date('Y-m-d'),
                ]);
            }
            
            $cliente = DB::table('clientes')->select(['*'])
                    ->where('tipo_ident', $request->typeIdent)
                    ->where('identificacion', $request->identification)
                    ->first();
            //Inserto el usuario en users
            if($cliente) {
                $user = new User();
                
                $user->name = $request->firtsname . ' ' . $request->lastname;
                $user->email = $request->email;
                $user->cliente_id = $cliente->id;
                $user->password = bcrypt($request->password);
                $user->perfil = 'cliente';
                $user->estado = '1';

                if($user->save()) {
                    Mail::to($user->email)->send(new UsuarioCreado($user));

                    if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

                        return response()->json(['ok' => true, 'mensaje' => '']);
                    } else {
                        return response()->json(['ok' => false, 'mensaje' => 'Email o contraseña incorrectos']);
                    }
                } else {
                    $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
                }
            } else {
                return response()->json(['ok' => false, 'mensaje' => 'No existe este cliente/usuario']);
            }
            
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        //Session::flush();
        return view('tienda.index');
    }

    public function importar_polizas()
    {
 
        return view('tienda.importar_polizas');
    }

    public function exportar_polizas()
    {
 
        return view('tienda.exportar_polizas');
    }

    public function exportar_excel(Request $request)
    {
        return Excel::download(new ExportMispoliza, 'cotizaciones-soat.xlsx');
 
        return view('tienda.exportar_polizas')->with('message','Importacion de polizas realizada');
    }

    public function exportar_soat_cotizaciones(Request $request)
    {
        return Excel::download(new ExportSoatCotizados($request), 'cotizaciones-soat.xlsx');
 
        return view('tienda.exportar-soat-cotizados')->with('message','Importacion de polizas realizada');
    }

    public function importar_excel(Request $request)
    {
        $file = $request->file('file');
        Excel::import(new ImportMispoliza, $file);
 
        return back()->with('message','Importacion de polizas realizada');
    }



    
   
    public function enviarEmailContacto(Request $request)
    {
        //dd($request);
        $insert_cliente = DB::table('email_cotizaciones')->insertGetId([
            'seguro' => 'Contactenos',
            'nombre' =>$request->nombres,
            'email' => $request->email,
            'telefono' => $request->telefono,
            'mensaje' => $request->mensaje,
            'fecha_creacion' => date('Y-m-d'),
        ]);
        
        Mail::to('luis0320@gmail.com')
                ->bcc([
                    'gerencia@prontoyseguros.com', 
                    'servicioalcliente@prontoyseguros.com',
                ],[
                    'Contactenos',
                    'Pagina',
                    
                ])->send(new EmailContacto($request));
        
        return view('tienda.contactenos_respuesta');
        
        
        return redirect()->back()->with(['ok' => true, 'mensaje' => 'Su mensaje ha sido enviado. Nos contactaremos con usted lo más pronto posible para atender su requerimiento.']);
    }

     public function enviarCancelacion(Request $request)
    {
        //dd($request->motivo);
        $insert_cliente = DB::table('cancelaciones')->insertGetId([
            'motivo' => $request->motivo,
            'aseguradora' =>$request->aseguradora,
            'fechadecancelacion' => $request->fechacancelacion,
            'cedula' => $request->cedula,
            //'asunto' => $request->asunto,
            'nombre' => $request->nombre,
            'beneficiario' => $request->beneficiario,
            'metododepago' => $request->metododepago,
            'estado' => 1,
            'fecharegistro' => date('m-d-Y h:i:s a', time()),
            
        ]);
        
        Mail::to('luis0320@gmail.com')
                ->bcc([
                    'negociosweb@prontoyseguros.com', 
                ],[
                    'web',                    
                ])->send(new EmailCancelacion($request));
        //Mail::to('luis0320@gmail.com')->send(new EmailCancelacion($request));
               
        
        dd('guardado y enviado');
        return view('tienda.contactenos_respuesta');
        
        
        return redirect()->back()->with(['ok' => true, 'mensaje' => 'Su mensaje ha sido enviado. Nos contactaremos con usted lo más pronto posible para atender su requerimiento.']);
    }

    public function enviarEmail(Request $request)
    {
        $insert_cliente = DB::table('email_cotizaciones')->insertGetId([
            'seguro' => $request->seguro,
            'nombre' =>$request->nombres,
            'email' => $request->email,
            'telefono' => $request->telefono,
            'mensaje' =>'',
            'fecha_creacion' => date('Y-m-d'),
        ]);

        //Mail::to($request->contacto)->send(new EmailExequial($request));
        Mail::to($request->contacto)
                ->bcc([
                    'negociosweb@gmail.com', 
                    'asesor4@prontoyseguros.com',
                ],[
                    'Jhon Calero',
                    'Mercadeo',
                    
                ])->send(new EmailExequial($request));


        return view('tienda.contactenos_respuesta');
        
        
        return redirect()->back()->with(['ok' => true, 'mensaje' => 'Su mensaje ha sido enviado. Nos contactaremos con usted lo más pronto posible para atender su requerimiento.']);
    }

    public function listado(Request $request)
    {
        $marcar = DB::table('tipcar')
                ->select(['*'])
                ->groupBy('tipcar_cod_marca')
                ->orderBy('tipcar_des_marca', 'asc')
                ->get();


        $conten = DB::table('conten')
                ->select(['*'])
                ->where('conten_precio_lista','>', '0')
                ->where('conten_disp_iprodu','>', '1')
//                ->where('conten_familia','!=', '')
                ->get();

        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();

        $anchos = DB::table('medidas')
                ->select(['medidas_ancho'])
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'ASC')
                ->paginate(self::NRO_RESULTADOS);
        
        return view('listado')->with(compact('marcar', 'anchos', 'conten', 'baterias'));
    }

    public function consumoriesgo(Request $request)
    {
        dd('entra');
        return response()->json(['ok' => true, 'mensaje' => '']);
    }

    public function productosMarcaLlanta2($marcallanta){
        //Se obtienen los códigos de marca, modelo y línea del vehículo
        //a través de las descripciones enviadas en la ruta.
        $total_productos = 0;
        $marca2 = $this->convertString($marcallanta);
        
        $cod_marcas = array();
        
        $marcar = DB::table('tipcar')
            ->select(['*'])
            ->groupBy('tipcar_cod_marca')
            ->orderBy('tipcar_des_marca', 'asc')
            ->get();

        //Modelos de vehículos
        $modelos_vehiculos = array();

        //Líneas de vehículos
        $lineas_vehiculos = array();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();
         //Medidas de llantas Camion
        $anchos_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', '>', '0')
                ->groupBy('medidas_camion_ancho')
                ->orderBy('medidas_camion_ancho', 'asc')
                ->get();

        $precio_desde = (request()->filtro_precio_desde) ? request()->filtro_precio_desde : null;
        $precio_hasta = (request()->filtro_precio_hasta) ? request()->filtro_precio_hasta : null;

        $filtros = json_decode(json_encode([
            'precio_desde' => $precio_desde,
            'precio_hasta' => $precio_hasta,
            'marca2' => $marca2
        ]));

        $conten = Conten::busquedaFiltros($filtros);

        $conten = $conten->get()->sortByDesc('promociones');

        $conten = (new Collection($conten))->paginate(self::NRO_RESULTADOS);

        $total_productos = $conten->total();

        $marcas_seleccionadas = $cod_marcas;
        $cod_marca = null;
        $marca = null;
        $marca2 = null;
        $linea = null;
        $linea2 = null;
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'marca';
        $descripcion_busqueda = 'Marca: ' . $marcallanta ;

        $seleccionados = [];
        
        $marcas_filtro = Conten::busquedaFiltrosMarca($filtros)
        ->select('conten_cod_marca', 'conten_marcas')
        ->groupBy('conten_cod_marca')
        ->get();
        
        return view('listado')->with(compact(
            'marcar',
            'anchos',
            'anchos_camion',
            'conten',
            'descripcion_busqueda',
            'total_productos',
            'marcas_filtro',
            'baterias',
            'tipo_busqueda',
            'seleccionados'
        ));
    
    }

    public function productosMarcaLlanta($marcallanta){
        //Se obtienen los códigos de marca, modelo y línea del vehículo
        //a través de las descripciones enviadas en la ruta.
        $total_productos = 0;
        $marca2 = $this->convertString($marcallanta);
        
        $cod_marcas = array();
        
        $marcar = DB::table('tipcar')
            ->select(['*'])
            ->groupBy('tipcar_cod_marca')
            ->orderBy('tipcar_des_marca', 'asc')
            ->get();

        //Modelos de vehículos
        $modelos_vehiculos = array();

        //Líneas de vehículos
        $lineas_vehiculos = array();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();
         //Medidas de llantas Camion
        $anchos_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', '>', '0')
                ->groupBy('medidas_camion_ancho')
                ->orderBy('medidas_camion_ancho', 'asc')
                ->get();

        //Promociones
        $promociones = DB::table('promter')
                ->join('conten', 'conten_cod_conten', 'promter_cod_conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('tipopromoc', 'tipopromoc_cod_tipopromoc', 'promter_cod_tipopromoc')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->where('conten_marcas', $marca2)
                ->where('conten_cod_marca','<', '9')
                ->where('conten_precio_lista','>', '0')
                ->where('promter_fec_hasta', '>=', date('Y-m-d H:i:s'));
//        dd(request()->filtro_marcas);

        $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
        $promociones = ($filtro_marcas) ? $promociones->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $promociones;
        $promociones = (request()->filtro_precio_desde) ? $promociones->where('promter_prec_normal', '>=', request()->filtro_precio_desde) : $promociones;
        $promociones = (request()->filtro_precio_hasta) ? $promociones->where('promter_prec_normal', '<=', request()->filtro_precio_hasta) : $promociones;
        
        $array_promociones = $promociones->pluck('conten_cod_conten')->toArray();
        $conten_promociones = $promociones->orderBy('conten_cod_marca')->orderBy('promter_porc_prom','desc')->get();
        
        //Productos
        $conten = DB::table('conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->select(['*'])
                ->where('conten_marcas', $marca2)
                ->where('conten_precio_lista','>', '0')
                ->where('conten_cod_marca','<', '9');
        $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
        $conten = ($filtro_marcas) ? $conten->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $conten;
        $conten = (request()->filtro_precio_desde) ? $conten->where('conten_precio_lista', '>=', request()->filtro_precio_desde) : $conten;
        $conten = (request()->filtro_precio_hasta) ? $conten->where('conten_precio_lista', '<=', request()->filtro_precio_hasta) : $conten;
        $conten = $conten->orderBy('conten_cod_marca');
        $conten = (count($array_promociones) > 0) ? $conten->whereNotIn('conten_cod_conten', $array_promociones) : $conten;
        $conten = $conten->paginate(self::NRO_RESULTADOS);

        $total_productos = $conten_promociones->count() + $conten->total();

        $marcas_seleccionadas = $cod_marcas;
        $cod_marca = null;
        $marca = null;
        $marca2 = null;
        $linea = null;
        $linea2 = null;
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'marca';
        $descripcion_busqueda = 'Marca: ' . $marcallanta ;

        $seleccionados = [];
        
        $marcas_filtro = DB::table('conten')
                ->select(['conten_cod_marca', 'conten_marcas'])
                ->where('conten_marcas', '<>', '')
                ->where('conten_cod_marca','<', '9')
                ->orderBy('conten_cod_marca', 'asc')
                ->groupBy('conten_cod_marca')
                ->get();
        
        
        return view('listado')->with(compact(
            'marcar', 
            'anchos', 
            'anchos_camion', 
            'conten', 
            'descripcion_busqueda', 
            'conten_promociones',
            'total_productos', 
            'marcas_filtro',
            'baterias',
            'tipo_busqueda',
            'seleccionados'
        ));

    
    }
    /**
     * Se obtienen los códigos de marca, modelo y línea del vehículo a través de las descripciones enviadas en la ruta.
     */
    public function productosVehiculo($marca, $linea, $modelo, Request $request){
        $total_productos = 0;
        //convierto los parametros a string
        $marca2 = $this->convertString($marca);
        $linea2 = $this->convertString($linea);
        //consulto las marcas dependiendo del parametro obtenido
        $table_marcas = DB::table('tipcar')
                ->select(['*'])
                ->where('tipcar_des_marca', $marca2)->first();
                
        
        $cod_marca = ($table_marcas) ? $table_marcas->tipcar_cod_marca : null;
        
        $tipcar = DB::table('tipcar')->where('tipcar_cod_marca', $cod_marca)
                ->where('tipcar_ano', $modelo)
                ->where('tipcar_tipo', $linea2)
                ->get();

        $array_anchos = array();
        $array_perfiles = array();
        $array_rines = array();
        
        foreach($tipcar as $t) {
            $array_anchos[] = $t->tipcar_ancho;
            $array_perfiles[] = $t->tipcar_perfil;
            $array_rines[] = $t->tipcar_rin;
        }
                
        $marcar = DB::table('tipcar')
                ->select(['*'])
                ->groupBy('tipcar_cod_marca')
                ->orderBy('tipcar_des_marca', 'asc')
                ->get();

         $promociones = DB::table('promter')
                ->join('conten', 'conten_cod_conten', 'promter_cod_conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->join('tipopromoc', 'tipopromoc_cod_tipopromoc', 'promter_cod_tipopromoc')
                ->whereIn('conten_llanta_ancho', $array_anchos)
                ->whereIn('conten_llanta_perfil', $array_perfiles)
                ->whereIn('conten_llanta_rin', $array_rines)
                ->where('conten_disp_iprodu','>', '1')
                ->where('conten_cod_marca','<', '9')
                ->where('promter_fec_hasta', '>=', date('Y-m-d H:i:s'));
        $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
        $promociones = ($filtro_marcas) ? $promociones->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $promociones;
        $promociones = (request()->filtro_precio_desde) ? $promociones->where('promter_prec_normal', '>=', request()->filtro_precio_desde) : $promociones;
        $promociones = (request()->filtro_precio_hasta) ? $promociones->where('promter_prec_normal', '<=', request()->filtro_precio_hasta) : $promociones;
        
        $array_promociones = $promociones->pluck('conten_cod_conten')->toArray();
        $conten_promociones = $promociones->orderBy('conten_cod_marca')->orderBy('promter_porc_prom','desc')->get();
        
        $conten = DB::table('conten')
                ->select(['*'])
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->whereIn('conten_llanta_ancho', $array_anchos)
                ->whereIn('conten_llanta_perfil', $array_perfiles)
                ->whereIn('conten_llanta_rin', $array_rines)
                ->where('conten_precio_lista','>', '0')
                ->where('conten_cod_marca','<', '9')
                ->where('Estado','=', '1');

         
        $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
        $conten = ($filtro_marcas) ? $conten->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $conten;
        $conten = (request()->filtro_precio_desde) ? $conten->where('conten_precio_lista', '>=', request()->filtro_precio_desde) : $conten;
        $conten = (request()->filtro_precio_hasta) ? $conten->where('conten_precio_lista', '<=', request()->filtro_precio_hasta) : $conten;
        $conten = $conten->orderBy('conten_cod_marca');
        $conten = (count($array_promociones) > 0) ? $conten->whereNotIn('conten_cod_conten', $array_promociones) : $conten;
        $conten = $conten->paginate(self::NRO_RESULTADOS);

        $total_productos = $conten_promociones->count() + $conten->total();

        $modelos_vehiculos = DB::table('tipcar')->where('tipcar_cod_marca', $cod_marca)
                ->groupBy('tipcar_ano')
                ->get();

        //Líneas de vehículos
        $lineas_vehiculos = DB::table('tipcar')->where('tipcar_cod_marca', $cod_marca);
        if ($modelo) {
            $lineas_vehiculos = $lineas_vehiculos->where('tipcar_ano', $modelo);
        }
        $lineas_vehiculos = $lineas_vehiculos
                ->groupBy('tipcar_tipo')
                ->get();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        $perfiles = DB::table('medidas')
                ->groupBy('medidas_perfil')
                ->where('medidas_ancho', $anchos->first()->medidas_ancho)
                ->orderBy('medidas_perfil', 'asc')
                ->get();
        
        $rines = DB::table('medidas')
                ->where('medidas_rin', '>', '0')
                ->where('medidas_ancho', $anchos->first()->medidas_ancho)
                ->where('medidas_perfil', $perfiles->first()->medidas_perfil)
                ->groupBy('medidas_rin')
                ->orderBy('medidas_rin', 'asc')
                ->get();
        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();

        //Medidas de llantas Camion
        $anchos_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', '>', '0')
                ->groupBy('medidas_camion_ancho')
                ->orderBy('medidas_camion_ancho', 'asc')
                ->get();

        //Marcas de llantas
        $marcas = DB::table('marcas')
                ->where('marcas_estado', config('parametros.cod_estado_activo'))
                ->limit(20)
                ->get();
        
        $ancho = null;
        $perfil = null;
        $rin = null;
        
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'vehiculo';
        $busqueda_medidas = '';
        if(count($array_anchos) > 0){
            $busqueda_medidas = ' - Medidas: ';

            foreach ($array_anchos as $key => $ancho) {
                $busqueda_medidas .= '('. $array_anchos[$key] . ' ' . $array_perfiles[$key] . ' R' . $array_rines[$key]. ')';
            }
        } 
        
        $descripcion_busqueda = 'Vehículo '.strtoupper($marca2 . '  ' . $linea2 . ' ' . $modelo). $busqueda_medidas ;

        
        $marcas_seleccionadas = array();

        $marca_arr_sel = DB::table('tipcar')
            ->select(['tipcar_cod_marca'])
            ->where('tipcar_des_marca', $marca2)
            ->groupBy('tipcar_cod_marca')
            ->orderBy('tipcar_des_marca', 'asc')
            ->first();

        $marca_sel = null;
        if($marca_arr_sel){
            $marca_sel = $marca_arr_sel->tipcar_cod_marca;
        }
        
        $marcas_filtro = DB::table('conten')
                ->select(['conten_cod_marca', 'conten_marcas'])
                ->where('conten_marcas', '<>', '')
                ->where('conten_cod_marca','<', '9')
                ->orderBy('conten_cod_marca', 'asc')
                ->groupBy('conten_cod_marca')
                ->get();
        $seleccionados = [
            'marca' => $marca_sel, 
            'linea' => $linea2, 
            'modelo' => $modelo
        ];

        
        return view('listado')->with(compact(
            'marcar', 
            'anchos', 
            'anchos_camion', 
            'conten', 
            'descripcion_busqueda', 
            'conten_promociones', 
            'total_productos', 
            'marcas_filtro',
            'baterias',
            'tipo_busqueda',
            'seleccionados'
        ));
        
    }


    public function soatcotizados(){

        $codigo = 'PyS'.Str::random(8);

        $cotizacionsoats = DB::table('cotizacionsoats') 
                    ->select(['*'])
                    ->orderBy('id', 'DESC')
                    ->get();

        return view('tienda.cotizaciones_soat')->with(compact(
            'cotizacionsoats',
            'codigo'
        ));

    }

    /**
     * Se obtienen los códigos de marca, modelo y línea del vehículo a través de las descripciones enviadas en la ruta.
     */
    public function productosVehiculo2($marca, $linea, $modelo, Request $request){
        $total_productos = 0;
        //convierto los parametros a string
        $marca2 = $this->convertString($marca);
        $linea2 = $this->convertString($linea);
        //consulto las marcas dependiendo del parametro obtenido
        $table_marcas = DB::table('tipcar')
                ->select(['*'])
                ->where('tipcar_des_marca', $marca2)->first();
                
        
        $cod_marca = ($table_marcas) ? $table_marcas->tipcar_cod_marca : null;
        
        $tipcar = DB::table('tipcar')->where('tipcar_cod_marca', $cod_marca)
                ->where('tipcar_ano', $modelo)
                ->where('tipcar_tipo', $linea2)
                ->get();

        $array_anchos = array();
        $array_perfiles = array();
        $array_rines = array();
        
        foreach($tipcar as $t) {
            $array_anchos[] = $t->tipcar_ancho;
            $array_perfiles[] = $t->tipcar_perfil;
            $array_rines[] = $t->tipcar_rin;
        }
                
        $marcar = DB::table('tipcar')
                ->select(['*'])
                ->groupBy('tipcar_cod_marca')
                ->orderBy('tipcar_des_marca', 'asc')
                ->get();

        $cod_marcas = array();
        if (isset($request->filtro_marcas) && is_array($request->filtro_marcas)) {
            $cod_marcas = array_merge($cod_marcas,$request->filtro_marcas);
        }

        $precio_desde = (request()->filtro_precio_desde) ? request()->filtro_precio_desde : null;
        $precio_hasta = (request()->filtro_precio_hasta) ? request()->filtro_precio_hasta : null;
        
        $filtros = json_decode(json_encode([
            'ancho'  => $array_anchos,
            'perfil' => $array_perfiles,
            'rin'    => $array_rines,
            'marcas'    => $cod_marcas,
            'precio_desde' => $precio_desde,
            'precio_hasta' => $precio_hasta
        ]));

        $conten = Conten::busquedaFiltros($filtros);

        $conten = $conten->get()->sortByDesc('promociones');

        $conten = (new Collection($conten))->paginate(self::NRO_RESULTADOS);

        $total_productos = $conten->count();

        $modelos_vehiculos = DB::table('tipcar')->where('tipcar_cod_marca', $cod_marca)
                ->groupBy('tipcar_ano')
                ->get();

        //Líneas de vehículos
        $lineas_vehiculos = DB::table('tipcar')->where('tipcar_cod_marca', $cod_marca);
        if ($modelo) {
            $lineas_vehiculos = $lineas_vehiculos->where('tipcar_ano', $modelo);
        }
        $lineas_vehiculos = $lineas_vehiculos
                ->groupBy('tipcar_tipo')
                ->get();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        $perfiles = DB::table('medidas')
                ->groupBy('medidas_perfil')
                ->where('medidas_ancho', $anchos->first()->medidas_ancho)
                ->orderBy('medidas_perfil', 'asc')
                ->get();
        
        $rines = DB::table('medidas')
                ->where('medidas_rin', '>', '0')
                ->where('medidas_ancho', $anchos->first()->medidas_ancho)
                ->where('medidas_perfil', $perfiles->first()->medidas_perfil)
                ->groupBy('medidas_rin')
                ->orderBy('medidas_rin', 'asc')
                ->get();
        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();

        //Medidas de llantas Camion
        $anchos_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', '>', '0')
                ->groupBy('medidas_camion_ancho')
                ->orderBy('medidas_camion_ancho', 'asc')
                ->get();

        //Marcas de llantas
        $marcas = DB::table('marcas')
                ->where('marcas_estado', config('parametros.cod_estado_activo'))
                ->limit(20)
                ->get();
        
        $ancho = null;
        $perfil = null;
        $rin = null;
        
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'vehiculo';
        $busqueda_medidas = '';
        if(count($array_anchos) > 0){
            $busqueda_medidas = ' - Medidas: ';

            foreach ($array_anchos as $key => $ancho) {
                $busqueda_medidas .= '('. $array_anchos[$key] . ' ' . $array_perfiles[$key] . ' R' . $array_rines[$key]. ')';
            }
        } 
        
        $descripcion_busqueda = 'Vehículo '.strtoupper($marca2 . '  ' . $linea2 . ' ' . $modelo). $busqueda_medidas ;

        
        $marcas_seleccionadas = array();

        $marca_arr_sel = DB::table('tipcar')
            ->select(['tipcar_cod_marca'])
            ->where('tipcar_des_marca', $marca2)
            ->groupBy('tipcar_cod_marca')
            ->orderBy('tipcar_des_marca', 'asc')
            ->first();

        $marca_sel = null;
        if($marca_arr_sel){
            $marca_sel = $marca_arr_sel->tipcar_cod_marca;
        }
        
        $marcas_filtro = Conten::busquedaFiltrosMarca($filtros)
            ->select('conten_cod_marca', 'conten_marcas')
            ->groupBy('conten_cod_marca')
            ->get();

        $seleccionados = [
            'marca' => $marca_sel, 
            'linea' => $linea2, 
            'modelo' => $modelo
        ];

        return view('listado')->with(compact(
            'marcar',
            'anchos',
            'anchos_camion',
            'conten',
            'descripcion_busqueda',
            'total_productos',
            'marcas_filtro',
            'baterias',
            'tipo_busqueda',
            'seleccionados'
        ));
        
    }

    public function productosMedida2($ancho, $perfil, $rin, $marcas = null, Request $request)
    {
        $total_productos = 0;
        //Se obtiene código de la marca según descripción enviada en la ruta.
        $cod_marcas = array();
        if (isset($request->filtro_marcas) && is_array($request->filtro_marcas)) {
            $cod_marcas = array_merge($cod_marcas,$request->filtro_marcas);
        }
        
        $marcar = DB::table('tipcar')
                ->select(['*'])
                ->groupBy('tipcar_cod_marca')
                ->orderBy('tipcar_des_marca', 'asc')
                ->get();

        //Modelos de vehículos
        $modelos_vehiculos = array();

        //Líneas de vehículos
        $lineas_vehiculos = array();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        $perfiles = DB::table('medidas')
                ->where('medidas_ancho', $ancho)
                ->groupBy('medidas_perfil')
                ->orderBy('medidas_perfil', 'asc')
                ->get();
        
        $rines = DB::table('medidas')
                ->where('medidas_ancho', $ancho)
                ->where('medidas_perfil', $perfil)
                ->where('medidas_rin', '>', '0')
                ->groupBy('medidas_rin')
                ->orderBy('medidas_rin', 'asc')
                ->get();
        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();
         //Medidas de llantas Camion
        $anchos_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', '>', '0')
                ->groupBy('medidas_camion_ancho')
                ->orderBy('medidas_camion_ancho', 'asc')
                ->get();

        $precio_desde = ($request->has('filtro_precio_desde')) ? $request->filtro_precio_desde : null;
        $precio_hasta = ($request->has('filtro_precio_hasta')) ? $request->filtro_precio_hasta : null;

        $filtros = json_decode(json_encode([
            'marcas' => $cod_marcas,
            'ancho'  => [$ancho],
            'perfil' => [$perfil],
            'rin'    => [$rin],
            'precio_desde' => $precio_desde,
            'precio_hasta' => $precio_hasta
        ]));

        $conten = Conten::busquedaFiltros($filtros);

        $conten = $conten->get()->sortByDesc('promociones');
        //$conten = $conten->get()->orderBy('conten_cod_marca', 'asc');

        $conten = (new Collection($conten))->paginate(self::NRO_RESULTADOS);

        $total_productos = $conten->count();

        $marcas_seleccionadas = $cod_marcas;
        $cod_marca = null;
        $marca = null;
        $marca2 = null;
        $linea = null;
        $linea2 = null;
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'medida';
        $descripcion_busqueda = 'Medida: ' . $ancho . ' ' . $perfil . ' R' . $rin;
        
        $marcas_filtro = Conten::busquedaFiltrosMarca($filtros)
            ->select('conten_cod_marca', 'conten_marcas')
            ->groupBy('conten_cod_marca')
            ->get();

        $seleccionados = [
            'ancho' => $ancho, 
            'perfil' => $perfil, 
            'rin' => $rin
        ];
            
        return view('listado')->with(compact(
            'marcar',
            'anchos',
            'anchos_camion',
            'conten',
            'descripcion_busqueda',
            'total_productos',
            'marcas_filtro',
            'baterias',
            'tipo_busqueda',
            'seleccionados'
        ));
    }

    public function productosMedida($ancho, $perfil, $rin, $marcas = null, Request $request)
    {
        $total_productos = 0;
        //Se obtiene código de la marca según descripción enviada en la ruta.
        $cod_marcas = array();
        if (isset($request->filtro_marcas) && is_array($request->filtro_marcas)) {
            $cod_marcas = array_merge($cod_marcas,$request->filtro_marcas);
        }
        
        $marcar = DB::table('tipcar')
                ->select(['*'])
                ->groupBy('tipcar_cod_marca')
                ->orderBy('tipcar_des_marca', 'asc')
                ->get();

        //Modelos de vehículos
        $modelos_vehiculos = array();

        //Líneas de vehículos
        $lineas_vehiculos = array();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        $perfiles = DB::table('medidas')
                ->where('medidas_ancho', $ancho)
                ->groupBy('medidas_perfil')
                ->orderBy('medidas_perfil', 'asc')
                ->get();
        
        $rines = DB::table('medidas')
                ->where('medidas_ancho', $ancho)
                ->where('medidas_perfil', $perfil)
                ->where('medidas_rin', '>', '0')
                ->groupBy('medidas_rin')
                ->orderBy('medidas_rin', 'asc')
                ->get();
        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();
         //Medidas de llantas Camion
        $anchos_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', '>', '0')
                ->groupBy('medidas_camion_ancho')
                ->orderBy('medidas_camion_ancho', 'asc')
                ->get();

        //Promociones
        $promociones = DB::table('promter')
                ->join('conten', 'conten_cod_conten', 'promter_cod_conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('tipopromoc', 'tipopromoc_cod_tipopromoc', 'promter_cod_tipopromoc')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->where('conten_llanta_ancho', $ancho)
                ->where('conten_llanta_perfil', $perfil)
                ->where('conten_llanta_rin', $rin)
                ->where('conten_cod_marca','<', '9')
                ->where('conten_precio_lista','>', '0')
                ->where('promter_fec_hasta', '>=', date('Y-m-d H:i:s'));
//        dd(request()->filtro_marcas);
        $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
        $promociones = ($filtro_marcas) ? $promociones->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $promociones;
        $promociones = (request()->filtro_precio_desde) ? $promociones->where('promter_prec_normal', '>=', request()->filtro_precio_desde) : $promociones;
        $promociones = (request()->filtro_precio_hasta) ? $promociones->where('promter_prec_normal', '<=', request()->filtro_precio_hasta) : $promociones;
        
        $array_promociones = $promociones->pluck('conten_cod_conten')->toArray();
        $conten_promociones = $promociones->orderBy('conten_cod_marca')->orderBy('promter_porc_prom','desc')->get();
                         //dd($conten_promociones);
        //Productos
        $conten = DB::table('conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->select(['*'])
                ->where('conten_llanta_ancho', $ancho)
                ->where('conten_llanta_perfil', $perfil)
                ->where('conten_llanta_rin', $rin)
                ->where('conten_precio_lista','>', '0')
                ->where('Estado','=', '1')
                ->where('conten_cod_marca','<', '9');
        $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
        $conten = ($filtro_marcas) ? $conten->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $conten;
        $conten = (request()->filtro_precio_desde) ? $conten->where('conten_precio_lista', '>=', request()->filtro_precio_desde) : $conten;
        $conten = (request()->filtro_precio_hasta) ? $conten->where('conten_precio_lista', '<=', request()->filtro_precio_hasta) : $conten;
        $conten = $conten->orderBy('conten_cod_marca');
        $conten = (count($array_promociones) > 0) ? $conten->whereNotIn('conten_cod_conten', $array_promociones) : $conten;
        $conten = $conten->paginate(self::NRO_RESULTADOS);

        $total_productos = $conten_promociones->count() + $conten->total();

        $marcas_seleccionadas = $cod_marcas;
        $cod_marca = null;
        $marca = null;
        $marca2 = null;
        $linea = null;
        $linea2 = null;
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'medida';
        if ($perfil=='R') {
            $descripcion_busqueda = 'Medida: ' . $ancho . ' ' . $perfil  . ' ' . $rin;
        }
        else
            {
            $descripcion_busqueda = 'Medida: ' . $ancho . ' ' . $perfil . ' R' . $rin;
        }
        
        /*$marcas_filtro = Conten::busquedaFiltrosMarca($filtros)
            ->select('conten_cod_marca', 'conten_marcas')
            ->groupBy('conten_cod_marca')
            ->get();*/
        $marcas_filtro = DB::table('conten')
                ->select(['conten_cod_marca', 'conten_marcas'])
                ->where('conten_marcas', '<>', '')
                ->where('conten_cod_marca','<', '9')
                ->orderBy('conten_cod_marca', 'asc')
                ->groupBy('conten_cod_marca')
                ->get();

        $seleccionados = [
            'ancho' => $ancho, 
            'perfil' => $perfil, 
            'rin' => $rin
        ];
            
        return view('listado')->with(compact(
            'marcar', 
            'anchos', 
            'anchos_camion', 
            'conten', 
            'descripcion_busqueda', 
            'conten_promociones',
            'total_productos', 
            'marcas_filtro',
            'baterias',
            'tipo_busqueda',
            'seleccionados'
        ));
    }

    public function productosMedidaCamion2($ancho, $perfil, $rin, $marcas = null, Request $request)
    {
        $total_productos = 0;
        //Se obtiene código de la marca según descripción enviada en la ruta.
        $cod_marcas = array();
        if (isset($request->filtro_marcas) && is_array($request->filtro_marcas)) {
            $cod_marcas = array_merge($cod_marcas,$request->filtro_marcas);
        }
        
        $marcar = DB::table('tipcar')
                ->select(['*'])
                ->groupBy('tipcar_cod_marca')
                ->orderBy('tipcar_des_marca', 'asc')
                ->get();

        //Modelos de vehículos
        $modelos_vehiculos = array();

        //Líneas de vehículos
        $lineas_vehiculos = array();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        $perfiles = DB::table('medidas')
                ->where('medidas_ancho', $ancho)
                ->groupBy('medidas_perfil')
                ->orderBy('medidas_perfil', 'asc')
                ->get();
        
        $rines = DB::table('medidas')
                ->where('medidas_ancho', $ancho)
                ->where('medidas_perfil', $perfil)
                ->where('medidas_rin', '>', '0')
                ->groupBy('medidas_rin')
                ->orderBy('medidas_rin', 'asc')
                ->get();
        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();
         //Medidas de llantas Camion
        $anchos_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', '>', '0')
                ->groupBy('medidas_camion_ancho')
                ->orderBy('medidas_camion_ancho', 'asc')
                ->get();

        $precio_desde = ($request->has('filtro_precio_desde')) ? $request->filtro_precio_desde : null;
        $precio_hasta = ($request->has('filtro_precio_hasta')) ? $request->filtro_precio_hasta : null;


        $filtros = json_decode(json_encode([
            'marcas' => $cod_marcas,
            'ancho'  => [$ancho],
            'perfil' => [$perfil],
            'rin'    => [$rin],
            'precio_desde' => $precio_desde,
            'precio_hasta' => $precio_hasta,
            'conten_tipo_conten' => 1
        ]));

        //Promociones
        $promociones = DB::table('promter')
                ->join('conten', 'conten_cod_conten', 'promter_cod_conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('tipopromoc', 'tipopromoc_cod_tipopromoc', 'promter_cod_tipopromoc')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->where('conten_llanta_ancho', $ancho)
                ->where('conten_llanta_perfil', $perfil)
                ->where('conten_llanta_rin', $rin)
                ->where('conten_cod_marca','<', '9')
                ->where('conten_tipo_conten', 'Llantas CVT Radial')
                ->where('conten_precio_lista','>', '0')
                ->where('promter_fec_hasta', '>=', date('Y-m-d H:i:s'));
//        dd(request()->filtro_marcas);
            $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
            $promociones = ($filtro_marcas) ? $promociones->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $promociones;
            $promociones = (request()->filtro_precio_desde) ? $promociones->where('promter_prec_normal', '>=', request()->filtro_precio_desde) : $promociones;
            $promociones = (request()->filtro_precio_hasta) ? $promociones->where('promter_prec_normal', '<=', request()->filtro_precio_hasta) : $promociones;
            
            $array_promociones = $promociones->pluck('conten_cod_conten')->toArray();
            $conten_promociones = $promociones->orderBy('conten_cod_marca')->orderBy('promter_porc_prom','desc')->get();
                             //dd($conten_promociones);
            //Productos
            $conten = DB::table('conten')
                    ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                    ->join('Productos', 'conten_cod_conten', 'Codigo')
                    ->select(['*'])
                    ->where('conten_llanta_ancho', $ancho)
                    ->where('conten_llanta_perfil', $perfil)
                    ->where('conten_llanta_rin', $rin)
                    ->where('conten_precio_lista','>', '0')
                    ->where('conten_tipo_conten', 'Llantas CVT Radial')
                    ->where('conten_cod_marca','<', '9');
            $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
            $conten = ($filtro_marcas) ? $conten->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $conten;
            $conten = (request()->filtro_precio_desde) ? $conten->where('conten_precio_lista', '>=', request()->filtro_precio_desde) : $conten;
            $conten = (request()->filtro_precio_hasta) ? $conten->where('conten_precio_lista', '<=', request()->filtro_precio_hasta) : $conten;
            $conten = $conten->orderBy('conten_cod_marca');
            $conten = (count($array_promociones) > 0) ? $conten->whereNotIn('conten_cod_conten', $array_promociones) : $conten;
            $conten = $conten->paginate(self::NRO_RESULTADOS);

            $total_productos = $conten_promociones->count() + $conten->total();
        $marcas_seleccionadas = $cod_marcas;
        $cod_marca = null;
        $marca = null;
        $marca2 = null;
        $linea = null;
        $linea2 = null;
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'medida';
        if ($perfil=='R') {
            $descripcion_busqueda = 'Medida: ' . $ancho . ' ' . $perfil  . ' ' . $rin;
        }
        else
            {
            $descripcion_busqueda = 'Medida: ' . $ancho . ' ' . $perfil . ' R' . $rin;
        }
        //$descripcion_busqueda = 'Medida: ' . $ancho . ' ' . $perfil . ' R' . $rin;
        
        $marcas_filtro = Conten::busquedaFiltrosMarca($filtros)
            ->select('conten_cod_marca', 'conten_marcas')
            ->groupBy('conten_cod_marca')
            ->get();

        $seleccionados = [
            'ancho' => $ancho, 
            'perfil' => $perfil, 
            'rin' => $rin
        ];
            
        return view('listadoCamion')->with(compact(
            'marcar',
            'anchos',
            'anchos_camion',
            'conten',
            'descripcion_busqueda',
            'total_productos',
            'marcas_filtro',
            'baterias',
            'tipo_busqueda',
            'seleccionados'
        ));
    }

    public function productosMedidaCamion($ancho, $perfil, $rin, $marcas = null, Request $request)
    {
        $total_productos = 0;
        //dd("llega llantas_camion");
        //Se obtiene código de la marca según descripción enviada en la ruta.
        $cod_marcas = array();
        if($marcas) {
            $array_marcas = explode(',', strtoupper($marcas));
            $table_marcas = DB::table('marcas')->whereIn('marcas_descripcion', $array_marcas)->get();
            foreach($table_marcas as $m) {
                $cod_marcas[] = trim($m->marcas_cod_marca);
            }
        }
        
       $marcar = DB::table('tipcar')
                ->select(['*'])
                ->groupBy('tipcar_cod_marca')
                ->orderBy('tipcar_des_marca', 'asc')
                ->get();

        //Modelos de vehículos
        $modelos_vehiculos = array();

        //Líneas de vehículos
        $lineas_vehiculos = array();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        $perfiles = DB::table('medidas')
                ->where('medidas_ancho', $ancho)
                ->groupBy('medidas_perfil')
                ->orderBy('medidas_perfil', 'asc')
                ->get();
        
        $rines = DB::table('medidas')
                ->where('medidas_ancho', $ancho)
                ->where('medidas_perfil', $perfil)
                ->where('medidas_rin', '>', '0')
                ->groupBy('medidas_rin')
                ->orderBy('medidas_rin', 'asc')
                ->get();

        //Medidas de llantas Camion
        $anchos_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', '>', '0')
                ->groupBy('medidas_camion_ancho')
                ->orderBy('medidas_camion_ancho', 'asc')
                ->get();
        
        $perfiles_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', $ancho)
                ->groupBy('medidas_camion_perfil')
                ->orderBy('medidas_camion_perfil', 'asc')
                ->get();
        
        $rines_camion = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', $ancho)
                ->where('medidas_camion_perfil', $perfil)
                ->where('medidas_camion_rin', '>', '0')
                ->groupBy('medidas_camion_rin')
                ->orderBy('medidas_camion_rin', 'asc')
                ->get();

        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();

        //Promociones
        $promociones = DB::table('promter')
                ->join('conten', 'conten_cod_conten', 'promter_cod_conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('tipopromoc', 'tipopromoc_cod_tipopromoc', 'promter_cod_tipopromoc')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->where('conten_llanta_ancho', $ancho)
                ->where('conten_llanta_perfil', $perfil)
                ->where('conten_llanta_rin', $rin)
                ->where('conten_tipo_conten', 'Llantas CVT Radial')
                ->where('conten_precio_lista','>', '0')
                ->where('conten_disp_iprodu','>', '1')
                ->where('promter_fec_hasta', '>=', date('Y-m-d H:i:s'));
//        dd(request()->filtro_marcas);
        $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
        $promociones = ($filtro_marcas) ? $promociones->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $promociones;
        $promociones = (request()->filtro_precio_desde) ? $promociones->where('promter_prec_normal', '>=', request()->filtro_precio_desde) : $promociones;
        $promociones = (request()->filtro_precio_hasta) ? $promociones->where('promter_prec_normal', '<=', request()->filtro_precio_hasta) : $promociones;
        
        $array_promociones = $promociones->pluck('conten_cod_conten')->toArray();
        $conten_promociones = $promociones->orderBy('conten_cod_marca')->orderBy('promter_porc_prom','desc')->get();
        //
        //Productos
        $conten = DB::table('conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->select(['*'])
                ->where('conten_llanta_ancho', $ancho)
                ->where('conten_llanta_perfil', $perfil)
                ->where('conten_llanta_rin', $rin)
                ->where('conten_tipo_conten', 'Llantas CVT Radial')
                ->where('conten_precio_lista','>', '0')
                ->where('conten_disp_iprodu','>', '1');
        $filtro_marcas = (request()->filtro_marcas) ? implode(',', request()->filtro_marcas) : null;
        $conten = ($filtro_marcas) ? $conten->whereRaw('conten_cod_marca in (' . $filtro_marcas . ')') : $conten;
        $conten = (request()->filtro_precio_desde) ? $conten->where('conten_precio_lista', '>=', request()->filtro_precio_desde) : $conten;
        $conten = (request()->filtro_precio_hasta) ? $conten->where('conten_precio_lista', '<=', request()->filtro_precio_hasta) : $conten;
        $conten = $conten->orderBy('conten_cod_marca');
        $conten = (count($array_promociones) > 0) ? $conten->whereNotIn('conten_cod_conten', $array_promociones) : $conten;
        $conten = $conten->paginate(self::NRO_RESULTADOS);

        $total_productos = $conten_promociones->count() + $conten->total();

        $marcas_seleccionadas = $cod_marcas;
        $cod_marca = null;
        $marca = null;
        $marca2 = null;
        $linea = null;
        $linea2 = null;
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'medida';
        if ($perfil=='R') {
            $descripcion_busqueda = 'Medida: ' . $ancho . ' ' . $perfil  . ' ' . $rin;
        }
        else
            {
            $descripcion_busqueda = 'Medida: ' . $ancho . ' ' . $perfil . ' R' . $rin;
        }
        
        $marcas_filtro = DB::table('conten')
                ->select(['conten_cod_marca', 'conten_marcas'])
                ->where('conten_marcas', '<>', '')
                ->where('conten_cod_marca','<','9')
                ->orderBy('conten_cod_marca', 'asc')
                ->groupBy('conten_cod_marca')
                ->get();
        
        //xdd($conten_promociones);
        return view('listadoCamion')->with(compact(
            'marcar', 
            'anchos', 
            'anchos_camion', 
            'conten', 
            'descripcion_busqueda', 
            'conten_promociones',
            'total_productos', 
            'marcas_filtro',
            'baterias',
            'tipo_busqueda',
            'seleccionados'
        ));
    }


    public function productosBaterias($vehiculo_baterias, $marcas_baterias, $modelo_bateria, $marcas = null)
    {
        $total_productos = 0;
        //dd($modelo_bateria);
        //Se obtiene código de la marca según descripción enviada en la ruta.
        $cod_marcas = array();
        
        
       $marcar = DB::table('tipcar')
                ->select(['tipcar_des_marca'])
                ->groupBy('tipcar_cod_marca')
                ->orderBy('tipcar_des_marca', 'asc')
                ->get();

        //Modelos de vehículos
        $modelos_vehiculos = array();

        //Líneas de vehículos
        $lineas_vehiculos = array();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        
        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();

        //Promociones
        $promociones = DB::table('promter')
                ->join('conten', 'conten_cod_conten', 'promter_cod_conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->join('tipopromoc', 'tipopromoc_cod_tipopromoc', 'promter_cod_tipopromoc')
                ->where('conten_cod_marca','=', '15000')
                ->where('promter_fec_hasta', '>=', date('Y-m-d H:i:s'));
//        dd(request()->filtro_marcas);
      
        $promociones = (request()->filtro_precio_desde) ? $promociones->where('promter_prec_normal', '>=', request()->filtro_precio_desde) : $promociones;
        $promociones = (request()->filtro_precio_hasta) ? $promociones->where('promter_prec_normal', '<=', request()->filtro_precio_hasta) : $promociones;
        

        $conten_promociones = array();
        $array_promociones = $promociones->pluck('conten_cod_conten')->toArray();
        //Productos
        $conten = DB::table('conten')
            ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
            ->select(['*'])
            ->where('conten_precio_lista','>', '0')
            ->where('conten_disp_iprodu','>', '1')
            ->where('conten_cod_marca','=', '10000');

       
        $conten = $conten->orderBy('conten_cod_marca');
        $conten = (count($array_promociones) > 0) ? $conten->whereNotIn('conten_cod_conten', $array_promociones) : $conten;
        $conten = $conten->paginate(self::NRO_RESULTADOS);

        $total_productos =  $conten->total();

        $marcas_seleccionadas = $cod_marcas;
        $cod_marca = null;
        $marca = null;
        $marca2 = null;
        $linea = null;
        $linea2 = null;
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'medida';
        $descripcion_busqueda = 'Baterias para : ' . $vehiculo_baterias . ' ' . $marcas_baterias . ' ' . $modelo_bateria;
        
        $marcas_filtro = DB::table('conten')
                ->select(['conten_cod_marca', 'conten_marcas'])
                ->where('conten_marcas', '<>', '')
                ->where('conten_cod_marca','>', '500000')
                ->orderBy('conten_cod_marca', 'asc')
                ->groupBy('conten_cod_marca')
                ->get();
        //dd($total_productos);
        return view('listado_baterias')->with(compact('marcar', 'anchos', 'conten', 'descripcion_busqueda', 'conten_promociones','total_productos', 'marcas_filtro','baterias', 'tipo_busqueda'));
    }

    public function getDetalleProducto(Request $request)
    {
        $marcar = DB::table('tipcar')
                ->select(['*'])
                ->groupBy('tipcar_cod_marca')
                ->orderBy('tipcar_des_marca', 'asc')
                ->get();

        $anchos = DB::table('medidas')
                ->select(['medidas_ancho'])
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'ASC')
                ->get();

        $anchos_camion = DB::table('medidas_camion')
                ->select(['medidas_camion_ancho'])
                ->where('medidas_camion_ancho', '>', '0')
                ->groupBy('medidas_camion_ancho')
                ->orderBy('medidas_camion_ancho', 'ASC')
                ->get();

        $baterias = DB::table('baterias')
                ->select(['*'])
                ->groupBy('baterias_vehiculo')
                ->get();

        $conten = DB::table('conten')
                ->leftjoin('tecnologias', 'conten_cod_conten', 'tecnologias_cod_conten')
                ->join('Productos', 'conten_cod_conten', 'Codigo')
                ->select(['*'])
                ->where('conten_cod_conten', $request->producto)
                ->first();

        $conten = Conten::with([
            'tecnologias',
            'promociones' => function($query) {
                $query->where('promter_fec_hasta', '>=', date('Y-m-d H:i:s'))
                ->where('promter_fec_desde', '<=', date('Y-m-d H:i:s'))
                ->with('tipoPromocion');
            },
            'detalle'
        ])
        ->find($request->producto);

        $promter = $conten->promociones;
        
        $descripcion_busqueda = $request->descripcion;
        $tipo_busqueda = 'detalle';
        $seleccionados = [];
        
        $marcas_filtro = null;
        
        return view('detalle-producto')->with(compact(
            'marcar',
            'anchos',
            'anchos_camion',
            'conten',
            'promter',
            'descripcion_busqueda',
            'marcas_filtro',
            'baterias',
            'tipo_busqueda',
            'seleccionados'
        ));
    }

    public function getModelos(Request $request)
    {
        $cod_marca = $request->cod_marca;
        //dd($cod_marca); 
        //$contenArray = DB::select('select * from tipcar where tipcar_cod_marca =1 group by tipcar_ano order by tipcar_ano desc')->get();
        $contenArray = DB::table('tipcar')
                ->select(['tipcar_ano'])
                ->where('tipcar_cod_marca', $cod_marca)
                ->groupBy('tipcar_ano')
                ->orderBy('tipcar_ano', 'DESC')
                ->get();
        //dd($contenArray);  
        return view('combos.modelo')->with(compact('contenArray', 'cod_marca'));
    }

    public function getLineas(Request $request)
    {
        $cod_modelo = $request->cod_modelo;
        $cod_marca = $request->cod_marca;
        $contenArray = DB::table('tipcar')
                ->select(['*'])
                ->where('tipcar_ano', $cod_modelo)
                ->where('tipcar_cod_marca', $cod_marca)
                ->orderBy('tipcar_tipo', 'ASC')
                ->get();
        return view('combos.lineas')->with(compact('contenArray', 'cod_marca', 'cod_modelo'));
    }

    public function getPerfil(Request $request)
    {
        $ancho = $request->ancho;
        //dd($ancho);
        $contenArray = DB::table('medidas')
                ->select(['medidas_perfil'])
                ->where('medidas_ancho', '=', $ancho)
                ->where('medidas_perfil', '>', '0')
                ->groupBy('medidas_perfil')
                ->orderBy('medidas_perfil', 'asc')
                ->get();

        return view('combos.perfil')->with(compact('contenArray', 'ancho'));
    }

    public function getPerfilCamion(Request $request)
    {
        $ancho = $request->ancho;
        
        $contenArray = DB::table('medidas_camion')
                ->select(['medidas_camion_perfil'])
                ->where('medidas_camion_ancho', '=', $ancho)
                ->groupBy('medidas_camion_perfil')
                ->orderBy('medidas_camion_perfil', 'asc')
                ->get();

        //dd($ancho);
        return view('combos.perfil_camion')->with(compact('contenArray', 'ancho'));
    }

    public function getRin(Request $request)
    {
        $ancho = $request->ancho;
        $perfil = $request->perfil;
        //dd($perfil);
        $contenArray = DB::table('medidas')
                ->where('medidas_ancho', '=', $ancho)
                ->where('medidas_perfil', '=', $perfil)
                ->where('medidas_rin', '>', '0')
                ->orderBy('medidas_rin', 'ASC')
                ->get();

        return view('combos.rin')->with(compact('contenArray'));
    }
    
    public function getRinCamion(Request $request)
    {
        $ancho = $request->ancho;
        $perfil = $request->perfil;
        //dd($ancho);
        /*$contenArray = DB::table('medidas_camion')
                ->where('medidas_camion_ancho', '=', $ancho)
                ->where('medidas_camion_perfil', '=', $perfil)
                ->where('medidas_camion_rin', '>', '0')
                ->orderBy('medidas_camion_rin', 'ASC')
                ->get();
        */
        $contenArray = DB::table('medidas_camion_dec')
                ->where('medidas_camion_ancho', '=', $ancho)
                ->where('medidas_camion_perfil', '=', $perfil)
                ->where('medidas_camion_rin', '>', '0')
                ->orderBy('medidas_camion_rin', 'ASC')
                ->get();
        
        return view('combos.rin_camion')->with(compact('contenArray'));
    }

    public function getModelos_baterias(Request $request)
    {
        $vehiculo_baterias = $request->vehiculo_baterias;
        $marcas_baterias = $request->marcas_baterias;
        //dd($ancho);
        $contenArray = DB::table('baterias')
                ->select(['*'])
                ->where('baterias_vehiculo', '=', $vehiculo_baterias)
                ->where('baterias_marca', '=', $marcas_baterias)
                ->groupBy('baterias_diseño')
                ->get();
        
        return view('combos.modelo_baterias')->with(compact('contenArray'));
    }

    public function getMarcas_baterias(Request $request)
    {
        $vehiculo_baterias = $request->vehiculo_baterias;
        //dd($vehiculo_baterias);

        $contenArray = DB::table('baterias')
                ->select(['*'])
                ->where('baterias_vehiculo', '=', $vehiculo_baterias)
                ->groupBy('baterias_marca')
                ->get();
        
        

        return view('combos.marcas_baterias')->with(compact('contenArray', 'vehiculo_baterias'));
    }


    //Legales
    public function getLegalesPromociones()
    {
        return view('legales.promociones');
    }
    public function getLegalesQuejasReclamos()
    {
        return view('legales.quejas-reclamos');
    }
    public function getLegalesDevoluciones()
    {
        return view('legales.devoluciones');
    }
    public function getLegalesGarantias()
    {
        return view('legales.garantias');
    }
    public function getLegalesMediosPagos()
    {
        return view('legales.medios-pago');
    }

     //Contactenos
    public function getContactenos()
    {
        $departamentos = DB::table('depart')->orderBy('depart_nombre', 'asc')->get();
        return view('contactenos')->with([
            'departamentos' => $departamentos
        ]);
        
    }
    

    public function getBaterias()
    {
        return view('baterias');
    }
    public function getMarcas()
    {
        return view('marcas');
    }
    public function getLubricantes()
    {
        return view('lubricantes');
    }
    public function getEnvios()
    {
        return view('envios');
    }
    //Servicios
    public function getServiciosAlineacion()
    {
        return view('servicios.alineacion');
    }
    public function getServiciosAire()
    {
        return view('servicios.aire');
    }
    public function getServiciosCamber()
    {
        return view('servicios.camber');
    }
    public function getServiciosBalanceo()
    {
        return view('servicios.balanceo');
    }
    public function getServiciosSuspencion()
    {
        return view('servicios.suspencion');
    }
    public function getServiciosMontaje()
    {
        return view('servicios.montaje');
    }
    public function getServiciosRines()
    {
        return view('servicios.rines');
    }
    public function getServiciosAceite()
    {
        return view('servicios.aceite');
    }
    public function getServiciosFrenos()
    {
        return view('servicios.frenos');
    }
    public function getServiciosSincronizacion()
    {
        return view('servicios.sincronizacion');
    }
    public function getServiciosNitrogeno()
    {
        return view('servicios.nitrogeno');
    }

    public function getInicio()
    {
        return view('inicio');
    }
    
    private function convertString($string){
        $string = trim($string);
        $string = explode('-', $string);
        $string = implode(' ', $string);
        $string = explode('!', $string);
        $string = implode('/', $string);
        $string = strtoupper($string);

        return $string;
    }
    

}
