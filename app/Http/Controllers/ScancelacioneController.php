<?php

namespace App\Http\Controllers;

use App\Models\Scancelacione;
use App\Models\Observacione;
use App\Models\Motivcancelacione;
use App\Models\Compania;
use App\Models\Beneficiario;
use App\Models\Financiera;
use App\Models\Estado;
use MP;
use Mail;
use DB;
use App\Mail\EmailCancelacion;
use Illuminate\Http\Request;

/**
 * Class ScancelacioneController
 * @package App\Http\Controllers
 */
class ScancelacioneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $perfil = auth()->user()->perfil;
        $usuario = auth()->user()->id;

        if($perfil == 'admin'){
            $scancelaciones = Scancelacione::orderBy('id','DESC')->paginate();
        }else{
            $scancelaciones = Scancelacione::where('u_idusuarios',$usuario)->orderBy('id','DESC')->paginate();

        }

        return view('scancelacione.index', compact('scancelaciones'))
            ->with('i', (request()->input('page', 1) - 1) * $scancelaciones->perPage());
    }



    public function consulta(Request $request)
    {
     
            $scancelaciones = Scancelacione::where('placa',$request->placa)->orderBy('id','DESC')->paginate();

        return view('scancelacione.index', compact('scancelaciones'))
            ->with('i', (request()->input('page', 1) - 1) * $scancelaciones->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $scancelacione = new Scancelacione();
        $companias = Compania::pluck('nombre','id');
        $motivcancelaciones = Motivcancelacione::pluck('motivnombre','id');
        $beneficiarios = Beneficiario::pluck('benefnombre','id');
        $financieras = Financiera::pluck('nombre','id');
        $estados = Estado::pluck('nombreestado','id');

        return view('scancelacione.create', compact('scancelacione','companias','motivcancelaciones','beneficiarios','financieras','estados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Scancelacione::$rules);


  

        $scancelacione = $request->all();

        if( $request->hasFile('filecarta')){
            $scancelacione['filecarta']=$request->file('filecarta')->store('uploads','public');    
        }
        if( $request->hasFile('filelevantamiento')){
            $scancelacione['filelevantamiento']=$request->file('filelevantamiento')->store('uploads','public');    
        }
        if( $request->hasFile('filedevolucion')){
            $scancelacione['filedevolucion']=$request->file('filedevolucion')->store('uploads','public');    
        }
        if( $request->hasFile('filetarjetadepropiedad')){
            $scancelacione['filetarjetadepropiedad']=$request->file('filetarjetadepropiedad')->store('uploads','public');    
        }
        if( $request->hasFile('filepoliza')){
            $scancelacione['filepoliza']=$request->file('filepoliza')->store('uploads','public');    
        }

        Scancelacione::create($scancelacione);


        Mail::to('negociosweb@prontoyseguros.com')
        ->bcc([
            'negociosweb@prontoyseguros.com', 
        ],[
            'web',                    
        ])->send(new EmailCancelacion($request));
            //Mail::to('luis0320@gmail.com')->send(new EmailCancelacion($request));

        return redirect()->route('scancelaciones.index')
            ->with('success', 'La solicitud de cancelación se ha creado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scancelacione = Scancelacione::find($id);
        $companias = Compania::pluck('nombre','id');
        $motivcancelaciones = Motivcancelacione::pluck('motivnombre','id');
        $beneficiarios = Beneficiario::pluck('benefnombre','id');
        $financieras = Financiera::pluck('nombre','id');
        $estados = Estado::pluck('nombreestado','id');

        $observaciones = Observacione::rightJoin("users","users.id","=","observaciones.u_idusuarios")
        ->where('ob_idcancelaciones',$id)
        ->select('observaciones.*', 'users.name')
        ->orderBy('id','DESC')
        ->paginate();


        return view('scancelacione.show', compact('scancelacione','observaciones','companias','motivcancelaciones','beneficiarios','financieras','estados'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $scancelacione = Scancelacione::find($id);
        $companias = Compania::pluck('nombre','id');
        $motivcancelaciones = Motivcancelacione::pluck('motivnombre','id');
        $beneficiarios = Beneficiario::pluck('benefnombre','id');
        $financieras = Financiera::pluck('nombre','id');
        $estados = Estado::pluck('nombreestado','id');

        return view('scancelacione.edit', compact('scancelacione','companias','motivcancelaciones','beneficiarios','financieras','estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Scancelacione $scancelacione
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Scancelacione $scancelacione)
    {
        request()->validate(Scancelacione::$rules);

        $scancelacione->update($request->all());

        return redirect()->route('scancelaciones.index')
            ->with('success', 'Scancelacione updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $scancelacione = Scancelacione::find($id)->delete();

        return redirect()->route('scancelaciones.index')
            ->with('success', 'Scancelacione deleted successfully');
    }
}
