<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class EmailRenovacion extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->request->nombres);
        return $this->subject('Contacto prontoyseguros.com - Solicitud Renovacion')
                ->from('jeferenovaciones@prontoyseguros.com', $this->request->nombres)
                ->view('tienda.emails.solicitudrenovacion')->with([
            'request' => $this->request,
        ]);
    }
}
