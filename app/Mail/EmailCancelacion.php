<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class EmailCancelacion extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->request->email);
        return $this->subject('Contacto prontoyseguros.com - Nueva Cancelacion')
                ->from($this->request->email, $this->request->nombre)
                ->view('tienda.emails.notificacioncancelacion')->with([
            'request' => $this->request,
        ]);
    }
}
