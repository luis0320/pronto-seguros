<?php

use GuzzleHttp\Client;
use App\Http\Controllers\UserController;
//use SoapClient;



/** Cancelaciones*/
Route::resource('companias', CompaniaController::class)->middleware('auth');
Route::resource('beneficiarios', BeneficiarioController::class)->middleware('auth'); 
Route::resource('financieras', FinancieraController::class)->middleware('auth'); 
Route::resource('estados', EstadoController::class)->middleware('auth'); 
Route::resource('observaciones', ObservacioneController::class)->middleware('auth'); 
Route::resource('motivcancelaciones', MotivcancelacioneController::class)->middleware('auth'); 
Route::resource('scancelaciones', ScancelacioneController::class)->middleware('auth');
Route::resource('activosoattarifas', ActivosoattarifaController::class)->middleware('auth');
Route::resource('mispolizas', MispolizaController::class)->middleware('auth');
Route::post('consulta', 'ScancelacioneController@consulta')->name('consulta')->middleware('auth');


/** Soat */
Route::post('license-plate', 'SoatController@licensePlate')->name('licenseplate');
Route::get('license-plate', 'SoatController@licensePlate')->name('licenseplate');
Route::post('license-plate-prueba', 'SoatController@licensePlateprueba')->name('licenseplateprueba');
Route::get('license-plate-prueba', 'SoatController@licensePlateprueba')->name('licenseplateprueba');
Route::post('operation-car', 'SoatController@operationCar')->name('operationcar');
Route::get('operation-car', 'SoatController@operationCar')->name('operationcar');
Route::post('operation-car-prueba', 'SoatController@operationCarprueba')->name('operationcarprueba');
Route::get('operation-car-prueba', 'SoatController@operationCarprueba')->name('operationcarprueba');
Route::get('new-insurance-policy-budget', 'SoatController@newInsurancePolicyBudget')->name('newInsurancePolicyBudget');
Route::post('new-insurance-policy-budget', 'SoatController@newInsurancePolicyBudget')->name('newInsurancePolicyBudget');
Route::post('user-data', 'SoatController@userData')->name('userdata');
Route::post('user-data-prueba', 'SoatController@userDataprueba')->name('userdataprueba');
Route::get('operacion-nacional', 'SoatController@nationalOperation')->name('nationalOperation');
Route::get('operacion-nacional-prueba', 'SoatController@nationalOperationprueba')->name('nationalOperationprueba');
Route::get('type-document', 'SoatController@typeDocument')->name('typeDocument');
Route::get('new-insurance-policy-budget-prueba', 'SoatController@newInsurancePolicyBudgetprueba')->name('newInsurancePolicyBudgetprueba');
Route::post('new-insurance-policy-budget-prueba', 'SoatController@newInsurancePolicyBudgetprueba')->name('newInsurancePolicyBudgetprueba');




/** AXA */
Route::post('cotizar-poliza', 'AxaController@policyQuote')->name('consultar_poliza');
Route::get('cotizar-poliza', 'AxaController@policyQuote')->name('consultar_poliza');
Route::post('imprimir-cotizacion', 'AxaController@printPolicy')->name('imprimir_cotizacion');
Route::get('imprimir-cotizacion', 'AxaController@printPolicy')->name('imprimir_cotizacion');
Route::post('emision-poliza', 'AxaController@policyEmission')->name('emision_poliza');
Route::get('emision-poliza', 'AxaController@policyEmission')->name('emision_poliza');

/** Purchases */
Route::post('purchase', 'PaymentController@purchase')->name('purchase');
Route::get('purchase', 'PaymentController@purchase')->name('purchase');
Route::post('payment', 'PaymentController@payment')->name('payment');
Route::get('payment', 'PaymentController@payment')->name('payment');
Route::post('soat-pagos', 'PaymentController@multicash')->name('policyQuote');


Route::post('purchase-Prueba', 'PaymentController@purchasePrueba')->name('purchasePrueba');
Route::get('purchase-Prueba', 'PaymentController@purchasePrueba')->name('purchasePrueba');
// Route::post('api', 'SoatController@licensePlate')->name('api');
/*Route::get('/api', function () {
	//$client = new GuzzleHttp\Client();
	//$client = new Client([
	//	'base_uri' => 'https://pre.transfiriendo.com:448/SOATNETSEAPI/api/Vehicle/GetVehicle?NumberPlate=emt040',
	//]);
	$client = new GuzzleHttp\Client();
	$response = $client->request('GET', 'https://pre.transfiriendo.com:448/SOATNETSEAPI/api/Vehicle/GetVehicle?NumberPlate=emt040',[
			'headers'=> [
				'Client' => ['987654059'],
				'Authorization' => ['bearer 390E27347EF0F703A95C6176C316556D3D8D4999-7811646'],
			]
		]);
	//$test = json_decode($response, true);

	$data =  json_decode( $response->getBody()->getContents() );
	return $data;
});*/


Route::get('/divisas', function () {
	$opts = array(
		'ssl' => array('chipers'=> 'RCA-SHA', 'verify_peer'=> false , 'verify_peer_name'=>false)
	);
	$params = array("moneda" => "2");
	//$url = "https://www.banguat.gob.gt/variables/ws/TipoCambio.asmx?wsdl";
	$url = "https://tmms.axacolpatria.co:3275/AXAColpatria/GestionPolizas/CotizacionPolizaAuto/GestionPolizaAuto.svc?Wsdl";
	try{
		//$this->service = InstanceSoapClient::init();
		//dd($client);
		$client = new SoapClient ($url, $opts);
		dd($client);
		$fecha = $client->Variables();
		//$cambio = $client->TipoCambioDia()->TipoCambioDiaResult->CambioDolar->VarDolar->referencia;
		dd($fecha);

	}catch(SoapFault $fault){
		echo '<br>'.$fault;

	}
});
Route::get('servicios', 'AxaController@servicios')->name('servicios');

Route::get('/', function () {
    return view('tienda.index');
});
/* */
Route::get('nosotros', 'HomeController@nosotros')->name('nosotros');
Route::get('inicio', 'HomeController@inicio')->name('inicio');
Route::get('contactenos', 'HomeController@contactenos')->name('contactenos');
Route::get('colmena', 'HomeController@colmena')->name('colmena');
Route::get('contactenos_respuesta', 'HomeController@contactenos')->name('contactenos_respuesta');
Route::get('politica-de-proteccion-de-datos-personales', 'HomeController@policitaprotecciondedatosp')->name('politica-de-proteccion-de-datos-personales');
Route::get('soatsuramericana', 'HomeController@soatsuramericana')->name('soatsuramericana');
Route::get('cotizarsoatsura', 'HomeController@cotizarsoatsura')->name('cotizarsoatsura')->middleware('auth');



Route::get('aseguradoras', 'HomeController@aseguradoras')->name('aseguradoras');
Route::get('todo-riesgo', 'HomeController@riesgo')->name('todo-riesgo');
Route::get('todo-riesgo3', 'HomeController@riesgo3')->name('todo-riesgo3');
Route::post('todo-riesgo3', 'HomeController@riesgo3')->name('todo-riesgo3');
Route::get('soat', 'HomeController@soat')->name('soat');
// Ruta alterna de soat para pruebas
Route::get('soat-test', 'HomeController@soatTest')->name('soat-test');
Route::get('salud', 'HomeController@salud')->name('salud');
Route::get('educativo', 'HomeController@educativo')->name('educativo');
Route::get('personal', 'HomeController@personal')->name('personal');
Route::get('cotizar', 'HomeController@cotizar_todoriesgo')->name('cotizar');
Route::get('mascotas', 'HomeController@mascotas')->name('mascotas');
/*Vida*/
Route::get('vida', 'HomeController@vida')->name('vida');
Route::get('vida_renta', 'HomeController@vida_renta')->name('vida_renta');
Route::get('vida_medida', 'HomeController@vida_medida')->name('vida_medida');
Route::get('vida_mas', 'HomeController@vida_mas')->name('vida_mas');
Route::get('protege_vida', 'HomeController@protege_vida')->name('protege_vida');
Route::get('enfermedades_graves', 'HomeController@enfermedades_graves')->name('enfermedades_graves');
/*endvida*/

Route::get('exequial', 'HomeController@exequial')->name('exequial');
Route::get('hogar', 'HomeController@hogar')->name('hogar');
Route::get('cancelaciones', 'HomeController@cancelaciones')->name('cancelaciones');
Route::get('administrar-cancelaciones', 'HomeController@admincancelaciones')->name('administrar-cancelaciones');
Route::post('enviar-cancelacion', 'HomeController@enviarCancelacion')->name('enviar-cancelacion');

Route::get('pyme', 'HomeController@pyme')->name('pyme');
Route::get('cumplimiento', 'HomeController@cumplimiento')->name('cumplimiento');
Route::get('maquinaria', 'HomeController@maquinaria')->name('maquinaria');
Route::get('construccion', 'HomeController@construccion')->name('construccion');
Route::get('civil', 'HomeController@civil')->name('civil');
Route::get('carga', 'HomeController@carga')->name('carga');
Route::post('registro-cliente', 'HomeController@registro')->name('registro-cliente');

/*Inicio Manejo Email*/
Route::post('enviar-email', 'HomeController@enviarEmailContacto')->name('enviar-email');
Route::post('email-seguro', 'HomeController@enviarEmail')->name('email-seguro');


/*Fin Manejo Email*/
/*Inicio Administrador*/
Route::post('administrador', 'HomeController@administrador')->name('administrador');
Route::get('moduloadministrador', 'HomeController@moduloadministrador')->name('moduloadministrador')->middleware('auth');
Route::get('login', 'HomeController@login')->name('login');
Route::get('registrarse', 'HomeController@registrarse')->name('registrarse');
Route::get('logout', 'HomeController@logout')->name('logout');
Route::get('graficos', 'HomeController@graficos')->name('graficos')->middleware('auth');
Route::get('tablas', 'HomeController@tablas')->name('tablas');
Route::get('reporte-usuarios', 'HomeController@reporte_usuarios')->name('reporte-usuarios')->middleware('auth');
Route::get('detalle-usuario', 'HomeController@detalle_usuario')->name('detalle-usuario')->middleware('auth');
Route::get('detalle-mensaje', 'HomeController@detalle_mensaje')->name('detalle-mensaje');
Route::get('recuperarcontraseña', 'HomeController@recuperarcontrasena')->name('recuperarcontraseña');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/home', 'HomeController@inicio')->name('home');
Route::get('actualizacion-usuario', 'HomeController@actualizacionusuario')->name('actualizacion-usuario');
Route::post('actualizar-usuario-admin', 'HomeController@actualizarusuarioadmin')->name('actualizar-usuario-admin');
Route::post('actualizar-mensaje-email', 'HomeController@actualizarmensajeemail')->name('actualizar-mensaje-email');
Route::post('actualizar-usuario', 'HomeController@actualizarusuario')->name('actualizar-usuario');
Route::get('soatcotizados', 'HomeController@soatcotizados')->name('soatcotizados')->middleware('auth');
Route::get('importar-polizas', 'HomeController@importar_polizas')->name('importar-polizas')->middleware('auth');
Route::post('importar_excel', 'HomeController@importar_excel')->name('importar_excel')->middleware('auth');
Route::get('exportar-polizas', 'HomeController@exportar_polizas')->name('exportar-polizas')->middleware('auth');
Route::get('exportar-excel', 'HomeController@exportar_excel')->name('exportar-excel')->middleware('auth');
Route::post('mailrenovaciones', 'HomeController@mailrenovaciones')->name('mailrenovaciones')->middleware('auth');

Route::post('email-renovaciones', 'HomeController@mailrenovaciones')->name('email-renovaciones')->middleware('auth');

Route::get('exportar-soat-cotizados', 'HomeController@exportar_soat_cotizados')->name('exportar-soat-cotizados')->middleware('auth');
Route::post('exportar-soat', 'HomeController@exportar_soat_cotizaciones')->name('exportar-soat')->middleware('auth');


/*Fin Administrador*/


Route::get('informacion_todo_riesgo', 'HomeController@informacion_todoriesgo')->name('informacion_todo_riesgo');
Route::post('consumo-riesgo', 'HomeController@consumoriesgo')->name('consumo-riesgo');
